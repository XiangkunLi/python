#rootdir = "E:\\Dropbox\\"
#rootdir = "/home/users/mnt/xli"
#rootdir = "/ccc/cont003/home/gen10062/lix/scripts"

#import os,sys
#sys.path.append(rootdir+"OTHER\\Sources\\python")
from universal import *

from scipy.optimize import minimize
from scipy.misc import derivative
from scipy.interpolate import interp2d
from scipy import signal

from joblib import Parallel, delayed
import multiprocessing

def generate(Qtot, NP, Ek, x_rms, y_rms, z_rms, eps_x_rms = 1e-6, eps_y_rms = 1e-6, energy_spread = 0, alpha = 0, xc = 0, yc = 0, zc = 0):
    print "Generating initial beam distribution..."
    ww = np.ones(NP)*Qtot/g_qe/NP
    
    uz0 = gamma2bg(1.+Ek/g_mec2)
    uz_rms = energy_spread*(1.+Ek/g_mec2)/uz0*Ek/g_mec2
    
    xux_avg = -alpha*eps_x_rms
    ux_rms2 = (eps_x_rms**2+xux_avg**2)/x_rms**2
    
    yuy_avg = -alpha*eps_y_rms
    uy_rms2 = (eps_y_rms**2+yuy_avg**2)/y_rms**2
    
    mean = [xc, 0]
    cov = [[x_rms**2, xux_avg], [xux_avg, ux_rms2]]
    xb, ux = np.random.multivariate_normal(mean, cov, NP).T
    
    mean = [yc, 0]
    cov = [[y_rms**2, yuy_avg], [yuy_avg, uy_rms2]]
    yb, uy = np.random.multivariate_normal(mean, cov, NP).T
    
    mean = [zc, uz0]
    cov = [[z_rms**2, 0], [0, uz_rms**2]]
    zb, uz = np.random.multivariate_normal(mean, cov, NP).T
    
    # make linear longitudinal distribution
    # iz = zb.argsort(); iuz = uz.argsort()
    # xb = xb[iz]; yb = yb[iz]; zb = zb[iz]; ux = ux[iz]; uy = uy[iz]; uz = uz[iuz]
    #

    np.savetxt('beam.ini',np.array([xb, ux, yb, uy, zb, uz, ww]).T,fmt='%15.6E')
    print "Generating initial beam distribution...done"
    return

def generate_(Qtot, NP, Ek, x_rms, y_rms, z_rms, xc = 0, yc = 0, zc = 0, eps_x_rms = 0, eps_y_rms = 0, energy_spread = 0):
    print "Generating initial beam distributiondef ..."
    ww = np.ones(NP)*Qtot/g_qe/NP
    
    mean = [xc, yc, zc]
    cov = [[x_rms**2, 0, 0], [0, y_rms**2, 0], [0, 0, z_rms**2]]
    xb, yb, zb = np.random.multivariate_normal(mean, cov, NP).T
    
    uz0 = gamma2bg(1.+Ek/g_mec2)

    ux_rms = eps_x_rms/x_rms
    uy_rms = eps_y_rms/y_rms
    uz_rms = energy_spread*(1.+Ek/g_mec2)/uz0*Ek/g_mec2

    mean = [0, 0, uz0]
    cov = [[ux_rms**2, 0, 0], [0, uy_rms**2, 0], [0, 0, uz_rms**2]]
    ux, uy, uz = np.random.multivariate_normal(mean, cov, NP).T

    # make linear longitudinal distribution
    # iz = zb.argsort(); iuz = uz.argsort()
    # xb = xb[iz]; yb = yb[iz]; zb = zb[iz]; ux = ux[iz]; uy = uy[iz]; uz = uz[iuz]
    #

    np.savetxt('beam.ini',np.array([xb, ux, yb, uy, zb, uz, ww]).T,fmt='%15.6E')
    print "Generating initial beam distribution...done"
    return

def generate_t(Qtot, NP, Ek, x_rms, y_rms, z_rms, xc = 0, yc = 0, zc = 0, eps_x_rms = 0, eps_y_rms = 0, energy_spread = 0, f0=0):
    print "Generating initial beam distribution..."
    ww = np.ones(NP)*Qtot/g_qe/NP
    
    mean = [xc, yc]
    cov = [[x_rms**2, 0], [0, y_rms**2]]
    xb, yb = np.random.multivariate_normal(mean, cov, NP).T
    
    L0 = 2.*np.sqrt(2)*z_rms; aa = 1./L0; bb = 2./L0*(aa-f0)
    px = np.random.random(NP)
    zb = (-aa+np.sqrt(aa**2-2.*bb*(-bb/8.*L0**2+L0/2.*aa-px)))/bb-bb/12.*L0**3+zc
    #print L0, zb.max()-zb.min()
    uz0 = gamma2bg(1.+Ek/g_mec2)

    ux_rms = eps_x_rms/x_rms
    uy_rms = eps_y_rms/y_rms
    uz_rms = energy_spread*(1.+Ek/g_mec2)/uz0*Ek/g_mec2

    mean = [0, 0, uz0]
    cov = [[ux_rms**2, 0, 0], [0, uy_rms**2, 0], [0, 0, uz_rms**2]]
    ux, uy, uz = np.random.multivariate_normal(mean, cov, NP).T

    #iz = zb.argsort(); iuz = uz.argsort()
    #xb = xb[iz]; yb = yb[iz]; zb = zb[iz]; ux = ux[iz]; uy = uy[iz]; uz = uz[iuz]

    np.savetxt('beam.ini',np.array([xb, ux, yb, uy, zb, uz, ww]).T,fmt='%15.6E')
    print "Generating initial beam distribution...done"
    return

class Beam():
    def __init__(self, Qtot = 1e-12, NP = 10000):
        self.Qtot = Qtot
        self.NP = NP
        self.key_value = {'x':0, 'ux':1, 'y':2, 'uy':3, 'z':4, 'uz':5, 'w':6}
        self.data=[]
        return
    def load(self, filename):
        self.data = np.loadtxt(filename)
        self.NP = len(self.data)
        self.Qtot = self.data[:, 6].sum()*g_qe
        self.post()
        self.stat_old = self.stat
        with open('beam.post.dat','a') as f_handle:
            np.savetxt(f_handle,np.atleast_2d(self.stat),fmt='%14.6E')
        return
    def get(self, i):
        return self.data[i,:-1]
    def set(self, i, x):
        self.data[i,:-1] = x
        return
    def update(self, new_data):
        for i in np.arange(len(new_data)):
            self.data[i,:-1] = new_data[i]
        return
    def post(self):
        r = warp_post(self.data, self.key_value)
        self.stat = r
        return
    def get_pdf(self, npr = 7, npz = 7, N_cutoff = 5):
        x = self.data[:,0]; y = self.data[:,2]; z = self.data[:,4]
        r = np.sqrt(x**2+y**2); z = z-z.mean()
        
        sigma_r = 0.5*(np.std(x)+np.std(y)); sigma_z = np.std(z); print sigma_z, sigma_r
        counts, bins = np.histogram(r, bins=32, normed = True, range=(0, 5*sigma_r))
        rr = bins; fr = np.concatenate((counts, [0]))
        fr_r = interp1d(rr, fr, bounds_error = False, fill_value = 0)
        
        # plt.plot(rr*1e6, fr/rr, '-*')
        # plt.grid()
        # plt.show()
        
        counts, bins = np.histogram(z, bins=64, normed = True, range=(-5*sigma_z, 5*sigma_z))
        zz = bins; fz = np.concatenate((counts, [0]))
        fz_z = interp1d(zz, fz, bounds_error = False, fill_value = 0)
        
        # plt.plot(zz*1e6, fz, '-*')
        # plt.grid()
        # plt.show()
        
        def fz(z, sigma_z = 0, f0 = 0):
            ff = fz_z(z); return ff
        def fr(r, sigma_r = 0):
            ff = fr_r(r); return ff
        self.fz = fz
        self.fr = fr
        
        return
    def get_rho_RZ(self, npr = 8, npz = 8, N_cutoff = 5):
        
        x = self.data[:,0]; y = self.data[:,2]; z = self.data[:,4]; #r = np.sqrt(x**2+y**2); 
        z = z-z.mean()
        sigma_r = 0.5*(np.std(x)+np.std(y)); sigma_z = np.std(z); #print sigma_z, sigma_r
        
        dr = sigma_r/npr; dz = sigma_z/npz 
        nr = npr*N_cutoff; nz = npz*N_cutoff
        rho_r = np.zeros(nr+1)
        rho_z = np.zeros(nz*2+1)

        rr = np.linspace(0, sigma_r, nr+1)*N_cutoff
        zz = np.linspace(-sigma_z, sigma_z, 2*nz+1)*N_cutoff
        ddr = 2.*np.pi*dr*dr
        for i in np.arange(len(x)):
            ir = int(np.sqrt(x[i]**2+y[i]**2)/dr+0.5)
            if z[i]>=0:
                iz = nz+int(z[i]/dz+0.5)
            else:
                iz = nz-int(-z[i]/dz+0.5)
            if ir>=0 and ir<nr+1 and iz>=0 and iz < nz*2+1:
                if ir == 0:
                    rho_r[ir] += (8.)
                else:
                    rho_r[ir] += (1./(ir+0.))
                rho_z[iz] += 1
        rho_r = 1.0/len(x)*rho_r/ddr
        rho_z = rho_z/rho_z.sum()/dz
        
        select = (rho_z>0); zz = zz[select]; rho_z = rho_z[select]; zbmin = zz.min()-dz/2.; zbmax = zz.max()+dz/2.
        
        L0 = 2.*np.sqrt(2)*sigma_z; a0 = 1./L0; b0 = 2./L0*(a0-0)        
        f_z = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f_z, zz, rho_z, p0 = (a0, b0))
        a, b = popt
        
        #def fz(z, sigma_z = 0, f0 = 0):
        #    f0 = f_z(z, a, b)
        #    if z < zbmin or z > zbmax:
        #        f0 = 0
        #    return f0
        #def fr(r, sigma_r = 0):
        #    ff = fr_r(r); return ff
        #self.fz = fz
        return [a, -b, -zbmax, -zbmin]

def electric(x, y, z, t = 0):
    return [0, 0, 0]

def magnetic(x, y, z, t = 0):
    return [0, 0, 0]

class EMsolver():
    def __init__(self, nx=0, ny=0, nz=0, dx=0, dy=0, dz=0, xmin=0, xmax=0, ymin=0, ymax=0, zmin=0, zmax=0):
        self.nx = nx; self.ny = ny; self.nz = nz
        self.dx = dx; self.dy = dy; self.dz = dz
        self.xmin = xmin; self.xmax = xmax
        self.ymin = ymin; self.ymax = ymax
        self.zmin = zmin; self.zmax = zmax
        if ymin == 0 or ymax == 0:
            self.ymin = xmin; self.ymax = xmax
        if dy == 0:
            self.dy = dx
        self.em3d = {}
    def add_external_field(self, name, func):
        self.em3d.update({name:func})
    def get_field(self, x, y, z, t = 0):
        field = [0, 0, 0, 0, 0, 0]
        for name, func in self.em3d.iteritems():
            field = np.add(field, func(x, y, z, t))
        return field

def I0(x):
    return sp.special.iv(0, x)
def K0(x):
    return sp.special.kn(0, x)

def unwrap_multi_i(arg, **kwarg):
    return BeamWake.multi_i(*arg, **kwarg)
def unwrap_multi_j(arg, **kwarg):
    return BeamWake.multi_j(*arg, **kwarg)

class BeamWake(Beam):
    def __init__(self, ne = 1.5e23, sigma_r = 1e-6, sigma_z = 1e-6, Qtot=1e-12, Ek = 150, Nr = 32, Nz = 32, ldist = 'g'):
        Beam.__init__(self, Qtot)

        lamp = 2.*np.pi*g_c*np.sqrt(g_me*g_eps0/ne/g_qe**2)
        self.kp = 2.*np.pi/lamp
        
        self.sigma_r = sigma_r; self.sigma_z = sigma_z; self.Qtot = Qtot
        
        beta = gamma2beta(1.+Ek/g_mec2)
        self.vb = beta*g_c
        self.rmax = 10*sigma_r; self.zextent = 10*sigma_z
        self.Nr = Nr; self.Nz = Nz

        if ldist == 't':
            f0 = 0
            L0 = 2.*np.sqrt(2)*sigma_z; a0 = 1./L0; b0 = 2./L0*(a0-f0)
            dL = b0/12.*L0**3; a0 = a0+b0*dL
            self.a0 = a0; self.b0 = -b0
            self.zbmin = -L0/2.+dL; self.zbmax = L0/2.+dL
        self.ldist = ldist
        return
    def fr(self, r, sigma_r = 0):
        if sigma_r == 0:
            sigma_r = self.sigma_r
        return 1./(2.*np.pi)/sigma_r**2*np.exp(-r**2/2./sigma_r**2)
    def fr1(self, r, sigma_r = 0):
        if sigma_r == 0:
            sigma_r = self.sigma_r
        if r < 2.*sigma_r:
            return 1./(1.*np.pi)/sigma_r**2
        else:
            return 0
    def fz(self, z, sigma_z = 0):
        if self.ldist == 'g':
            if sigma_z == 0:
                sigma_z = self.sigma_z
            f0 = 1./np.sqrt(2.*np.pi)/sigma_z*np.exp(-z**2/2./sigma_z**2)
        elif self.ldist == 't':
            f0 = self.a0+self.b0*z
            if z < self.zbmin or z > self.zbmax:
                f0 = 0
        return f0
    
    def integral_z1(self, z):
        kp = self.kp; sigma_z = self.sigma_z
        def integrand(zp, z):
            return self.Qtot/g_eps0*self.fz(zp, sigma_z)*np.cos(kp*(z-zp))
        ans, err = quad(integrand, -15*sigma_z, z, args = (z))
        return ans
    def integral_z2(self, z):
        kp = self.kp; sigma_z = self.sigma_z
        def integrand(zp, z):
            return self.Qtot/g_eps0*self.fz(zp, sigma_z)*np.sin(kp*(z-zp))
        ans, err = quad(integrand, -15*sigma_z, z, args = (z))
        return ans
    
    def integral_r1(self, r):
        kp = self.kp; sigma_r = self.sigma_r
        def integrand(rp, r):
            return kp**2*rp*self.fr(rp, sigma_r)*I0(kp*rp)*K0(kp*r)
        ans, err = quad(integrand, 0, r,                args = (r))
        return ans
    def integral_r2(self, r):
        kp = self.kp; sigma_r = self.sigma_r
        def integrand(rp, r):
            return kp**2*rp*self.fr(rp, sigma_r)*I0(kp*r)*K0(kp*rp)
        ans, err = quad(integrand, r, 15*sigma_r, args = (r))
        return ans

    def r_dependence(self, r, z):
        if r == 0:
            return self.integral_r2(r)
        return (self.integral_r1(r)+self.integral_r2(r))
    
    def axial_field( self, r, z):
        if r == 0:
            return self.integral_z1(z)*self.integral_r2(r)
        return self.integral_z1(z)*(self.integral_r1(r)+self.integral_r2(r))    
    def radial_field(self, r, z):
        if r == 0:
            return 0
        der1 = derivative(self.integral_r1, r, dx=1e-8, n=1, order=5)
        der2 = derivative(self.integral_r2, r, dx=1e-8, n=1, order=5)
        #print r, z, der1, der2
        return self.integral_z2(z)*(der1+der2)/self.kp
    
    def get_field(self, x, y, z):
        r = np.sqrt(x**2+y**2)
        Er = -self.radial_field(r, -z)
        Ez = self.axial_field(r, -z)
        if r == 0:
            Ex = Ey = 0
        else:
            Ex = x/r*Er; Ey = y/r*Er
        return [Ex, Ey, Ez]

    def multi_i(self, r):
        zz = np.linspace(-self.zextent, self.zextent, self.Nz)
#        Er = np.zeros(self.Nz)
        EE = np.zeros((self.Nz, 2))
        for j, z in enumerate(zz):
            EE[j,0] = -self.radial_field(r, -z)
            EE[j,1] =  self.axial_field( r, -z)
        return EE
    def multi_j(self, r, z):
        Er = -self.radial_field(r, -z)
        Ez =  self.axial_field( r, -z)
        return Er, Ez
    def build(self, z0 = 0, t0 = 0, Ek = 0, num_cores = 1):
        print "Building beam wake field analytically..."
        if Ek != 0:
            beta = gamma2beta(1.+Ek/g_mec2)
            self.vb = beta*g_c
        Nr, Nz = self.Nr, self.Nz
        rr = np.linspace(0, self.rmax, Nr)
        zz = np.linspace(-self.zextent, self.zextent, Nz)
        
        Er = np.zeros((Nr, Nz))
        Ez = np.zeros((Nr, Nz))

        if num_cores>1:
            # multi-core with multi_i
            results = Parallel(n_jobs=num_cores)(delayed(unwrap_multi_i)(i) for i in zip([self]*len(rr), rr))
            results = np.array(results); #print results.shape
            for i in np.arange(Nr):
                Er[i,:] = results[i,:,0]
                Ez[i,:] = results[i,:,1]

            # multi-core with multi_j
            # for i in np.arange(Nr):
            #     num = Nz
            #     r1 = rr[i]
            #     results = Parallel(n_jobs=num_cores)(delayed(unwrap_multi_j)(j) for j in zip([self]*len(zz), [r1]*len(zz), zz))
            #     results = np.array(results)
            #     Er[i,:] = results[:,0]
            #     Ez[i,:] = results[:,1]
        else:
            # run with only one core
            for i in np.arange(Nr):
                for j in np.arange(Nz):
                    r, z = rr[i], zz[j]
                    Er[i,j] = -self.radial_field(r, -z)
                    Ez[i,j] =  self.axial_field( r, -z)

        fEr_2d = interp2d(rr, zz+z0, Er.T)
        fEz_2d = interp2d(rr, zz+z0, Ez.T)
        
        def EM3D(x, y, z, t = 0):
            r = np.sqrt(x*x+y*y)
            z = z-g_c*(t-t0)
            Er = fEr_2d(r, z)
            if r == 0:
                Ex = Er; Ey = Er
            else:
                Ex = x/r*Er; Ey = y/r*Er
            Ez = fEz_2d(r, z)
            return [-Ex[0], -Ey[0], -Ez[0], 0, 0, 0]
        self.EM3D = EM3D
        print "Building beam wake field analytically...done"

class LaserWake():
    def __init__(self, lpc, Nr = 32, Nz = 64):
        self.a0 = lpc.a0; self.w0 = lpc.w0; self.tau0 = lpc.tau0; self.E0 = lpc.E0
        self.lam0 = lpc.lam0; self.lamp = lpc.wavelength(); self.kp = 2.*np.pi/self.lamp
        self.vp = lpc.phase_velocity()*g_c; self.L_pd = lpc.depletion_length(); #print self.L_pd
        
        self.rmax = 2.*self.w0; self.zmin = -0.5*self.lamp; self.zmax = 1.5*self.lamp
        self.Nr = Nr; self.Nz = Nz
        
        return
    
    def f_z(self, z):
        a0 = self.a0; tau0 = self.tau0
        return a0*np.exp(-z**2/tau0**2)#*np.cos(k0*z)
    def deriv_Ez_z(self, t, y):
        '''t -> z/m; y[0] -> phi; y[1] -> kp*Ez/E0'''
        beta_g = self.vp/g_c; kp = self.kp
        gamma_g = beta2gamma(beta_g)

        a = self.f_z(t)
        gamma_tran = np.sqrt(1+a**2/2)

        dydt = [0, 0]
        dydt[0] = y[1]
        dydt[1] = kp**2*gamma_g**2*(beta_g*(1-gamma_tran**2/gamma_g**2/(1+y[0])**2)**(-0.5)-1)

        return dydt
    def integrator_Ez(self, t1, dt, t0, y0):
        '''return (z, phi, kp*Ez/E0)'''

        r = ode(self.deriv_Ez_z).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()

        sol = np.append(t0, y0) 
        while r.successful() and r.t<t1:
            r.integrate(r.t+dt)
            a = np.append(r.t, r.y)
            sol = np.append(sol,a)
        sol = sol.reshape(len(sol)/3, 3)
        return sol
    def deriv_Er_z(self, t, y, r1):
        w0 = self.w0
        dydt = [0]
        dydt[0] = (-4.*r1/w0**2)*np.exp(-2*r1**2/w0**2)*self.get_Ez(t)
        return dydt
    def integrator_Er(self, t1, dt, t0, y0, r1):
        '''return (z, Er)'''
        
        r = ode(self.deriv_Er_z).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params(r1)
        
        sol = np.append(t0, y0) 
        while r.successful() and r.t<t1:
            r.integrate(r.t+dt)
            a = np.append(r.t, r.y)
            sol = np.append(sol,a)
        return sol.reshape(len(sol)/2, 2)
    
    def delta_n(self, z, phi):
        a0 = self.a0; 
        beta_g = self.vp/g_c
        gamma_g = beta2gamma(beta_g)
        
        aa = self.f_z(z)
        gamma_tran = np.sqrt(1+aa**2/2)
        dn = gamma_g**2*beta_g*((1.-gamma_tran**2/gamma_g**2/(1+phi)**2)**(-0.5)-beta_g)
        return dn-1
    def build(self, z0 = 0, t0 = 0):
        kp_sigmaL = self.tau0*self.kp/np.sqrt(2.0)
        coef = kp_sigmaL/np.sqrt(2.0)*np.exp(-kp_sigmaL**2/4.0+0.5); print kp_sigmaL, coef
        
        lamp = self.lamp
        w0 = self.w0
        vp = self.vp
        
        Nr, Nz = self.Nr, self.Nz
        rr = np.linspace(0, self.rmax, Nr)
        zz = np.linspace(self.zmin, self.zmax, Nz)
        
        Er = np.zeros((Nr, Nz))
        Ez = np.zeros((Nr, Nz))
        
        ####
        t1, dt, tt0  = zz[-1], zz[1]-zz[0], zz[0]
        y0=[0., 0.]
        sol = self.integrator_Ez(t1, dt, tt0, y0)
        
        phi = sol[:, 1]; print len(zz),len(phi)
        dn = self.delta_n(zz, phi[0:len(zz)])
        fdn_z = interp1d(-zz, dn, fill_value = 0, bounds_error = False)
        def get_dn(z):
            print 'position of the beam in global CS: ', z*1e6, ' and in local CS: ', (z-z0)*1e6
            return fdn_z(z-z0)
        self.get_dn = get_dn
        
        Ez0 = sol[0:len(zz), 2]*self.E0/self.kp*coef
        fEz_z = interp1d(zz, Ez0, fill_value = 0, bounds_error = False)
        def get_Ez(z):
            return fEz_z(z)
        self.get_Ez = get_Ez
        ###
        
        Er[0,:] = np.zeros(Nz)
        Ez[0,:] = Ez0
        
        for i in np.arange(1, Nr):
            r1 = rr[i]
            Ez[i,:] = Ez[0,0:len(zz)]*np.exp(-2.*r1**2/w0**2)
            
            t1, dt, tt0  = zz[-1], zz[1]-zz[0], zz[0]
            y0=[0.]
            sol = self.integrator_Er(t1, dt, tt0, y0, r1)
            Er[i,:] = sol[0:len(zz),1].T
        
        fEr_2d = interp2d(rr, -zz+z0,-Er.T)
        fEz_2d = interp2d(rr, -zz+z0, Ez.T)
        
        def EM3D(x, y, z, t = 0):
            r = np.sqrt(x*x+y*y)
            z = z - g_c*(t-t0)
            Er = fEr_2d(r, z)
            if r == 0:
                Ex = Er; Ey = Er
            else:
                Ex = x/r*Er; Ey = y/r*Er
            Ez = fEz_2d(r, z)
            return [-Ex[0], -Ey[0], -Ez[0], 0, 0, 0]
        self.EM3D = EM3D   
        
        return
    
    def buildfromfile(self, Ex_file, Ez_file, By_file, time = 5.00346142797e-11, laser_zc = 4, xmax_norm = 2.5, zmin = -145e-6, zmax = 5e-6):
        print "Building laser wake field from files..."
        Ex = np.loadtxt(Ex_file)
        Ez = np.loadtxt(Ez_file)
        By = np.loadtxt(By_file)

        self.xmax = xmax_norm*self.lamp
        self.zmin, self.zmax = zmin, zmax
        
        Nx, Nz = Ex.shape
        xx = np.linspace(-self.xmax, self.xmax, Nx)-0.8e-6
        zz = np.linspace( self.zmin, self.zmax, Nz)+laser_zc/self.kp+(g_c-self.vp)*time

        fEx_2d = interp2d(xx, zz, Ex.T)
        fEz_2d = interp2d(xx, zz, Ez.T)
        fBy_2d = interp2d(xx, zz, By.T)

        def EM3D(x, y, z, t = 0):
            r = np.sqrt(x*x+y*y)
            z = z-self.vp*t
            ratio = 1.0+self.vp*t/self.L_pd
            Er = (fEx_2d(r, z)-g_c*fBy_2d(r, z))*ratio
            if r == 0:
                Ex = Er; Ey = Er
            else:
                Ex = x/r*Er; Ey = y/r*Er
            Ez = fEz_2d(r, z)*ratio
            return [-Ex[0], -Ey[0], -Ez[0], 0, 0, 0]
        self.EM3D = EM3D
        print "Building laser wake field from files...done"
        return

class LaserWake0():
    def __init__(self, lpc, xmax_norm = 2.5, zmin = -145e-6, zmax = 5e-6):
        self.a0 = lpc.a0; self.w0 = lpc.w0; self.tau0 = lpc.tau0; self.E0 = lpc.E0
        self.lam0 = lpc.lam0; self.lamp = lpc.wavelength(); self.kp = 2.*np.pi/self.lamp
        self.vp = lpc.phase_velocity()*g_c; self.L_pd = lpc.depletion_length(); #print self.L_pd
        
        self.xmax = xmax_norm*self.w0
        self.zmin = zmin; self.zmax = zmax
        return
    
    def f_z(self, z):
        a0 = self.a0; tau0 = self.tau0
        return a0*np.exp(-z**2/tau0**2)#*np.cos(k0*z)
    def deriv(self, t, y):
        '''t -> z/m; y[0] -> phi; y[1] -> kp*Ez/E0'''
        beta_g = self.vp/g_c; kp = self.kp
        gamma_g = beta2gamma(beta_g)

        a = self.f_z(t)
        gamma_tran = np.sqrt(1+a**2/2)

        dydt = [0, 0]
        dydt[0] = y[1]
        dydt[1] = kp**2*gamma_g**2*(beta_g*(1-gamma_tran**2/gamma_g**2/(1+y[0])**2)**(-0.5)-1)

        return dydt
    def integrator(self, t1, dt, t0, y0):
        '''return (z, phi, kp*Ez/E0)'''

        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()

        sol = np.append(t0, y0) 
        while r.successful() and r.t<t1:
            r.integrate(r.t+dt)
            a = np.append(r.t, r.y)
            sol = np.append(sol,a)
        sol = sol.reshape(len(sol)/3, 3)
        return sol
    def delta_n(self, z, phi):
        a0 = self.a0; 
        beta_g = self.vp/g_c
        gamma_g = beta2gamma(beta_g)
        
        aa = self.f_z(z)
        gamma_tran = np.sqrt(1+aa**2/2)
        dn = gamma_g**2*beta_g*((1.-gamma_tran**2/gamma_g**2/(1+phi)**2)**(-0.5)-beta_g)
        return dn-1
    def build(self):
        lamp = self.lamp
        t1, dt, t0 = 1.5*lamp, lamp/128, -0.5*lamp
        y0=[0., 0.]
        sol = self.integrator(t1, dt, t0, y0)
        z = -sol[:, 0]
        phi = sol[:, 1]
        Ez = sol[:, 2]*self.E0/self.kp
        dn = self.delta_n(z, phi)
        fEz_z = interp1d(z, Ez, fill_value = 0, bounds_error = False)
        fphi_z = interp1d(z, phi, fill_value = 0, bounds_error = False)
        fdn_z = interp1d(z, dn, fill_value = 0, bounds_error = False)
        vp = self.vp
        def get_Ez(z, t = 0):
            return fEz_z(z-vp*t)
        def get_phi(z, t = 0):
            return fphi_z(z-vp*t)
        def get_dn(z, t = 0):
            #print 'beam is at:', z-vp*t
            return fdn_z(z-vp*t)
        self.get_Ez = get_Ez
        self.get_phi = get_phi
        self.get_dn = get_dn
        return
    def buildfromfile(self, Ex_file, Ez_file, By_file, time = 5.00346142797e-11, laser_zc = 4):
        print "Building laser wake field from files..."
        Ex = np.loadtxt(Ex_file)
        Ez = np.loadtxt(Ez_file)
        By = np.loadtxt(By_file)

        Nx, Nz = Ex.shape
        xx = np.linspace(-self.xmax, self.xmax, Nx)-0.8e-6
        zz = np.linspace( self.zmin, self.zmax, Nz)+laser_zc/self.kp+(g_c-self.vp)*time

        fEx_2d = interp2d(xx, zz, Ex.T)
        fEz_2d = interp2d(xx, zz, Ez.T)
        fBy_2d = interp2d(xx, zz, By.T)

        def EM3D(x, y, z, t = 0):
            r = np.sqrt(x*x+y*y)
            z = z-self.vp*t
            ratio = 1.0+self.vp*t/self.L_pd
            Er = (fEx_2d(r, z)-g_c*fBy_2d(r, z))*ratio
            if r == 0:
                Ex = Er; Ey = Er
            else:
                Ex = x/r*Er; Ey = y/r*Er
            Ez = fEz_2d(r, z)*ratio
            return [-Ex[0], -Ey[0], -Ez[0], 0, 0, 0]
        self.EM3D = EM3D
        print "Building laser wake field from files...done"
        return

from scipy.integrate import tplquad

class SpaceCharge3D():
    '''
    Space charge model in 3D coordinates.
    '''
    def __init__(self, Qtot, sigma_x, sigma_y, sigma_z, gamma = 1):
        self.Qtot = Qtot
        self.sigma_x = sigma_x; self.sigma_y = sigma_y; self.sigma_z = sigma_z
        
        self.sigma_z_static = sigma_z*gamma
        self.gamma = gamma
        self.get_rho_3D0()
        return
    def get_rho_3D0(self):
        def rho_3D0(x_static, y_static, z_static):
            '''x_static, y_static, z_static: coordinates in static frame'''
            sigma_x_static = self.sigma_x; sigma_y_static = self.sigma_y
            sigma_z_static = self.sigma_z_static
            f_x_static = 1./np.sqrt(2.*np.pi)/sigma_x_static*np.exp(-x_static**2/2./sigma_x_static**2)
            f_y_static = 1./np.sqrt(2.*np.pi)/sigma_y_static*np.exp(-y_static**2/2./sigma_y_static**2)
            f_z_static = 1./np.sqrt(2.*np.pi)/sigma_z_static*np.exp(-z_static**2/2./sigma_z_static**2)
            return f_x_static*f_y_static*f_z_static
        self.rho_3D0 = rho_3D0
        return

    def integrand_Ex(self, zp, rp, tp, x_static, y_static, z_static, rho_3D):
        xp = rp*np.cos(tp); yp = rp*np.sin(tp);
        dx = (x_static-xp); rr = np.sqrt((x_static-xp)**2+(y_static-yp)**2+(z_static-zp)**2)
        return dx/rr**3*rp*rho_3D(xp, yp, zp)
    def fEx(self, x, y, z, flag = 0):
        '''x, y, z: coordinates in lab frame'''
        if flag == 0:
            rho_3D = self.rho_3D0
        sigma_x_static = self.sigma_x; sigma_y_static = self.sigma_y; sigma_z_static = self.sigma_z_static
        x_static = x; y_static = y; z_static = z*self.gamma
        ans, err = tplquad(self.integrand_Ex, 0, 2.*np.pi, lambda x: 0, lambda x: 5*sigma_x_static,\
                      lambda x, y: -5*sigma_z_static, lambda x, y: 5*sigma_z_static,\
                           args=(x_static, y_static, z_static, rho_3D))
        return Qtot/4./np.pi/g_eps0*ans*self.gamma
    def integrand_Ey1(self, zp, rp, tp, x_static, y_static, z_static, rho_3D):
        xp = rp*np.cos(tp); yp = rp*np.sin(tp);
        dy = (y_static-yp); rr = np.sqrt((x_static-xp)**2+(y_static-yp)**2+(z_static-zp)**2)
        return dy/rr**3*rp*rho_3D(xp, yp, zp)
    def fEy1(self, x, y, z, flag = 0):
        '''x, y, z: coordinates in lab frame'''
        if flag == 0:
            rho_3D = self.rho_3D0
        sigma_x_static = self.sigma_x; sigma_y_static = self.sigma_y; sigma_z_static = self.sigma_z_static
        x_static = x; y_static = y; z_static = z*self.gamma
        ans, err = tplquad(self.integrand_Ey1, 0, 2.*np.pi, lambda x: 0, lambda x: 5*sigma_x_static,\
                      lambda x, y: -5*sigma_z_static, lambda x, y: 5*sigma_z_static,\
                           args=(x_static, y_static, z_static, rho_3D))
        return Qtot/4./np.pi/g_eps0*ans*self.gamma
    def integrand_Ey(self, zp, yp, xp, x_static, y_static, z_static, rho_3D):
        dy = (y_static-yp); rr = np.sqrt((x_static-xp)**2+(y_static-yp)**2+(z_static-zp)**2)
        return dy/rr**3*rho_3D(xp, yp, zp)
    def fEy(self, x, y, z, flag = 0):
        '''x, y, z: coordinates in lab frame'''
        if flag == 0:
            rho_3D = self.rho_3D0
        sigma_x_static = self.sigma_x; sigma_y_static = self.sigma_y; sigma_z_static = self.sigma_z_static
        x_static = x; y_static = y; z_static = z*self.gamma
        ans, err = tplquad(self.integrand_Ey, -5*sigma_x_static, 5*sigma_x_static,\
                           lambda x: -5*sigma_y_static, lambda x: 5*sigma_y_static,\
                           lambda x, y: -5*sigma_z_static, lambda x, y: 5*sigma_z_static,\
                           args=(x_static, y_static, z_static, rho_3D))
        return Qtot/4./np.pi/g_eps0*ans*self.gamma
    def integrand_Ez(self, zp, rp, tp, x_static, y_static, z_static, rho_3D):
        xp = rp*np.cos(tp); yp = rp*np.sin(tp);
        dz = (z_static-zp); rr = np.sqrt((x_static-xp)**2+(y_static-yp)**2+(z_static-zp)**2)
        return dz/rr**3*rp*rho_3D(xp, yp, zp)
    def fEz(self, x, y, z, flag = 0):
        '''x, y, z: coordinates in lab frame'''
        if flag == 0:
            rho_3D = self.rho_3D0
        sigma_x_static = self.sigma_x; sigma_y_static = self.sigma_y; sigma_z_static = self.sigma_z_static
        x_static = x; y_static = y; z_static = z*self.gamma
        ans, err = tplquad(self.integrand_Ez, 0, 2.*np.pi, lambda x: 0, lambda x: 5*sigma_x_static,\
                      lambda x, y: -5*sigma_z_static, lambda x, y: 5*sigma_z_static,\
                           args=(x_static, y_static, z_static, rho_3D))
        return Qtot/4./np.pi/g_eps0*ans
    
def sgolay2d ( z, window_size, order, derivative=None):
    """
    """
    # number of terms in the polynomial expression
    n_terms = ( order + 1 ) * ( order + 2)  / 2.0
    
    if  window_size % 2 == 0:
        raise ValueError('window_size must be odd')
    
    if window_size**2 < n_terms:
        raise ValueError('order is too high for the window size')

    half_size = window_size // 2
    
    # exponents of the polynomial. 
    # p(x,y) = a0 + a1*x + a2*y + a3*x^2 + a4*y^2 + a5*x*y + ... 
    # this line gives a list of two item tuple. Each tuple contains 
    # the exponents of the k-th term. First element of tuple is for x
    # second element for y.
    # Ex. exps = [(0,0), (1,0), (0,1), (2,0), (1,1), (0,2), ...]
    exps = [ (k-n, n) for k in range(order+1) for n in range(k+1) ]
    
    # coordinates of points
    ind = np.arange(-half_size, half_size+1, dtype=np.float64)
    dx = np.repeat( ind, window_size )
    dy = np.tile( ind, [window_size, 1]).reshape(window_size**2, )

    # build matrix of system of equation
    A = np.empty( (window_size**2, len(exps)) )
    for i, exp in enumerate( exps ):
        A[:,i] = (dx**exp[0]) * (dy**exp[1])
        
    # pad input array with appropriate values at the four borders
    new_shape = z.shape[0] + 2*half_size, z.shape[1] + 2*half_size
    Z = np.zeros( (new_shape) )
    # top band
    band = z[0, :]
    Z[:half_size, half_size:-half_size] =  band -  np.abs( np.flipud( z[1:half_size+1, :] ) - band )
    # bottom band
    band = z[-1, :]
    Z[-half_size:, half_size:-half_size] = band  + np.abs( np.flipud( z[-half_size-1:-1, :] )  -band ) 
    # left band
    band = np.tile( z[:,0].reshape(-1,1), [1,half_size])
    Z[half_size:-half_size, :half_size] = band - np.abs( np.fliplr( z[:, 1:half_size+1] ) - band )
    # right band
    band = np.tile( z[:,-1].reshape(-1,1), [1,half_size] )
    Z[half_size:-half_size, -half_size:] =  band + np.abs( np.fliplr( z[:, -half_size-1:-1] ) - band )
    # central band
    Z[half_size:-half_size, half_size:-half_size] = z
    
    # top left corner
    band = z[0,0]
    Z[:half_size,:half_size] = band - np.abs( np.flipud(np.fliplr(z[1:half_size+1,1:half_size+1]) ) - band )
    # bottom right corner
    band = z[-1,-1]
    Z[-half_size:,-half_size:] = band + np.abs( np.flipud(np.fliplr(z[-half_size-1:-1,-half_size-1:-1]) ) - band ) 
    
    # top right corner
    band = Z[half_size,-half_size:]
    Z[:half_size,-half_size:] = band - np.abs( np.flipud(Z[half_size+1:2*half_size+1,-half_size:]) - band ) 
    # bottom left corner
    band = Z[-half_size:,half_size].reshape(-1,1)
    Z[-half_size:,:half_size] = band - np.abs( np.fliplr(Z[-half_size:, half_size+1:2*half_size+1]) - band ) 
    
    # solve system and convolve
    if derivative == None:
        m = np.linalg.pinv(A)[0].reshape((window_size, -1))
        return signal.fftconvolve(Z, m, mode='valid')
    elif derivative == 'col':
        c = np.linalg.pinv(A)[1].reshape((window_size, -1))
        return signal.fftconvolve(Z, -c, mode='valid')        
    elif derivative == 'row':
        r = np.linalg.pinv(A)[2].reshape((window_size, -1))
        return signal.fftconvolve(Z, -r, mode='valid')        
    elif derivative == 'both':
        c = np.linalg.pinv(A)[1].reshape((window_size, -1))
        r = np.linalg.pinv(A)[2].reshape((window_size, -1))
        return signal.fftconvolve(Z, -r, mode='valid'), signal.fftconvolve(Z, -c, mode='valid')   

class SpaceChargeRZ0():
    def __init__(self, Qtot, sigma_r, sigma_z, nr, nz, gamma = 1):
        self.Qtot = Qtot
        self.sigma_r = sigma_r; self.sigma_z = sigma_z
        self.nr = nr; self.nz = nz
        self.dr = 5.*self.sigma_r/nr; self.dz = 5.*self.sigma_z/nz
        
        self.sigma_z_static = sigma_z*gamma
        self.dz_static = 5.*self.sigma_z_static/nz
        self.gamma = gamma
        self.get_rho_RZ0()
        return
    def get_rho_RZ0(self):
        def rho_RZ0(r_static, z_static):
            sigma_r_static = self.sigma_r; sigma_z_static = self.sigma_z_static
            f_z = 1./np.sqrt(2.*np.pi)/sigma_z_static*np.exp(-z_static**2/2./sigma_z_static**2)
            f_r = 1./(2.*np.pi)/sigma_r_static**2*np.exp(-r_static**2/2./sigma_r_static**2)
            return f_r*f_z
        self.rho_RZ0 = rho_RZ0
        return
    def get_rho_RZ(self, x, y, z, npr = 7, npz = 7, N_cutoff = 5):
        sigma_r = 0.5*(np.std(x)+np.std(y)); sigma_z = np.std(z)
        dr = sigma_r/npr; dz = sigma_z/npz 
        nr = npr*N_cutoff; nz = npz*N_cutoff
        rho = np.zeros((nr, nz*2+1))

        rr = np.linspace(0, sigma_r, nr)*N_cutoff
        zz = np.linspace(-sigma_z, sigma_z, 2*nz+1)*N_cutoff*self.gamma
        dd = 2.*np.pi*dr*dr*dz
        for i in np.arange(len(x)):
            ir = int(np.sqrt(x[i]**2+y[i]**2)/dr+0.5)
            if z[i]>=0:
                iz = nz+int(z[i]/dz+0.5)
            else:
                iz = nz-int(-z[i]/dz+0.5)
            if ir>=0 and ir<nr and iz>=0 and iz < nz*2+1:
                if ir == 0:
                    rho[ir, iz] += (8.)
                else:
                    rho[ir, iz] += (1./(ir+0.))
        rho = 1.0/len(x)*rho/dd/self.gamma
        
        #plt.plot(rr*1e6, rho[:,nz], '-*')
        rho = sgolay2d(rho, window_size=11, order=4)
        #plt.plot(rr*1e6, rho[:,nz], '-*')
        #plt.plot(rr*1e6, self.rho_RZ0(rr, 0), '-*')
        #plt.show()

        self.rho_RZ = interp2d(rr, zz, rho.T, kind = 'cubic')
        return
    def integrand_Er(self, tp, rp, zp, r_static, z_static):
        xp = rp*np.cos(tp); yp = rp*np.sin(tp); x_static = r_static; y_static = 0
        dx = (x_static-xp); rr = np.sqrt((x_static-xp)**2+(y_static-yp)**2+(z_static-zp)**2)
        return dx/rr**3
    def fEr(self, r_lab, z_lab, flag = 0):
        if flag == 0:
            rho_RZ = self.rho_RZ0
        else:
            rho_RZ = self.rho_RZ
        r_static = r_lab; z_static = z_lab*self.gamma; sum_ = 0
        nr = self.nr; nz = self.nz; dr = self.dr; dz = self.dz_static
        for i in np.arange(nr):
            rp = dr*(i+0.)
            for j in np.arange(-nz, nz+1):
                zp = dz*(j+0.)
                if i == 0:
                    dQ = rho_RZ(rp, zp)*dr*dr*dz/8.
                else:
                    dQ = rho_RZ(rp, zp)*rp*dr*dz
                ans, err = quad(self.integrand_Er, 0, 2.*np.pi, args=(rp, zp, r_static, z_static))
                sum_ += (ans*dQ)
        return sum_*(self.Qtot/4./np.pi/g_eps0)*self.gamma
    def integrand_Ez(self, tp, rp, zp, r_static, z_static):
        xp = rp*np.cos(tp); yp = rp*np.sin(tp); x_static = r_static; y_static = 0
        dz = (z_static-zp); rr = np.sqrt((x_static-xp)**2+(y_static-yp)**2+(z_static-zp)**2)
        return dz/rr**3
    def fEz(self, r_lab, z_lab, flag = 0):
        if flag == 0:
            rho_RZ = self.rho_RZ0
        else:
            rho_RZ = self.rho_RZ
        r_static = r_lab; z_static = z_lab*self.gamma; sum_ = 0
        nr = self.nr; nz = self.nz; dr = self.dr; dz = self.dz_static
        for i in np.arange(nr):
            rp = dr*(i+0.)
            for j in np.arange(-nz, nz+1):
                zp = dz*(j+0.)
                if i == 0:
                    dQ = rho_RZ(rp, zp)*dr*dr*dz/8.
                else:
                    dQ = rho_RZ(rp, zp)*rp*dr*dz
                ans, err = quad(self.integrand_Ez, 0, 2.*np.pi, args=(rp, zp, r_static, z_static))
                sum_ += (ans*dQ)
        return sum_*(self.Qtot/4./np.pi/g_eps0)

def G2D(r, z):
    '''
    Green function in 2D RZ CS
    '''
    return 1./np.sqrt(r*r+z*z)
def integral_G2D_1(rho, z):
    '''
    Infinite integral of green function in 2D RZ CS
    '''
    EPS = 1e-12
    r = np.sqrt(rho*rho+z*z)
    if abs(r) < EPS:
        return 0
    if abs(rho) < EPS:
        return 0.5*z*r
    return 0.5*rho*rho*np.log(r+z)+0.5*z*r
def integral_G2D_2(rho, z):
    '''
    Infinite integral of green function in 2D RZ CS
    '''
    EPS = 1e-12
    r = np.sqrt(rho*rho+z*z)
    if abs(r) < EPS:
        res = 0
    elif abs(rho) < EPS:
        res = z*np.log(r)
    elif abs(z) < EPS:
        res = rho*np.log(r)
    else:
        res = rho*np.log(r+z)+z*np.log(r+rho)
    return res

def transposed_integral_G2D(r, z, rp, zp, dr, dz):
    '''
    Transposed finite integral of green function in 2D RZ CS
    '''
    rl, ru, zl, zu = rp-dr/2.-r, rp+dr/2.-r, zp-dz/2.-z, zp+dz/2.-z
    if rp == 0:
        rl = -r
    r1 = (integral_G2D_1(ru, zu)-integral_G2D_1(rl, zu))-(integral_G2D_1(ru, zl)-integral_G2D_1(rl, zl))
    r2 = (integral_G2D_2(ru, zu)-integral_G2D_2(rl, zu))-(integral_G2D_2(ru, zl)-integral_G2D_2(rl, zl))
    return r1+r*r2

def integral_G2D(rp, zp, ri, zi):
    '''
    Infinite integral of green function in 2D RZ CS
    '''
    EPS = 1e-12
    r = np.sqrt((rp-ri)**2+(zp-zi)**2)
    if abs(r) < EPS:
        res = 0
    elif abs(rp-ri) < EPS:
        res = 0.5*(zp-zi)*r+ri*(zp-zi)*np.log(r+rp-ri)+0.25*rp**2
    elif abs(zp-zi) < EPS:
        res = -0.5*(rp+ri)*(rp-ri)*np.log(r)+0.25*rp**2
    else:
        res = 0.5*(zp-zi)*r+ri*(zp-zi)*np.log(r+rp-ri)-0.5*(rp+ri)*(rp-ri)*np.log(r+zp-zi)+0.25*rp**2
    return res
def finite_integral_G2D(r, z, rp, zp, dr, dz):
    '''
    Finite integral of green function in 2D RZ CS
    '''
    rl, ru, zl, zu = max(0, rp-dr/2.), rp+dr/2., zp-dz/2., zp+dz/2.
    rr = (integral_G2D(ru, zu, r, z)-integral_G2D(rl, zu, r, z))-\
         (integral_G2D(ru, zl, r, z)-integral_G2D(rl, zl, r, z))
    return rr

def G3D(x, y, z):
    '''
    Green function in 3D cartesian CS
    '''
    return 1./np.sqrt(x*x+y*y+z*z)
def integral_G3D(x, y, z):
    '''
    Infinite integral of green function in 3D cartesian CS, according to Eq.(2) on 129901-2. 
    Here, `infinite` refers to the integrating limits
    '''
    #x=float(x); y = float(y); z= float(z); 
    EPS = 1e-12
    r = np.sqrt(x*x+y*y+z*z)
    if abs(r) < EPS:
        return 0
    if abs(x) < EPS:
        return y*z*np.log(r)
    if abs(y) < EPS:
        return z*x*np.log(r)
    if abs(z) < EPS:
        return x*y*np.log(r)
    
    x2 = x*x; y2 = y*y; z2 = z*z
    xy = x*y; yz = y*z; zx = z*x
    s = yz*np.log(x+r)+zx*np.log(y+r)+xy*np.log(z+r)
    a = -0.5*x2*np.arctan(yz/x/r)
    b = -0.5*y2*np.arctan(zx/y/r)
    c = -0.5*z2*np.arctan(xy/z/r)
    return s+a+b+c
def finite_integral_G3D(xl, xu, yl, yu, zl, zu):
    '''
    Finite integral of green function in 3D cartesian CS
    Parameters
      xl, xu: lower and upper limits in x direction
      yl, yu: lower and upper limits in y direction
      zl, zu: lower and upper limits in z direction
    Returns
      rr: integral over a volume defined by (xl, xu, yl, yu, zl, zu)
    '''
    rr = (integral_G3D(xu, yu, zu)-integral_G3D(xu,yl,zu)-integral_G3D(xl,yu,zu)+integral_G3D(xl,yl,zu))-\
         (integral_G3D(xu, yu, zl)-integral_G3D(xu,yl,zl)-integral_G3D(xl,yu,zl)+integral_G3D(xl,yl,zl))
    return rr
def transposed_integral_G3D(x, y, z, xp, yp, zp, dx, dy, dz):
    '''
    Transposed finite integral of green function in 3D Cartesian CS
    '''
    xl, xu, yl, yu, zl, zu = x-xp+dx/2., x-xp-dx/2., y-yp+dy/2., y-yp-dy/2., z-zp+dz/2., z-zp-dz/2.
    rr = (integral_G3D(xu, yu, zu)-integral_G3D(xu,yl,zu)-integral_G3D(xl,yu,zu)+integral_G3D(xl,yl,zu))-\
         (integral_G3D(xu, yu, zl)-integral_G3D(xu,yl,zl)-integral_G3D(xl,yu,zl)+integral_G3D(xl,yl,zl))
    return -rr

class SpaceChargeRZ():
    def __init__(self, Qtot, sigma_r, sigma_z, nr, nz, gamma = 1):
        self.Qtot = Qtot
        self.sigma_r = sigma_r; self.sigma_z = sigma_z
        self.nr = nr; self.nz = nz
        self.dr = 5.*self.sigma_r/nr; self.dz = 5.*self.sigma_z/nz
        
        self.sigma_z_static = sigma_z*gamma
        self.dz_static = 5.*self.sigma_z_static/nz
        self.gamma = gamma
        self.get_rho_RZ0()
        return
    def get_rho_RZ0(self):
        def rho_RZ0(r_static, z_static):
            sigma_r_static = self.sigma_r; sigma_z_static = self.sigma_z_static
            f_z = 1./np.sqrt(2.*np.pi)/sigma_z_static*np.exp(-z_static**2/2./sigma_z_static**2)
            f_r = 1./(2.*np.pi)/sigma_r_static**2*np.exp(-r_static**2/2./sigma_r_static**2)
            return f_r*f_z
        self.rho_RZ0 = rho_RZ0
        return
    def get_rho_RZ1(self):
        def rho_RZ0(r_static, z_static):
            sigma_r_static = self.sigma_r
            r = np.sqrt(r_static**2+z_static**2)
            if r >= sigma_r_static:
                return 0
            return 3./4./np.pi/sigma_r_static**3
        self.rho_RZ0 = rho_RZ0
        return
    def get_rho_RZ(self, x, y, z, npr = 7, npz = 7, N_cutoff = 5):
        sigma_r = 0.5*(np.std(x)+np.std(y)); sigma_z = np.std(z)
        dr = sigma_r/npr; dz = sigma_z/npz 
        nr = npr*N_cutoff; nz = npz*N_cutoff
        rho = np.zeros((nr, nz*2+1))

        rr = np.linspace(0, sigma_r, nr)*N_cutoff
        zz = np.linspace(-sigma_z, sigma_z, 2*nz+1)*N_cutoff*self.gamma
        dd = 2.*np.pi*dr*dr*dz
        for i in np.arange(len(x)):
            ir = int(np.sqrt(x[i]**2+y[i]**2)/dr+0.5)
            if z[i]>=0:
                iz = nz+int(z[i]/dz+0.5)
            else:
                iz = nz-int(-z[i]/dz+0.5)
            if ir>=0 and ir<nr and iz>=0 and iz < nz*2+1:
                if ir == 0:
                    rho[ir, iz] += (8.)
                else:
                    rho[ir, iz] += (1./(ir+0.))
        rho = 1.0/len(x)*rho/dd/self.gamma
        
        #plt.plot(rr*1e6, rho[:,nz], '-*')
        rho = sgolay2d(rho, window_size=11, order=4)
        #plt.plot(rr*1e6, rho[:,nz], '-*')
        #plt.plot(rr*1e6, self.rho_RZ0(rr, 0), '-*')
        #plt.show()

        self.rho_RZ = interp2d(rr, zz, rho.T, kind = 'cubic')
        return
    def phi_static0(self, r_static, z_static, flag = 0):
        if flag == 0:
            rho_RZ = self.rho_RZ0
        else:
            rho_RZ = self.rho_RZ
        
        def integrand(tp, rp, zp, r_static, z_static):
            xp = rp*np.cos(tp); yp = rp*np.sin(tp); x_static = r_static; y_static = 0
            inv_G = np.sqrt((x_static-xp)**2+(y_static-yp)**2+(z_static-zp)**2)
            if inv_G == 0:
                return 0
            return 1./inv_G
        
        nr = self.nr; nz = self.nz; dr = self.dr; dz = self.dz_static
        sum_phi = 0
        for i in np.arange(nr+1):
            for j in np.arange(-nz, nz+1):
                rp = dr*(i+0.); zp = dz*(j+0.)
                if i == 0:
                    dQ = rho_RZ(rp, zp)*dr*dr*dz/8.
                else: 
                    dQ = rho_RZ(rp, zp)*rp*dr*dz
                ans, err = quad(integrand, 0, 2.*np.pi, args=(rp, zp, r_static, z_static))
                sum_phi += (ans*dQ)
        return sum_phi*(self.Qtot/4./np.pi/g_eps0)
    def phi_static(self, r_static, z_static, flag = 0):
        if flag == 0:
            rho_RZ = self.rho_RZ0
        else:
            rho_RZ = self.rho_RZ
               
        nr = self.nr; nz = self.nz
        dr = self.dr; dz = self.dz_static
        sum_phi = 0; sum_km = 0
        for i in np.arange(nr+1):
            for j in np.arange(-nz, nz+1):
                rp = dr*(i+0.); zp = dz*(j+0.)
                if i == 0:
                    dQ = rho_RZ(rp, zp)*dr*dr*dz/8.
                else:
                    dQ = rho_RZ(rp, zp)*rp*dr*dz
                r2 = (rp-r_static)*(rp-r_static)+(zp-z_static)*(zp-z_static); m = -4.*r_static*rp/r2
                km = sp.special.ellipk(m)
                sum_km += km
                ans = 4./np.sqrt(r2)*km
                sum_phi += (ans*dQ)
        print sum_km
        return sum_phi*(self.Qtot/4./np.pi/g_eps0)
    def phi_static1(self, r_static, z_static, flag = 0):
        if flag == 0:
            rho_RZ = self.rho_RZ0
        else:
            rho_RZ = self.rho_RZ
        
        def integrand(zp, rp, tp, r_static, z_static):
            xp = rp*np.cos(tp); yp = rp*np.sin(tp); x_static = r_static; y_static = 0
            inv_G = np.sqrt((x_static-xp)**2+(y_static-yp)**2+(z_static-zp)**2)
            if inv_G == 0:
                return 0
            return rp/inv_G*rho_RZ(rp, zp)
        
        nr = self.nr; nz = self.nz; dr = self.dr; dz = self.dz_static
        sum_phi = 0
        for i in np.arange(nr+1):
            for j in np.arange(-nz, nz+1):
                rp = dr*(i+0.); zp = dz*(j+0.)
                ans, err = tplquad(integrand, 0, 2*np.pi, lambda x:max(0, rp-dr/2.), lambda x:rp+dr/2.,\
                                   lambda x,y:zp-dz/2., lambda x,y:zp+dz/2., args=(r_static, z_static))
                sum_phi += ans
        return sum_phi*(self.Qtot/4./np.pi/g_eps0)
    def phi_static_RZ(self, r_static, z_static, flag = 0):
        if flag == 0:
            rho_RZ = self.rho_RZ0
        else:
            rho_RZ = self.rho_RZ
        nr = self.nr; nz = self.nz
        dr = self.dr; dz = self.dz_static
        sum_phi = 0; sum_km = 0; sum_G = 0
        for i in np.arange(nr+1):
            for j in np.arange(-nz, nz+1):
                rp = dr*(i+0.); zp = dz*(j+0.)
                
                r2 = (rp-r_static)**2+(zp-z_static)**2; m = -4.*r_static*rp/r2
                km = sp.special.ellipk(m); #print km,
                int_G = transposed_integral_G2D(r_static, z_static, rp, zp, dr, dz)
                #sum_km += km
                #sum_G += int_G
                sum_phi += (int_G*rho_RZ(rp, zp)*4*km)
        #print sum_km, sum_G
        return sum_phi*(self.Qtot/4./np.pi/g_eps0)
    def phi_static_3D(self, r_static, z_static, flag = 0):
        if flag == 0:
            rho_RZ = self.rho_RZ0
        else:
            rho_RZ = self.rho_RZ
        nx = ny = self.nr; nz = self.nz
        dx = dy = self.dr; dz = self.dz_static
        x_static = r_static; y_static = 0; z_static = z_static
        sum_phi = 0
        for i in np.arange(-nz, nz+1):
            for j in np.arange(-ny, ny+1):
                for k in np.arange(-nx, nx+1):
                    xp = dx*(k+0.); yp = dy*(j+0.); zp = dz*(i+0.); rp = np.sqrt(xp*xp+yp*yp)
                    sum_phi += (transposed_integral_G3D(x_static, y_static, z_static, xp, yp, zp, dx, dy, dz)*rho_RZ(rp, zp))
        return sum_phi*(self.Qtot/4./np.pi/g_eps0)
    def phi_static2(self, r_static, z_static, flag = 0):
        if flag == 0:
            rho_RZ = self.rho_RZ0
        else:
            rho_RZ = self.rho_RZ
        
        def integrand(zp, rp, tp, r_static, z_static):
            xp = rp*np.cos(tp); yp = rp*np.sin(tp); x_static = r_static; y_static = 0
            inv_G = np.sqrt((x_static-xp)**2+(y_static-yp)**2+(z_static-zp)**2)
            if inv_G == 0:
                return 0
            return rp/inv_G*rho_RZ(rp, zp)
        
        sigma_r = self.sigma_r; sigma_z = self.sigma_z_static
        ans, err = tplquad(integrand, 0, 2*np.pi, lambda x:0, lambda x:5*sigma_r,\
                                   lambda x,y:-5*sigma_z, lambda x,y:5*sigma_z, args=(r_static, z_static))
        return ans*(self.Qtot/4./np.pi/g_eps0)
    def build(self, nor = 5, noz = 5):
        sigma_r = self.sigma_r; sigma_z = self.sigma_z_static
        dr = self.dr; dz = self.dz_static; #print dz, dr
        rr = np.arange(0, nor*sigma_r, dr)+dr/2.
        zz = np.arange(-noz*sigma_z, noz*sigma_z, dz)+dz/2.
        
        phi = np.zeros((len(rr), len(zz)))
        for i in np.arange(len(rr)):
            print i,
            for j in np.arange(len(zz)):
                #print j,
                phi[i,j] = self.phi_static_3D(rr[i], zz[j])
        phi_rz = interp2d(rr, zz, phi.T)
        print
        
        def phi_2d(r, z):
            if r<0:
                return phi_rz(-r, z)
            return phi_rz(r, z)
        self.phi_2d = phi_2d
        
        rr = np.arange(-nor*sigma_r, nor*sigma_r, dr)+dr/2.; #print rr/sigma_r; print zz/sigma_z
        R, Z = np.meshgrid(rr, zz)
        
        PHI = np.array([phi_2d(r, z) for r, z in zip(np.ravel(R), np.ravel(Z))])
        PHI = PHI.reshape(R.shape)
        
        Ez, Er = np.gradient(PHI, dz, dr)
        Er_rz = interp2d(rr, zz, Er)
        Ez_rz = interp2d(rr, zz, Ez)
        
        gamma = self.gamma; beta = gamma2beta(gamma)
        def EM3D(x, y, z, t = 0):
            z = z*gamma; r = np.sqrt(x*x+y*y)
            Er = Er_rz(r, z)
            Ez = Ez_rz(r, z)
            if r == 0:
                Ex = Er; Ey = Er
            else:
                Ex = x/r*Er; Ey = y/r*Er
            # return normalied E and B
            return [gamma*Ex[0], gamma*Ey[0], Ez[0], beta*gamma*Ey[0], -beta*gamma*Ex[0], 0]
        self.EM3D = EM3D
        return
    

lpc=PlasmaChannel()
lw=LaserWake(lpc)
bw=BeamWake()
beam=Beam()
emsolver=EMsolver()

def unwrap_step_i(arg, **kwarg):
    return Tracking.step_i(*arg, **kwarg)

class Tracking(Beam):
    def __init__(self, t1, dt, t0 = 0, out_inteval = 100):
        Beam.__init__(self, Qtot = 1e-12, NP = 10000)
        self.time = t0
        self.dt = dt
        self.tend = t1
        self.stepped = 0
        self.out_inteval = out_inteval
        return
    def deriv(self, t, y):
        '''t -> time/s; y[0] -> x/m; y[1] -> bg_x; y[2] -> y/m; y[3] -> bg_x; y[4] -> z/m; y[5] -> bg_z'''
        Ex, Ey, Ez, Bx, By, Bz = emsolver.get_field(y[0], y[2], y[4], t)

        bgx, bgy, bgz = y[1], y[3], y[5]
        gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)

        dydt = [0, 0, 0, 0, 0, 0]
        dydt[0] = y[1]*g_c/gamma
        dydt[1] = (bgy*Bz-bgz*By+Ex/g_c*gamma)/gamma/g_me*g_qe
        dydt[2] = y[3]*g_c/gamma
        dydt[3] = (bgz*Bx-bgx*Bz+Ey/g_c*gamma)/gamma/g_me*g_qe
        dydt[4] = y[5]*g_c/gamma
        dydt[5] = (bgx*By-bgy*Bx+Ez/g_c*gamma)/gamma/g_me*g_qe

        return dydt
    def step_i(self, i):
        y0 = self.get(i)
        t0 = self.time

        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()
        r.integrate(r.t+self.dt)

#        self.set(i, r.y)
        return r.y

    def step(self, nstep, num_cores=16):
        '''return (x, bgx, y, bgy, z, byz, t)'''
        while nstep>0:
            self.stepped += 1

            if num_cores>1:
                # multi-core run
                num = np.arange(self.NP)
                results = Parallel(n_jobs=num_cores)(delayed(unwrap_step_i)(i) for i in zip([self]*len(num), num))
                self.update(results)

            else:
                # run with one core
                for i in np.arange(self.NP):
                    y0 = self.get(i)
                    t0 = self.time
                    r = ode(self.deriv).set_integrator('dopri5')
                    r.set_initial_value(y0, t0).set_f_params()
                    r.integrate(r.t+self.dt)
                    self.set(i, r.y)

            self.time += self.dt
            nstep = nstep-1
            # self.update_bw()
            if self.stepped % self.out_inteval == 0:
                self.dump()
        print str.format('iter = %6d, time = %12.6E' % (self.stepped, self.time))
        return
    def dump(self):
        filename = str.format('beam-%06d.dat' % self.stepped)
        shape = self.data.shape
        tmp = np.zeros((shape[0], shape[1]+6)); tt = self.time
        for i in np.arange(len(tmp)):
            xx = self.data[i]
            fld = emsolver.get_field(xx[0], xx[2], xx[4], tt)
            tmp[i] = np.concatenate((xx, fld))
        np.savetxt(filename, tmp, fmt = '%15.6E')
        with open('beam.post.dat','a') as f_handle:
#            self.post()
            np.savetxt(f_handle,np.atleast_2d(self.stat),fmt='%14.6E')
        return
    def update_bw(self, scale = 0.05):
        global beam, bw
        r0 = beam.stat_old
        beam.post()
        r1 = beam.stat
        dn = lw.get_dn(r1[0], self.time)#; print dn
        ne = 1.5e23*(1.+dn)
        if abs(r1[3]-r0[3])/r0[3] > scale or abs(r1[4]-r0[4])/r0[4] > scale or abs(r1[5]-r0[5])/r0[5] > scale:
            beam.stat_old = beam.stat
            bw = BeamWake(ne, (r1[3]+r1[4])*0.5e-3, r1[5]*1e-3, beam.Qtot, Ek = r1[6])
            bw.build(z0 = r1[0], t0 = self.time)
            emsolver.add_external_field('BW', bw.EM3D)
        return
    def ref(self, y0, t1, dt, t0 = 0):
        '''return (x, bgx, y, bgy, z, byz, t)'''

        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()

        sol = np.append(y0, t0) 
        while r.successful() and r.t<t1:
            r.integrate(r.t+self.dt)
            a = np.append(r.y, r.t)
            sol = np.append(sol, a)

        return sol.reshape(len(sol)/7, 7)


###from tracking import *
##
###workdir = rootdir+"\\CEA\\Results\\scaling-laws-refined-3d\\LPA-tracking"
###os.chdir(workdir)
##
###global n0, lpc, beam, emsolver, lw, bw
##
##n0 = 1.5e23
##lpc = PlasmaChannel(a0 = np.sqrt(2.), kp_w0 = 3.3, kp_sigma0 = np.sqrt(2.), ne = n0)
##
###Ex_file = '/data/xli/WarpSimul/LPA-tracking/field-maps/Ex-1-16-32x1.dat'
###Ez_file = '/data/xli/WarpSimul/LPA-tracking/field-maps/Ez-1-16-32x1.dat'
###By_file = '/data/xli/WarpSimul/LPA-tracking/field-maps/By-1-16-32x1.dat'
##
###Ex_file = '/ccc/scratch/cont003/gen10062/lix/LPA-tracking/laser-wake-field/Ex-1-16-32x1.dat'
###Ez_file = '/ccc/scratch/cont003/gen10062/lix/LPA-tracking/laser-wake-field/Ez-1-16-32x1.dat'
###By_file = '/ccc/scratch/cont003/gen10062/lix/LPA-tracking/laser-wake-field/By-1-16-32x1.dat'
##
##Ex_file = "D:\\Dropbox\\CEA\\Results\\scaling-laws-refined-3d\\laser-wake-field"+"\\Ex-1-16-32x1.dat"
##Ez_file = "D:\\Dropbox\\CEA\\Results\\scaling-laws-refined-3d\\laser-wake-field"+"\\Ez-1-16-32x1.dat"
##By_file = "D:\\Dropbox\\CEA\\Results\\scaling-laws-refined-3d\\laser-wake-field"+"\\By-1-16-32x1.dat"
##
##
##lw = LaserWake(lpc)
##lw.build()
##lw.buildfromfile(Ex_file, Ez_file, By_file)
##
##emsolver = EMsolver()
##beam = Beam()
##
##NCOUNT = 0
##num_cores = 1
##
##import timeit
##
##def object_func(opt):
##    global NCOUNT
##
##    removed_file = 'beam.post.dat'
##    if os.path.exists(removed_file):
##        os.remove(removed_file)
##
##    Qtot = 30e-12; NP = 100; Ek = 150; zc = -60e-6+opt[1]*1e-6
##
##    x_rms = 1.3e-6; y_rms = x_rms; z_rms = 1.0e-6*opt[0]
##    eps_x_rms = 1e-6; eps_y_rms = 1e-6; energy_spread = 0.5e-2
##
##    generate(Qtot, NP, Ek, zc = zc, x_rms = x_rms, y_rms = y_rms, z_rms = z_rms, eps_x_rms = eps_x_rms, eps_y_rms = eps_y_rms, energy_spread = energy_spread)
##    # beam_dist_plot(figtype = '-ini.png')
##
##    dn = lw.get_dn(zc)
##    ne = n0*(1.+dn)
##
##    bw = BeamWake(ne, x_rms, z_rms, Qtot, Ek)
##
##    time1 = timeit.default_timer()
##    bw.build(z0 = zc, num_cores=num_cores)
##    time2 = timeit.default_timer()
##    print time2-time1
##    #return 0
##    emsolver.add_external_field('LW', lw.EM3D)
##    emsolver.add_external_field('BW', bw.EM3D)
##
##    beam.load('beam.ini')
##
##    t0 = 0; dt = 10e-12; t1 = 2000e-12; dt_out = 50e-12
##    n_steps = int(t1/dt)
##
##    track = Tracking(t1, dt, t0, out_inteval = int(dt_out/dt))
##
##    Ek = 5000
##
##    n_stepped = 0
##    stime = 0
##    while n_stepped<n_steps:
##        time1 = timeit.default_timer()
##        track.step(1, num_cores)
##        time2 = timeit.default_timer()
##        stime = stime+(time2-time1)
##
##        n_stepped += 1
##    
##        print '1 step takes: ', stime/n_stepped
##        
##        beam.post()
##        if beam.stat[6] > Ek:
##            track.dump()
##            break
##    print 'Total time:', stime
##    # beam_plot()
##
##    import shutil
##    destination = str.format('%04d-(%.2f)-(%.2f)' % (NCOUNT, opt[0], opt[1]))
##    os.mkdir(destination)
##    files = os.listdir("./")
##    for ff in files:
##        if ff.endswith(".png"):
##            shutil.copy(ff,destination)
##    shutil.copy('beam.post.dat',destination)
##    shutil.copy('lpa-tracking.log',destination)
##
##    d1 = np.loadtxt('beam.post.dat')
##    if d1[:,6].max()<Ek:
##        return 1e2
##
##    z_E = interp1d(d1[:,6], d1[:,0])
##    xemit_E = interp1d(d1[:,6], d1[:,1])
##    yemit_E = interp1d(d1[:,6], d1[:,2])
##    Erms_E = interp1d(d1[:,6], d1[:,7])
##    
##    res = Erms_E(Ek)/Ek/10.
##    rr = np.concatenate(([NCOUNT], opt, [z_E(Ek), xemit_E(Ek), yemit_E(Ek), Ek, Erms_E(Ek), res]))
##    with open('beam.opt.dat','a') as f_handle:
##        np.savetxt(f_handle,np.atleast_2d(rr),fmt='%14.6E')
##    NCOUNT += 1
##    return res
##
### run a simpy case
##NCOUNT=0
##object_func([3, 0])
##exit()
##
### differential evolution
##from scipy.optimize import differential_evolution
##bounds = [(2, 5), (-3, 1.5)]
##result = differential_evolution(object_func, bounds, popsize=15, recombination=0.5)
##result.x, result.fun
##exit()
##
### parameter scanning
##zrms = np.array([2, 2.5, 3, 3.5, 4, 4.5, 5])
###zinj = np.array([-1.25, -1.0, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5])
##zinj = np.array([-1.0, -0.5, 0, 0.5, 1.0, 1.5])
##
##for i, v1 in enumerate(zrms):
##    for j, v2 in enumerate(zinj):
##        opt = np.array([v1, v2])
##        object_func(opt)
##
