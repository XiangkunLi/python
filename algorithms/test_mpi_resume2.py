import sys
sys.path.append('/afs/ifh.de/group/pitz/data/lixiangk/work/sync/python')
from my_object import *


#from platypus import NSGAII, PoolEvaluator, Real, Problem, Generator, RandomGenerator
from platypus import *
from platypus.mpipool import MPIPool
import sys
import logging

import timeit
import pickle
import numpy as np

class GeneratorFromFile(Generator):
    
    def __init__(self, filename, usecols = None):
        super(GeneratorFromFile, self).__init__()
        self.x = np.loadtxt(filename, usecols = usecols)
        self.NCOUNT = 0
    def generate(self, problem):
        solution = Solution(problem)
        
        nvars = problem.nvars
	nobjs = problem.nobjs
		
        solution.variables =  self.x[self.NCOUNT, 0:nvars]
        #solution.objectives = self.x[self.NCOUNT, nvars:nvars+nobjs]

        #solution.feasible = True
	#solution.evaluated = False
        
        self.NCOUNT += 1
        
        return solution


class NSGAII_resume(AbstractGeneticAlgorithm):
    
    def __init__(self, problem,
                 population_size = 100,
                 generator = RandomGenerator(),
                 selector = TournamentSelector(2),
                 variator = None,
                 archive = None,
                 nth_run = 1,
                 **kwargs):
        super(NSGAII_resume, self).__init__(problem, population_size, generator, **kwargs)
        self.selector = selector
        self.variator = variator
        self.archive = archive
        self.nth_run = nth_run
        
    def step(self):
        if self.nfe == 0:
            self.initialize()
        else:
            self.iterate()

        if self.archive is not None:
            self.result = self.archive
        else:
            self.result = self.population

        self.dump()

    def dump(self):

        ### save the current population
        current = []
        for k in np.arange(len(self.population)):
            r_vars = self.population[k].variables[:]
            r_objs = self.population[k].objectives[:]
            current.append(list(r_vars)+list(r_objs))
        with open('population@%04d.%03d' % (self.nfe, self.nth_run), 'w') as f_handle:
            print(self.nfe, np.atleast_2d(current)[-1])
            np.savetxt(f_handle, np.atleast_2d(current), fmt='%14.6f')
        ###

        ### print/save the current result, added on September 13, 2019
        current = []
        for k in np.arange(len(self.result)):
            r_vars = self.result[k].variables[:]
            r_objs = self.result[k].objectives[:]
            current.append(list(r_vars)+list(r_objs))
        with open('result@%04d.%03d' % (self.nfe, self.nth_run), 'w') as f_handle:
            print(self.nfe, np.atleast_2d(current)[-1])
            np.savetxt(f_handle, np.atleast_2d(current), fmt='%14.6f')
        ###
        
        ### save the attributes except evaluator
        fullname = 'attribute@%04d.pkl' % self.nfe
        att = self.__dict__.copy()
        att['evaluator'] = []
        #print('att is: ', att)
        with open(fullname, 'wb') as f_handler:
            pickle.dump(att, f_handler)
        ###
        
        ### save the algorithm w/o evaluator
        evaluator = self.evaluator
        self.evaluator = None
        
        fullname = 'algorithm@%04d.%03d.pkl' % (self.nfe, self.nth_run)
        with open(fullname, 'wb') as f_handler:
            pickle.dump(self, f_handler)
        
        self.evaluator = evaluator

    def initialize(self):
        super(NSGAII_resume, self).initialize()
        
        if self.archive is not None:
            self.archive += self.population
        
        if self.variator is None:
            self.variator = default_variator(self.problem)

    def initialize_to_resume(self, generator = None):
        if generator is None:
            generator = self.generator
        
        self.population = [generator.generate(self.problem) for _ in range(self.population_size)]

	self.evaluate_all(self.population)
        
        if self.archive is not None:
            self.archive += self.population
        
        if self.variator is None:
            self.variator = default_variator(self.problem)

        self.dump()

    def iterate(self):
        offspring = []
        
        while len(offspring) < self.population_size:
            parents = self.selector.select(self.variator.arity, self.population)
            offspring.extend(self.variator.evolve(parents))

        #retrieve the simulation manually
        #offspring = [self.generator.generate(self.problem) for _ in range(self.population_size)]
        
        self.evaluate_all(offspring)

        ### print/save the current offspring, added on September 23, 2019
        import numpy as np
        current = []
        for k in np.arange(len(offspring)):
            r_vars = offspring[k].variables[:]
            r_objs = offspring[k].objectives[:]
            current.append(list(r_vars)+list(r_objs))
        with open('offspring@%04d.%03d' % (self.nfe, self.nth_run), 'w') as f_handle:
            print(self.nfe, np.atleast_2d(current)[-1])
            np.savetxt(f_handle, np.atleast_2d(current), fmt='%14.6f')
        ###
        
        offspring.extend(self.population)
        nondominated_sort(offspring)
        self.population = nondominated_truncate(offspring, self.population_size)

        
        if self.archive is not None:
            self.archive.extend(self.population)

def initialize_algorithm(fullname):
    #fullname = 'algorithm@0095.001.pkl'
    with open(fullname, 'rb') as f_handle:
        algo = pickle.load(f_handle)

    print(algo.__dict__)
    return algo

logging.basicConfig(level=logging.INFO)

if __name__ == "__main__":
    # define the problem definition
    problem = Problem(4, 3)
    problem.types[0] = Real(2.4, 4.8)
    problem.types[1] = Real(-15, 15)
    problem.types[2] = Real(-30, -5)
    problem.types[3] = Real(340, 380)
    problem.function = obj_EMSY4000pC_NSGAII_60um

    #from platypus import NSGAII, DTLZ2
    #problem = DTLZ2()

    # define the generator for initialization
    #generator_init = GeneratorFromFile('result@.003') # Continue after the first run
    
    pool = MPIPool()

    time1 = timeit.default_timer()
    print(pool)
    # only run the algorithm on the master process
    if not pool.is_master():
        pool.wait()
        sys.exit(0)
        
    # instantiate the optimization algorithm to run in parallel
    with PoolEvaluator(pool) as evaluator:
        #algorithm = NSGAII_resume(problem, evaluator=evaluator, population_size = 95)
        #algorithm.initialize_to_resume(generator_init)
        
        algorithm = initialize_algorithm('algorithm@1139.003.pkl')
        algorithm.evaluator = evaluator
        algorithm.nth_run = 4

        #retrieve the optimization process
        #algorithm.generator = generator_init

        #algorithm.dump()
        
        algorithm.run(2000)
    
    # display the results
    for solution in algorithm.result:
        print(solution.objectives)

    time2 = timeit.default_timer()
    print('time elapsed: ', time2-time1)

    pool.close()
