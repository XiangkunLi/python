import numpy as np
from scipy.constants import c
from warp import PicklableFunction, me


def printer(text, *args):
    print(text)
    if len(args)>0:
        logfile = args[0]
    else:
        logfile = 'warp.log'

    if me == 0:
        with open(logfile, 'a') as f_handle:
            f_handle.write(text+'\n')

clight = c
class BeamInjector( object ):

    def __init__(self, elec, w3d, top, dim, filename, xmean=0., ymean=0., zmean=0.,
                 js=None, lmomentum=0, gamma_boost=1, logfile = None, **kw):
        """
        Initialize an injector for the plasma.

        Parameters
        ----------
        elec: a Species object, as defined in Warp
           The particles that represent the electrons

        w3d, top: Forthon objects

        dim : str
           The dimension of the simulation (either "2d", "circ" or "3d")

        """
        # Register the species to be injected
        self.elec = elec
        self.dim = dim
        self.w3d = w3d
        self.top = top

        self.logfile = logfile

        self.lmomentum = lmomentum
        self.kw = kw
        self.add_available_dist_boosted(filename, xmean, ymean, zmean,
                                        js = js, lmomentum = lmomentum, gamma_boost = gamma_boost, **kw)

        # For the continuous injection:
        # self.injection_direction = injection_direction

        # Inject particles between current time and next step
        self.load_beam( self.top.time, self.top.time+top.dt)

    def add_available_dist_boosted(self, filename, xmean=0., ymean=0., zmean=0.,
                                   js=None, lmomentum=0, gamma_boost=1, **kw):
        """
        Add particles with a Gaussian distribution.
        - xmean,ymean,zmean: center of the cylinder, defaults to 0.
        - zdist='random': type of random distribution along z, possible values are
                       'random' and 'regular'
        - nz=1000: number of data points to use along z
        - fourfold=False: whether to use four fold symmetry.
        - js: particle species number, don't set it unless you mean it
        - lmomentum=false: Set to false when velocities are input as velocities, true
                        when input as massless momentum (as WARP stores them).
                        Only used when top.lrelativ is true.
        - gamma_boost: boost factor
        Note that the lreturndata option doesn't work.
        """

        inverse_gamma_boost = 1./gamma_boost
        beta_boost = np.sqrt(1.-inverse_gamma_boost**2)
        def drift_backward(x, xp, L):
            return [x+xp*L, xp]
        def lab2boosted(z):
            return [gamma_boost*z, -gamma_boost*beta_boost*z/clight]
        
        data = np.loadtxt(filename)
        nop = len(data)

        #kw['lmomentum'] = lmomentum
        self.lmomentum = lmomentum

        # coordinates in lab frame
        xa = data[:,0]
        ya = data[:,1]
        ztemp = np.mean(data[:,2])
        za = data[:,2]-ztemp+zmean
        vxa = data[:,3]*clight
        vya = data[:,4]*clight
        vza = data[:,5]*clight
        
        if lmomentum:
            gi = 1./np.sqrt(1.+(vxa*vxa+vya*vya+vza*vza)/clight**2)
        else:
            gi = 1.

        # transfrom from lab frame to boosted frame here
        # injection time in boosted frame (t = 0 in lab frame)
        tb = -gamma_boost*beta_boost*za/clight 

        xb = xa             # x at t = tb in boosted frame (t = 0 in lab frame)
        yb = ya             # y at t = tb in boosted frame (t = 0 in lab frame)
        zb = za*gamma_boost # z at t = tb in boosted frame (t = 0 in lab frame)

        ga = np.sqrt(1.+(vxa*vxa+vya*vya+vza*vza)/clight**2) # gamma = sqrt(1+bg**2)
        ccc = (1.-beta_boost*vza/ga/clight)

        vxb = vxa/ga/gamma_boost/ccc # here vxb = beta_x
        vyb = vya/ga/gamma_boost/ccc
        vzb = (vza/ga-beta_boost*clight)/ccc
        gb = 1./np.sqrt(1.-(vxb*vxb+vyb*vyb+vzb*vzb)/clight**2)
        
        #vzb = vzb*gb
        #vxb = vxb*gb
        #vyb = vyb*gb
        gi = 1./gb
        # end of transform

        self.xb = xb
        self.yb = yb
        self.zb = zb
        self.tb = tb
        self.vxb = vxb
        self.vyb = vyb
        self.vzb = vzb
        self.gi = gi
        self.js = js
            
    def load_beam( self, tmin, tmax ):
        """
        Load beam between tmin and tmax.

        Parameters
        ----------
        tmin, tmax : floats (seconds)
           Injection time between which the beam is to be loaded, in the
           local domain
        """

        select = (self.tb>=tmin)*(self.tb<tmax)
        nos = np.sum(select)
        
        if nos > 0:
            
            printer('%6d electrons loaded at: %.3f ps' % (nos, self.top.time*1e12), self.logfile)

            xb = self.xb[select]
            yb = self.yb[select]
            zb = self.zb[select]
            tb = self.tb[select]
            vxb = self.vxb[select]
            vyb = self.vyb[select]
            vzb = self.vzb[select]
            gi = self.gi[select]
            js = self.js

            Lb = (self.top.time-tb)*vzb
            xb = xb+vxb/vzb*Lb
            yb = yb+vyb/vzb*Lb
            zb = zb+Lb

            vxb = vxb/gi
            vyb = vyb/gi
            vzb = vzb/gi

            # Load the electrons (on top of each other)
            if self.elec is not None:
                kw = self.kw
                self.elec.addparticles(xb, yb, zb, vxb, vyb, vzb, gi, js, lmomentum = self.lmomentum, **kw)

    def continuous_injection(self):
        """
        Routine which is called by warp at each timestep
        """

        tmin = self.top.time
        tmax = tmin+self.top.dt
        
        self.load_beam(tmin, tmax)
