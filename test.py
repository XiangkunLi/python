import sys
sys.path.append('/afs/ifh.de/group/pitz/data/lixiangk/work/scripts')

from _differentialevolution99 import *
from astra_modules import *
from my_object import *

from timeit import default_timer
import numpy as np

import time

from scipy.interpolate import interp1d, interp2d

x = [5, 0, 0, 360]
obj = obj_fun(x)
exit()


# Simple parameter scan
# var1 = np.linspace(-30, 30, 61) #np.array([350])
# var2 = np.linspace(10, 17, 71) #np.array([80000, 100000, 150000])
# combi = np.array([[v1, v2] for v1 in var1 for v2 in var2])
#
# for i in combi:
#     res = obj_fun(i)
#     with open('phi2_scan.dat','a') as f_handle:
#         np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
#exit()

# Parameter scan with differential evolution
# from joblib import Parallel, delayed
#
# num_cores = 8
# results = Parallel(n_jobs=num_cores)(delayed(obj_fun)(i) for i in combi)
# results = np.array(results)
# np.savetxt('phi2_scan.dat', results, fmt = '%12.6E')
# print results
# exit()

t1 = default_timer()

init = np.loadtxt('input_nb')

num_cores = 16
bounds = [(3, 6), (-15, 15), (-30, 30), (320, 380)]
result = differential_evolution(obj_fun, bounds, strategy='best1bin', init = init, num_cores = num_cores)

t2 = default_timer()

print 'Best solution is:', result.x, result.fun
print 'Total time consumed: ', t2-t1, ' s'
