# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 22:22:27 2021

@author: lixiangk

Laser VC2 images processing and 2D Gaussian fit
Laser VC2 images ovelapped with QE map
Emission curve linear fit
Beam transport
"""
from image_process.ImageProcess import *
from universal import *

VC2DIR = r'\\afs\ifh.de\group\pitz\doocs\measure\Laser\TransverseProfile\VC2\2020'
EMSYDIR = r'\\afs\ifh.de\group\pitz\doocs\measure\TransvPhSp\2021\ProjEmittance'
simdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim'

def squareROI(ROI):
    xc, yc = ROI[0]+ROI[2]//2, ROI[1]+ROI[3]//2
    width = int(np.min(ROI[2:4]))
    ROI = [xc-width//2, yc-width//2, width, width]
    return ROI

#%% Load VC2 .imc and .bkc, process the image and plot, then 2D Gaussian fit
fname = get_file('.imc', initialdir = VC2DIR)
imgs, camInfos, n = PITZ_loadImage(fname)

fname = fname[0:-4]+'.bkc'
bkgs, camInfos, n = PITZ_loadImage(fname)

info = camInfos[0]; print('Scale: ', info.scale)

# Select ROI
tmp = np.mean(imgs, axis = 0)
showCrosshair = True
fromCenter = False
ROI = getROI(tmp, showCrosshair, fromCenter); print('ROI', ROI)
#ROI = [0, 0, w, h]
ROI = squareROI(ROI)

imgs = imgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]
bkgs = bkgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]

sop = np.sum(np.sum(np.sum(imgs)))/n

# Get background
env, avg, std = getBackground(bkgs, treat = None)
print(np.mean(np.mean(env)))

treat = 'average'
treat = 'envelope'
#treat = None

emcalc_filter = False

fig, ax = plt.subplots()

res = []
for i in np.arange(len(imgs)):
    if treat == 'average':
        tmp = imgs[i]-avg; # print(i, np.max(img))
    elif treat == 'envelope':
        tmp = imgs[i]-env; # print(i, np.max(img))
    else:
        pass
            
    tmp[tmp<0] = 0
    
    if emcalc_filter is True:
        tmp = emcalcFilter(tmp, std, 1, 1)
        
    imgs[i] = tmp
    
    
    [xc, yc, xrms, yrms] = calcRMS(tmp[::-1,::-1], info.scale)
    [xc, yc, x2, y2, xy] = calcCov(tmp[::-1,::-1], info.scale)
    res.append([xc, yc, xrms, yrms])
    print([xc, yc, xrms, yrms])
    print([xc, yc, x2, y2, xy])
    

    title = str.format('Xrms = %.2f mm, Yrms = %.2f mm' % (xrms, yrms))


    ax.imshow(tmp)
    ax.set_title('frame: %d' % i)
    plt.pause(0.01)
    plt.cla()
plt.close()

img = np.mean(imgs, axis = 0)
signal = img

# Filter out noisy pixels
#signal[signal<=20] = 0
#signal[signal>2] = 1

[xc, yc, xrms, yrms] = calcRMS(signal[::-1,::-1], info.scale)
[xc, yc, x2, y2, xy] = calcCov(signal[::-1,::-1], info.scale)

title = str.format('Xrms = %.2f mm, Yrms = %.2f mm' % (xrms, yrms))
#%
plottype = 'imshow'
#plottype = 'contourf'
#plottype= 'pcolormesh'

fig, ax = plt.subplots(figsize = (4, 4))

h, w = signal.shape
X = (np.arange(0, w)+0.5)*info.scale-xc
Y = (np.arange(0, h)+0.5)*info.scale-yc
    
if plottype is 'contourf':
    
    aX, aY = np.meshgrid(X, Y)
    
    # Noise level for 2D plot
    noise = 0.001
    v = np.linspace(noise, 1, 99)*np.max(signal)
    
    # 2020/04/13 - make x positive on the right side
    cax = ax.contourf(aX, aY, signal[::-1,::-1], v, linestyles = None, zorder=-9)
    
    #cax = ax.contourf(aX, aY, signal[::-1,::-1], v, linestyles = None, zorder=-9)
    
    ax.set_rasterization_zorder(-1)
    ax.set_aspect('auto')
    
elif plottype is 'imshow':
       
    tmp = np.copy(signal)
    tmp[tmp==0] = np.nan
    # 2020/04/13 - make x positive on the right side
    cax = ax.imshow(tmp[:,::-1], extent = (X[0], X[-1], Y[0], Y[-1]),\
                    aspect = 'auto')
    #cax = ax.imshow(tmp, extent = (X[0], X[-1], Y[0], Y[-1]),\
    #                aspect = 'auto')
    
    ax.set_rasterization_zorder(-1)
    del tmp
    
elif plottype is 'pcolormesh':
    pass

cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04)
#cbar.set_ticks(np.linspace(0, 900, 10))
cbar.ax.tick_params(labelsize = 12, pad = 2)

#ax.grid()

#ax.set_title(title, fontsize = 12)
ax.set_title(r'$\sigma_x$ = %.2f mm, $\sigma_y$ = %.2f mm' % (xrms, yrms), fontsize = 12)
ax.set_xlabel(r'$x$ (mm)', fontsize = 12)
ax.set_ylabel(r'$y$ (mm)', fontsize = 12)
mpl.rc('xtick', labelsize = 11, direction = 'in', top   = True)
mpl.rc('ytick', labelsize = 11, direction = 'in', right = True)

fig.savefig(fname[0:-4]+'.png')

#% Plot a circle around the beam
tmp = np.copy(signal)
tmp[tmp<10] = 0
tmp[tmp>=10] = 1
xg, yg, xrms, yrms = calcRMS(tmp[::-1,::-1], info.scale)
xyrms = np.sqrt(xrms*yrms)

#xyrms = 1.0
phi = np.linspace(-np.pi, np.pi, 361)
xx = xg-xc+2*xyrms*np.cos(phi)
yy = yg-yc+2*xyrms*np.sin(phi)
ax.plot(xx, yy, '--', color = 'gray')
#ax.set_xlim(-5, 5)
#ax.set_ylim(-5, 5)

fig.savefig(fname[0:-4]+'_circled.png')

#%% 2D Gaussian fit
def Gaussian2D(xy, amplitude, xo, yo, sigma_x, sigma_y, theta, offset = 0):
    
    x, y = xy
    xo = float(xo)
    yo = float(yo)    
    a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
    b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)
    c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
    g = offset + amplitude*np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo) 
                            + c*((y-yo)**2)))
    return g.ravel()

h, w = signal.shape
X = (np.arange(0, w)-w//2+0.5)*info.scale
Y = (np.arange(0, h)-h//2+0.5)*info.scale
x, y = np.meshgrid(X, Y)
#signal[signal==np.nan] = 0

initial_guess = (800, 0, 0, 1, 1, 0)
popt, pcov = curve_fit(Gaussian2D, (x, y), signal[::-1,:].ravel(),\
                       p0=initial_guess)
amplitude, xo, yo, sigma_x, sigma_y, theta = popt

data_fitted = Gaussian2D((x, y), *popt).reshape(signal.shape)

#fig, [ax1, ax2, ax3] = plt.subplots(1 , 3, figsize = (12, 4))
reset_margin()
fig = plt.figure(figsize=(4, 4))
ax1 = fig.add_axes([0.18, 0.15, 0.75, 0.75]); ax1.patch.set_alpha(0)
ax2 = fig.add_axes([0.18, 0.15, 0.75, 0.10]); ax2.patch.set_alpha(0)
ax3 = fig.add_axes([0.18, 0.15, 0.10, 0.75]); ax3.patch.set_alpha(0)

#ax.hold(True)
tmp = np.copy(signal)
tmp[tmp<=5] = np.nan

#extent=(x.min(), x.max(), y.min(), y.max())

ax1.imshow(tmp, cmap=plt.cm.jet, extent=(x.min(), x.max(), y.min(), y.max()))
#data_fitted[300:-1] = 0
ax1.contour(x, y, data_fitted, 8, colors='w')

ax1.set_xlabel(r'$x$ (mm)', fontsize = 12)
ax1.set_ylabel(r'$y$ (mm)', fontsize = 12)
ax1.grid()
xmax, ymax = 3, 3
ax1.set_xlim(-xmax, xmax)
ax1.set_ylim(-ymax, ymax)

rr = np.max(np.sum(signal, axis = 0))
ax2.plot(X, np.sum(signal, axis = 0)/rr, '-')
ax2.plot(X, np.sum(data_fitted.reshape(signal.shape)/rr, axis = 0), '--')
#ax2.grid()
ax2.set_xlim(-xmax, xmax)
ax2.axis('off')

ax2.set_xlabel(r'$x$ (mm)', fontsize = 12)
#ax2.set_title(r'$\sigma_x$ = %.2f mm' % (sigma_x))

rr = np.max(np.sum(signal, axis = 1)[::-1])
ax3.plot(np.sum(signal, axis = 1)[::-1]/rr, Y, '-')
ax3.plot(np.sum(data_fitted.reshape(signal.shape), axis = 1)/rr, Y, '--')
#ax3.grid()
ax3.set_ylim(-ymax, ymax)
ax3.axis('off')

#ax3.set_ylabel(r'$y$ (mm)', fontsize = 12)
ax1.set_title(r'$\sigma_x$ = %.2f mm, $\sigma_y$ = %.2f mm' % (sigma_x, sigma_y), fontsize = 12)

fig.savefig(fname[0:-4]+'_fitted.png')

#%% 2D interpolation of VC2 image for particle tracking
from scipy.interpolate import RegularGridInterpolator

h, w = signal.shape
X = np.arange(0, w)*info.scale-xg
Y = np.arange(0, h)*info.scale-yg

#f2D = RegularGridInterpolator((X, Y), signal[::-1,::-1].T, bounds_error = False, fill_value = 0)
f2D = RegularGridInterpolator((X, Y), signal_QE.T, bounds_error = False, fill_value = 0)

fname = 'beam_500k.ini'
fname = get_file()

dist = np.loadtxt(fname)
x = dist[:,0]
y = dist[:,1]
w = dist[:,7]

charge = np.sum(w)

r = np.sqrt(x**2+y**2)

Rmax = xyrms
Rmax = 0.75
Rmax = 1
w = np.where(r<=2*Rmax*1e-3, f2D((x*1e3, y*1e3))*w, 0)

dist[:,7] = w

dist[:,7] *= charge/np.sum(w)

fname = 'beam2D_500k2.ini'
fname = get_file_to_save()
np.savetxt(fname, dist, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4d%4d')

#%% 2D interpolation of QE map
from scipy.interpolate import RegularGridInterpolator

workdir = r'\\afs\ifh.de\group\pitz\doocs\measure\Cathodes\QEmap\2019\20190211N'
workdir = r'\\afs\ifh.de\group\pitz\doocs\measure\Cathodes\QEmap\2020\20200223N'

workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\Shift\20210414N'
os.chdir(workdir)

QE = np.loadtxt('fineQEmap.QEs.txt')
aX = np.loadtxt('fineQEmap.Xs.txt')
aY = np.loadtxt('fineQEmap.Ys.txt')

X = aX[0]
Y = aY[:,0]

#QE[0:16, 1:16] = 0.

f2D_QE = RegularGridInterpolator((X, Y), QE.T/np.max(QE),
                                 bounds_error = False, fill_value = 0)

fig, ax = plt.subplots()

v = np.linspace(0, 1, 49)*np.max(QE)
ax.contourf(aX, aY, QE, v)

#%% Overlap VC2 image with QE map
h, w = signal.shape
X = np.arange(0, w)*info.scale-xg
Y = np.arange(0, h)*info.scale-yg

aX, aY = np.meshgrid(X, Y)
XY = [xy for i, xy in enumerate(zip(aX.flatten(), aY.flatten()))]
ratio = f2D_QE(XY) 

signal_QE = np.copy(signal[::-1, ::-1])*np.reshape(ratio, aX.shape)

[xc, yc, xrms, yrms] = calcRMS(signal_QE, info.scale)
title = str.format('Xrms = %.2f mm, Yrms = %.2f mm' % (xrms, yrms))

#plottype = 'imshow'
plottype = 'contourf'

fig, ax = plt.subplots(figsize = (4, 4))

if plottype is 'contourf':
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-xc
    Y = np.arange(0, h)*info.scale-yc
    aX, aY = np.meshgrid(X, Y)
    
    # Noise level for 2D plot
    noise = 0.001
    v = np.linspace(noise, 1, 99)*np.max(signal_QE)
    
    # 2020/04/13
    cax = ax.contourf(aX, aY, signal_QE, v, linestyles = None, zorder=-9)
    
    #cax = ax.contourf(aX, aY, signal[::-1,::-1], v, linestyles = None, zorder=-9)
    
    ax.set_rasterization_zorder(-1)
    ax.set_aspect('auto')
    
elif plottype is 'imshow':
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-xc
    Y = np.arange(0, h)*info.scale-yc
    
    tmp1 = np.copy(signal_QE)
    tmp1[tmp1==0] = np.nan
    # 2020/04/13
    cax = ax.imshow(tmp1, extent = (X[0], X[-1], Y[0], Y[-1]),\
                    aspect = 'auto')
    #cax = ax.imshow(tmp, extent = (X[0], X[-1], Y[0], Y[-1]),\
    #                aspect = 'auto')
    del tmp1
cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04)
#cbar.set_ticks(np.linspace(0, 900, 10))
cbar.ax.tick_params(labelsize = 12, pad = 2)

#ax.grid()

ax.set_title(title, fontsize = 12)
ax.set_xlabel(r'$x$ (mm)', fontsize = 12)
ax.set_ylabel(r'$y$ (mm)', fontsize = 12)
mpl.rc('xtick', labelsize = 11, direction = 'in', top   = True)
mpl.rc('ytick', labelsize = 11, direction = 'in', right = True)

fig.savefig(fname[0:-4]+'_QE.png')

#%% Emission curve
workdir = os.path.join(simdir, '2020', 'Shift', '20210418M')
workdir = os.path.join(simdir, '2020', 'Shift', '20210301M')
os.chdir(workdir)

fig, ax = plt.subplots(figsize = (4, 4))

#data = np.loadtxt('QE_BPM_0924.txt')

data = np.loadtxt('QE_BPM_1223.txt')
ax.plot(data[:,6]/1e3, (data[:,4]-data[:,2])/1e3)

end = 3
linear = lambda x, a: a*x
popt, pcov = curve_fit(linear, data[:end,6]/1e3, (data[:end,4]-data[:end,2])/1e3)
tmp = np.linspace(0, 10)
ax.plot(tmp, linear(tmp, *popt), 'k--')

ax.grid()
ax.set_xlabel(r'Laser energy (nJ)')
ax.set_ylabel(r'Bunch charge (nC)')

#ax.legend(['BSA 2.5 mm, 15N', 'BSA 3.0 mm, 15N', 'BSA 3.5 mm, 15N', 'BSA 3.5 mm, 18M'])

fig.tight_layout()
fig.savefig('emission-curves.png')

#%%

fig, ax = plt.subplots(figsize = (4, 4))

#data = np.loadtxt('QE_BPM_0924.txt')

data = np.loadtxt('BSA0.8mm@40MV_QE_0222.txt')
data = np.loadtxt('BSA0.7mm@30MV_QE_1714.txt')
xx = data[:,6]/1e3*0.1894 #0.2113
yy = (data[:,4]-data[:,2])/1e3
ax.plot(xx, yy)

end = 2
linear = lambda x, a: a*x
popt, pcov = curve_fit(linear, xx[1:end+1], yy[1:end+1])
tmp = np.linspace(0, 0.2)
#ax.plot(tmp, linear(tmp, *popt), 'r--')

data = np.loadtxt('BSA0.8mm@40MV_QE_BPM_0137.txt')
data = np.loadtxt('BSA0.7mm@30MV_QE_BPM_0213.txt')
xx = data[:,6]/1e3*0.1044 #0.1242*1
yy = (data[:,4]-data[:,2])/1e3
ax.plot(xx, yy)

end = 4
linear = lambda x, a: a*x
popt, pcov = curve_fit(linear, xx[:end], yy[:end])
#tmp = np.linspace(0, 1)
#ax.plot(tmp, linear(tmp, *popt), 'b--')

ax.grid()
ax.set_xlabel(r'Laser energy $\times$ QE (nJ)')
ax.set_ylabel(r'Bunch charge (nC)')

ax.legend(['2019', '2021'])
ax.set_xlim(0, 0.1)
ax.set_ylim(0, 0.2)

fig.tight_layout()
fig.savefig('emission-curves@30MV.png')
#%% Beam transport
import matplotlib.path as mpath
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection

def label(xy, text, **kwargs):
    '''
    Put a label centered at the coordinate defined by `xy`

    Parameters
    ----------
    xy : TYPE
        DESCRIPTION.
    text : TYPE
        DESCRIPTION.
    **kwargs : TYPE
        DESCRIPTION.

    Returns
    -------
    None.
    
    Examples:
        label((0, 0), "Text")

    '''
    y = xy[1] - 0.15  # shift y-value for label so that it's below the artist
    plt.text(xy[0], y, text, ha="center", family='sans-serif', size=14, **kwargs)

def rhombus(xy, w, h, **kwargs):
    '''
    Put a rhombus centered at the coordinate given by `xy`, with a width of `w` and height of `h`
    
    Parameters
    ----------
    xy : a tunple or list or array of length 2
        The coord.
    w : TYPE
        DESCRIPTION.
    h : TYPE
        DESCRIPTION.
    **kwargs : TYPE
        DESCRIPTION.

    Returns
    -------
    rhom : TYPE
        DESCRIPTION.

    Examples:
        rhom = rhombus((quads['HIGH1.Q4'], 0.2), 0.3, 0.4, color='green'); 
        ax.add_patch(rhom)
    '''
    
    x, y = xy
    rhom = mpatches.Polygon(np.array([[x-w/2., x, x+w/2., x, x-w/2.],[y, y+h/2., y, y-h/2., y]]).T, **kwargs)
    return rhom

workdir = os.path.join(r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\Shift\20210417M')
os.chdir(workdir)

fig, ax = plt.subplots(figsize = (6, 3))

data = np.loadtxt('transport-2021-04-17T1400.csv', skiprows = 2, delimiter = ',')

ax.plot(data[:,0], data[:,1], 'r-*')
ax.plot(data[:,0], data[:,2], 'b-o')

data = np.loadtxt('transport-2021-04-17T1417.csv', skiprows = 2, delimiter = ',')
start = 5
ax.plot(data[start:,0], data[start:,1], 'r--*')
ax.plot(data[start:,0], data[start:,2], 'b--o')

data = np.loadtxt('transport-2021-04-17T1444.csv', skiprows = 2, delimiter = ',')
start = 5
ax.plot(data[start:,0], data[start:,1], 'r:*')
ax.plot(data[start:,0], data[start:,2], 'b:o')

ax.grid()
#ax.legend(['$\sigma_x$', '$\sigma_y$'], ncol = 2, loc = 'upper center')

rhom = rhombus((quads['HIGH1.Q4'], 0.2), 0.3, 0.4, color='green'); ax.add_patch(rhom)
rhom = rhombus((quads['HIGH1.Q6'], 0.2), 0.3, 0.4, color='green'); ax.add_patch(rhom)
rhom = rhombus((quads['HIGH1.Q7'], 0.2), 0.3, 0.4, color='green'); ax.add_patch(rhom)

ax.set_xlabel('distance from cathode (z)')
ax.set_ylabel('RMS size (mm)')

ax.set_xlim(5, 20)
#ax.set_ylim(0.5, 2.0)

fig.tight_layout()
fig.savefig('beam-transport-all.png')

#%% Load .imc and .bkc, process the image and plot
fname = get_file('.imc')
imgs, camInfos, nop = PITZ_loadImage(fname)

fname = fname[0:-4]+'.bkc'
bkgs, camInfos, nop = PITZ_loadImage(fname)

info = camInfos[0]; print('Scale: ', info.scale)

# Select ROI
ROI = getROI(np.mean(imgs, axis = 0)); print('ROI', ROI)
#ROI = squareROI(ROI)
#ROI = [0, 0, w, h]

imgs = imgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]
bkgs = bkgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]

# Get background
env, avg, std = getBackground(bkgs, treat = None); print(np.mean(np.mean(env)))

treat = 'average'
#treat = 'envelope'
nstd = 1

if treat is 'average':
    emcalc_filter = 1
else:
    emcalc_filter = 0

for i in np.arange(len(imgs)):
    if treat is 'average':
        img = imgs[i]-avg; # print(i, np.max(img))
    elif treat is 'envelope':
        img = imgs[i]-env-nstd*std; # print(i, np.max(img))
        
    img[img<0] = 0
    
    if emcalc_filter:
        img = emcalcFilter(img, std, 1, 1)
    
    imgs[i] = img

img = np.mean(imgs, axis = 0)
signal = img

# Filter out noisy pixels
#signal[signal<5] = 0

#signal = np.mean(imgs, axis = 0)
#signal = avg

[xc, yc, xrms, yrms] = calcRMS(signal[::-1], info.scale)
title = str.format('$\sigma_x$ = %.2f mm, $\sigma_y$ = %.2f mm' % (xrms, yrms))

sop = np.sum(np.sum(np.sum(imgs)))/nop
r = []
r.append([xrms, yrms, sop])

plottype = 'imshow'
#plottype = 'contourf'

reset_margin(left=0.2, right = 0.8)
fig, ax = plt.subplots(figsize = (3, 4))

if plottype == 'contourf':
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-xc
    Y = np.arange(0, h)*info.scale-yc
    aX, aY = np.meshgrid(X, Y)
    
    # Noise level for 2D plot
    noise = 0.001
    v = np.linspace(noise, 1, 99)*np.max(signal)

    cax = ax.contourf(aX, aY, signal[::-1,::-1], v, linestyles = None, zorder=-9)
    ax.set_rasterization_zorder(-1)
    #ax.set_aspect('auto')
    
elif plottype == 'imshow':
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-xc
    Y = np.arange(0, h)*info.scale-yc
    
    tmp = np.copy(signal)
    tmp[tmp==0] = np.nan
    cax = ax.imshow(tmp[:,::-1], extent = (X[0], X[-1], Y[0], Y[-1]),
                    aspect = 'auto')
    del tmp

cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04) #, format=format)
#cbar.set_ticks(np.linspace(0, 900, 10))
cbar.ax.tick_params(labelsize = 9, pad = 2)
cbar.ax.set_ylabel('Pixels', labelpad = 2)
#ax.grid()

#ax.set_title(title, fontsize = 11)
ax.set_xlabel(r'$x$ (mm)')
ax.set_ylabel(r'$y$ (mm)')

# xmax = 4
# ymax = 2
# ax.set_xlim(-xmax, xmax)
# ax.set_ylim(-ymax, ymax)

#plt.tight_layout()
fig.savefig(fname[0:-4]+'.png')

#%%
def ROI(image, method):
    if method == 'ROI':
        image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        blimage = cv2.medianBlur(image, 15)
        circles = cv2.HoughCircles(blimage, cv2.HOUGH_GRADIENT, 1, 255, param1=100, param2=60, minRadius=0,
                                   maxRadius=0)
        if circles is not None:
            print(circles)
            circles = np.int16(np.around(circles)) # need int instead of uint to correctly calculate y-r (to get -1 instead of 65535)
            for x,y,r in circles[0, :]:
                print('x, y, r:', x, y, r)
                height, width = image.shape
                print('height, width:', height, width)

                border = 6

                cv2.circle(image, (x, y), r, (0, 255, 0), border)
                cv2.circle(image, (x, y), 2, (0, 0, 255), 3)

                mask = np.zeros(image.shape, np.uint8) # black background
                cv2.circle(mask, (x, y), r, (255), border)  # white mask for black border
                cv2.circle(mask, (x, y), r, (255), -1) # white mask for (filled) circle
                #image = cv2.bitwise_and(image, mask)  # image with black background
                image = cv2.bitwise_or(image, ~mask)  # image with white background

                x1 = max(x-r - border//2, 0)      # eventually  -(border//2+1)
                x2 = min(x+r + border//2, width)  # eventually  +(border//2+1)
                y1 = max(y-r - border//2, 0)      # eventually  -(border//2+1)
                y2 = min(y+r + border//2, height) # eventually  +(border//2+1)
                print('x1, x2:', x1, x2)
                print('y1, y2:', y1, y2)

                image = image[y1:y2,x1:x2]
                print('height, width:', image.shape)
    else:
        print('method is wrong')

    return image

#%% 2D real/phase space analysis
# First, load .imc and .bkc, process the image and plot
fname = get_file('.imc')
imgs, camInfos, nop = PITZ_loadImage(fname)

fname = fname[0:-4]+'.bkc'
bkgs, camInfos, nop = PITZ_loadImage(fname)

info = camInfos[0]; print('Scale: ', info.scale)

# Select ROI
ROI = getROI(np.mean(imgs, axis = 0)); print('ROI', ROI)
ROI = squareROI(ROI)
#ROI = [0, 0, w, h]

imgs = imgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]
bkgs = bkgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]

# Get background
env, avg, std = getBackground(bkgs, treat = None); print(np.mean(np.mean(env)))

treat = 'average'
#treat = 'envelope'
nstd = 1

if treat is 'average':
    emcalc_filter = 1
else:
    emcalc_filter = 0

for i in np.arange(len(imgs)):
    if treat is 'average':
        img = imgs[i]-avg; # print(i, np.max(img))
    elif treat is 'envelope':
        img = imgs[i]-env-nstd*std; # print(i, np.max(img))
        
    #img[img<0] = 0
    #if emcalc_filter:
    #    img = emcalcFilter(img, std, 1, 1)
    
    imgs[i] = img

img = np.mean(imgs, axis = 0)
img[img<0] = 0
if emcalc_filter:
    img = emcalcFilter(img, std, 1, 1)
        
signal = img

# Filter out noisy pixels
#signal[signal<5] = 0

[xc, yc, xrms, yrms] = calcRMS(signal[::-1], info.scale)

title = str.format('$\sigma_x$ = %.2f mm, $\sigma_y$ = %.2f mm' % (xrms, yrms))

# sop = np.sum(np.sum(np.sum(imgs)))/nop
# r = []
# r.append([xrms, yrms, sop])

# shift the beam to center it in ROI
tmp = np.copy(signal)
tmp[tmp<200] = 0
tmp[tmp>=200] = 1
xg, yg, _, _ = calcRMS(tmp[::-1, ::-1], info.scale)


xshift = ROI[2]//2-int(xg/info.scale)
yshift = ROI[3]//2-int(yg/info.scale)

signal = np.roll(signal, -xshift, axis = 1)
signal = np.roll(signal, -yshift, axis = 0)

h, w = signal.shape
X = np.arange(0, w)*info.scale-ROI[2]*info.scale/2
Y = np.arange(0, h)*info.scale-ROI[3]*info.scale/2
aX, aY = np.meshgrid(X, Y)


plottype = 'imshow'
#plottype = 'contourf'

reset_margin()
fig, ax = plt.subplots(figsize = (4, 4))

if plottype is 'contourf':
    
    # Noise level for 2D plot
    noise = 0.001
    v = np.linspace(noise, 1, 99)*np.max(signal)

    cax = ax.contourf(aX, aY, signal[::-1,::-1], v, linestyles = None, zorder=-9)
    ax.set_rasterization_zorder(-1)
    #ax.set_aspect('auto')
    
elif plottype is 'imshow':
    
    tmp = np.copy(signal)
    tmp[tmp==0] = np.nan
    cax = ax.imshow(tmp[:,::-1], extent = (X[0], X[-1], Y[0], Y[-1]),
                    aspect = 'auto')
    del tmp

cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04) #, format=format)
#cbar.set_ticks(np.linspace(0, 900, 10))
cbar.ax.tick_params(labelsize = 9, pad = 2)
cbar.ax.set_ylabel('Pixels', labelpad = 2)
#ax.grid()

ax.set_title(title, fontsize = 11)
ax.set_xlabel(r'$x$ (mm)')
ax.set_ylabel(r'$y$ (mm)')

xmax = 8
ax.set_xlim(-xmax, xmax)
ax.set_ylim(-xmax, xmax)
ax.grid()
#fig.savefig(fname[0:-4]+'_avg.png')
#fig.savefig('MOI_avg.png')


#%%
signal = s3
h, w = signal.shape
X = np.arange(0, w)*info.scale-w*info.scale/2
Y = np.arange(0, h)*info.scale-h*info.scale/2
aX, aY = np.meshgrid(X, Y)

### Calculate emittance and Twiss parameters of the full beam and the beam core
temp = np.copy(signal)
[xc, yc, x2, y2, xy] = calcCov(temp[::-1,::-1], info.scale)

[xc, yc, xrms, yrms] = calcRMS(signal[::-1], info.scale)
title = str.format('$\sigma_x$ = %.2f mm, $\sigma_y$ = %.2f mm' % (xrms, yrms))

emit100 = np.sqrt(x2*y2-xy**2)
beta100 = x2/emit100
gamma100 = y2/emit100
alpha100 = -xy/emit100

# For the core
mm = np.max(np.max(temp))
select10 = temp<mm*0.1
temp[select10] = 0

[xc, yc, x2, y2, xy] = calcCov(temp[::-1,::-1], info.scale)

emit90 = np.sqrt(x2*y2-xy**2)
beta90 = x2/emit90
gamma90 = y2/emit90
alpha90 = -xy/emit90

### Calculate action and phase
#action = np.zeros(signal.shape)
select = signal>0
temp = signal[select]
action = (gamma90*aX[select]**2+2*alpha90*aX[select]*aY[select]+beta90*aY[select]**2)/2
phase = np.arctan2(alpha90*aX[select]+beta90*aY[select],aX[select])/np.pi*180

fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (8, 4))
ax1.plot(action, temp/mm, '.')
ax1.set_yscale('log')
ax1.grid()

tmp = np.copy(signal); tmp[tmp==0] = np.nan
# cax = ax2.imshow(tmp[::-1,::-1], extent = (X[0], X[-1], Y[0], Y[-1]),
#           aspect = 'auto')
cax = ax2.imshow(tmp[:,::-1], extent = (X[0], X[-1], Y[0], Y[-1]),
          aspect = 'auto')

step = emit100/10
action2 = np.arange(1, np.ceil(np.max(action)/step*2), 2)*step/2
counts = np.zeros(action2.shape)

for i in np.arange(len(action2)):
    select = (action>(action2[i]-step/2)) * (action<=(action2[i]+step/2))
    counts[i] = np.sum(temp[select])
    
action_1per  = np.min(action2[counts<0.01*np.max(counts)])
action_10per = np.max(action2[counts>0.10*np.max(counts)])
emit1 = 2*action_1per
emit10 = 2*action_10per

atmp = np.linspace(0, 2*np.pi, 361)
xtmp, ytmp = np.cos(atmp), np.sin(atmp)
#ax2.plot(xtmp*(emit90)**0.5, ytmp*(emit90)**0.5, 'b--', label = '90%')
ax2.plot(xtmp*(2*action_10per)**0.5, ytmp*(2*action_10per)**0.5, 'r--', label = '10%')
ax2.plot(xtmp*(2*action_1per)**0.5, ytmp*(2*action_1per)**0.5, 'g--', label = '1%')
ax2.grid()
ax2.legend(loc = 'upper left', ncol = 2, fontsize = 12)

ax2.set_xlabel(r'$x$ (mm)')
ax2.set_ylabel(r'$y$ (mm)')
ax2.grid()
ax2.set_title(title, fontsize = 11)

cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04) #, format=format)
#cbar.set_ticks(np.linspace(0, 900, 10))
cbar.ax.tick_params(labelsize = 9, pad = 2)
cbar.ax.set_ylabel('Pixels', labelpad = 2)

xmax = 6
ax2.set_xlim(-xmax, xmax)
ax2.set_ylim(-xmax, xmax)

plt.tight_layout()

fig.savefig('action3.png')

#%
#fig, ax90 = plt.subplots()

_ = ax90.hist(np.sqrt(2*action/emit90), bins = 100, weights = temp, histtype = r'step', density = True, cumulative = True)

#fig, ax10  = plt.subplots()

_ = ax10.hist(np.sqrt(2*action/emit10), bins = 100, weights = temp, histtype = r'step', density = True, cumulative = True)

#%%

fig, ax = plt.subplots()

for signal in [s1, s2, s3]:
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-w*info.scale/2
    Y = np.arange(0, h)*info.scale-h*info.scale/2
    aX, aY = np.meshgrid(X, Y)
    
    [xc, yc, xrms, yrms] = calcRMS(signal[::-1], info.scale)
    
    projection = np.sum(signal[:,::-1], axis = 0)
    ax.plot(X/xrms, projection/np.max(projection), '-')

ax.grid()
ax.set_yscale('log')
ax.set_ylim(1e-6, 2)
ax.set_xlabel(r'$x/\sigma_x$')
ax.set_ylabel(r'Intensity (arb. unit)')
#ax.legend(['1', '2', '3'])
fig.savefig('X-projection_noLegend.png')

fig, ax = plt.subplots()

for signal in [s1, s2, s3]:
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-w*info.scale/2
    Y = np.arange(0, h)*info.scale-h*info.scale/2
    aX, aY = np.meshgrid(X, Y)
    
    [xc, yc, xrms, yrms] = calcRMS(signal[::-1,::-1], info.scale)
    
    projection = np.sum(signal[::-1,::-1], axis = 1)
    ax.plot(Y/yrms, projection/np.max(projection), '-')

ax.grid()
ax.set_yscale('log')
ax.set_ylim(1e-6, 2)
ax.set_xlabel(r'$y/\sigma_y$')
ax.set_ylabel(r'Intensity (arb. unit)')
#ax.legend(['1', '2', '3'])
fig.savefig('Y-projection_noLegend.png')
#%%
signal = s1
h, w = signal.shape
X = np.arange(0, w)*info.scale-w*info.scale/2
Y = np.arange(0, h)*info.scale-h*info.scale/2
aX, aY = np.meshgrid(X, Y)

[xc, yc, xrms, yrms] = calcRMS(signal[::-1], info.scale)
#%%
plottype = 'imshow'
#plottype = 'contourf'

reset_margin()
fig, ax = plt.subplots(figsize = (4, 4))

if plottype is 'contourf':
    
    # Noise level for 2D plot
    noise = 0.001
    v = np.linspace(noise, 1, 99)*np.max(signal)

    cax = ax.contourf(aX, aY, signal[::-1,::-1], v, linestyles = None, zorder=-9)
    ax.set_rasterization_zorder(-1)
    #ax.set_aspect('auto')
    
elif plottype is 'imshow':
    
    tmp = np.copy(signal)
    tmp[tmp==0] = np.nan
    cax = ax.imshow(tmp[:,::-1], extent = (X[0], X[-1], Y[0], Y[-1]),
                    aspect = 'auto')
    del tmp

cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04) #, format=format)
#cbar.set_ticks(np.linspace(0, 900, 10))
cbar.ax.tick_params(labelsize = 9, pad = 2)
cbar.ax.set_ylabel('Pixels', labelpad = 2)
#ax.grid()

ax.set_title(title, fontsize = 11)
ax.set_xlabel(r'$x$ (mm)')
ax.set_ylabel(r'$y$ (mm)')

xmax = 8
ax.set_xlim(-xmax, xmax)
ax.set_ylim(-xmax, xmax)
ax.grid()
#fig.savefig(fname[0:-4]+'_avg.png')

#%% Integration
counts_int = np.zeros(counts.shape)
emit_int = np.zeros(counts.shape)

for i in np.arange(len(counts)):
    counts_int[i] = np.sum(counts[:i])
    emit_int[i] = np.sum(action[action<=action2[i]]*temp[action<action2[i]])/np.sum(temp[action<action2[i]])
    
x_limit = np.ceil(np.min(action2[counts_int/counts_int[-1]>0.999])/5)*5

# Fit core emittance
action_fit = np.max(action2[counts>0.1*np.max(counts)])

#%% TDS current profile
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\Shift\20210303N'
os.chdir(workdir)

data = np.loadtxt('0504.txt')
data[:,0] *= (2.20/1.98)
data[:,2] *= (2.20/2.13)

step1 = data[1,0]-data[0,0]
step2 = data[1,2]-data[0,2]

scale1 = -4.0/(np.sum(data[:,1])*step1*1e-12*1e9)
scale2 = 4.0/(np.sum(data[:,3])*step2*1e-12*1e9)

data[:,1] *= scale1
data[:,3] *= scale2

# correct the tilt, making the peak current same at both phases
scale = np.sqrt(np.max(data[:,1])/np.max(data[:,3]))

fig, ax = plt.subplots()
ax.plot(data[:,0]*scale, data[:,1]/scale, '-')
#ax.plot(data[:,2]/scale, data[:,3]*scale, '-')


ax.grid()
ax.set_xlim(-20, 20)
ax.set_ylim(0, 220)

ax.set_xlabel(r'$t$ (ps)')
ax.set_ylabel(r'$I$ (A)')

fig.savefig('0504.png')

#%% LPS plot: Load .imc and .bkc, process the image and plot

dispersion = 0.9057 # m
momentum = 17.87 # MeV/c
#momentum = 16.79 # MeV/c

FWHM = 14.4 # ps
#FWHM = 16.6 # ps

sparameter = np.sqrt(1.87*1.92)*1.25
mm_to_ps = 1/g_c/sparameter*1e-3/1e-12


fname = get_file('.imc')
imgs, camInfos, nop = PITZ_loadImage(fname)

fname = fname[0:-4]+'.bkc'
bkgs, camInfos, nop = PITZ_loadImage(fname)

info = camInfos[0]; print('Scale: ', info.scale)

# Select ROI
ROI = getROI(np.mean(imgs, axis = 0)); print('ROI', ROI)
#ROI = squareROI(ROI)
#ROI = [0, 0, w, h]

imgs = imgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]
bkgs = bkgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]

# Get background
env, avg, std = getBackground(bkgs, treat = None); print(np.mean(np.mean(env)))

treat = 'average'
treat = 'envelope'
nstd = 1

if treat is 'average':
    emcalc_filter = 1
else:
    emcalc_filter = 0

for i in np.arange(len(imgs)):
    if treat is 'average':
        img = imgs[i]-avg; # print(i, np.max(img))
    elif treat is 'envelope':
        img = imgs[i]-env-nstd*std; # print(i, np.max(img))
        
    img[img<0] = 0
    
    if emcalc_filter:
        img = emcalcFilter(img, std, 1, 1)
    
    imgs[i] = img

img = np.mean(imgs, axis = 0)
signal = img

# Filter out noisy pixels
#signal[signal<5] = 0

#signal = np.mean(imgs, axis = 0)
#signal = avg

# Set the scale from mm to keV/c for x and ps for y
xscale, yscale = info.scale*momentum/dispersion, info.scale*mm_to_ps

FWHM0 = cal_FWHM(np.arange(len(signal)), np.sum(signal, axis = 1))*yscale
mm_to_ps *= FWHM/FWHM0 # correct for the right bunch length

# Rescale y with the measured bunch length
yscale = info.scale*mm_to_ps

[xc, yc, xrms, yrms] = calcRMS(signal[::-1], xscale, yscale)
title = str.format('$\sigma_x$ = %.2f keV/c, $\sigma_y$ = %.2f ps' % (xrms, yrms))

sop = np.sum(np.sum(np.sum(imgs)))/nop
r = []
r.append([xrms, yrms, sop])

plottype = 'imshow'
#plottype = 'contourf'

reset_margin(left=0.2, right = 0.8)
fig, ax = plt.subplots(figsize = (3, 4))

if plottype is 'contourf':
    h, w = signal.shape
    X = np.arange(0, w)*xscale-xc
    Y = np.arange(0, h)*yscale-yc
    aX, aY = np.meshgrid(X, Y)
    
    # Noise level for 2D plot
    noise = 0.001
    v = np.linspace(noise, 1, 99)*np.max(signal)

    cax = ax.contourf(aX, aY, signal[::-1,::-1], v, linestyles = None, zorder=-9)
    ax.set_rasterization_zorder(-1)
    #ax.set_aspect('auto')
    
elif plottype is 'imshow':
    h, w = signal.shape
    X = np.arange(0, w)*xscale-xc
    Y = np.arange(0, h)*yscale-yc
    
    tmp = np.copy(signal)
    tmp[tmp==0] = np.nan
    cax = ax.imshow(tmp[::-1,::1], extent = (X[0], X[-1], Y[0], Y[-1]),
                    aspect = 'auto')
    del tmp

cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04) #, format=format)
#cbar.set_ticks(np.linspace(0, 900, 10))
cbar.ax.tick_params(labelsize = 9, pad = 2)
cbar.ax.set_ylabel('Pixels', labelpad = 2)
#ax.grid()

#ax.set_title(title, fontsize = 11)
ax.set_xlabel(r'$\Delta P$ (keV/c)')
ax.set_ylabel(r'$t$ (ps)')

xmax = 100
ymax = 15
ax.set_xlim(-xmax, xmax)
ax.set_ylim(-ymax, ymax)

ax.set_aspect(10)
#plt.tight_layout()
fig.savefig(fname[0:-4]+'.png')

#%%
fig, ax = plt.subplots()
ax.plot(np.sum(signal, axis = 1), '-')