#%matplotlib inline
#%matplotlib notebook
from universal import *

from linecache import getline

color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']

def plot_config():
    from cycler import cycler
    from matplotlib.ticker import AutoMinorLocator

    fsize = 14 # a quarter of the paper width: 20 pt; half of the paper width: 12
    font = {'size' : fsize, 'family' : 'serif'}
    color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
    linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
    marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']
    
    mpl.rc('font', **font)
    mpl.rc('xtick', labelsize = 10, direction = 'in', top   = True)
    mpl.rc('ytick', labelsize = 10, direction = 'in', right = True)
    mpl.rc('xtick.major', size = 5, width = 1)
    mpl.rc('ytick.major', size = 5, width = 1)
    mpl.rc('xtick.minor', size = 3, width = 0.7, visible = True)
    mpl.rc('ytick.minor', size = 3, width = 0.7, visible = True)
    
    mpl.rc('lines', linewidth=2, markersize=6, color='r')
    # mpl.rc('lines', linestyle = 'solid')
    mpl.rc('axes', labelpad = 0,\
           prop_cycle=(cycler('color', color_cycle) + cycler('linestyle', linestyle_cycle) + cycler('marker', marker_cycle)))
    mpl.rc('legend', fontsize = 12, labelspacing = 0.05, handletextpad=0.4, frameon=False, handlelength=2.1)
    
    mpl.rc('figure', dpi = 100, figsize = (4, 4))
    mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)
    
    mpl.rc('image', cmap = 'jet')
    
    return
plot_config()

rootdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\work'
simdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim'
os.chdir(simdir)

# Frequently used functions
I2B = lambda I: -(0.0000372+0.000588*I)
B2I = lambda B: (-B-0.0000372)/0.000588
momentum2kinetic(6.3)

def astra2warp(infilename, outfilename, Q_coef = -1.0):
    data = np.loadtxt(infilename)
    data[1:,2] += data[0,2]
    data[1:,5] += data[0,5]
    
    select = (data[:,9]>0); data = data[select]
    data[:,3] = data[:,3]/g_mec2/1e6
    data[:,4] = data[:,4]/g_mec2/1e6
    data[:,5] = data[:,5]/g_mec2/1e6
    
    data[:,6] = data[:,7]*1e-9/g_qe*Q_coef # number of electrons for each macro particle
    
    np.savetxt(outfilename, data[:,0:7], fmt = '%14.6E')
    return

def linear_interp(x, p1, p2):
    '''
    Linear interpolation. Given two points p1 = [x1, y1], p2 = [x2, y2], get the y-value at x
    Parameters
      x: the position of the point in the known dimension
      p1 = [x1, y1, ..]: the position of the first known point
      p2 = [x2, y2, ..]: the position of the second known point
    Returns
      y: the position of the point in the dimension to be interpolated 
    '''
    return p2[1:]+(x-p2[0])/(p2[0]-p1[0])*(p2[1:]-p1[1:])

workdir = r'C:\Users\lixiangk\Desktop\New folder'
os.chdir(workdir)

### load particles
dist0 = np.loadtxt(r'beam_und1.ini')

dist0[1:,2] += dist0[0,2]
dist0[1:,5] += dist0[0,5]

x, y, z, ux, uy, uz, w, w1, w1, w3 = dist0.T[:]
ux = ux/(g_mec2*1e6)
uy = uy/(g_mec2*1e6)
uz = uz/(g_mec2*1e6)

bg = np.sqrt(ux**2+uy**2+uz**2)
gg = np.sqrt(1.0+bg**2)
vx, vy, vz = ux/gg*g_c, uy/gg*g_c, uz/gg*g_c
###

dist1 = np.copy(dist0)

### boost factor
gamma_boost = 12.25
beta_boost = gamma2beta(gamma_boost)
###

### 
t = 0
z1 = z+5e-3
dt = (beta_boost*z1/g_c-t)/(1-beta_boost*vz/g_c)
###

for i in np.arange(len(dt)):
    if np.mod(i, 1000) == 0:
        print i,
    zi = z1[i]+dt[i]*vz[i]
    zpos = (int(zi*1e3)//10)*10
    
    fname = os.path.join(str.format('ast.%04d.001' % zpos))
    d10 = [float(v) for v in getline(fname, 1).split()]
    d11 = [float(v) for v in getline(fname, i+1).split()]
    
    fname = os.path.join(str.format('ast.%04d.001' % (zpos+10)))
    d20 = [float(v) for v in getline(fname, 1).split()]
    d21 = [float(v) for v in getline(fname, i+1).split()]
    
    if i>1:
        d11[2] += d10[2]; d11[5] += d10[5]
        d21[2] += d20[2]; d21[5] += d20[5]
    else:
        d11 = d10; d21 = d20;
    
    p1 = np.concatenate(([d11[2]], d11[0:6]))
    p2 = np.concatenate(([d21[2]], d21[0:6]))
    dist1[i,0:6] = linear_interp(zi, p1, p2)
    
dist1[:,2] = -dist1[:,2]

### plot
xt, yt, zt = dist1[::3,:].T[0:3]

fig, ax = plt.subplots(figsize = (6, 4))
ax.plot(zt*1e3, xt*1e3, '.')
ax.plot(zt*1e3, yt*1e3, '.')
ax.grid()

ax.set_xlabel(r'$z$ (mm)')
ax.set_ylabel(r'$x,y$ (mm)')
ax.set_xlim()
fig.savefig('labview1.eps')
###

dist1[1:,2] -= dist1[0,2]
dist1[1:,5] -= dist1[0,5]

dist1[:,3] = -dist1[:,3]
dist1[:,4] = -dist1[:,4]

np.savetxt('beam_und.ast', dist1, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4d%4d')

astra2warp('beam_und.ast', 'beam_und.warp')


