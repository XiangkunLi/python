"""
This file defines a set of external fields, i.e. fields that are
calculated analytically and applied to the particles at each timestep.

These fields are not actually evolved on the simulation grid.

Usage:
1. Define the `FieldMap3D` object
  gun3d = FieldMap3D('3D_gun42cavity', 60e6, z0 = 0, freq = 1.3e9, phi0 = 0)
  or for solenoid
  sol3d = StaticM3D('3D_gunsolenoidsPITZ', 0.2, z0 = 0)
2. Install it in warp by
  add_external_fieldmaps([gun3d, sol3d])
   or manually
  installothereuser( gun3d.add_field_on_particles )
  installothereuser( sol3d.add_field_on_particles )

Modified from: "warp_install\init_tools\external_fields.py" 
April 20, 2020, X.-K. Li 
"""

from warp import *
import numpy as np
from scipy.constants import m_e, c, e
from scipy.interpolate import RegularGridInterpolator

def index_min(x):
    ''' 
    Parameters
      x: an 1-D array
    Returns
      [i, v]: the index of the minimum and itself
    '''
    i,v=0,x[0]
    for index,value in enumerate(x):
        if value<v:
            i,v=index,value
    return [i,v]

def index_max(x):
    ''' 
    Parameters
      x: an 1-D array
    Returns
      [i, v]: the index of the maximum and itself
    '''
    i,v=0,x[0]
    for index,value in enumerate(x):
        if value>v:
            i,v=index,value
    return [i,v]

def read2array(fname):

    xgrid = np.loadtxt(fname, max_rows = 1)
    ygrid = np.loadtxt(fname, max_rows = 1, skiprows = 1)
    zgrid = np.loadtxt(fname, max_rows = 1, skiprows = 2)
    field = np.loadtxt(fname, skiprows = 3)
    
    nx, ny, nz = len(xgrid)-1, len(ygrid)-1, len(zgrid)-1
    def _convert_((i, j, k)):
        return field[j+k*ny,i]

    II = np.meshgrid(np.arange(nx), np.arange(ny), np.arange(nz), indexing = 'ij')
    
    fld3d = _convert_(II) #np.zeros(shape = (len(xgrid)-1, len(ygrid)-1, len(zgrid)-1))
    
    return [xgrid[1:], ygrid[1:], zgrid[1:], fld3d]

def add_external_fieldmaps( FM3D_list ):
    """
    Add a linearly-polarized laser with simultaneous spatial
    and temporal focusing (SSTF).

    See Durst et al., Opt Commun. 2008, 281(7):1796-1805 for
    the analytical formula.

    Parameters
    ----------
    """
    # Install the object in Warp, so that it calls the function
    # add_field_on_particles at each timestep
    for FW3D in FM3D_list:
        installothereuser( FM3D.add_field_on_particles )

class FieldMap3D( object ):
    """Class that calculates analytically the field of an SSTF laser"""

    def __init__( self, basename, amp0, z0 = 0, freq = 1.3e9, phi0 = 0) :
        """
        Register the parameters

        See the docstring of add_external_sstf_laser for the parameters
        """
        # Intermediate variables
        self.E0x = amp0
        self.E0y = amp0
        self.E0z = amp0
        self.B0x = amp0
        self.B0y = amp0
        self.B0z = amp0

        self.build(basename, z0, freq, phi0)
        
    def add_field_on_particles( self ):
        """
        Function to be called at each timestep, through `installothereuser`

        This function adds the external, analytical SSTF field to
        the fields felt by the macroparticles.
        """
        # Extract the time
        t = top.time

        # Get the indices of the current group of particles
        jmin = w3d.jmin
        jmax = w3d.jmax
        if jmax <= jmin:
            return()

        # Extract the arrays of particle positions
        x = top.pgroup.xp[ jmin:jmax ]
        y = top.pgroup.yp[ jmin:jmax ]
        z = top.pgroup.zp[ jmin:jmax ]

        # Calculate the laser profile (this normalized to 1 at focus)
        field_profile = self.compute_profile( x, y, z, t )

        Ex, Ey, Ez, Bx, By, Bz = field_profile.T[:]
        
        # Add the fields (with proper dimension and amplitude)
        # to the particle field array
        
        top.pgroup.ex[ jmin:jmax ] += self.E0x * Ex
        top.pgroup.ey[ jmin:jmax ] += self.E0y * Ey
        top.pgroup.ez[ jmin:jmax ] += self.E0z * Ez
        top.pgroup.bx[ jmin:jmax ] += self.B0x * Bx
        top.pgroup.by[ jmin:jmax ] += self.B0y * By
        top.pgroup.bz[ jmin:jmax ] += self.B0z * Bz


    def compute_profile( self, x, y, z, t ):
        """
        Return the normalized laser profile (normalized to 1 at focus)
        at the position of the particles

        Parameters
        ----------
        x, y, z: 1darray of floats (meter)
            Arrays of shape (n_particles,) which contain the particle positions

        t: float (second)
            Time at which the field is calculated

        Returns
        -------
        A 1darray of reals, of shape (n_particles,)
        """
        # Single scalar, that calculates the position of the centroid
        
        profile = self.EM3D(x, y, z, t)
        
        return profile
    
    def build(self, basename, z0, freq, phi0):
        '''
        Parameters
          basename: 
        '''
        
        fname = basename+'.ex'
        xgrid, ygrid, zgrid, Ex3D = read2array(fname)

        fname = basename+'.ey'
        xgrid, ygrid, zgrid, Ey3D = read2array(fname)

        fname = basename+'.ez'
        xgrid, ygrid, zgrid, Ez3D = read2array(fname)
        
        fname = basename+'.bx'
        xgrid, ygrid, zgrid, Bx3D = read2array(fname)

        fname = basename+'.by'
        xgrid, ygrid, zgrid, By3D = read2array(fname)
        
        fname = basename+'.bz'
        xgrid, ygrid, zgrid, Bz3D = read2array(fname)
            
        fEx3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Ex3D, bounds_error = False, fill_value = 0)
        fEy3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Ey3D, bounds_error = False, fill_value = 0)
        fEz3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Ez3D, bounds_error = False, fill_value = 0)

        fBx3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Bx3D, bounds_error = False, fill_value = 0)
        fBy3D = RegularGridInterpolator((xgrid, ygrid, zgrid), By3D, bounds_error = False, fill_value = 0)
        fBz3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Bz3D, bounds_error = False, fill_value = 0)

        def EM3D(x, y, z, t = 0, *args):
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            x = np.asarray(x).flatten()
            y = np.asarray(y).flatten()
            z = np.asarray(z).flatten()

            nn = np.max([x.size, y.size, z.size])

            if x.size == 1:
                x = np.ones(nn)*x[0]
            if y.size == 1:
                y = np.ones(nn)*y[0]
            if z.size == 1:
                z = np.ones(nn)*z[0]

            if not np.isscalar(t):
                t = np.asarray(t)

            z1 = z-z0
            
            omega = 2.*np.pi*freq
            phi1 = (phi0)*np.pi/180
            
            omega_t = omega*t+phi1
            ss = np.sin(omega_t)
            cc = np.cos(omega_t)
            
            E0x = fEx3D((x, y, z1))*ss
            E0y = fEy3D((x, y, z1))*ss
            E0z = fEz3D((x, y, z1))*ss
            
            B0x = fBx3D((x, y, z1))*cc
            B0y = fBy3D((x, y, z1))*cc
            B0z = fBz3D((x, y, z1))*cc

            F2d = np.zeros((nn, 6))

            F2d[:, 0] = E0x
            F2d[:, 1] = E0y
            F2d[:, 2] = E0z
            
            F2d[:, 3] = B0x
            F2d[:, 4] = B0y
            F2d[:, 5] = B0z

            return F2d
        
        self.EM3D = EM3D
        
class StaticM3D( FieldMap3D ):
    """Class that calculates analytically the field of an SSTF laser"""

    def __init__( self, basename, amp0, z0 = 0) :
        """
        Register the parameters

        See the docstring of add_external_sstf_laser for the parameters
        """
        # Intermediate variables
        self.E0x = amp0
        self.E0y = amp0
        self.E0z = amp0
        self.B0x = amp0
        self.B0y = amp0
        self.B0z = amp0

        self.build(basename, z0)
        
    def build(self, basename, z0):
        '''
        Parameters
          basename: 
        '''
        
        fname = basename+'.bx'
        xgrid, ygrid, zgrid, Bx3D = read2array(fname)

        fname = basename+'.by'
        xgrid, ygrid, zgrid, By3D = read2array(fname)
        
        fname = basename+'.bz'
        xgrid, ygrid, zgrid, Bz3D = read2array(fname)
            
        fBx3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Bx3D, bounds_error = False, fill_value = 0)
        fBy3D = RegularGridInterpolator((xgrid, ygrid, zgrid), By3D, bounds_error = False, fill_value = 0)
        fBz3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Bz3D, bounds_error = False, fill_value = 0)

        def EM3D(x, y, z, t = 0, *args):
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            x = np.asarray(x).flatten()
            y = np.asarray(y).flatten()
            z = np.asarray(z).flatten()

            nn = np.max([x.size, y.size, z.size])

            if x.size == 1:
                x = np.ones(nn)*x[0]
            if y.size == 1:
                y = np.ones(nn)*y[0]
            if z.size == 1:
                z = np.ones(nn)*z[0]

            if not np.isscalar(t):
                t = np.asarray(t)

            z1 = z-z0
            
            B0x = fBx3D((x, y, z1))
            B0y = fBy3D((x, y, z1))
            B0z = fBz3D((x, y, z1))

            F2d = np.zeros((nn, 6))

            F2d[:, 3] = B0x
            F2d[:, 4] = B0y
            F2d[:, 5] = B0z

            return F2d
        
        self.EM3D = EM3D


def add_external_fieldmap( filename, z0 = 0, N_u = 120, lam_u = 0.03,
                           NoH = 15, gamma_boost = None ):
    """
    Add a linearly-polarized laser with simultaneous spatial
    and temporal focusing (SSTF).

    See Durst et al., Opt Commun. 2008, 281(7):1796-1805 for
    the analytical formula.

    Parameters
    ----------
    """
    # Create an SstfLaser object to store the parameters
    undulator = Undulator3D()
    #undulator.build0( filename, z0 = z0, N_u = N_u, lam_u = lam_u, NoH = NoH )
    undulator.build( filename, z0 = z0, gamma_boost = gamma_boost )

    # Install the object in Warp, so that it calls the function
    # add_field_on_particles at each timestep
    installothereuser( undulator.add_field_on_particles )

class Undulator3D( object ):
    """Class that calculates analytically the field of an SSTF laser"""

    def __init__( self, amp0 = 1) :
        """
        Register the parameters

        See the docstring of add_external_sstf_laser for the parameters
        """
        # Intermediate variables
        self.E0x = amp0
        self.E0y = amp0
        self.E0z = amp0
        self.B0x = amp0
        self.B0y = amp0
        self.B0z = amp0

    def add_field_on_particles( self ):
        """
        Function to be called at each timestep, through `installothereuser`

        This function adds the external, analytical SSTF field to
        the fields felt by the macroparticles.
        """
        # Extract the time
        t = top.time

        # Get the indices of the current group of particles
        jmin = w3d.jmin
        jmax = w3d.jmax
        if jmax <= jmin:
            return()

        # Extract the arrays of particle positions
        x = top.pgroup.xp[ jmin:jmax ]
        y = top.pgroup.yp[ jmin:jmax ]
        z = top.pgroup.zp[ jmin:jmax ]

        # Calculate the laser profile (this normalized to 1 at focus)
        field_profile = self.compute_profile( x, y, z, t )

        Ex, Ey, Ez, Bx, By, Bz = field_profile.T[:]
        
        # Add the fields (with proper dimension and amplitude)
        # to the particle field array
        
        top.pgroup.ex[ jmin:jmax ] += self.E0x * Ex
        top.pgroup.ey[ jmin:jmax ] += self.E0y * Ey
        top.pgroup.ez[ jmin:jmax ] += self.E0z * Ez
        top.pgroup.bx[ jmin:jmax ] += self.B0x * Bx
        top.pgroup.by[ jmin:jmax ] += self.B0y * By
        top.pgroup.bz[ jmin:jmax ] += self.B0z * Bz


    def compute_profile( self, x, y, z, t ):
        """
        Return the normalized laser profile (normalized to 1 at focus)
        at the position of the particles

        Parameters
        ----------
        x, y, z: 1darray of floats (meter)
            Arrays of shape (n_particles,) which contain the particle positions

        t: float (second)
            Time at which the field is calculated

        Returns
        -------
        A 1darray of reals, of shape (n_particles,)
        """
        # Single scalar, that calculates the position of the centroid
        
        profile = self.EM3D(x, y, z, t)
        
        return profile

    def build(self, basename, z0 = 0, gamma_boost = None):
        '''
        Parameters
          basename: 
        '''
        self.gamma_boost = gamma_boost
        
        fname = basename+'.bx'
        xgrid, ygrid, zgrid, Bx3D = read2array(fname)

        fname = basename+'.by'
        xgrid, ygrid, zgrid, By3D = read2array(fname)

        fname = basename+'.bz'
        xgrid, ygrid, zgrid, Bz3D = read2array(fname)
            
        fBx3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Bx3D, bounds_error = False, fill_value = 0)
        fBy3D = RegularGridInterpolator((xgrid, ygrid, zgrid), By3D, bounds_error = False, fill_value = 0)
        fBz3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Bz3D, bounds_error = False, fill_value = 0)

        def EM3D(x, y, z, t = 0, *args):
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            x = np.asarray(x).flatten()
            y = np.asarray(y).flatten()
            z = np.asarray(z).flatten()

            nn = np.max([x.size, y.size, z.size])

            if x.size == 1:
                x = np.ones(nn)*x[0]
            if y.size == 1:
                y = np.ones(nn)*y[0]
            if z.size == 1:
                z = np.ones(nn)*z[0]

            if not np.isscalar(t):
                t = np.asarray(t)

            gamma_boost = self.gamma_boost
            if gamma_boost is None:
                gamma_boost = 1
            beta_boost = np.sqrt(1.0-1.0/gamma_boost**2)

            #z0_boost = z0 / gamma_boost - beta_boost * c * t
            #z1 = (z - z0_boost) * gamma_boost

            z_lab = gamma_boost*(z+beta_boost*c*top.time)
            z1 = z_lab-z0

            B0x = fBx3D((x, y, z1))
            B0y = fBy3D((x, y, z1))
            B0z = fBz3D((x, y, z1))

            E1x = -gamma_boost*beta_boost*c*B0y
            E1y =  gamma_boost*beta_boost*c*B0x

            B1x =  gamma_boost*B0x
            B1y =  gamma_boost*B0y
            B1z =  B0z

            F2d = np.zeros((nn, 6))

            F2d[:, 0] = E1x
            F2d[:, 1] = E1y
            
            F2d[:, 3] = B1x
            F2d[:, 4] = B1y
            F2d[:, 5] = B1z

            return F2d
        
        self.EM3D = EM3D
        
    def build0(self, filename, z0 = 0, N_u = 120, lam_u = 0.03, NoH = 15):
        '''
        Parameters
          NoH: number of harmonics
        '''

        L_u = N_u*lam_u
        
        data = np.loadtxt(filename)
        zz = data[:,0]; By = data[:,1]; dz = zz[1]-zz[0]
        
        # find the rough center
        zc1 = np.sum(zz*np.abs(By))/np.sum(np.abs(By))
        zz1 = zz-zc1
        
        # find exact center where By=0
        index, tmp = index_min(np.abs(zz1))
        Nz_quater = int(lam_u/4./dz)
        from scipy.interpolate import interp1d
        fz_By = interp1d(By[index-Nz_quater:index+Nz_quater], zz1[index-Nz_quater:index+Nz_quater])
        zc = fz_By(0)
        zz = zz1-zc
        
        select = (zz<=L_u/2.)*(zz>=-L_u/2.)
        zz = zz[select]
        By = By[select]
        
        # Fourier analysis
        nharm = np.arange(0, N_u*NoH+1)
        
        k_n = 2.*np.pi*nharm/L_u
        
        xi = 0
        ky_n = k_n/np.sqrt(1+xi**2)
        kx_n = xi*ky_n
        
        a_0 = np.sum(By*dz)/L_u; #print a_0
        #b_0 = 0
        
        a_n = [2./L_u*np.sum(By*np.cos(k_n[i]*zz)*dz) for i in nharm]
        b_n = [2./L_u*np.sum(By*np.sin(k_n[i]*zz)*dz) for i in nharm]
        
        a_n = np.array(a_n); a_n[0] = a_n[0]/2.; #print a_n[0]
        b_n = np.array(b_n); #print b_n[0]
        
        def EM3D(x, y, z, t = 0, *args):
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            x = np.asarray(x).flatten()
            y = np.asarray(y).flatten()
            z = np.asarray(z).flatten()
            
            nn = np.max([x.size, y.size, z.size])
            
            if x.size == 1:
                x = np.ones(nn)*x[0]
            if y.size == 1:
                y = np.ones(nn)*y[0]
            if z.size == 1:
                z = np.ones(nn)*z[0]
            
            if not np.isscalar(t):
                t = np.asarray(t)
            
            z1 = z-z0
            B1y = [np.sum(( a_n*np.cos(k_n*z1[i])+b_n*np.sin(k_n*z1[i]))*np.cosh(k_n*y[i])) for i in np.arange(nn)]
            B1z = [np.sum((-a_n*np.sin(k_n*z1[i])+b_n*np.cos(k_n*z1[i]))*np.sinh(k_n*y[i])) for i in np.arange(nn)]
            
            B1y = np.where(z1<-L_u/2., 0, B1y)
            B1y = np.where(z1> L_u/2., 0, B1y)
            
            B1z = np.where(z1<-L_u/2., 0, B1z)
            B1z = np.where(z1> L_u/2., 0, B1z)
            
            F2d = np.zeros((nn, 6))
            F2d[:,4] = B1y
            F2d[:,5] = B1z
            
            return F2d
        
        self.EM3D = EM3D
