import numpy as np
import matplotlib as mpl
mpl.use('Agg')

import matplotlib.pyplot as plt 
import matplotlib.image as mgimg
from matplotlib import animation
from matplotlib.animation import FuncAnimation,FFMpegFileWriter

fig = plt.figure(figsize = (6, 3))
plt.axis('off')
# initiate an empty  list of "plotted" images 
myimages = []

#Writer = animation.writers['ffmpeg']
#writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)

#loops through available png:s
for p in np.arange(2, 25, 2):

    ## Read in picture
    fname = "z-hist2-%.1f.png" % p 
    img = mgimg.imread(fname)
    imgplot = plt.imshow(img)

    # append AxesImage object to the list
    myimages.append([imgplot])

## create an instance of animation
my_anim = animation.ArtistAnimation(fig, myimages, interval=1000, blit=True, repeat_delay=1000)

## NB: The 'save' method here belongs to the object you created above
#writer = FFMpegFileWriter(fps=25,codec="libx264")
my_anim.save("animation.mp4")#, writer=writer)

## Showtime!
plt.show()