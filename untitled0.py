# -*- coding: utf-8 -*-
"""
Created on Wed Jun  9 20:57:43 2021

@author: lixiangk
"""

from  transport.TransferMatrix import *

#%%

rho, theta = 0.6, np.pi/3.0
D1 = Dipole(rho, theta, gamma = M2K(19))

l0, l1 = 0.14, 0.6994
L1 = Drift(l1)

TR = np.ones(6)
[D1, L1] = [TR*np.array(M.evalf(5), dtype = 'double')
                                for M in [D1, L1]]

M = L1 @ D1; print(M)