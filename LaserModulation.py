# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 15:55:24 2020

@author: lixiangk
"""

from astra_modules import *

workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\Genesis-demo\importdist'
os.chdir(workdir)


#%%
BSA = 2.5 # mm
sig_x = BSA/4
sig_y = BSA/4
C_sig_x = BSA/sig_x
C_sig_y = BSA/sig_y

LT = 10 # ps
RT = 0.001  # ps

Ipart = 500000
Q_total = 500e-3 # nC

genName = 'gen.in'
generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons',
                       Q_total = Q_total,
                       Ref_Ekin = 16.5, dist_pz = 'i',
                       Dist_z = 'p', LT = 20e-3/, RT = 2e-3, 
                       Dist_x = '2', sig_x = sig_x, Dist_px = 'g', Nemit_x = 0,
                       Dist_y = '2', sig_y = sig_y, Dist_py = 'g', Nemit_y = 0,
                       C_sig_x = C_sig_x, C_sig_y = C_sig_y)
generator.set(Dist_x = 'r', sig_x = sig_x, Dist_y = 'r', sig_y = sig_y)
generator.set(Dist_z = 'p', Lt = LT*1e-3, Rt = RT*1e-3)


generator.write(genName)
os.system('generator '+genName)

#%%
LT = 21.5 # ps
RT = 2  # ps

# Define the modulated profile
sig_tg = 2.0 # global rms, ps
sig_tl = 0.2 # local rms
tl = np.arange(-LT//2+1, LT//2)  # centers of local profiles

tt = np.linspace(-LT/2, LT/2, 1000)
pdf = np.sum(np.array([np.exp(-(tt-ti)**2/2./sig_tl**2)*np.exp(-ti**2/2.0/sig_tg**2) for ti in tl]), axis = 0)

cdf = np.copy(pdf)
for i in np.arange(1, len(cdf)):
    cdf[i] += cdf[i-1]
cdf /= cdf[-1]

ft_CDF = interp1d(cdf, tt, bounds_error = False, fill_value = 0)
#%%
import numpy as np
from scipy import fft
from scipy.interpolate import interp1d

from matplotlib import pyplot as plt

LT = 21.5 # ps
RT = 2 # ps
freq = 0.5 #1/(100e-6/g_c*1e12) # THz

def envelope(t, t0, s):
    arg = - (t-t0)**2 / (s ** 2)
    i = np.exp(arg)
    return i

def envelope_FT(t, LT, RT):
    
    i = 1/LT*1/(1+np.exp(2/RT*(2*np.abs(t)-LT)))
    return i


def modulation0(t, f, p):
    arg = 2 * np.pi * (t * f) + p
    i = (4 + np.cos(arg))
    return i

def modulation(t, f, p):
    #arg = 2 * np.pi * (t * f) + p
    arg = np.mod(t, 1./f)*f
    i = (1 + arg**2)
    return i

def intensity(t):
    factor = 1.0
    i = envelope_FT(t, LT, RT) * modulation(t * factor, freq, 0.0)
    i = i ** 2
    i = i / sum(i)
    return i

T = np.linspace(0, 1.5*LT, 50000)
pdf = intensity(T-0.75*LT)

cdf = np.copy(pdf)
for i in np.arange(1, len(cdf)):
    cdf[i] += cdf[i-1]
cdf /= cdf[-1] # normalize

ft_CDF = interp1d(cdf, T, bounds_error = False, fill_value = 0)
easy_plot(T, pdf, '-')
easy_plot(T, cdf, '-')
#%%
data = np.loadtxt('beam_neg_chirp.ini')
time0 = data[:,2]/g_c*1e12 # ps

time1 = ft_CDF((time0+0.75*LT)/LT/1.5)
time1 = (time1-0.75*LT)*1.5

data[:,2] = time1*1e-12*g_c
np.savetxt('beam_modulated2.ini', data,
           fmt = '%20.12E%20.12E%20.12E%20.12E%20.12E%20.12E%20.12E%20.12E%4d%4d')

fig, [ax0, ax1, ax2] = plt.subplots(ncols = 3, figsize = (12, 4))
ax0.plot(T, pdf, '-')
ax0.set_title("MBI Lyot function")

ax1.plot(T, cdf, '-')
ax1.set_title("CDF")

_ = ax2.hist(time0*g_c*1e-9, bins = 500, histtype = r'step')
_ = ax2.hist(time1*g_c*1e-9, bins = 5000, histtype = r'step')
ax2.set_title('Modulated beam profile')
ax2.legend(['Flattop', 'Modulated'])

plt.tight_layout()
fig.savefig('Modulated2.png')

#%%
fig, ax = plt.subplots()
_ = ax.hist(time0*g_c*1e-9, bins = 500, histtype = r'step', density = 0)
_ = ax.hist(time1*g_c*1e-9, bins = 5000, histtype = r'step', density = 0)
ax.set_title('Modulated beam profile')
ax.legend(['Flattop', 'Modulated'])
#ax.set_ylim(0, 0.3)
ax.set_xlabel(r'Position (mm)')
ax.set_ylabel('Current (arb. unit)')

plt.tight_layout()
fig.savefig('ModulatedProfile.png')

#%%

data = pd_loadtxt('beam_modulated2.ini')
data = pd_loadtxt('beam_neg_chirp.ini')

data[1:,2] += data[0,2]
data[1:,5] += data[0,5]

select = (data[:,2]>=-100e-6*40)*(data[:,2]<=100e-6*40); data = data[select]

x, y, z = data[:,0:3].T[:]
t = z/g_c

Px, Py, Pz = data[:,3:6].T[:]
xp = Px/Pz
yp = Py/Pz
p = np.sqrt(Px**2+Py**2+Pz**2)/1e6/g_mec2

import h5py

f = h5py.File('test_dist_flattop.h5', 'w')

dx = f.create_dataset("x", data = x)
dy = f.create_dataset("y", data = y)
dy = f.create_dataset("t", data = t)

dxp = f.create_dataset("xp", data = xp)
dyp = f.create_dataset("yp", data = yp)
dp = f.create_dataset("p", data = p)

f.close()

#%%
import h5py

f = h5py.File('test_dist.h5', 'r')
print(f.keys())

z = f.get('t')[:]*g_c

f.close()

plt.figure()
_ = plt.hist(z/slicewidth, bins = 1000, histtype = r'step', density = 0)
plt.grid()
plt.xlabel('# of slices')
plt.ylabel('Current (arb. unit)')
#plt.xlim(30, 50)

#%%
import h5py

fname = 'test_100A_rd.h5'
f = h5py.File(fname, 'r')
print(f.keys())

nslice = f['slicecount'][0]
slicewidth = f['slicelength'][0]
nmacro = len(f['slice000001/x'][:])

beamdist = np.zeros((nslice*nmacro, 7))
current = []
for i in np.arange(nslice):
    name = 'slice%06d' % (i+1)
    
    beamdist[i*nmacro:i*nmacro+nmacro,0] = f[name+'/x'][:]
    beamdist[i*nmacro:i*nmacro+nmacro,1] = f[name+'/y'][:]
    beamdist[i*nmacro:i*nmacro+nmacro,2] = f[name+'/theta'][:]*(slicewidth/2/np.pi)+i*slicewidth
    beamdist[i*nmacro:i*nmacro+nmacro,3] = f[name+'/px'][:]
    beamdist[i*nmacro:i*nmacro+nmacro,4] = f[name+'/py'][:]
    beamdist[i*nmacro:i*nmacro+nmacro,5] = f[name+'/gamma'][:]
    
    beamdist[i*nmacro:i*nmacro+nmacro,6] = f[name+'/current'][0]; 
    
    current.append([i+1, f[name+'/current'][0]])
    print(i+1, f[name+'/current'][0], f[name+'/current'][0])

f.close()

tmp = np.array(current)
print(np.sum(tmp[:,1])*100e-6/g_c)


plt.figure()
select = (beamdist[:,2]>0)*(beamdist[:,2]<25e-3)
_ = plt.hist(beamdist[select,2]/slicewidth, bins = 5000, histtype = r'step', weights = beamdist[select,-1], density = True)
plt.grid()
plt.xlabel('# of slices')
plt.ylabel('Current (arb. unit)')
#plt.xlim(30, 50)
plt.savefig('current_profile@'+fname+'.png')

#%%
import h5py
import numpy.random as rd

f = h5py.File('test_1.0.0.par.h5', 'r')
fout = h5py.File('test_100A_rd.h5', 'w')

keys = ['beamletsize', 'one4one', 'refposition', 'slicecount', 'slicelength', 'slicespacing']
for key in keys:
    a = f.get(key)
    dtmp = fout.create_dataset(key, data = a[:])
    
for key in keys:
    print(f.get(key))
    print(fout.get(key))
    
nslice = f['slicecount'][0]
slicewidth = f['slicelength'][0]
nmacro = len(f['slice000001/x'][:])

current = []
for i in np.arange(nslice):
    name = 'slice%6.6d' % (i+1)
    
    x = f[name+'/x']
    dx = fout.create_dataset(name+'/x', data = x[:])
    
    y = f[name+'/y']
    dy = fout.create_dataset(name+'/y', data = y[:])
    
    px = f[name+'/px']
    dpx = fout.create_dataset(name+'/px', data = px[:])
    
    py = f[name+'/py']
    dpy = fout.create_dataset(name+'/py', data = py[:])
    
    gamma = f[name+'/gamma']
    dgamma = fout.create_dataset(name+'/gamma', data = gamma[:])
    
    if i>500:
        t = f[name+'/theta']
    else:
        #t = np.linspace(0.5, 1.5, 8192)*np.pi
        t = (rd.rand(8192)+0.5)*np.pi
    dt = fout.create_dataset(name+'/theta', data = t[:])

    c = f[name+'/current']
    if i>500:
        dc = fout.create_dataset(name+'/current', data = c[:])
    else:
        dc = fout.create_dataset(name+'/current', data = c[:]/2)
    
f.close()
fout.close()

#%%
import h5py

f = h5py.File('test1.0.par.h5', 'r')
print(f.keys())

nslice = f['slicecount'][0]
slicewidth = f['slicelength'][0]
nmacro = len(f['slice000001/x'][:])

beamdist = np.zeros((nslice*nmacro, 7))
for i in np.arange(nslice):
    name = 'slice%06d' % (i+1)
    
    current = f[name+'/current'][0]
    theta = f[name+'/theta'][:]
    if current > 0:
        beamdist[i*nmacro:i*nmacro+nmacro,0] = f[name+'/x'][:]
        beamdist[i*nmacro:i*nmacro+nmacro,1] = f[name+'/y'][:]
        beamdist[i*nmacro:i*nmacro+nmacro,2] = theta*(slicewidth/2/np.pi)+i*slicewidth
        beamdist[i*nmacro:i*nmacro+nmacro,3] = f[name+'/px'][:]
        beamdist[i*nmacro:i*nmacro+nmacro,4] = f[name+'/py'][:]
        beamdist[i*nmacro:i*nmacro+nmacro,5] = f[name+'/gamma'][:]
    
        beamdist[i*nmacro:i*nmacro+nmacro,6] = current; 
        
    print(i+1, f[name+'/current'][0], np.sum(theta==np.inf),)

f.close()

plt.figure()
_ = plt.hist(beamdist[:,2]/100e-6, bins = 1000, histtype = r'step', weights = beamdist[:,-1], density = True)
plt.grid()

#%%
f = h5py.File('test1.Run2.5.par.h5', 'r')
print(f.keys())

nslice = f['slicecount'][0]
slicewidth = f['slicelength'][0]
slicespacing = f['slicespacing'][0]

nmacro = len(f['slice000001/x'][:])

beamdist = np.zeros((nslice*nmacro, 7))
for i in np.arange(nslice):
    name = 'slice%06d' % (i+1)
    
    current = f[name+'/current'][0]
    theta = f[name+'/theta'][:]
    if current > 0:
        beamdist[i*nmacro:i*nmacro+nmacro,0] = f[name+'/x'][:]
        beamdist[i*nmacro:i*nmacro+nmacro,1] = f[name+'/y'][:]
        beamdist[i*nmacro:i*nmacro+nmacro,2] = theta*(slicewidth/2/np.pi)+i*slicespacing
        beamdist[i*nmacro:i*nmacro+nmacro,3] = f[name+'/px'][:]
        beamdist[i*nmacro:i*nmacro+nmacro,4] = f[name+'/py'][:]
        beamdist[i*nmacro:i*nmacro+nmacro,5] = f[name+'/gamma'][:]
    
        beamdist[i*nmacro:i*nmacro+nmacro,6] = current; 
        
    print(i+1, f[name+'/current'][0], np.sum(theta==np.inf), end = ',')

f.close()

plt.figure()
_ = plt.hist(beamdist[:,2]/100e-6, bins = 1000, histtype = r'step', weights = beamdist[:,-1], density = True)
plt.grid()

#%%
import h5py

f = h5py.File('modulated.21.0.par.h5', 'r')
print(f.keys())

nslice = f['slicecount'][0]
slicewidth = f['slicelength'][0]
nmacro = len(f['slice000001/x'][:])

beamdist = np.zeros((nslice*nmacro, 7))
for i in np.arange(nslice):
    name = 'slice%06d' % (i+1)
    
    current = f[name+'/current'][0]
    theta = f[name+'/theta'][:]
    if current > 0:
        beamdist[i*nmacro:i*nmacro+nmacro,0] = f[name+'/x'][:]
        beamdist[i*nmacro:i*nmacro+nmacro,1] = f[name+'/y'][:]
        beamdist[i*nmacro:i*nmacro+nmacro,2] = theta*(slicewidth/2/np.pi)+i*slicewidth
        beamdist[i*nmacro:i*nmacro+nmacro,3] = f[name+'/px'][:]
        beamdist[i*nmacro:i*nmacro+nmacro,4] = f[name+'/py'][:]
        beamdist[i*nmacro:i*nmacro+nmacro,5] = f[name+'/gamma'][:]
    
        beamdist[i*nmacro:i*nmacro+nmacro,6] = current; 
        
    print(i+1, f[name+'/current'][0], np.sum(theta==np.inf))
    

f.close()

plt.figure()
_ = plt.hist(beamdist[:,2], bins = 1000, histtype = r'step', weights = beamdist[:,-1], density = True)
plt.grid()

#%%
fname = 'test1.Run2.out.h5'
f = h5py.File(fname, 'r')
for key in f.get('Global').keys():
    print(key, f.get('Global').get(key)[:])
    
#%%
from interface import *

fname = 'test1.Run2.out.h5'
pg = PostGenesis(fname, debug = 1, harmonic = 8)

#%% 

def add_particles(beamdist0, mpart = 8192):
    '''
    Resampling to add samples to a known set, rewriting the C++ code used in Genesis 1.3 V4 into Python.
    Add particles to the input distribution `beamdist0` and return a new distribution of size `mpart`.

    Parameters
    ----------
    beamdist0 : 2D array of size (ndist0, 6)
        6D particle distribution, with columns storing coordinates of x, y, theta, px, py, gamma, respectively.
    mpart : int
        Number of particles of the new distribution to return.

    Returns
    -------
    beamdist : 2D array of size (mpart, 6)
        6D particle distribution, with columns storing coordinates of x, y, theta, px, py, gamma, respectively.


    Examples
    -------
    ```
    mpart = 4096
    beamdist0 = np.random.rand(4000, 6) # x, y, theta, px, py, gamma
    beamdist = add_particles(beamdist0, mpart)
    ```
    '''
    #mpart = 4096
    #beamdist0 = np.random.rand(4000, 6) # x, y, theta, px, py, gamma
    
    ndist0 = beamdist0.shape[0]
    
    beamdist = np.zeros((mpart, 6))
    beamdist[:ndist0] = beamdist0
    
    # Step 1 - calculate the center and the rms beam size
    avg = np.mean(beamdist0, axis = 0)
    std = np.std( beamdist0, axis = 0)
    
    # Step 2 - invert the beam size for normalization and check for "cold" dimensions, e.g. zero energy spread
    std_inv = np.where(std == 0, 1, 1/std)
    
    # Step 3 - normalize distribution so that it is aligned to the origin and has an rms size of unity in all dimensions
    beamdist[:ndist0] = (beamdist[:ndist0]-avg)*std_inv
    
    # Step 4 - add particles
    for i in np.arange(ndist0, mpart):
        n1 = np.int(np.random.rand()*ndist0)
        rmin = 1e9
        for j in np.arange(ndist0):
            distance = np.sqrt(np.sum((beamdist[n1]-beamdist[j])**2))
            if distance<rmin and j != n1:
                n2 = j
                rmin = distance
        print(n1, n2)      
        beamdist[i] = 0.5*(beamdist[n1]+beamdist[n2])+(2*np.random.rand()-1)*(beamdist[n1]-beamdist[n2])
        
    # Step 5 - scale back
    beamdist = beamdist/std_inv+avg
    
    return beamdist

def prepare_slices(fname, mpart = 8192, **kwargs):
    
    # fname = 'test_100A_rd.h5'
    
    fout = h5py.File(fname, 'w')
    
    keys = ['beamletsize', 'one4one', 'refposition', 'slicecount', 'slicelength', 'slicespacing']
    
    for key in keys:
        if key in kwargs.keys():
            a = kwargs.get(key)
            dtmp = fout.create_dataset(key, data = a[:])
            
    slicecount = fout.get('slicecount')[0]
    slicelength = fout.get('slicelength')[0]
    

    for i in np.arange(slicecount):
        name = 'slice%6.6d' % (i+1)
    
        x, y, theta, px, py, gamma = beamdist.T[:]
        
        dx = fout.create_dataset(name+'/x', data = x[:])
        dy = fout.create_dataset(name+'/y', data = y[:])
        
        dpx = fout.create_dataset(name+'/px', data = px[:])
        dpy = fout.create_dataset(name+'/py', data = py[:])
        
        dgamma = fout.create_dataset(name+'/gamma', data = gamma[:]) 
        
        theta = theta
        dt = fout.create_dataset(name+'/theta', data = theta[:])
        
        current = 1
        dc = fout.create_dataset(name+'/current', data = current)
        
    fout.close()
    return

import h5py
import numpy.random as rd

f = h5py.File('test_1.0.0.par.h5', 'r')
fout = h5py.File('test_100A_rd.h5', 'w')

keys = ['beamletsize', 'one4one', 'refposition', 'slicecount', 'slicelength', 'slicespacing']
for key in keys:
    a = f.get(key)
    dtmp = fout.create_dataset(key, data = a[:])
    
for key in keys:
    print(f.get(key))
    print(fout.get(key))
    
nslice = f['slicecount'][0]
slicewidth = f['slicelength'][0]
nmacro = len(f['slice000001/x'][:])

current = []
for i in np.arange(nslice):
    name = 'slice%6.6d' % (i+1)
    
    x = f[name+'/x']
    dx = fout.create_dataset(name+'/x', data = x[:])
    
    y = f[name+'/y']
    dy = fout.create_dataset(name+'/y', data = y[:])
    
    px = f[name+'/px']
    dpx = fout.create_dataset(name+'/px', data = px[:])
    
    py = f[name+'/py']
    dpy = fout.create_dataset(name+'/py', data = py[:])
    
    gamma = f[name+'/gamma']
    dgamma = fout.create_dataset(name+'/gamma', data = gamma[:])
    
    if i>500:
        t = f[name+'/theta']
    else:
        #t = np.linspace(0.5, 1.5, 8192)*np.pi
        t = (rd.rand(8192)+0.5)*np.pi
    dt = fout.create_dataset(name+'/theta', data = t[:])

    c = f[name+'/current']
    if i>500:
        dc = fout.create_dataset(name+'/current', data = c[:])
    else:
        dc = fout.create_dataset(name+'/current', data = c[:]/2)
    
f.close()
fout.close()