# -*- coding: utf-8 -*-
"""
Created on Mon May 31 22:36:29 2021

@author: lixiangk
"""

from universal import *
from scipy.integrate import quad

def bunching_factor(ft, width, omega = 2*np.pi):
    r = quad(ft, -np.inf, np.inf, args = (width, omega))
    
    return r[0]**2

def ft(t, width, omega = 2*np.pi):
    
    if t>-width/2 and t<width/2:
        r = 1/width;
    else:
        r = 0
    return r*np.exp(1j*omega*t)

def ft_Gaussian(t, width, omega = 2*np.pi):
    return 1/np.sqrt(2*np.pi)/width*np.exp(-t**2/2/width**2)*np.exp(1j*omega*t)

bf2 = bunching_factor(ft, 1)
print(bf2)

bf2 = bunching_factor(ft_Gaussian, 0.25)
print(bf2)

sigma, omega = 0.25, 2*np.pi
print(np.exp(-omega**2*sigma**2))


widths = np.linspace(0.1, 0.75, 100)
bf2 = np.array([bunching_factor(ft, width, omega) for width in widths])

fig, ax = plt.subplots()

ax.plot(widths, bf2, '-')
ax.grid()

#ax.set_yscale('log')

#%%
