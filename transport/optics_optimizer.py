# -*- coding: utf-8 -*-
"""
Created on Thu May  6 15:29:14 2021

@author: lixiangk
"""

from transport.TransferMatrix import *

#%%
l1, f1, l2, f2, l3 = symbols('l1', 'f1', 'l2', 'f2', 'l3')
L1 = Drift(l1)
L2 = Drift(l2)
L3 = Drift(l3)

Q1 = ThinLens(1/f1)
Q2 = ThinLens(1/f2)

