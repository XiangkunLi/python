import numpy as np
import scipy as sp
import matplotlib as mpl
mpl.use('Agg')
mpl.rc('figure', dpi = 300)
import matplotlib.pyplot as plt

from scipy.interpolate import interp1d
from scipy.optimize import curve_fit

import h5py, pickle
import os,sys

if sys.platform == "linux" or sys.platform == "linux2":
    sys.path.append(r'/afs/ifh.de/group/pitz/data/lixiangk/work/sync/python')
elif sys.platform == "win32":
    sys.path.append(r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python')
elif sys.platform == "darwin":
    print("OS X")
else:
    print("Unknown platform!")

from universal import *
from opmd_viewer.openpmd_timeseries import OpenPMDTimeSeries
from opmd_viewer.addons import LpaDiagnostics

from mpl_toolkits.mplot3d import axes3d

if len(sys.argv) > 1:
    nth = int(sys.argv[1])
else:
    nth = 27

if len(sys.argv) > 2:
    ith = int(sys.argv[2])
else:
    ith = 0

if len(sys.argv) > 3:
    workdir = sys.argv[3]
else:
    workdir = '16x2x2_12'

os.chdir(workdir)

write = 'pickle'
#write = 'ascii'
write_dir = 'lab_diags'

write_step = 1

save_dir = os.path.join(write_dir, "data")
if not os.path.exists(save_dir):
    os.mkdir(save_dir)

ts = OpenPMDTimeSeries('%s/hdf5/' % write_dir, check_all_files=False)

def unique_lexsort(data):
    sorted_data = data[np.lexsort(data.T),:]
    row_mask = np.append([True], np.any(np.diff(sorted_data,axis=0),1))
    return sorted_data[row_mask]

def check_keys(iter = 2, kind = 'particles'):

    fname=str.format("%s/hdf5/data%08d.h5" % (write_dir, iter))
    root='data/%d'%iter

    f = h5py.File(fname, "r")
    keys = f[root+'/'+kind].keys()
    return keys
#print('particles: ', check_keys())
#print('fields: ', check_keys(kind='fields'))
#exit()

def save():
    print(ith, nth)
    for i in np.arange(ith,nth,1):
        iter = i*write_step

        if 1:

            data = {}
            
            time = ts.get_time(iteration=iter); print(iter, time, '1')
            
            try:
                fields = check_keys(iter, kind = 'fields')
            
                if 'E' in fields:
                    Ex, info_Ex = ts.get_field( iteration=iter,  field='E', coord='x',
                                               m='all', plot=False )
                    Ey, info_Ey = ts.get_field( iteration=iter,  field='E', coord='y',
                                               m='all', plot=False )
                    Ez, info_Ez = ts.get_field( iteration=iter,  field='E', coord='z',
                                               m='all', plot=False )

                    extent = info_Ex.imshow_extent
                    extent = [iter, time, extent[0], extent[1], extent[2], extent[3]]

                    if write == 'ascii':
                        np.savetxt(save_dir+os.sep+'Ex-'+'%d'%iter+'.dat', Ex[:,:],
                                   fmt='%16.9E')
                        np.savetxt(save_dir+os.sep+'Ey-'+'%d'%iter+'.dat', Ey[:,:],
                                   fmt='%16.9E')
                        np.savetxt(save_dir+os.sep+'Ez-'+'%d'%iter+'.dat', Ez[:,:],
                                   fmt='%16.9E')

                        with open(save_dir+os.sep+'extent.dat','a') as f_handle:
                            np.savetxt(f_handle,np.atleast_2d(extent), fmt='%16.9E')

                    elif write == 'pickle':
                        data.update(Ex = Ex)
                        data.update(Ey = Ey)
                        data.update(Ez = Ez)
                        data.update(extent = extent)
            
                if 'B' in fields:
                    Bx, info_Bx = ts.get_field( iteration=iter,  field='B', coord='x',
                                               m='1', plot=False )
                    By, info_By = ts.get_field( iteration=iter,  field='B', coord='y',
                                               m='1', plot=False )
                    Bz, info_Bz = ts.get_field( iteration=iter,  field='B', coord='z',
                                               m='1', plot=False )

                    if write == 'ascii':
                        np.savetxt(save_dir+os.sep+'Bx-'+'%d'%iter+'.dat', Bx[:,:], 
                                   fmt='%16.9E')
                        np.savetxt(save_dir+os.sep+'By-'+'%d'%iter+'.dat', By[:,:],
                                   fmt='%16.9E')
                        np.savetxt(save_dir+os.sep+'Bz-'+'%d'%iter+'.dat', Bz[:,:],
                                   fmt='%16.9E')
                    elif write == 'pickle':
                        data.update(Bx = Bx)
                        data.update(By = By)
                        data.update(Bz = Bz)

                if 'J' in fields:
                    Jx, info_Jx = ts.get_field( iteration=iter,  field='J', coord='x',
                                               m='all', plot=False )
                    Jy, info_Jy = ts.get_field( iteration=iter,  field='J', coord='y',
                                               m='all', plot=False )
                    Jz, info_Jz = ts.get_field( iteration=iter,  field='J', coord='z',
                                               m='all', plot=False )

                    if write == 'ascii':
                        np.savetxt(save_dir+os.sep+'Jx-'+'%d'%iter+'.dat', Jx[:,:], 
                                   fmt='%16.9E')
                        np.savetxt(save_dir+os.sep+'Jy-'+'%d'%iter+'.dat', Jy[:,:],
                                   fmt='%16.9E')
                        np.savetxt(save_dir+os.sep+'Jz-'+'%d'%iter+'.dat', Jz[:,:],
                                   fmt='%16.9E')
                    elif write == 'pickle':
                        data.update(Jx = Jx)
                        data.update(Jy = Jy)
                        data.update(Jz = Jz)

                if 'rho' in fields:
                    rho, info_rho = ts.get_field( iteration=iter,  field='rho', slicing_dir='y', plot=False )

                    if write == 'ascii':
                        np.savetxt(save_dir+os.sep+'rho-'+'%d'%iter+'.dat', rho[:,:], fmt='%16.9E')
                    elif write == 'pickle':
                        data.update(rho = rho)
            except Exception as err:
                print(Exception)
                pass
            
            particles = check_keys(iter); print(particles)
            if 'electrons' in particles and iter == 1:
                x = ts.get_particle(var_list=['x', 'y', 'z', 'ux', 'uy', 'uz', 'w', 'id'], 
                                    iteration=iter, species='electrons', plot=False)
                plasma = np.array([x[0]/1.0e6, x[1]/1.0e6, x[2]/1.0e6, x[3], x[4], x[5], x[6]]).T
                # a = unique_lexsort(a)

                if write == 'ascii':
                    np.savetxt(save_dir+os.sep+'plasma-'+'%d'%iter+'.dat', plasma[:,:], fmt='%16.9E')
                elif write == 'pickle':
                    data.update(plasma = plasma)
                
            if 'beam' in particles:
                x = ts.get_particle(var_list=['x', 'y', 'z', 'ux', 'uy', 'uz', 'w', 'id'], \
                                    iteration=iter, species='beam', plot=False)
                beam = np.array([x[0]/1.0e6, x[1]/1.0e6, x[2]/1.0e6, x[3], x[4], x[5], x[6], x[7]]).T
                #beam = unique_lexsort(beam)
                
                print(beam.shape)
                if write == 'ascii':
                    np.savetxt(save_dir+os.sep+'beam-'+'%d'%iter+'.dat', beam[:,:],
                               fmt = '%25.16E')
                elif write == 'pickle':
                    data.update(beam = beam)

            if write == 'pickle':
                fullname = save_dir+os.sep+'data-%d'%iter+'.pkl'
                with open(fullname, 'wb') as f:
                    pickle.dump(data, f)

        #except:
        #    print('Exception!')
        #    continue
    return

save()

