
# coding: utf-8

# In[1]:


#get_ipython().magic(u'matplotlib inline')
#%matplotlib notebook
from universal import *

from scipy.optimize import minimize
from scipy.misc import derivative
from scipy.interpolate import interp2d
from scipy import signal
import cPickle as pickle


# In[2]:


def plot_config():
    from cycler import cycler
    from matplotlib.ticker import AutoMinorLocator

    fsize = 14 # a quarter of the paper width: 20 pt; half of the paper width: 12
    font = {'size' : fsize, 'family' : 'serif'}
    color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
    linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
    marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']
    
    mpl.rc('font', **font)
    mpl.rc('xtick', labelsize = 10, direction = 'in', top   = True)
    mpl.rc('ytick', labelsize = 10, direction = 'in', right = True)
    mpl.rc('xtick.major', size = 5, width = 1)
    mpl.rc('ytick.major', size = 5, width = 1)
    mpl.rc('xtick.minor', size = 3, width = 0.7, visible = True)
    mpl.rc('ytick.minor', size = 3, width = 0.7, visible = True)
    
    mpl.rc('lines', linewidth=2, markersize=6, color='r')
    # mpl.rc('lines', linestyle = 'solid')
    mpl.rc('axes', labelpad = 0, prop_cycle=(cycler('color', color_cycle) + cycler('linestyle', linestyle_cycle) + cycler('marker', marker_cycle)))
    mpl.rc('legend', fontsize = 12, labelspacing = 0.05, handletextpad=0.4, frameon=False, handlelength=2.1)
    
    mpl.rc('figure', dpi = 100, figsize = (4, 4))
    mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)
    
    mpl.rc('image', cmap = 'jet')
    
    return
plot_config()

rootdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\work'
simdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim'


# In[3]:


# Static potential and electric field of a uniformly charged ball
def static_potential(r, R, Q = 100e-12):
    cc = Q/4./np.pi/g_eps0
    if r<=R:
        phi = cc*((R**2-r**2)/2./R**3+1./R)
    else:
        phi = cc/r
    return phi
def electric_field(r, R, Q = 100e-12):
    cc = Q/4./np.pi/g_eps0
    if r<=R:
        Er = cc*r/R**3
    else:
        Er = cc/r**2
    return Er

'''
R = 1e-3
r = np.linspace(0, 15*R, 501)
phi = np.array([static_potential(x, R) for x in r])
Er = np.array([electric_field(x, R) for x in r])

fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
ax1.plot(r/R, phi, '-')
ax1.grid()
ax2.plot(r/R, Er, '-')
ax2.grid()

np.savetxt('R1mm.dat', np.array([r, phi, Er]).T, fmt = '%12.6E')
'''

# ## Class-level scripts
def GF3D((x, y, z)):
    '''
    Green function (GF) in 3D cartesian CS
    '''
    r = np.sqrt(x*x+y*y+z*z)
    return np.where(r>0, 1./np.sqrt(x*x+y*y+z*z), 1e100)
def IGF3D((x, y, z)):
    '''
    Indefinite integral of or integrated green function in 3D cartesian CS, according to Eq.(2) on 129901-2. 
    Here, `indefinite` refers to the integrating limits
    ''' 
    r = np.sqrt(x*x+y*y+z*z)
    
    #EPS = 1e-12
    #if abs(r) < EPS:
    #    return 0
    #if abs(x) < EPS:
    #    return y*z*np.log(r)
    #if abs(y) < EPS:
    #    return z*x*np.log(r)
    #if abs(z) < EPS:
    #    return x*y*np.log(r)
    
    x2 = x*x; y2 = y*y; z2 = z*z
    xy = x*y; yz = y*z; zx = z*x
    s = yz*np.log(x+r)+zx*np.log(y+r)+xy*np.log(z+r)
    a = -0.5*x2*np.arctan(yz/x/r)
    b = -0.5*y2*np.arctan(zx/y/r)
    c = -0.5*z2*np.arctan(xy/z/r)
    return s+a+b+c
def shifted_IGF3D(x, y, z, xc = 0, yc = 0, zc = 0):
    return IGF3D(x+xc, y+yc, z+zc)
def cyclic_IGF3D_mapping((i, j, k), IGF_MAP):
    # n = np.array([-1, 1, 1, -1, 1, -1, -1, 1])
    # v = np.array([IGF_MAP[i, j, k], IGF_MAP[i, j, k+1],\
    #             IGF_MAP[i, j+1, k], IGF_MAP[i, j+1, k+1],\
    #             IGF_MAP[i+1, j, k], IGF_MAP[i+1, j, k+1],\
    #             IGF_MAP[i+1, j+1, k], IGF_MAP[i+1, j+1, k+1]])
    # print i, j, k
    rr = -IGF_MAP[i, j, k] +IGF_MAP[i, j, k+1]  +IGF_MAP[i, j+1, k]  -IGF_MAP[i, j+1, k+1]+         IGF_MAP[i+1, j, k]-IGF_MAP[i+1, j, k+1]-IGF_MAP[i+1, j+1, k]+IGF_MAP[i+1, j+1, k+1]
    return rr

from scipy.interpolate import RegularGridInterpolator, Rbf
from numpy.fft import fftn, ifftn
import inspect

"""
class SpaceCharge3DFFT():
    def __init__(self, nx = 32, ny = 32, nz = 32, N_cutoff = 5, debug = 0,\
                 Lspch = True, Lmirror = False, Lbin = False, Lemit = False, Nbin = 1):
        
        self.nx = nx; self.ny = ny; self.nz = nz
        self.N_cutoff = N_cutoff       
        self.debug = debug
        
        self.Lspch = Lspch; self.Lmirror = Lmirror; self.Lemit = False
        self.Lbin = Lbin; self.Nbin = Nbin
        
        return
    def get_rho_3D0(self, sigma_x = 1e-3, sigma_y = 1e-3, sigma_z = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D gaussian distributions
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_z_static = sigma_z*gamma0
         
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff
        
        self.gamma0 = gamma0
        
        def rho_3D((x_static, y_static, z_static)):
            xc, yc, zc = 0, 0, 0
            f_x = 1./np.sqrt(2.*np.pi)/sigma_x*np.exp(-(x_static-xc)**2/2./sigma_x**2)
            f_y = 1./np.sqrt(2.*np.pi)/sigma_y*np.exp(-(y_static-yc)**2/2./sigma_y**2)
            f_z = 1./np.sqrt(2.*np.pi)/sigma_z*np.exp(-(z_static-zc)**2/2./sigma_z_static**2)
            
            return f_x*f_y*f_z
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zmean = 0; self.zref = 0
        self.rho = rho
        return
    def get_rho_3D1(self, R = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D uniform distributions confined in a sphere
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_x = R; sigma_y = R; sigma_z = R
        sigma_z_static = sigma_z*gamma0
        
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff  
        
        def rho_3D((x_static, y_static, z_static)):
            radius = R #*np.sqrt(15)/5.0;
            xc, yc, zc = 0, 0, 0
            r = np.sqrt((x_static-xc)**2+(y_static-yc)**2+(z_static/gamma0-zc)**2)
            rho = np.where(r>radius, 0, 3./(4*np.pi*radius**3))
            return rho
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zgrid = zgrid_static/gamma0
        
        self.zmean = 0; self.zref = 1.5*R
        self.rho = rho*Qtot
        return
    
    def get_rho_3D(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                   Lmirror = None, Lemit = None, format = 'Astra'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
            
            #self.zref = 0 #dist[0,iz]
            #dist[0,iz] = 0
            
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9; self.Qtot = np.sum(w)
            
            self.zmean =  weighted_mean(z, w)
            self.zmean = (np.max(z)+np.min(z))*0.5
            z -= self.zmean
                        
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            gamma0 = weighted_mean(gamma, w)
            z_static = z*gamma0

        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z_static = weighted_std(z_static, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        self.sigma_z = self.sigma_z_static/gamma0;       #print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        zmin_static = np.max([np.min(z_static), -N_cutoff*self.sigma_z_static])
        zmax_static = np.min([np.max(z_static),  N_cutoff*self.sigma_z_static])
        zmin, zmax = zmin_static/gamma0, zmax_static/gamma0
        
        zmin, zmax = np.min(z), np.max(z)
        nx0 = 1 # Guarding grids on one side
        ny0 = 1
        nz0 = 1
        if self.Lemit:
            zmin = -self.zmean
            nz0 = 2
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            print inspect.stack()[0][3], 'gamma = ', self.gamma0
            print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
            print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
            print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
            print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
            print inspect.stack()[0][3], 'mesh sizes in rest frame: ', hx, hy, hz_static
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1)
        
        zgrid_static = zgrid*gamma0
        
        ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                  (zgrid_static[0]-0.5*hz_static, zgrid_static[-1]+0.5*hz_static)]
        rho, edges = np.histogramdd((x, y, z_static), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = w)
        
        if self.debug:
            #print xgrid/self.sigma_x, ygrid/self.sigma_y, zgrid/self.sigma_z_static
            #print rho
            #print ranges
            fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (8, 4))
            data = np.abs(rho[:,Ny/2-1, Nz/2-1])
            ax1.plot(xgrid*1e3, data/np.max(data), '-*')
            ax1.grid()
            ax1.set_xlabel(r'$x$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            [aX,aY] = np.meshgrid(xgrid, ygrid)
            data = np.abs(rho[:,:,Nz/2-1])
            v = np.linspace(0.05, 1., 96)
            ax2.contourf(aX*1e3, aY*1e3, data/np.max(data), v, linestyles = None)
            ax2.grid()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$y$ (mm)')
            
            fig.savefig('charge-density.eps')
        
        #rho = sgolay2d(rho, window_size=11, order=4)   
        #self.rho_RZ = interp2d(rr, zz, rho.T, kind = 'cubic')
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        self.zgrid_static = zgrid_static
        self.rho = rho/(hx*hy*hz_static)
        return  
    def build(self, Xshift = (0, 0, 0)):
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z_static = self.sigma_z_static
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        #if N_cutoff is None:
        #    N_cutoff = self.N_cutoff
        N_cutoff = self.N_cutoff
        
        if self.Lspch:
            # Create arrays for GFs and densities
            GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))

            xgrid = self.xgrid
            ygrid = self.ygrid
            zgrid_static = self.zgrid_static
            RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho

            hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz_static = zgrid_static[1]-zgrid_static[0]
            if self.debug:
                print inspect.stack()[0][3], Nx, Ny, Nz
                print inspect.stack()[0][3], hx, hy, hz_static

            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xf, yf, zf = Xshift
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_MAP = IGF3D(XX)

            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC = cyclic_IGF3D_mapping(II, IGF_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_FFT = fftn(GFC)
            RHOC_FFT = fftn(RHOC)
            PHIC_FFT = GFC_FFT*RHOC_FFT

            PHIC = ifftn(PHIC_FFT)
            self.PHI = PHIC[:Nx+1,:Ny+1,:Nz+1]
            
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf = (0+self.zmean)*2; #print zf
            
            GFC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            
            RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[:,:,::-1]
            #RHOC_mirror = RHOC[:,:,::-1]
            #zgrid = -self.zgrid[::-1]
            
            if self.debug:
                print inspect.stack()[0][3], Nx, Ny, Nz
                print inspect.stack()[0][3], hx, hy, hz_static
            
            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_mirror_MAP = IGF3D(XX)
            
            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC_mirror = cyclic_IGF3D_mapping(II, IGF_mirror_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_mirror_FFT = fftn(GFC_mirror)
            RHOC_mirror_FFT = fftn(RHOC_mirror)
            PHIC_mirror_FFT = GFC_mirror_FFT*RHOC_mirror_FFT

            PHIC_mirror = ifftn(PHIC_mirror_FFT)
            self.PHI_mirror = PHIC_mirror[:Nx+1,:Ny+1,:Nz+1]
        
        if self.debug and self.Lmirror:
            
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI, hx, hy, hz_static) 
            Ex_static_m, Ey_static_m, Ez_static_m = np.gradient(self.PHI_mirror, hx, hy, hz_static) 
            cc = -1./4/np.pi/g_eps0
        
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.plot(xgrid/sigma_x, -Ex_static_m[:, Ny/2-1, Nz/4-1]*cc/1e3, '-o')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.plot(zgrid_static/sigma_z_static, -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '-o')
            #print Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3
            #print -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx/2-1, Ny/2-1, :], '-*')
            ax1.plot(GFC_mirror[Nx/2-1, Ny/2-1, :], '-o')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[Nx/2-1, Ny/2-1, :], '-*')
            ax2.plot(RHOC_mirror[Nx/2-1, Ny/2-1, :], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :], '-*')
            ax3.plot(self.PHI_mirror[Nx/2-1, Ny/2-1, :], '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        if self.Lspch and self.Lmirror:
            self.PHI -= self.PHI_mirror
        
        Ex_static, Ey_static, Ez_static = np.gradient(self.PHI, hx, hy, hz_static)     
        cc = -1./4/np.pi/g_eps0
        
        if self.debug:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx/2-1, Ny/2-1, :], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[Nx/2-1, Ny/2-1, :], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :]*cc*(-1), '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        fEx3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static), Ex_static, bounds_error = False, fill_value = None)
        fEy3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static), Ey_static, bounds_error = False, fill_value = None)
        fEz3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static), Ez_static, bounds_error = False, fill_value = None)
        
        gamma0 = self.gamma0; beta = gamma2beta(gamma0); zmean = self.zmean
        def EM3D(x, y, z, t = 0):
            #if np.isscalar(x):
            #    x = [x]
            #if np.isscalar(y):
            #    y = [y]
            #if np.isscalar(z):
            #    z = [z]
            x = np.atleast_1d(x)
            y = np.atleast_1d(y)
            z = np.atleast_1d(z)
            n = np.max(len(x), len(y), len(z))
            #if len(x)<n:
            #    x = [x[0] for i in np.arange(n)]
            #if len(y)<n:
            #    y = [y[0] for i in np.arange(n)]
            #if len(z)<n:
            #    z = [z[0] for i in np.arange(n)]
            
            #x = np.asarray(x)
            #y = np.asarray(y)
            #z = np.asarray(z)
            z_static = (z-zmean)*gamma0
            if not np.isscalar(t):
                t = np.asarray(t)
            
            Ex_static = fEx3D_static((x, y, z_static))*cc
            Ey_static = fEy3D_static((x, y, z_static))*cc
            Ez_static = fEz3D_static((x, y, z_static))*cc
            
            # return normalied E and B in lab frame
            F2d = np.zeros((n, 6))
            F2d[:,0] = gamma0*Ex_static
            F2d[:,1] = gamma0*Ey_static
            F2d[:,2] = Ez_static
            F2d[:,3] = beta*gamma0*Ey_static/g_c
            F2d[:,4] =-beta*gamma0*Ex_static/g_c
            
            return F2d
        
        if self.debug and 0:
            X, Y, Z = np.meshgrid(xx, yy, zz, indexing = 'ij')

            fEx3D = Rbf(X, Y, Z, Ex, function = 'linear') # or X.ravel()
            fEy3D = Rbf(X, Y, Z, Ey, function = 'linear')
            fEz3D = Rbf(X, Y, Z, Ez, function = 'linear')

            gamma = self.gamma; beta = gamma2beta(gamma)
            def EM3D(x, y, z, t = 0):
                z = z*gamma
                Ex = fEx3D(x, y, z)*cc
                Ey = fEy3D(x, y, z)*cc
                Ez = fEz3D(x, y, z)*cc
                # return normalied E and B
                return [gamma*Ex, gamma*Ey, Ez, beta*gamma*Ey, -beta*gamma*Ex, 0]
        
        self.EM3D = EM3D
        return
    
    def get_rho_3D_binned(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                          nbins = 4, format = 'Astra', Lemit = True):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
            
            self.zref = dist[0,iz]
            
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9
            
            self.zmean =  weighted_mean(z, w); print self.zmean
            self.zmean = (np.max(z)+np.min(z))*0.5; print self.zmean
            z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            gamma0 = weighted_mean(gamma, w); std_gamma0 = weighted_std(gamma, w)
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        zmin = np.max([np.min(z), -N_cutoff*self.sigma_z])
        zmax = np.min([np.max(z),  N_cutoff*self.sigma_z])
        
        zmin, zmax = np.min(z), np.max(z)
        
        if Lemit:
            zmin = -self.zmean
        
        nx0 = 1# Guarding grids on one side
        ny0 = 1
        nz0 = 1
        
        if Lemit:
            nz0 = 2
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*nx0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        # Select sub-group of particles
        bg = np.sqrt(gamma**2-1)
        bg0 = weighted_mean(bg, w); std_bg0 = weighted_std(bg, w)
        #print bg0, std_bg0
        
        s1 = (bg<bg0-std_bg0)
        s2 = (bg>=bg0-std_bg0)*(bg<bg0)
        s3 = (bg>=bg0)*(bg<bg0+std_bg0)
        s4 = (bg>=bg0+std_bg0)
        ss = [s1, s2, s3, s4]
        
        Qtot = []
        gamma0 = []
        rho = []
        zgrid_static = []
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1)
        
        
        fig, ax = plt.subplots()
        ax.grid()
        for si in ss:
            #print [np.sum(i) for i in ss]
            xi = x[si]
            yi = y[si]
            zi = z[si]
            wi = w[si]
            gi = gamma[si]
            
            ax.plot(zi*1e3, bg[si], '.')
            #import pdb; pdb.set_trace()
            g0i = weighted_mean(gi, wi)
            q0i = np.sum(wi)
            
            hz_static = hz*g0i   
            zmin_static, zmax_static = zmin*g0i, zmax*g0i      
            zgridi_static = np.linspace(zmin_static-nz0*hz_static, zmax_static+nz0*hz_static, Nz+1)
            
            ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx),                      (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),                      (zgridi_static[0]-0.5*hz_static, zgridi_static[-1]+0.5*hz_static)]
            #print ranges
            rhoi, edges = np.histogramdd((xi, yi, zi*g0i), bins = (Nx+1, Ny+1, Nz+1),                                        range = ranges, weights = wi)
            
            gamma0.append(g0i)
            Qtot.append(q0i)
            zgrid_static.append(zgridi_static)
            rho.append(rhoi/(hx*hy*hz_static))
            
            del zgridi_static
            del rhoi
        
        self.dx = hx; self.dy = hy; self.dz = hz 
        #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        
        if self.debug:
            fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (12, 4))
            add = np.zeros(len(zgrid))
            for i in np.arange(4):
                #data = np.abs(rho[i][Nx/2-1, Ny/2-1, :]*gamma0[i])
                rhoi_z = np.abs(np.sum(np.sum(rho[i], axis = 0), axis = 0)*gamma0[i])
                add += rhoi_z
                ax1.plot(zgrid*1e3, rhoi_z, label = '%d' % i)
            ax1.plot(zgrid*1e3, add, '--', label = 'all')
            ax1.grid()
            ax1.legend()
            ax1.set_xlabel(r'$z$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            add = np.zeros(len(xgrid))
            for i in np.arange(4):
                rhoi_x = np.abs(np.sum(np.sum(rho[i], axis = 1), axis = 1)*gamma0[i])
                add += rhoi_x
                ax2.plot(xgrid*1e3, rhoi_x, label = '%d' % i)
            ax2.plot(xgrid*1e3, add, '--', label = 'add')
            ax2.grid()
            ax2.legend()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density.eps')
        
        # Save 
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        
        self.rho = rho
        
        return  
    def build_binned(self, Xshift = (0, 0, 0), N_cutoff = None, nbins = 4):
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z = self.sigma_z
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        if N_cutoff is None:
            N_cutoff = self.N_cutoff
        
        xgrid = self.xgrid
        ygrid = self.ygrid
        zgrid = self.zgrid
        hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz = zgrid[1]-zgrid[0]
        
        if self.Lspch:
            # Create arrays for GFs and densities
            nbins = len(self.Qtot)
            self.PHI = []
            for i in np.arange(nbins):
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i]
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                xf, yf, zf = Xshift
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*self.gamma0[i]-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC = ifftn(PHIC_FFT)
                
                self.PHI.append(PHIC[:Nx+1,:Ny+1,:Nz+1])
        
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf = (0+self.zmean)*2; #print zf
            
            nbins = len(self.Qtot)
            self.PHI_mirror = []
            for i in np.arange(nbins):
                
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i][:,:,::-1]
                #RHOC_mirror = RHOC[:,:,::-1]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                g0i = self.gamma0[i]
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*g0i-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC_mirror)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC_mirror = ifftn(PHIC_FFT)
                self.PHI_mirror.append(PHIC_mirror[:Nx+1,:Ny+1,:Nz+1])
                
                self.PHI[i] -= self.PHI_mirror[i]
        
        # Field interpolation
        cc = -1./4/np.pi/g_eps0
        fEx3D_static = []
        fEy3D_static = []
        fEz3D_static = []
        for i in np.arange(nbins):
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI[i], hx, hy, hz*self.gamma0[i])     
        
            fEx3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ex_static,                                                    bounds_error = False, fill_value = None)
            fEy3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ey_static,                                                    bounds_error = False, fill_value = None)
            fEz3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ez_static,                                                    bounds_error = False, fill_value = None)
            
            fEx3D_static.append(fEx3Di_static)
            fEy3D_static.append(fEy3Di_static)
            fEz3D_static.append(fEz3Di_static)
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[:, Ny/2-1, Nz/2-1], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[:, Ny/2-1, Nz/2-1], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[:, Ny/2-1, Nz/2-1], '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        gamma0 = self.gamma0; zmean = self.zmean
        def EM3D(x, y, z, t = 0):
            if np.isscalar(x):
                x = [x]
                y = [y]
                z = [z]
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            if not np.isscalar(t):
                t = np.asarray(t)
            
            F2d = np.zeros((len(x), 6))
            for i in np.arange(nbins):
                g0i = gamma0[i]; b0i = gamma2beta(g0i)
                z_static = (z-zmean)*g0i
                Ex_static = fEx3D_static[i]((x, y, z_static))*cc
                Ey_static = fEy3D_static[i]((x, y, z_static))*cc
                Ez_static = fEz3D_static[i]((x, y, z_static))*cc
                
                # Normalied E and B in lab frame
                
                F2d[:,0] += g0i*Ex_static
                F2d[:,1] += g0i*Ey_static
                F2d[:,2] += Ez_static
                F2d[:,3] += b0i*g0i*Ey_static/g_c
                F2d[:,4] -= b0i*g0i*Ex_static/g_c           
                #F2D = np.add(F2D, [g0i*Ex_static, g0i*Ey_static, Ez_static, b0i*g0i*Ey_static, -b0i*g0i*Ex_static, 0])
            
            return F2D
        
        if self.debug and 0:
            X, Y, Z = np.meshgrid(xx, yy, zz, indexing = 'ij')

            fEx3D = Rbf(X, Y, Z, Ex, function = 'linear') # or X.ravel()
            fEy3D = Rbf(X, Y, Z, Ey, function = 'linear')
            fEz3D = Rbf(X, Y, Z, Ez, function = 'linear')

            gamma = self.gamma; beta = gamma2beta(gamma)
            def EM3D(x, y, z, t = 0):
                z = z*gamma
                Ex = fEx3D(x, y, z)*cc
                Ey = fEy3D(x, y, z)*cc
                Ez = fEz3D(x, y, z)*cc
                # return normalied E and B
                return [gamma*Ex, gamma*Ey, Ez, beta*gamma*Ey, -beta*gamma*Ex, 0]

        self.EM3D = EM3D
        return
"""

"""
# Backup on July 31 2019
class SpaceCharge3DFFT():
    def __init__(self, nx = 32, ny = 32, nz = 32, N_cutoff = 5, debug = 0,\
                 Lspch = True, Lmirror = False, Lbin = False, Lemit = False, Nbin = 1):
        
        self.nx = nx; self.ny = ny; self.nz = nz
        self.N_cutoff = N_cutoff       
        self.debug = debug
        
        self.Lspch = Lspch; self.Lmirror = Lmirror; self.Lemit = False
        self.Lbin = Lbin; self.Nbin = Nbin
        
        return
    def __del__(self):
        print 'SpaceCharge3DFFT instance is dead!'
    
    def get_rho_3D0(self, sigma_x = 1e-3, sigma_y = 1e-3, sigma_z = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D gaussian distributions
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_z_static = sigma_z*gamma0
         
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff
        
        self.gamma0 = gamma0
        
        def rho_3D((x_static, y_static, z_static)):
            xc, yc, zc = 0, 0, 0
            f_x = 1./np.sqrt(2.*np.pi)/sigma_x*np.exp(-(x_static-xc)**2/2./sigma_x**2)
            f_y = 1./np.sqrt(2.*np.pi)/sigma_y*np.exp(-(y_static-yc)**2/2./sigma_y**2)
            f_z = 1./np.sqrt(2.*np.pi)/sigma_z*np.exp(-(z_static-zc)**2/2./sigma_z_static**2)
            
            return f_x*f_y*f_z
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zmean = 0; self.zref = 0
        self.rho = rho
        return
    def get_rho_3D1(self, R = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D uniform distributions confined in a sphere
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_x = R; sigma_y = R; sigma_z = R
        sigma_z_static = sigma_z*gamma0
        
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff  
        
        def rho_3D((x_static, y_static, z_static)):
            radius = R #*np.sqrt(15)/5.0;
            xc, yc, zc = 0, 0, 0
            r = np.sqrt((x_static-xc)**2+(y_static-yc)**2+(z_static/gamma0-zc)**2)
            rho = np.where(r>radius, 0, 3./(4*np.pi*radius**3))
            return rho
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zgrid = zgrid_static/gamma0
        
        self.zmean = 0; self.zref = 1.5*R
        self.rho = rho*Qtot
        return
    
    def get_rho_3D(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                   Lmirror = None, Lemit = None, format = 'Astra', fig_ext = '.eps'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
                       
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9; self.Qtot = np.sum(w)
            
            self.zmean =  weighted_mean(z, w)
            #self.zmean = (np.max(z)+np.min(z))*0.5
            #z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            
            gamma0 = weighted_mean(gamma, w)
            #z_static = z*gamma0
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        self.sigma_z_static = self.sigma_z*gamma0;       #print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        N_cutoff_z = 5
        zmin = np.max([np.min(z), self.zmean-N_cutoff_z*self.sigma_z])
        zmax = np.min([np.max(z), self.zmean+N_cutoff_z*self.sigma_z])
        
        #zmin = np.min(z)
        #zmax = np.max(z)
        nx0 = 1 # Guarding grids on one side
        ny0 = 1
        nz0 = 1
        if self.Lemit:
            zmin = 0
            zmax = np.max(z)
            #zmin = -self.zmean
            nz0 = 2.5
        #print inspect.stack()[0][3], zmin, zmax, self.zmean
        
        #import pdb; pdb.set_trace()
        self.zmean = (zmin+zmax)*0.5
        z -= self.zmean
        z_static = z*gamma0
        
        zmax -= self.zmean
        zmin -= self.zmean
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            print inspect.stack()[0][3], 'gamma = ', self.gamma0
            print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
            print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
            print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
            print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
            print inspect.stack()[0][3], 'mesh sizes in rest frame: ', hx, hy, hz_static
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1)
        #import pdb; pdb.set_trace()
        zgrid_static = zgrid*gamma0
        
        ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                  (zgrid_static[0]-0.5*hz_static, zgrid_static[-1]+0.5*hz_static)]
        rho, edges = np.histogramdd((x, y, z_static), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = w)
        del edges
        
        if self.debug:
            #print xgrid/self.sigma_x, ygrid/self.sigma_y, zgrid/self.sigma_z_static
            #print rho
            #print ranges
            fig, [ax1, ax2, ax3] = plt.subplots(ncols = 3, figsize = (12, 4))
            rho_x = np.sum(np.sum(rho, axis = 1), axis = 1)
            ax1.plot(xgrid*1e3, rho_x, '-*')
            ax1.grid()
            ax1.set_xlabel(r'$x$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            [aX,aY] = np.meshgrid(xgrid, ygrid)
            data = np.abs(rho[:,:,Nz/2-1])
            v = np.linspace(0.05, 1., 96)
            ax2.contourf(aX*1e3, aY*1e3, data/np.max(data), v, linestyles = None)
            ax2.grid()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$y$ (mm)')
            
            rho_z = np.sum(np.sum(rho, axis = 0), axis = 0)
            ax3.plot(zgrid*1e3, rho_z, '-*')
            ax3.grid()
            ax3.set_xlabel(r'$z$ (mm)')
            ax3.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density'+fig_ext)
        
        #rho = sgolay2d(rho, window_size=11, order=4)   
        #self.rho_RZ = interp2d(rr, zz, rho.T, kind = 'cubic')
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        self.zgrid_static = zgrid_static
        self.rho = rho/(hx*hy*hz_static)
        return
    def build(self, Xshift = (0, 0, 0), **kwargs):
        if 'fig_ext' in kwargs.keys():
            fig_ext = kwargs['fig_ext'];
        else:
            fig_ext = '.eps'
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z_static = self.sigma_z_static
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        #if N_cutoff is None:
        #    N_cutoff = self.N_cutoff
        #N_cutoff = self.N_cutoff
        
        if self.Lspch:
            # Create arrays for GFs and densities
            GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
            
            xgrid = self.xgrid
            ygrid = self.ygrid
            zgrid_static = self.zgrid_static
            RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho
            
            hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz_static = zgrid_static[1]-zgrid_static[0]
            if self.debug:
                print inspect.stack()[0][3], Nx, Ny, Nz
                print inspect.stack()[0][3], hx, hy, hz_static

            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xf, yf, zf = Xshift
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_MAP = IGF3D(XX)

            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC = cyclic_IGF3D_mapping(II, IGF_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_FFT = fftn(GFC)
            RHOC_FFT = fftn(RHOC)
            PHIC_FFT = GFC_FFT*RHOC_FFT

            PHIC = ifftn(PHIC_FFT)
            self.PHI = PHIC[:Nx+1,:Ny+1,:Nz+1]
            
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf = (0+self.zmean)*2; #print zf
            
            GFC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            
            RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[:,:,::-1]
            
            if self.debug:
                print inspect.stack()[0][3], Nx, Ny, Nz
                print inspect.stack()[0][3], hx, hy, hz_static
            
            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_mirror_MAP = IGF3D(XX)
            
            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC_mirror = cyclic_IGF3D_mapping(II, IGF_mirror_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_mirror_FFT = fftn(GFC_mirror)
            RHOC_mirror_FFT = fftn(RHOC_mirror)
            PHIC_mirror_FFT = GFC_mirror_FFT*RHOC_mirror_FFT

            PHIC_mirror = ifftn(PHIC_mirror_FFT)
            self.PHI_mirror = PHIC_mirror[:Nx+1,:Ny+1,:Nz+1]
        
        if self.Lemit and self.Lmirror:
            
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI.real, hx, hy, hz_static) 
            Ex_static_m, Ey_static_m, Ez_static_m = np.gradient(self.PHI_mirror.real, hx, hy, hz_static) 
            cc = -1./4/np.pi/g_eps0
        
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.plot(xgrid/sigma_x, -Ex_static_m[:, Ny/2-1, Nz/4-1]*cc/1e3, '-o')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            
            ax2.plot((self.zgrid+self.zmean)*1e3, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.plot((self.zgrid+self.zmean)*1e3, -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '-o')
            ax2.plot((self.zgrid+self.zmean)*1e3, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3-Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '--')
            #print Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3
            #print -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3
            ax2.set_xlabel(r'$z$ (mm)')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.set_xlim(-0.02, 0.03)
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis'+fig_ext)
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx/2-1, Ny/2-1, :], '-*')
            ax1.plot(GFC_mirror[Nx/2-1, Ny/2-1, :], '-o')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            
            to_astra_unit = self.dx*self.dy*self.dz_static/self.dz*1e9
            ax2.plot((self.zgrid+self.zmean)*1e3, np.sum(np.sum(RHOC[:,:,:Nz+1], axis = 0), axis = 0)*to_astra_unit, '-*')
            ax2.plot((self.zgrid+self.zmean)*1e3, np.sum(np.sum(RHOC_mirror[:,:,:Nz+1], axis = 0), axis = 0)*to_astra_unit, '-o')
            ax2.set_xlim(-0.02, 0.03)
            ax2.grid()
            ax2.set_xlabel(r'$z$ (mm)')
            ax2.set_title(r'charge density (nC/m)')
            
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :], '-*')
            ax3.plot(self.PHI_mirror[Nx/2-1, Ny/2-1, :], '-o')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function'+fig_ext)
            
            #plt.close('all')
            
        if self.Lspch and self.Lmirror:
            self.PHI -= self.PHI_mirror
        
        del GFC, RHOC, GFC_FFT, RHOC_FFT, PHIC_FFT, IGF_MAP
        if self.Lmirror:
            del GFC_mirror, RHOC_mirror, GFC_mirror_FFT, RHOC_mirror_FFT, PHIC_mirror_FFT, IGF_mirror_MAP
        del XX, II
        
        Ex_static, Ey_static, Ez_static = np.gradient(self.PHI.real, hx, hy, hz_static)     
        cc = -1./4/np.pi/g_eps0
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx/2-1, Ny/2-1, :], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[Nx/2-1, Ny/2-1, :], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :]*cc*(-1), '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        fEx3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static), Ex_static, bounds_error = False, fill_value = None)
        fEy3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static), Ey_static, bounds_error = False, fill_value = None)
        fEz3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static), Ez_static, bounds_error = False, fill_value = None)
        
        cc = 1./4/np.pi/g_eps0 # 
        gamma0 = self.gamma0; beta = gamma2beta(gamma0); zmean = self.zmean
        def EM3D(x, y, z, t = 0):
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            z_static = (z-zmean)*gamma0
            
            n = np.max([len(x), len(y), len(z)])
            
            Ex_static = fEx3D_static((x, y, z_static))*cc
            Ey_static = fEy3D_static((x, y, z_static))*cc
            Ez_static = fEz3D_static((x, y, z_static))*cc
            
            # return normalied E and B in lab frame
            F2d = np.zeros((n, 6))
            F2d[:,0] = gamma0*Ex_static
            F2d[:,1] = gamma0*Ey_static
            F2d[:,2] = Ez_static
            F2d[:,3] = beta*gamma0*Ey_static/g_c
            F2d[:,4] =-beta*gamma0*Ex_static/g_c
            
            return F2d
        
        self.EM3D = EM3D
        return
    
    def get_rho_3D_binned(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                          nbins = 4, Lmirror = None, Lemit = None, format = 'Astra'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
            
            self.zref = dist[0,iz]
            
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9
            
            self.zmean =  weighted_mean(z, w); print self.zmean
            self.zmean = (np.max(z)+np.min(z))*0.5; print self.zmean
            z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            gamma0 = weighted_mean(gamma, w); std_gamma0 = weighted_std(gamma, w)
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        zmin = np.max([np.min(z), -N_cutoff*self.sigma_z])
        zmax = np.min([np.max(z),  N_cutoff*self.sigma_z])
        
        zmin = np.min(z)
        zmax = np.max(z)
        nx0 = 1 # Guarding grids on one side
        ny0 = 1
        nz0 = 1
        if self.Lemit:
            zmin = -self.zmean
            nz0 = 2
        #print inspect.stack()[0][3], zmin, zmax, self.zmean
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        # Select sub-group of particles
        bg = np.sqrt(gamma**2-1)
        bg0 = weighted_mean(bg, w); std_bg0 = weighted_std(bg, w)
        #print bg0, std_bg0
        
        s1 = (bg<bg0-std_bg0)
        s2 = (bg>=bg0-std_bg0)*(bg<bg0)
        s3 = (bg>=bg0)*(bg<bg0+std_bg0)
        s4 = (bg>=bg0+std_bg0)
        ss = [s1, s2, s3, s4]
        
        Qtot = []
        gamma0 = []
        rho = []
        zgrid_static = []
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1)
        
        
        fig, ax = plt.subplots()
        ax.grid()
        for si in ss:
            #print [np.sum(i) for i in ss]
            xi = x[si]
            yi = y[si]
            zi = z[si]
            wi = w[si]
            gi = gamma[si]
            
            ax.plot(zi*1e3, bg[si], '.')
            #import pdb; pdb.set_trace()
            g0i = weighted_mean(gi, wi)
            q0i = np.sum(wi)
            
            hz_static = hz*g0i   
            zmin_static, zmax_static = zmin*g0i, zmax*g0i      
            zgridi_static = np.linspace(zmin_static-nz0*hz_static, zmax_static+nz0*hz_static, Nz+1)
            
            ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                      (zgridi_static[0]-0.5*hz_static, zgridi_static[-1]+0.5*hz_static)]
            #print ranges
            rhoi, edges = np.histogramdd((xi, yi, zi*g0i), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = wi)
            
            gamma0.append(g0i)
            Qtot.append(q0i)
            zgrid_static.append(zgridi_static)
            rho.append(rhoi/(hx*hy*hz_static))
            
            del zgridi_static
            del rhoi
        
        self.dx = hx; self.dy = hy; self.dz = hz 
        #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        
        if self.debug:
            fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (12, 4))
            add = np.zeros(len(zgrid))
            for i in np.arange(4):
                #data = np.abs(rho[i][Nx/2-1, Ny/2-1, :]*gamma0[i])
                rhoi_z = np.abs(np.sum(np.sum(rho[i], axis = 0), axis = 0)*gamma0[i])
                add += rhoi_z
                ax1.plot(zgrid*1e3, rhoi_z, label = '%d' % i)
            ax1.plot(zgrid*1e3, add, '--', label = 'all')
            ax1.grid()
            ax1.legend()
            ax1.set_xlabel(r'$z$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            add = np.zeros(len(xgrid))
            for i in np.arange(4):
                rhoi_x = np.abs(np.sum(np.sum(rho[i], axis = 1), axis = 1)*gamma0[i])
                add += rhoi_x
                ax2.plot(xgrid*1e3, rhoi_x, label = '%d' % i)
            ax2.plot(xgrid*1e3, add, '--', label = 'add')
            ax2.grid()
            ax2.legend()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density.eps')
        
        # Save 
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        
        self.rho = rho
        
        return  
    def build_binned(self, Xshift = (0, 0, 0), N_cutoff = None, nbins = 4):
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z = self.sigma_z
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        if N_cutoff is None:
            N_cutoff = self.N_cutoff
        
        xgrid = self.xgrid
        ygrid = self.ygrid
        zgrid = self.zgrid
        hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz = zgrid[1]-zgrid[0]
        
        if self.Lspch:
            # Create arrays for GFs and densities
            nbins = len(self.Qtot)
            self.PHI = []
            for i in np.arange(nbins):
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i]
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                xf, yf, zf = Xshift
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*self.gamma0[i]-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC = ifftn(PHIC_FFT)
                
                self.PHI.append(PHIC[:Nx+1,:Ny+1,:Nz+1])
        
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf = (0+self.zmean)*2; #print zf
            
            nbins = len(self.Qtot)
            self.PHI_mirror = []
            for i in np.arange(nbins):
                
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i][:,:,::-1]
                #RHOC_mirror = RHOC[:,:,::-1]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                g0i = self.gamma0[i]
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*g0i-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC_mirror)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC_mirror = ifftn(PHIC_FFT)
                self.PHI_mirror.append(PHIC_mirror[:Nx+1,:Ny+1,:Nz+1])
                
                self.PHI[i] -= self.PHI_mirror[i]
        
        # Field interpolation
        cc = -1./4/np.pi/g_eps0
        fEx3D_static = []
        fEy3D_static = []
        fEz3D_static = []
        for i in np.arange(nbins):
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI[i], hx, hy, hz*self.gamma0[i])     
        
            fEx3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ex_static,                                                    bounds_error = False, fill_value = None)
            fEy3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ey_static,                                                    bounds_error = False, fill_value = None)
            fEz3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ez_static,                                                    bounds_error = False, fill_value = None)
            
            fEx3D_static.append(fEx3Di_static)
            fEy3D_static.append(fEy3Di_static)
            fEz3D_static.append(fEz3Di_static)
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[:, Ny/2-1, Nz/2-1], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[:, Ny/2-1, Nz/2-1], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[:, Ny/2-1, Nz/2-1], '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        gamma0 = self.gamma0; zmean = self.zmean
        def EM3D(x, y, z, t = 0):
            
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            z_static = (z-zmean)*gamma0
            
            n = np.max([len(x), len(y), len(z)])
            
            F2d = np.zeros((len(x), 6))
            for i in np.arange(nbins):
                g0i = gamma0[i]; b0i = gamma2beta(g0i)
                z_static = (z-zmean)*g0i
                Ex_static = fEx3D_static[i]((x, y, z_static))*cc
                Ey_static = fEy3D_static[i]((x, y, z_static))*cc
                Ez_static = fEz3D_static[i]((x, y, z_static))*cc
                
                # Normalied E and B in lab frame
                
                F2d[:,0] += g0i*Ex_static
                F2d[:,1] += g0i*Ey_static
                F2d[:,2] += Ez_static
                F2d[:,3] += b0i*g0i*Ey_static/g_c
                F2d[:,4] -= b0i*g0i*Ex_static/g_c           
                #F2D = np.add(F2D, [g0i*Ex_static, g0i*Ey_static, Ez_static, b0i*g0i*Ey_static, -b0i*g0i*Ex_static, 0])
            
            return F2D
        
        self.EM3D = EM3D
        return
"""

# In case of mirror charge, the first grid is 0.5*hz downstream the cathode
class SpaceCharge3DFFT():
    def __init__(self, nx = 32, ny = 32, nz = 32, N_cutoff = 5, debug = 0,\
                 Lspch = True, Lmirror = False, Lbin = False, Lemit = False, Nbin = 1):
        
        self.nx = nx; self.ny = ny; self.nz = nz
        self.N_cutoff = N_cutoff       
        self.debug = debug
        
        self.Lspch = Lspch; self.Lmirror = Lmirror; self.Lemit = False
        self.Lbin = Lbin; self.Nbin = Nbin
        
        return
    def __del__(self):
        print 'SpaceCharge3DFFT instance is dead!'
    
    def get_rho_3D0(self, sigma_x = 1e-3, sigma_y = 1e-3, sigma_z = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D gaussian distributions
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_z_static = sigma_z*gamma0
         
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff
        
        self.gamma0 = gamma0
        
        def rho_3D((x_static, y_static, z_static)):
            xc, yc, zc = 0, 0, 0
            f_x = 1./np.sqrt(2.*np.pi)/sigma_x*np.exp(-(x_static-xc)**2/2./sigma_x**2)
            f_y = 1./np.sqrt(2.*np.pi)/sigma_y*np.exp(-(y_static-yc)**2/2./sigma_y**2)
            f_z = 1./np.sqrt(2.*np.pi)/sigma_z*np.exp(-(z_static-zc)**2/2./sigma_z_static**2)
            
            return f_x*f_y*f_z
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zmean = 0; self.zref = 0
        self.rho = rho
        return
    def get_rho_3D1(self, R = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D uniform distributions confined in a sphere
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_x = R; sigma_y = R; sigma_z = R
        sigma_z_static = sigma_z*gamma0
        
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff  
        
        def rho_3D((x_static, y_static, z_static)):
            radius = R #*np.sqrt(15)/5.0;
            xc, yc, zc = 0, 0, 0
            r = np.sqrt((x_static-xc)**2+(y_static-yc)**2+(z_static/gamma0-zc)**2)
            rho = np.where(r>radius, 0, 3./(4*np.pi*radius**3))
            return rho
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zgrid = zgrid_static/gamma0
        
        self.zmean = 0; self.zref = 1.5*R
        self.rho = rho*Qtot
        return
    
    def get_rho_3D(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                   Lmirror = None, Lemit = None, format = 'Astra', fig_ext = '.eps'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
                       
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9; self.Qtot = np.sum(w)
            
            zc =  weighted_mean(z, w)
            #self.zmean = (np.max(z)+np.min(z))*0.5
            #z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            
            gamma0 = weighted_mean(gamma, w)
            #z_static = z*gamma0
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        self.sigma_z_static = self.sigma_z*gamma0;       #print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        N_cutoff_z = 5
        zmin = np.max([np.min(z), zc-N_cutoff_z*self.sigma_z])
        zmax = np.min([np.max(z), zc+N_cutoff_z*self.sigma_z])
        
        xmax = np.max([xmax, -xmin])
        ymax = np.max([ymax, -ymin])
        xmin, ymin = -xmax, -ymax
        #zmin = np.min(z)
        #zmax = np.max(z)
        nx0, ny0, nz0 = 1, 1, 1 # Guarding grids on one side
        if self.Lemit:
            zmin = 0
            zmax = np.max(z)
            nz0 = 2.5
        #print inspect.stack()[0][3], zmin, zmax, self.zmean
        
        #import pdb; pdb.set_trace()
        self.zmean = (zmin+zmax)*0.5 # zmean is geometric center
        z -= self.zmean
        z_static = z*gamma0
        
        zmax -= self.zmean
        zmin -= self.zmean
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            print inspect.stack()[0][3], 'gamma = ', self.gamma0
            print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
            print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
            print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
            print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
            print inspect.stack()[0][3], 'mesh sizes in rest frame: ', hx, hy, hz_static
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1); # print zgrid+self.zmean
        #import pdb; pdb.set_trace()
        zgrid_static = zgrid*gamma0
        
        ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                  (zgrid_static[0]-0.5*hz_static, zgrid_static[-1]+0.5*hz_static)]
        rho, edges = np.histogramdd((x, y, z_static), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = w)
        del edges
        
        if self.debug:
            #print xgrid/self.sigma_x, ygrid/self.sigma_y, zgrid/self.sigma_z_static
            #print rho
            #print ranges
            fig, [ax1, ax2, ax3] = plt.subplots(ncols = 3, figsize = (12, 4))
            rho_x = np.sum(np.sum(rho, axis = 1), axis = 1)
            ax1.plot(xgrid*1e3, rho_x, '-*')
            ax1.grid()
            ax1.set_xlabel(r'$x$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            [aX,aY] = np.meshgrid(xgrid, ygrid)
            data = np.abs(rho[:,:,Nz/2-1])
            v = np.linspace(0.05, 1., 96)
            ax2.contourf(aX*1e3, aY*1e3, data/np.max(data), v, linestyles = None)
            ax2.grid()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$y$ (mm)')
            
            rho_z = np.sum(np.sum(rho, axis = 0), axis = 0)
            ax3.plot(zgrid*1e3, rho_z, '-*')
            ax3.grid()
            ax3.set_xlabel(r'$z$ (mm)')
            ax3.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density'+fig_ext)
        
        #rho = sgolay2d(rho, window_size=11, order=4)   
        #self.rho_RZ = interp2d(rr, zz, rho.T, kind = 'cubic')
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        self.zgrid_static = zgrid_static
        self.rho = rho/(hx*hy*hz_static)
        
        return
    def build(self, Xshift = (0, 0, 0), **kwargs):
        if 'fig_ext' in kwargs.keys():
            fig_ext = kwargs['fig_ext'];
        else:
            fig_ext = '.eps'
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z_static = self.sigma_z_static
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        if self.Lspch:
            # Create arrays for GFs and densities
            GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
            
            xgrid = self.xgrid
            ygrid = self.ygrid
            zgrid_static = self.zgrid_static
            RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho
            
            hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz_static = zgrid_static[1]-zgrid_static[0]
            if self.debug:
                print inspect.stack()[0][3], Nx, Ny, Nz
                print inspect.stack()[0][3], hx, hy, hz_static
            
            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xf, yf, zf = Xshift; 
            zf0 = -hz_static/self.gamma0/2.
            zf = zf+zf0
            
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_MAP = IGF3D(XX)

            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC = cyclic_IGF3D_mapping(II, IGF_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_FFT = fftn(GFC)
            RHOC_FFT = fftn(RHOC)
            PHIC_FFT = GFC_FFT*RHOC_FFT

            PHIC = ifftn(PHIC_FFT)
            self.PHI = PHIC[:Nx+1,:Ny+1,:Nz+1]
            
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf0 = -hz_static/self.gamma0/2.
            zf = (0+self.zmean)*2; #print zf
            zf = (0+self.zmean)*2+zf0; #print zf
            
            GFC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            
            RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[:,:,::-1]
            
            if self.debug:
                print inspect.stack()[0][3], Nx, Ny, Nz
                print inspect.stack()[0][3], hx, hy, hz_static
            
            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_mirror_MAP = IGF3D(XX)
            
            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC_mirror = cyclic_IGF3D_mapping(II, IGF_mirror_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_mirror_FFT = fftn(GFC_mirror)
            RHOC_mirror_FFT = fftn(RHOC_mirror)
            PHIC_mirror_FFT = GFC_mirror_FFT*RHOC_mirror_FFT

            PHIC_mirror = ifftn(PHIC_mirror_FFT)
            self.PHI_mirror = PHIC_mirror[:Nx+1,:Ny+1,:Nz+1]
        
        if self.Lemit and self.Lmirror:
            
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI.real, hx, hy, hz_static) 
            Ex_static_m, Ey_static_m, Ez_static_m = np.gradient(self.PHI_mirror.real, hx, hy, hz_static) 
            cc = -1./4/np.pi/g_eps0
        
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            #ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            #ax1.plot(xgrid/sigma_x, -Ex_static_m[:, Ny/2-1, Nz/4-1]*cc/1e3, '-o')
            
            ax1.plot((self.zgrid+self.zmean)*1e3, Ex_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax1.plot((self.zgrid+self.zmean)*1e3, -Ex_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '-o')
            ax1.plot((self.zgrid+self.zmean)*1e3, Ex_static[Nx/2-1, Ny/2-1, :]*cc/1e3-Ex_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '--')
            ax1.set_xlim(-0.05, 0.03)
            #ax1.set_ylim(-200, 200)
            
            ax1.set_xlabel(r'$z$')
            ax1.set_ylabel(r'$E_x(z)$ (kV/m)')
            ax1.grid()
            
            ax2.plot((self.zgrid+self.zmean)*1e3, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.plot((self.zgrid+self.zmean)*1e3, -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '-o')
            ax2.plot((self.zgrid+self.zmean)*1e3, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3-Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '--')
            #print Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3
            #print -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3
            ax2.set_xlabel(r'$z$ (mm)')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.set_xlim(-0.05, 0.03)
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis'+fig_ext)
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx/2-1, Ny/2-1, :], '-*')
            ax1.plot(GFC_mirror[Nx/2-1, Ny/2-1, :], '-o')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            
            to_astra_unit = self.dx*self.dy*self.dz_static/self.dz*1e9
            ax2.plot((self.zgrid+self.zmean)*1e3, np.sum(np.sum(RHOC[:,:,:Nz+1], axis = 0), axis = 0)*to_astra_unit, '-*')
            ax2.plot((self.zgrid+self.zmean)*1e3, np.sum(np.sum(RHOC_mirror[:,:,:Nz+1], axis = 0), axis = 0)*to_astra_unit, '-o')
            ax2.set_xlim(-0.05, 0.03)
            ax2.grid()
            ax2.set_xlabel(r'$z$ (mm)')
            ax2.set_title(r'charge density (nC/m)')
            
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :], '-*')
            ax3.plot(self.PHI_mirror[Nx/2-1, Ny/2-1, :], '-o')
            ax3.set_xlim(-1, 5)
            ax3.set_ylim(-4.0e-7, -3.5e-7)
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function'+fig_ext)
            
            #plt.close('all')
            
        if self.Lspch and self.Lmirror:
            self.PHI -= self.PHI_mirror
        
        del GFC, RHOC, GFC_FFT, RHOC_FFT, PHIC_FFT, IGF_MAP
        if self.Lmirror:
            del GFC_mirror, RHOC_mirror, GFC_mirror_FFT, RHOC_mirror_FFT, PHIC_mirror_FFT, IGF_mirror_MAP
        del XX, II
        
        ######
        Ex_static, Ey_static, Ez_static = np.gradient(self.PHI.real, hx, hy, hz_static)     
        cc = -1./4/np.pi/g_eps0
        ######
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx/2-1, Ny/2-1, :], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            
            ax2.plot(RHOC[Nx/2-1, Ny/2-1, :], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :]*cc*(-1), '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        fEx3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static+zf0), Ex_static, bounds_error = False, fill_value = None)
        fEy3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static+zf0), Ey_static, bounds_error = False, fill_value = None)
        fEz3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static+zf0), Ez_static, bounds_error = False, fill_value = None)
        
        cc = 1./4/np.pi/g_eps0 # 
        gamma0 = self.gamma0; beta = gamma2beta(gamma0); zmean = self.zmean
        def EM3D(x, y, z, t = 0):
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            z_static = (z-zmean)*gamma0
            
            n = np.max([len(x), len(y), len(z)])
            
            Ex_static = fEx3D_static((x, y, z_static))*cc
            Ey_static = fEy3D_static((x, y, z_static))*cc
            Ez_static = fEz3D_static((x, y, z_static))*cc
            
            # return normalied E and B in lab frame
            F2d = np.zeros((n, 6))
            F2d[:,0] = gamma0*Ex_static
            F2d[:,1] = gamma0*Ey_static
            F2d[:,2] = Ez_static
            F2d[:,3] = beta*gamma0*Ey_static/g_c
            F2d[:,4] =-beta*gamma0*Ex_static/g_c
            
            return F2d
        
        if self.debug and 0:
            X, Y, Z = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')
            
            #fEx3D_static = Rbf(X, Y, Z, Ex_static, function = 'linear') # or X.ravel()
            #fEy3D_static = Rbf(X, Y, Z, Ey_static, function = 'linear')
            fEz3D_static = Rbf(X, Y, Z, Ez_static, function = 'linear')
            
            cc = 1./4/np.pi/g_eps0 # 
            gamma0 = self.gamma0; beta = gamma2beta(gamma0); zmean = self.zmean
            def EM3D(x, y, z, t = 0):
                
                if np.isscalar(x):
                    x = [x]
                if np.isscalar(y):
                    y = [y]
                if np.isscalar(z):
                    z = [z]
                
                x = np.asarray(x)
                y = np.asarray(y)
                z = np.asarray(z)
                z_static = (z-zmean)*gamma0
            
                n = np.max([len(x), len(y), len(z)])
                
                Ex_static = fEx3D((x, y, z_static))*cc
                Ey_static = fEy3D((x, y, z_static))*cc
                Ez_static = fEz3D(x, y, z_static)*cc
                
                # return normalied E and B in lab frame
                F2d = np.zeros((n, 6))
                F2d[:,0] = gamma0*Ex_static
                F2d[:,1] = gamma0*Ey_static
                F2d[:,2] = Ez_static
                F2d[:,3] = beta*gamma0*Ey_static/g_c
                F2d[:,4] =-beta*gamma0*Ex_static/g_c

                return F2d
            
        self.EM3D = EM3D
        return
    
    def get_rho_3D_binned(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                          nbins = 4, Lmirror = None, Lemit = None, format = 'Astra'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
            
            self.zref = dist[0,iz]
            
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9
            
            self.zmean =  weighted_mean(z, w); print self.zmean
            self.zmean = (np.max(z)+np.min(z))*0.5; print self.zmean
            z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            gamma0 = weighted_mean(gamma, w); std_gamma0 = weighted_std(gamma, w)
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        zmin = np.max([np.min(z), -N_cutoff*self.sigma_z])
        zmax = np.min([np.max(z),  N_cutoff*self.sigma_z])
        
        zmin = np.min(z)
        zmax = np.max(z)
        nx0 = 1 # Guarding grids on one side
        ny0 = 1
        nz0 = 1
        if self.Lemit:
            zmin = -self.zmean
            nz0 = 2
        #print inspect.stack()[0][3], zmin, zmax, self.zmean
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        # Select sub-group of particles
        bg = np.sqrt(gamma**2-1)
        bg0 = weighted_mean(bg, w); std_bg0 = weighted_std(bg, w)
        #print bg0, std_bg0
        
        s1 = (bg<bg0-std_bg0)
        s2 = (bg>=bg0-std_bg0)*(bg<bg0)
        s3 = (bg>=bg0)*(bg<bg0+std_bg0)
        s4 = (bg>=bg0+std_bg0)
        ss = [s1, s2, s3, s4]
        
        Qtot = []
        gamma0 = []
        rho = []
        zgrid_static = []
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1)
        
        
        fig, ax = plt.subplots()
        ax.grid()
        for si in ss:
            #print [np.sum(i) for i in ss]
            xi = x[si]
            yi = y[si]
            zi = z[si]
            wi = w[si]
            gi = gamma[si]
            
            ax.plot(zi*1e3, bg[si], '.')
            #import pdb; pdb.set_trace()
            g0i = weighted_mean(gi, wi)
            q0i = np.sum(wi)
            
            hz_static = hz*g0i   
            zmin_static, zmax_static = zmin*g0i, zmax*g0i      
            zgridi_static = np.linspace(zmin_static-nz0*hz_static, zmax_static+nz0*hz_static, Nz+1)
            
            ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                      (zgridi_static[0]-0.5*hz_static, zgridi_static[-1]+0.5*hz_static)]
            #print ranges
            rhoi, edges = np.histogramdd((xi, yi, zi*g0i), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = wi)
            
            gamma0.append(g0i)
            Qtot.append(q0i)
            zgrid_static.append(zgridi_static)
            rho.append(rhoi/(hx*hy*hz_static))
            
            del zgridi_static
            del rhoi
        
        self.dx = hx; self.dy = hy; self.dz = hz 
        #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        
        if self.debug:
            fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (12, 4))
            add = np.zeros(len(zgrid))
            for i in np.arange(4):
                #data = np.abs(rho[i][Nx/2-1, Ny/2-1, :]*gamma0[i])
                rhoi_z = np.abs(np.sum(np.sum(rho[i], axis = 0), axis = 0)*gamma0[i])
                add += rhoi_z
                ax1.plot(zgrid*1e3, rhoi_z, label = '%d' % i)
            ax1.plot(zgrid*1e3, add, '--', label = 'all')
            ax1.grid()
            ax1.legend()
            ax1.set_xlabel(r'$z$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            add = np.zeros(len(xgrid))
            for i in np.arange(4):
                rhoi_x = np.abs(np.sum(np.sum(rho[i], axis = 1), axis = 1)*gamma0[i])
                add += rhoi_x
                ax2.plot(xgrid*1e3, rhoi_x, label = '%d' % i)
            ax2.plot(xgrid*1e3, add, '--', label = 'add')
            ax2.grid()
            ax2.legend()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density.eps')
        
        # Save 
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        
        self.rho = rho
        
        return  
    def build_binned(self, Xshift = (0, 0, 0), N_cutoff = None, nbins = 4):
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z = self.sigma_z
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        if N_cutoff is None:
            N_cutoff = self.N_cutoff
        
        xgrid = self.xgrid
        ygrid = self.ygrid
        zgrid = self.zgrid
        hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz = zgrid[1]-zgrid[0]
        
        if self.Lspch:
            # Create arrays for GFs and densities
            nbins = len(self.Qtot)
            self.PHI = []
            for i in np.arange(nbins):
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i]
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                xf, yf, zf = Xshift
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*self.gamma0[i]-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC = ifftn(PHIC_FFT)
                
                self.PHI.append(PHIC[:Nx+1,:Ny+1,:Nz+1])
        
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf = (0+self.zmean)*2; #print zf
            
            nbins = len(self.Qtot)
            self.PHI_mirror = []
            for i in np.arange(nbins):
                
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i][:,:,::-1]
                #RHOC_mirror = RHOC[:,:,::-1]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                g0i = self.gamma0[i]
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*g0i-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC_mirror)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC_mirror = ifftn(PHIC_FFT)
                self.PHI_mirror.append(PHIC_mirror[:Nx+1,:Ny+1,:Nz+1])
                
                self.PHI[i] -= self.PHI_mirror[i]
        
        # Field interpolation
        cc = -1./4/np.pi/g_eps0
        fEx3D_static = []
        fEy3D_static = []
        fEz3D_static = []
        for i in np.arange(nbins):
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI[i], hx, hy, hz*self.gamma0[i])     
        
            fEx3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ex_static,                                                    bounds_error = False, fill_value = None)
            fEy3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ey_static,                                                    bounds_error = False, fill_value = None)
            fEz3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ez_static,                                                    bounds_error = False, fill_value = None)
            
            fEx3D_static.append(fEx3Di_static)
            fEy3D_static.append(fEy3Di_static)
            fEz3D_static.append(fEz3Di_static)
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[:, Ny/2-1, Nz/2-1], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[:, Ny/2-1, Nz/2-1], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[:, Ny/2-1, Nz/2-1], '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        gamma0 = self.gamma0; zmean = self.zmean
        def EM3D(x, y, z, t = 0):
            
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            z_static = (z-zmean)*gamma0
            
            n = np.max([len(x), len(y), len(z)])
            
            F2d = np.zeros((len(x), 6))
            for i in np.arange(nbins):
                g0i = gamma0[i]; b0i = gamma2beta(g0i)
                z_static = (z-zmean)*g0i
                Ex_static = fEx3D_static[i]((x, y, z_static))*cc
                Ey_static = fEy3D_static[i]((x, y, z_static))*cc
                Ez_static = fEz3D_static[i]((x, y, z_static))*cc
                
                # Normalied E and B in lab frame
                
                F2d[:,0] += g0i*Ex_static
                F2d[:,1] += g0i*Ey_static
                F2d[:,2] += Ez_static
                F2d[:,3] += b0i*g0i*Ey_static/g_c
                F2d[:,4] -= b0i*g0i*Ex_static/g_c           
                #F2D = np.add(F2D, [g0i*Ex_static, g0i*Ey_static, Ez_static, b0i*g0i*Ey_static, -b0i*g0i*Ex_static, 0])
            
            return F2D
        
        self.EM3D = EM3D
        return

'''
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python\space-charge'
os.chdir(workdir)

#dist = np.loadtxt('ast.0528.999')
dist = np.loadtxt('trk.002000.999')

dist[1:,2] += dist[0,2] 
dist[1:,5] += dist[0,5]

Qtot = 100e-12
sigma_x = 1e-3; sigma_y = 1e-3; sigma_z = 1e-3
Nx, Ny, Nz = 32, 32, 32
Nc = 5
gamma0 = 1. #+150./g_mec2; print gamma

xf, yf, zf = 0*sigma_x, 0, 0*sigma_x

import timeit
sc3d = SpaceCharge3DFFT(Nx, Ny, Nz, Nc, debug = 1, Lmirror = True)
time1 = timeit.default_timer()
#c3d.get_rho_3D1()
sc3d.get_rho_3D(dist = dist, Lemit = True)
sc3d.build(Xshift = (xf, yf, zf))
time2 = timeit.default_timer()
print 'time elapsed', time2-time1

sc3d4b = SpaceCharge3DFFT(Nx, Ny, Nz, Nc, debug = 1, Lmirror = True)
time1 = timeit.default_timer()
sc3d4b.get_rho_3D_binned(dist = dist, Lemit = True)
sc3d4b.build_binned(Xshift = (xf, yf, zf))
time2 = timeit.default_timer()
print 'time elapsed', time2-time1

def demo1(sc3d):
    sigma_x, sigma_y, sigma_z = sc3d.sigma_x, sc3d.sigma_y, sc3d.sigma_z
    Nx, Ny, Nz, Nc = sc3d.nx, sc3d.ny, sc3d.nz, sc3d.N_cutoff
    print sigma_x, sigma_y, sigma_z

    z0 = sc3d.zmean; print z0
    #z0 = 0;
    xx = np.linspace(-Nc*sigma_x, Nc*sigma_x, 2*Nx+1)+xf; #print xx
    yy = np.linspace(-Nc*sigma_y, Nc*sigma_y, 2*Ny+1); #print yy
    zz = np.linspace(-Nc*sigma_z, Nc*sigma_z, 2*Nz+1)+z0; #print zz
    #zz = sc3d.zgrid_static#/sc3d.gamma0[0]
    
    fig, ax = plt.subplots(figsize = (6, 4))
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0+0*sigma_z)[0] for x in xx])/1e3, '-*')
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0+1*sigma_z)[0] for x in xx])/1e3, '-o')
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0+2*sigma_z)[0] for x in xx])/1e3, '-^')
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0-1*sigma_z)[0] for x in xx])/1e3, '-o')
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0-2*sigma_z)[0] for x in xx])/1e3, '-^')
    ax.set_xlim(-Nc, Nc)
    #ax.set_ylim(-200, 200)
    #ax.set_yticks(np.linspace(-200, 200, 11))
    ax.grid()
    ax.set_xlabel(r'$x/\sigma_x$')
    ax.set_ylabel(r'$E_x(x)$ (kV/m)')
    ax.legend([r'$\sigma_z/z = 0$', r'$\sigma_z/z = 1$', r'$\sigma_z/z = 2$', r'$\sigma_z/z = -1$', r'$\sigma_z/z = -2$'])
    fig.savefig('Ex-x-z4.eps')

    fig, ax = plt.subplots(figsize = (6, 4))
    ax.plot(yy/sigma_y,            np.array([sc3d.EM3D(0, y, 0*sigma_z)[1] for y in yy])/1e3, '-*')
    ax.plot(yy/sigma_y,            np.array([sc3d.EM3D(0, y, 1*sigma_z)[1] for y in yy])/1e3, '-o')
    ax.plot(yy/sigma_y,            np.array([sc3d.EM3D(0, y, 2*sigma_z)[1] for y in yy])/1e3, '-^')
    ax.plot(yy/sigma_y,            np.array([sc3d.EM3D(0, y,-1*sigma_z)[1] for y in yy])/1e3, '-o')
    ax.plot(yy/sigma_y,            np.array([sc3d.EM3D(0, y,-2*sigma_z)[1] for y in yy])/1e3, '-^')
    ax.set_xlim(-Nc, Nc)
    #ax.set_ylim(-200, 200)
    ax.grid()
    ax.set_xlabel(r'$y/\sigma_y$')
    ax.set_ylabel(r'$E_y(y)$ (kV/m)')
    ax.legend([r'$\sigma_z/z = 0$', r'$\sigma_z/z = 1$', r'$\sigma_z/z = 2$', r'$\sigma_z/z = -1$', r'$\sigma_z/z = -2$'])
    fig.savefig('Ey-y-z4.eps')

    fig, ax = plt.subplots(figsize = (6, 4))
    ax.plot(zz*1e3, np.array([sc3d.EM3D(0, 0, z)[2] for z in zz])/1e3, '-')
    ax.plot(zz*1e3, np.array([sc3d.EM3D(sigma_x*0.375, 0, z)[2] for z in zz])/1e3, '-')
    ax.plot(zz*1e3, np.array([sc3d.EM3D(sigma_x*0.750, 0, z)[2] for z in zz])/1e3, '-')
    ax.plot(zz*1e3, np.array([sc3d.EM3D(sigma_x*1.125, 0, z)[2] for z in zz])/1e3, '-')
    ax.plot(zz*1e3, np.array([sc3d.EM3D(sigma_x*1.500, 0, z)[2] for z in zz])/1e3, '-')
    #ax.set_xlim(-Nc, Nc)
    #ax.set_ylim(-200, 200)
    ax.grid()
    ax.set_xlabel(r'$z$')
    ax.set_ylabel(r'$E_z(z)$ (kV/m)')
    ax.legend([r'$r/\sigma_x = 0$', r'$r/\sigma_x = 0.375$', r'$r/\sigma_x = 0.750$',               r'$r/\sigma_x = 1.125$', r'$r/\sigma_x = 1.500$'])
    fig.savefig('Ez-z-x4.eps')
    return
#demo1(sc3d)
def demo2():
    sigma_x, sigma_y, sigma_z = sc3d.sigma_x, sc3d.sigma_y, sc3d.sigma_z
    Nx, Ny, Nz, Nc = sc3d.nx, sc3d.ny, sc3d.nz, sc3d.N_cutoff
    print sigma_x, sigma_y, sigma_z
    
    z0 = sc3d.zmean; print z0
    
    xx = sc3d.xgrid
    yy = sc3d.ygrid
    zz = sc3d.zgrid+z0
    
    Nc = 3
    fig, ax = plt.subplots(figsize = (6, 4))
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0+0*sigma_z)[0] for x in xx])/1e3, 'ro')
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0+2*sigma_z)[0] for x in xx])/1e3, 'b^')
    
    z0 = sc3d4b.zmean
    xx = sc3d4b.xgrid
    ax.plot(xx/sigma_x,            np.array([sc3d4b.EM3D(x, 0, z0+0*sigma_z)[0] for x in xx])/1e3, 'g--')
    ax.plot(xx/sigma_x,            np.array([sc3d4b.EM3D(x, 0, z0+2*sigma_z)[0] for x in xx])/1e3, 'c--')
    
    
    ax.set_xlim(-Nc, Nc)
    #ax.set_ylim(-200, 200)
    #ax.set_yticks(np.linspace(-200, 200, 11))
    ax.grid()
    ax.set_xlabel(r'$x/\sigma_x$')
    ax.set_ylabel(r'$E_x(x)$ (kV/m)')
    ax.legend([r'$\sigma_z/z = 0$', r'$\sigma_z/z = 2$',               r'$\sigma_z/z = 0$, binned', r'$\sigma_z/z = 2$, binned'])
    fig.savefig('Ex-x-z41.eps')
    
    fig, ax = plt.subplots(figsize = (6, 4))
    ax.plot(zz*1e3, np.array([sc3d.EM3D(0, 0, z)[2] for z in zz])/1e3, 'ro')
    ax.plot(zz*1e3, np.array([sc3d.EM3D(sigma_x*1.5, 0, z)[2] for z in zz])/1e3, 'b^')
    
    zz = sc3d4b.zgrid+z0
    ax.plot(zz*1e3, np.array([sc3d4b.EM3D(0, 0, z)[2] for z in zz])/1e3, 'g--')
    ax.plot(zz*1e3, np.array([sc3d4b.EM3D(sigma_x*1.5, 0, z)[2] for z in zz])/1e3, 'c--')
    
    #ax.set_xlim(-Nc, Nc)
    #ax.set_ylim(-200, 200)
    ax.grid()
    ax.set_xlabel(r'$z$')
    ax.set_ylabel(r'$E_z(z)$ (kV/m)')
    ax.legend([r'$r/\sigma_x = 0$', r'$r/\sigma_x = 1.5$',               r'$r/\sigma_x = 0$, binned', r'$r/\sigma_x = 1.5$, binned'])
    fig.savefig('Ez-z-x41.eps')
    return
#demo2()
def demo3(sc3d):
    sigma_x, sigma_y, sigma_z = sc3d.sigma_x, sc3d.sigma_y, sc3d.sigma_z
    
    Nx, Ny, Nz, Nc = sc3d.nx, sc3d.ny, sc3d.nz, sc3d.N_cutoff
    print sigma_x, sigma_y, sigma_z
    
    zz = np.linspace(-Nc*sigma_z, Nc*sigma_z, Nz+1); #print xx
    Ez = np.array([sc3d.EM3D(0, 0, z)[2] for z in zz])
    
    data = np.loadtxt('R1mm.dat')
    
    fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
    ax1.plot((zz+zf)/sigma_z, Ez/1e3, '-*')
    ax1.plot(data[:,0]/sigma_z, data[:,2]/1e3, '-')
    #ax1.set_ylim(-200, 0)
    ax1.grid()
    ax1.set_xlabel(r'$z/R$')
    ax1.set_ylabel(r'$E_z$ (kV/m)')
    
    cc = -1./4/np.pi/g_eps0
    
    ax2.plot((zz+zf)/sigma_z, sc3d.PHI[Nx/2-1, Ny/2-1, :]*1./4/np.pi/g_eps0, '-*')
    ax2.plot(data[:,0]/sigma_z, data[:,1], '-')
    ax2.grid()
    #ax2.set_ylim(0, 500)
    ax2.set_xlabel(r'$r/R$')
    ax2.set_ylabel(r'$\phi$ (V)')
    
    plt.tight_layout()
    #fig.savefig('field-on-axis-from-uniform-ball2.eps')
    return
#demo3(sc3d)
'''

# # Tracking
def build_cavity(filename, z0, amp, freq, phi0 = 0):
    #print phi_0
    data = np.loadtxt(filename)
    
    a, b = np.max(data[:,1]), np.min(data[:,1])
    Ez_z = data[:,1]/a*amp
    
    dz = data[1,0]-data[0,0]
    
    dEz_dz1 = np.gradient(   Ez_z, dz)
    dEz_dz2 = np.gradient(dEz_dz1, dz)
    dEz_dz3 = np.gradient(dEz_dz2, dz) 
    
    fEz_0 = interp1d(data[:,0],    Ez_z, bounds_error = False, fill_value = 0)
    fEz_1 = interp1d(data[:,0], dEz_dz1, bounds_error = False, fill_value = 0)
    fEz_2 = interp1d(data[:,0], dEz_dz2, bounds_error = False, fill_value = 0)
    fEz_3 = interp1d(data[:,0], dEz_dz3, bounds_error = False, fill_value = 0)
    
    #dEz_dz = np.zeros((len(data[:,0]), 4))
    #dEz_dz[:,0] = data[:,0]
    #dEz_dz[:,1] = dEz_dz1
    #dEz_dz[:,2] = dEz_dz2
    #dEz_dz[:,3] = dEz_dz3
    #fEz_z = interp1d(dEz_dz[:,0], dEz_dz[:,1:4], bounds_error = False, fill_value = 0)
    
    def EM3D(x, y, z, t = 0):
        if np.isscalar(x):
            x = [x]
            y = [y]
            z = [z]
        x = np.asarray(x)
        y = np.asarray(y)
        z = np.asarray(z)
        if not np.isscalar(t):
            t = np.asarray(t)

        omega = 2.*np.pi*freq
        phi = (phi0+0)*np.pi/180

        ww = omega**2/g_c**2
        
        r = np.sqrt(x*x+y*y)
        
        z1 = z-z0
        Ez0 = fEz_0(z1); #print Ez0
        Ez1 = fEz_1(z1)
        Ez2 = fEz_2(z1)
        Ez3 = fEz_3(z1)
        
        Ez = (     Ez0-r**2/4. *(Ez2+ww*Ez0))*np.sin(omega*t+phi)
        Er = (-0.5*Ez1+r**2/16.*(Ez3+ww*Ez1))*np.sin(omega*t+phi) # Er/r instead of Er
        Bt = ( 0.5*Ez0-r**2/16.*(Ez2+ww*Ez0))*ww/omega*np.cos(omega*t+phi) # Bt/r instead of Bt
        
        Ez = np.where(z<0, 0, Ez)
        Er = np.where(z<0, 0, Er)
        Bt = np.where(z<0, 0, Bt)
        
        F2d = np.zeros((len(x), 6))
        F2d[:,0] = x*Er
        F2d[:,1] = y*Er
        F2d[:,2] = Ez
        F2d[:,3] =-y*Bt
        F2d[:,4] = x*Bt
        #F2d = [x*Er, y*Er, Ez, -y*Bt, x*Bt, 0*Bt]
        return F2d
    
    return EM3D
def build_solenoid(filename, z0, amp):
    
    data = np.loadtxt(filename)
    
    a, b = np.max(data[:,1]), np.min(data[:,1])
    Fz_z = data[:,1]/a*amp
    
    dz = data[1,0]-data[0,0]
    
    dFz_dz1 = np.gradient(Fz_z, dz)
    dFz_dz2 = np.gradient(dFz_dz1, dz)
    dFz_dz3 = np.gradient(dFz_dz2, dz)
    dFz_dz4 = np.gradient(dFz_dz3, dz)
    dFz_dz5 = np.gradient(dFz_dz4, dz)
    dFz_dz6 = np.gradient(dFz_dz5, dz)
    
    fFz_0 = interp1d(data[:,0], Fz_z,    bounds_error = False, fill_value = 0)
    fFz_1 = interp1d(data[:,0], dFz_dz1, bounds_error = False, fill_value = 0)
    fFz_2 = interp1d(data[:,0], dFz_dz2, bounds_error = False, fill_value = 0)
    fFz_3 = interp1d(data[:,0], dFz_dz3, bounds_error = False, fill_value = 0)
    fFz_4 = interp1d(data[:,0], dFz_dz4, bounds_error = False, fill_value = 0)
    fFz_5 = interp1d(data[:,0], dFz_dz5, bounds_error = False, fill_value = 0)
    fFz_6 = interp1d(data[:,0], dFz_dz6, bounds_error = False, fill_value = 0)
     
    def EM3D(x, y, z, t = 0):
        
        if np.isscalar(x):
            x = [x]
            y = [y]
            z = [z]
        x = np.asarray(x)
        y = np.asarray(y)
        z = np.asarray(z)
        
        if not np.isscalar(t):
            t = np.asarray(t)
        
        r = np.sqrt(x*x+y*y)
        
        z1 = z-z0
        Fz0 = fFz_0(z1)
        Fz1 = fFz_1(z1)
        Fz2 = fFz_2(z1)
        Fz3 = fFz_3(z1)
        Fz4 = fFz_4(z1)
        Fz5 = fFz_5(z1)
        Fz6 = fFz_6(z1)
        
        Bz = (Fz0-r*r/4.*Fz2+r**4/64.*Fz4-r**6/2034.*Fz6)
        Br = (-1/2.*Fz1+r**2/16.*Fz3-r**4/384.*Fz5) # Br/r instead of Br
        
        Bz = np.where(z<0, 0, Bz)
        Br = np.where(z<0, 0, Br)
        
        F2d = np.zeros((len(x), 6))
        F2d[:,3] = x*Br
        F2d[:,4] = y*Br
        F2d[:,5] = Bz
        #F2d = [0, 0, 0, x*Br, y*Br, Bz]
        return F2d
    return EM3D

class EMsolver():
    def __init__(self):
        self.em3d = {}
    def add_external_field(self, name, func):
        self.em3d.update({name:func})
    def update_external_field(self, name, func):
        self.em3d.update({name:func})
    def get_field(self, x, y, z, t = 0, namelist = None):
        field = np.zeros((1, 6))
        if namelist is None:
            namelist = self.em3d.keys()
        for name, func in self.em3d.iteritems():
            if name in namelist:
                field = np.add(field, func(x, y, z, t))
        return field

'''
field_maps = r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\field-maps'
gun3d = build_cavity(field_maps+os.sep+'gun42Cavity.txt', 0, 60, 1.3e9, 90)
sol3d = build_solenoid(field_maps+os.sep+'gunsolenoidsPITZ.txt', 0, 0.2)

em3d = EMsolver()
em3d.add_external_field('gun', gun3d)
em3d.add_external_field('solenoid', sol3d)

x, y, z = [0, 0, 0], [0, 0, 0.01], [0, 0.1, 0.1]
t = 0
#print gun3d(0, 0, 0), gun3d(0, 0, 0.1), gun3d(0, 0.01, 0.1)
get_ipython().magic(u'timeit gun3d(x, y, z, t)')
a = em3d.get_field([0, 0, 0], [0, 0, 0.01], [0, 0.1, 0.1]); print a
Ex, Ey, Ez, Bx, By, Bz = a.T[:]
print Ex, Ey, Ez

rr = np.linspace(0, 0.1, 200)
fr = np.array([em3d.get_field(v, 0, 0)[0,0] for v in rr])
zz = np.linspace(-0.1, 0.5, 200)
fz = np.array([em3d.get_field(0, 0.01, v, 0)[0,5] for v in zz])

fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (12, 4))
ax1.plot(rr, fr, '-')
ax1.grid()

ax2.plot(zz, fz, '-')
ax2.grid()
'''

class BeamDiagnostics:
    def __init__(self, fname = None, dist = None, start_index = 0):

#         self.keyIndex = {'avg_z':0, 'nemit_x':1, 'nemit_y':2, 'std_x':3, 'std_y':4, 'std_z':5,\
#                          'Ekin':6, 'std_Ekin':7, 'nemit_z':8, 'Q_b':9, 'avg_x':10, 'avg_y':11,\
#                          'alpha_x':12, 'beta_x':13, 'gamma_x':14, 'emit_x':15, 'alpha_y':16,\
#                          'beta_y':17, 'gamma_y':18, 'emit_y':19, 'cor_zEk':20, 'loss_cathode':21,\
#                          'loss_aperture':22, 'FWHM':23}
        keys = ['avg_z', 'nemit_x', 'nemit_y', 'std_x', 'std_y', 'std_z', 'Ekin', 'std_Ekin', 'nemit_z', 'Q_b',\
                'avg_x', 'avg_y', 'alpha_x', 'beta_x', 'gamma_x', 'emit_x', 'alpha_y', 'beta_y', 'gamma_y',\
                'emit_y', 'cor_Ek', 'loss_cathode', 'loss_aperture', 'FWHM', 'NoP']
        indices = np.arange(len(keys))
        units = ['m', 'um', 'um', 'mm', 'mm', 'mm', 'MeV', 'keV', 'keV mm', 'pC', 'm', 'm', ' ', 'm', 'm^-1',\
                 'm', ' ', 'm', 'm^-1', 'm', 'keV', ' ', ' ', 'mm', ' ']
        keyIndex = {}
        keyUnit = {}
        for i in np.arange(len(keys)):
            keyIndex[keys[i]] = indices[i] + start_index
            keyUnit[keys[i]] = units[i]
        self.keyIndex = keyIndex
        self.keyUnit = keyUnit
    
        if fname is not None:
            dist = np.loadtxt(fname)
            dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
            self.dist = dist
            self.diagnostics()
        elif dist is not None:
            self.dist = dist
            self.diagnostics()
        else:
            self.x = np.zeros(len(self.keyIndex))

    def __getitem__(self, key):
        return self.get(key)
    
    def __getattr__(self, key):
        return self.get(key)
    
    @property
    def x(self):
        return self.__x

    @x.setter
    def x(self, x):
        self.__x = x
        
    def get(self, key):
        '''
        Return the corresponding parameter if key is one of `self.kv.keys()`, or return the index of the key 
        if requesting by "idx_"+key and key is one of `self.kv.keys()`
        '''
        if key in self.keyIndex.keys():
            index = self.keyIndex[key]
            return self.x[index]
        elif key in ['idx_'+ i for i in self.keyIndex.keys()]:
            index = self.keyIndex[key[4:]]
            return index
        else:
            return -999
    
    def get_index(self, key):
        index = self.keyIndex[key]
        return index
    
    def diagnostics(self, key_value=None):
        '''
        Parameters
          x y z Px Py Pz w: 6D phase space of the particles
        Outputs
          [avg_z nemit_x nemit_y std_x std_y std_z Ekin std_Ekin nemit_z Q_b\
          avg_x avg_y alpha_x beta_x gamma_x emit_x alpha_y beta_y gamma_y emit_y\
          cor_zEk loss_cathode loss_aperture FWHM]: 1D array
        '''

        if key_value != None:
            ix = key_value['x']; iy = key_value['y']; iz = key_value['z']
            iux = key_value['ux']; iuy = key_value['uy']; iuz = key_value['uz']
            iw = key_value['w']
        else:
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
            
        dist = self.dist
        NoP = len(dist[:,1])
        select = (dist[:,9]==-89); n_lost_cathode = len(dist[select])
        select = (dist[:,9]==-15); n_lost_aperture = len(dist[select])
        
        select = (dist[:,9]>0); dist = dist[select]
        if np.sum(select) == 0:
            self.x = np.zeros(len(self.keyIndex))
            return
        
        x, y, z = dist[:,ix], dist[:,iy] ,dist[:,iz]
        bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
        w = dist[:,iw]*1e-9/g_qe

        gamma=np.sqrt(1+bgx**2+bgy**2+bgz**2)
        Ek=np.sqrt(1+bgx**2+bgy**2+bgz**2)*g_mec2-g_mec2

        x2 = weighted_mean(x*x, w); y2 = weighted_mean(y*y, w)
        xp2 = weighted_mean(bgx*bgx/bgz/bgz, w); yp2 = weighted_mean(bgy*bgy/bgz/bgz, w)
        xxp = weighted_mean(x*bgx/bgz, w); yyp = weighted_mean(y*bgy/bgz, w)
        emit_x = np.sqrt(x2*xp2-xxp*xxp); emit_y = np.sqrt(y2*yp2-yyp*yyp)
        alpha_x = -xxp/emit_x; beta_x = x2/emit_x; gamma_x = xp2/emit_x
        alpha_y = -yyp/emit_y; beta_y = y2/emit_y; gamma_y = yp2/emit_y

        std_z, zmean = weighted_std(z, w), weighted_mean(z, w)
        fwhm = get_FWHM(z)
        select = (z-zmean>-5*std_z)*(z-zmean<5*std_z); Ek1 = Ek[select]; z1 = z[select]; w1 = w[select]
        cor_zEkz = weighted_mean(Ek1*z1, w1)-weighted_mean(Ek1, w1)*weighted_mean(z1, w1)
        cor_zEkz = cor_zEkz/weighted_std(z1, w1)*1e3
        
        self.x = np.array([weighted_mean(z, w), nemixrms(x, bgx, w)*1e6, nemixrms(y, bgy, w)*1e6, weighted_std(x, w)*1e3,\
                           weighted_std(y, w)*1e3, weighted_std(z, w)*1e3, weighted_mean(Ek, w), weighted_std(Ek, w)*1e3,\
                           nemixrms(z, Ek, w)*1e6, np.sum(w)*g_qe*1e12, weighted_mean(x, w), weighted_mean(y, w), alpha_x,\
                           beta_x, gamma_x, emit_x, alpha_y, beta_y, gamma_y, emit_y, cor_zEkz, n_lost_cathode,\
                           n_lost_aperture, fwhm*1e3, NoP])
        
    def demo(self, name = None):
        if name is not None:
            filename = 'diag@'+name+'.txt'
        else:
            filename = 'diag@'+'.txt'
        
        a = sorted(self.keyIndex.items(), key=lambda x: x[1])
        ss = ''
        for i in np.arange(len(a)):
            ss += str('%17s: %15.6E %s\n' % (a[i][0], self.x[i], self.keyUnit.get(a[i][0])))
        print ss

        ff = open(filename, 'w')
        ff.write(ss)
        ff.close()
        return 

class Beam():
    def __init__(self, fname = None, dist = None, debug = 0):
        #self.diag = BeamDiagnostics(fname, dist)
        #BeamDiagnostics(fname, dist)
        self.sc3d = SpaceCharge3DFFT(debug = debug)
        
        #self.key_value = {'x':0, 'ux':1, 'y':2, 'uy':3, 'z':4, 'uz':5, 'w':6}
        self.key_value = {'x':0, 'y':1, 'z':2, 'Px':3, 'Py':4, 'Pz':5, 'w':7}
        
        if fname is not None:
            self.dist = np.loadtxt(fname)
            self.dist[1:,2] += self.dist[0,2]
            self.dist[1:,5] += self.dist[0,5]
        elif dist is not None:
            self.dist = dist
        else:
            self.dist = []
        
        return
    def load(self, fname):
        #self.diag = BeamDiagnostics(fname)
        self.dist = np.loadtxt(fname)        
        self.dist[1:,2] += self.dist[0,2]
        self.dist[1:,5] += self.dist[0,5]
        return
    def dump(self, fname):
        dist = np.copy(self.dist)
        if dist[0,9] == -89:
            select = (dist[:,9]>0)
            dist[0,2] = weighted_mean(dist[select,2], dist[select,7])
            dist[0,5] = weighted_mean(dist[select,5], dist[select,7])
        dist[1:,2] -= dist[0,2]
        dist[1:,5] -= dist[0,5]
        np.savetxt(fname, dist, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4.0f%4.0f')
        return
    def get_sc3d(self, **kwargs):
        self.sc3d.get_rho_3D(self.dist, **kwargs)
        self.sc3d.build(**kwargs)
        return self.sc3d.EM3D
    def diagnose(self, fname = None):
        #self.diag = BeamDiagnostics(dist = self.dist)
        self.diagnostics()
        if fname is not None:
            with open(fname, 'a') as f_handle:
                np.savetxt(f_handle, np.atleast_2d(self.x), fmt='%14.6E')
        return
    def diagnostics(self, key_value = None):
        '''
        Parameters
          x y z Px Py Pz w: 6D phase space of the particles
        Outputs
          [avg_z nemit_x nemit_y std_x std_y std_z Ekin std_Ekin nemit_z Q_b\
          avg_x avg_y alpha_x beta_x gamma_x emit_x alpha_y beta_y gamma_y emit_y\
          cor_zEk loss_cathode loss_aperture FWHM]: 1D array
        '''

        if key_value != None:
            ix = key_value['x']; iy = key_value['y']; iz = key_value['z']
            iux = key_value['ux']; iuy = key_value['uy']; iuz = key_value['uz']
            iw = key_value['w']
        else:
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
        dist = self.dist
        NoP = len(dist[:,1])
        select = (dist[:,9]==-89); n_lost_cathode = len(dist[select])
        select = (dist[:,9]==-15); n_lost_aperture = len(dist[select])

        select = (dist[:,9]>0); dist = dist[select]
        if np.sum(select) == 0:
            self.x = np.zeros(1) #np.zeros(len(self.keyIndex))
            return
        
        x, y, z = dist[:,ix], dist[:,iy] ,dist[:,iz]
        bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
        w = dist[:,iw]*1e-9/g_qe

        gamma=np.sqrt(1+bgx**2+bgy**2+bgz**2)
        Ek=np.sqrt(1+bgx**2+bgy**2+bgz**2)*g_mec2-g_mec2

        x2 = weighted_mean(x*x, w); y2 = weighted_mean(y*y, w)
        xp2 = weighted_mean(bgx*bgx/bgz/bgz, w); yp2 = weighted_mean(bgy*bgy/bgz/bgz, w)
        xxp = weighted_mean(x*bgx/bgz, w); yyp = weighted_mean(y*bgy/bgz, w)
        emit_x = np.sqrt(x2*xp2-xxp*xxp); emit_y = np.sqrt(y2*yp2-yyp*yyp)
        alpha_x = -xxp/emit_x; beta_x = x2/emit_x; gamma_x = xp2/emit_x
        alpha_y = -yyp/emit_y; beta_y = y2/emit_y; gamma_y = yp2/emit_y

        std_z, zmean = weighted_std(z, w), weighted_mean(z, w)
        fwhm = get_FWHM(z)
        select = (z-zmean>-5*std_z)*(z-zmean<5*std_z); Ek1 = Ek[select]; z1 = z[select]; w1 = w[select]
        cor_zEkz = weighted_mean(Ek1*z1, w1)-weighted_mean(Ek1, w1)*weighted_mean(z1, w1)
        cor_zEkz = cor_zEkz/weighted_std(z1, w1)*1e3
        
        self.x = np.array([weighted_mean(z, w), nemixrms(x, bgx, w)*1e6, nemixrms(y, bgy, w)*1e6, weighted_std(x, w)*1e3,\
                           weighted_std(y, w)*1e3, weighted_std(z, w)*1e3, weighted_mean(Ek, w), weighted_std(Ek, w)*1e3,\
                           nemixrms(z, Ek, w)*1e6, np.sum(w)*g_qe*1e12, weighted_mean(x, w), weighted_mean(y, w), alpha_x,\
                           beta_x, gamma_x, emit_x, alpha_y, beta_y, gamma_y, emit_y, cor_zEkz, n_lost_cathode,\
                           n_lost_aperture, fwhm*1e3, NoP])

def unwrap_step_i(arg, **kwarg):
    return Tracking.step_i(*arg, **kwarg)

class Tracking():
    def __init__(self, beam, em3d, t1, dt = 1e-12, t0 = None, diag_interval = 10, dump_interval = 100,\
                 Lspch = False, sc3d = None, Run = 1):
        self.beam = beam
        self.em3d = em3d
        self.sc3d = sc3d
        
        self.clock = None
        self.time = t0
        self.dt = dt
        self.tend = t1
        self.stepped = 0
        self.diag_interval = diag_interval
        self.dump_interval = dump_interval
        self.dump_factor = 1
        
        self.Lspch = Lspch
        self.Run = Run
        
        return
    
    def deriv(self, t, y):
        '''
        Calculate derivatives of many particles altogeter
        Parameters
          t: time/s
          y[0]: x/m
          y[1]: y/m
          y[2]: z/m
          y[3]: P_x*c
          y[4]: P_y*c
          y[5]: P_z*c
        Returns
          derivatives of y with respect to t
        '''
        #import pdb; pdb.set_trace()
        #print inspect.stack()[0][3], len(y)
        y = np.reshape(y, (len(y)/6, 6))
        #if len(f_args)>0:
        #    size = size()
        #    clock = f_args[:,0]*1e-9
        #    charge = f_args[:,1]
        #    index = f_args[:,2]
        #    status = f_args[:,3]
        if self.clock is not None:
            clock = self.clock
        else:
            clock = np.zeros(len(y))
        
        #Ex, Ey, Ez, Bx, By, Bz = np.zeros((len(y), 6)).T[:]
        Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(y[:,0], y[:,1], y[:,2], t+clock).T[:]
        
        #Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(y[0,0], y[0,1], y[0,2], t)[0]
        #import pdb; pdb.set_trace()
        
        Pxc, Pyc, Pzc = y[:,3], y[:,4], y[:,5]
        g_mec2_eV = g_mec2*1e6
        gamma = np.sqrt(g_mec2_eV**2+Pxc**2+Pyc**2+Pzc**2)/g_mec2_eV
        cc0 = g_c/gamma/g_mec2_eV
        cc1 = g_mec2_eV/g_c*gamma
        cc2 = 1./gamma/g_me*g_qe
        
        dydt = np.zeros((len(y), 6))
        dydt[:,0] = Pxc*cc0
        dydt[:,1] = Pyc*cc0
        dydt[:,2] = Pzc*cc0
        dydt[:,3] = (Pyc*Bz-Pzc*By+cc1*Ex)*cc2
        dydt[:,4] = (Pzc*Bx-Pxc*Bz+cc1*Ey)*cc2
        dydt[:,5] = (Pxc*By-Pyc*Bx+cc1*Ez)*cc2

        return dydt.flatten()
    
    def step_i(self, i):
        y0 = self.get(i)
        t0 = self.time

        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()
        r.integrate(r.t+self.dt)

        # self.set(i, r.y)
        return r.y 
    def step(self, nstep = 1, num_cores=1, NMAX = 10000, **kwagrs):
        '''
        Integrate all particles together
        Returns
          (x, y, z, Px*c, Py*c, Pz*c)
        '''
        
        while nstep>0:
            self.stepped += 1
            
            #if self.Lspch is True:
            #    sc3d = self.beam.get_sc3d(**kwagrs)
            #    self.em3d.update_external_field('spacecharge', sc3d)
            
            dist = self.beam.dist
            select = (dist[:,9]>0)
            dist = dist[select]
            if num_cores>1:
                # multi-core run
                # num = np.arange(self.NP)
                # results = Parallel(n_jobs=num_cores)(delayed(unwrap_step_i)(i) for i in zip([self]*len(num), num))
                # self.update(results)
                pass
            else:
                # run with one core
                nop = len(dist)
                if nop>NMAX:
                    nbeamlet = int(1.0*(nop-1)/NMAX)+1
                    #print inspect.stack()[0][3], 'beamlets: ', nbeamlet
                    r = ode(self.deriv).set_integrator('dopri5')
                    for i in np.arange(nbeamlet):
                        #print inspect.stack()[0][3], 'beamlet: #', i
                        i0 = i*NMAX
                        i1 = i*NMAX+NMAX
                        if i1>nop:
                            i1 = nop
                        y0 = dist[i0:i1,0:6].flatten()
                        t0 = self.time
                        
                        r.set_initial_value(y0, t0).set_f_params()
                        r.integrate(r.t+self.dt)
                        
                        dist[i0:i1,0:6] = np.reshape(r.y, (i1-i0, 6))
                else:
                    r = ode(self.deriv).set_integrator('dopri5')
                    y0 = dist[:,0:6].flatten()
                    t0 = self.time

                    r.set_initial_value(y0, t0).set_f_params()
                    r.integrate(r.t+self.dt)
                    
                    dist[:,0:6] = np.reshape(r.y, (nop, 6))
            
            self.beam.dist[select] = dist
            
            self.time += self.dt
            nstep = nstep-1
            
            self.beam.diagnose()
            self.zmean = self.beam.x[0]
            
            if self.stepped % self.diag_interval == 0:
                self.beam.diagnose('trk.diag.%03d' % self.Run)
            if self.stepped % self.dump_interval == 0:
                while int(self.zmean*self.dump_factor)<1:
                    self.dump_factor *= 10
                self.beam.dump('trk.%04d.%03d' % (self.zmean*self.dump_factor, self.Run))
            if self.stepped % 100 == 0:
                print str.format('Tracking: iter = %6d, time = %12.6E' % (self.stepped, self.time))
        return
    
    def is_emit(self):
        select = (self.beam.dist[:,9]==-1)|(self.beam.dist[:,9]==-3)
        if np.sum(select)>0:
            return True
        else:
            return False
    def cathode(self, namelist, fnamebase = 'trk.cathode'):
        r = [self.beam.x[0], self.time]
        for name in namelist:
            Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(0, 0, 0, self.time, namelist = [name]).T[:]
            r.append(Ez[0])
        r.append(self.beam.x[9]) # Qtot
        
        with open(fnamebase+('.%03d' % self.Run), 'a') as f_handle:
            np.savetxt(f_handle, np.atleast_2d(r), fmt='%14.6E')
         
    def emit(self, nstep = 1, num_cores=1, NMAX = 10000, NMIN = 30, **kwargs):
        '''
        Integrate all particles together
        Returns
          (x, y, z, Px*c, Py*c, Pz*c)
        '''
        
        while nstep>0:
            self.stepped += 1
            
            #if self.Lspch is True and self.stepped>5:
            #    sc3d = self.beam.get_sc3d(**kwargs)
            #    self.em3d.update_external_field('spacecharge', sc3d)
            
            # Get the particles not emitted yet
            # dist = self.beam.dist
            select = (self.beam.dist[:,9]==-1)|(self.beam.dist[:,9]==-3)
            # Get also the particles emitted already
            select2 = (self.beam.dist[:,9]>0)
            dt_max = self.dt
            #import pdb; pdb.set_trace()
            
            # Select those to be emitted
            if np.sum(select)<=NMIN:
                sort = (self.beam.dist[select,6]).argsort()[::-1]; # print sort
                t1 = self.beam.dist[select][sort][0,6] # first particle to emit this time
                tc = self.beam.dist[select][sort][-1,6] # first particle to emit next time
                dt_max = (t1-tc)*1.05e-9
            else:
                sort = (self.beam.dist[select,6]).argsort()[::-1]; # print sort
                t1 = self.beam.dist[select][sort][0,6]
                tc = self.beam.dist[select][sort][NMIN,6]
                select *= (self.beam.dist[:,6]>tc)
                dt_max = (t1-tc)*1e-9
            dist_emit = self.beam.dist[select]
            
            if 1: 
                # Particles emission one by one
                for i in np.arange(len(dist_emit)):
                    y0 = dist_emit[i,0:6]
                    
                    if self.time is None:
                        self.time = -t1*1e-9
                        print 'Initial time is set to ', self.time*1e12, ' ps'
                    
                    dt = dt_max-(t1-dist_emit[i,6])*1e-9
                    t0 = self.time+dt_max-dt
                    
                    Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(y0[0], y0[1], y0[2], t0).T[:]
                    if Ez[0]<=0:
                        dist_emit[i,9] = -89
                        continue
                    
                    r = ode(self.deriv).set_integrator('dopri5')
                    
                    r.set_initial_value(y0, t0).set_f_params()
                    r.integrate(r.t+dt)
                    
                    if not r.successful():
                        print inspect.stack()[0][3], 'Integration failed.'
                    
                    dist_emit[i,0:6] = r.y
                    if dist_emit[i,9] == -1:
                        dist_emit[i,9] = 5;
                    elif dist_emit[i,9] == -3:
                        dist_emit[i,9] = 3
                    else:
                        dist_emit[i,9] *= -1
            else:
                # Shift backwards such that all particles start movement at the same time
                nop = len(dist_emit)
                shift_dt = (t1-dist_emit[:,6])*1e-9
                Pxc, Pyc, Pzc = dist_emit[:,3], dist_emit[:,4], dist_emit[:,5]
                g_mec2_eV = g_mec2*1e6
                gamma = np.sqrt(g_mec2_eV**2+Pxc**2+Pyc**2+Pzc**2)/g_mec2_eV
                cc0 = g_qe/(gamma*g_me*g_c)
                dist_emit[:,0] += -Pxc*cc0*shift_dt
                dist_emit[:,1] += -Pyc*cc0*shift_dt
                dist_emit[:,2] += -Pzc*cc0*shift_dt

                y0 = dist_emit[:,0:6].flatten()
                dt = dt_max
                t0 = self.time
                r = ode(self.deriv).set_integrator('dopri5')

                r.set_initial_value(y0, t0).set_f_params()
                r.integrate(r.t+dt)
                if not r.successful():
                    print inspect.stack()[0][3], 'Integration failed.'
                
                dist_emit[:,0:6] = np.reshape(r.y, (nop, 6))
                dist_emit[:,9] = np.where(dist_emit[:,9]==-1, 5, -dist_emit[:,9])                   
            
            # Replace the distributions with those
            self.beam.dist[select] = dist_emit
            
            # Now it's turn to advance one step for those emitted already
            dist = self.beam.dist[select2]
            
            if num_cores>1:
                # multi-core run
                pass
            else:
                # run with one core
                nop = len(dist); # print inspect.stack()[0][3], nop
                if nop == 0:
                    pass
                elif nop>NMAX:
                    nbeamlet = int(1.0*(nop-1)/NMAX)+1
                    #print inspect.stack()[0][3], 'beamlets: ', nbeamlet
                    r = ode(self.deriv).set_integrator('dopri5')
                    for i in np.arange(nbeamlet):
                        #print inspect.stack()[0][3], 'beamlet: #', i
                        i0 = i*NMAX
                        i1 = i*NMAX+NMAX
                        if i1>nop:
                            i1 = nop
                        y0 = dist[i0:i1,0:6].flatten()
                        t0 = self.time
                        r.set_initial_value(y0, t0).set_f_params()
                        r.integrate(r.t+dt_max)
                        
                        if not r.successful():
                            print inspect.stack()[0][3], 'Integration failed.'
                        
                        dist[i0:i1,0:6] = np.reshape(r.y, (i1-i0, 6))
                else:
                    r = ode(self.deriv).set_integrator('dopri5')
                    y0 = dist[:,0:6].flatten()
                    t0 = self.time
                    r.set_initial_value(y0, t0).set_f_params()
                    r.integrate(r.t+dt_max)
                    
                    if not r.successful():
                        print inspect.stack()[0][3], 'Integration failed.'
                    
                    dist[:,0:6] = np.reshape(r.y, (nop, 6))
            
            self.beam.dist[select2] = dist
            
            self.time += dt_max
            nstep = nstep-1
            
            self.beam.diagnose()
            if self.stepped % self.diag_interval == 0:
                self.beam.diagnose('trk.diag.%03d' % self.Run)
            if self.stepped % self.dump_interval == 0:
                self.beam.dump('trk.%06d.%03d' % (self.stepped, self.Run))
            
            if self.stepped % 100 == 0:
                print str.format('Emitting: iter = %6d, time = %12.6E' % (self.stepped, self.time))
        return
    
    def ref(self, y0, t1, dt, t0 = 0):
        '''
        Returns
          2D arraylike: (x, y, z, P_x*c, P_y*c, P_z*3, t)
        '''

        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()

        sol = [list(y0)+[t0]+list(self.em3d.get_field(y0[0], y0[1], y0[2], t0)[0])]
        while r.successful() and r.t<t1:
            r.integrate(r.t+self.dt)
            
            a = list(r.y)+[r.t]+list(self.em3d.get_field(r.y[0], r.y[1], r.y[2], r.t)[0])
            sol.append(a)
        
        sol = np.array(sol)
        np.savetxt('trk.ref.001', sol, fmt = '%12.6E')
        return sol  
    def auto_phase(self, freq = 1.3e9, t1 = 1000e-12, dt = 10e-12):
        '''
        Returns
          2D arraylike: (x, y, z, P_x*c, P_y*c, P_z*3, t)
        '''
        y0 = self.beam.dist[0]
        dist = np.array([y0 for i in np.arange(901)])
        #print inspect.stack()[0][3], dist.shape
        
        phase = np.linspace(0, 90, 901)
        self.clock = phase/360./freq
        #print self.clock*1e12
        
        t0 = 0
        y0 = dist[:,0:6].flatten(); #print inspect.stack()[0][3], len(y0)
        
        #import pdb; pdb.set_trace()
        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()
        
        r.integrate(r.t+dt)
        #import pdb; pdb.set_trace()
        #print inspect.stack()[0][3], r.successful()
        if not r.successful():
            print inspect.stack()[0][3], 'Integration failed.'
        
        while r.successful() and r.t < t1:
            #print inspect.stack()[0][3], r.t
            r.integrate(r.t+dt)
        #print inspect.stack()[0][3], r.t
        
        dist = np.reshape(r.y, (len(r.y)/6, 6))
        np.savetxt('auto.dat', dist, fmt = '%12.6E')
        
        i, m = index_max(dist[:,5])
        p0 = phase[i]
        #select = (phase>p0-10)*(phase<p0+10)
        
        #fun = lambda x, a, b, c, d, e: a+b*x+c*x**2+d*x**3+e*x**4
        #popt, pcov = sp.optimize.curve_fit(fun, phase[select], 1./dist[select,5])
        #res = sp.optimize.minimize(fun, p0, tuple(popt))
        
        fig, ax = plt.subplots(figsize = (6, 4))
        ax.plot(phase[:], dist[:,5]/1e6, '-')
        #ax.plot(phase[select], 1./fun(phase[select], *popt)/1e6, '-')
        ax.grid()
        ax.set_xlabel(r'phase (degree)')
        ax.set_ylabel(u_kinetic)
        fig.savefig('phase_scan.eps')
        
        #return [res.x[0], momentum2kinetic(1./fun(res.x[0], *popt)/1e6)]
        return [p0, momentum2kinetic(m/1e6)]


def emitting():
    dist = np.loadtxt('beam1.ini')
    NMIN = 20

    select = (dist[:,9]==-1)|(dist[:,9]==-3); print np.sum(select)
    #import pdb; pdb.set_trace()

    if np.sum(select)<=NMIN:
        dist_emit = dist[select]
        sort = (dist[select,6]).argsort()[::-1]; # print sort
        t0 = dist[select][sort][0,6]
        tc = dist[select][sort][-1,6];  print tc, (t0-tc)*1e3
    else:
        sort = (dist[select,6]).argsort()[::-1]; # print sort
        t0 = dist[select][sort][0,6]
        tc = dist[select][sort][NMIN,6]; print tc, (t0-tc)*1e3
        select *= (dist[:,6]>tc)
        dist_emit = dist[select]

    dist_emit[:,9] = 5
    dist[select] = dist_emit

    np.savetxt('beam2.ini', dist, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4.0f%4.0f')

    fig, ax = plt.subplots(figsize = (6, 4))
    ax.hist(dist[:,6], bins = 20, range = (-0.003, 0.003))
    select = (dist[:,9]==5)
    ax.hist(dist[select,6], bins = 20, range = (-0.003, 0.003), histtype = u'step')
    return
#emitting()

'''
workdir = r"\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python\space-charge"
os.chdir(workdir)

field_maps = r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\field-maps'
gun3d = build_cavity(field_maps+os.sep+'gun42Cavity.txt', 0, 39.44e6, 1.3e9, 37)
sol3d = build_solenoid(field_maps+os.sep+'gunsolenoidsPITZ.txt', 0, -0.158797)

em3d = EMsolver()
em3d.add_external_field('gun', gun3d)
em3d.add_external_field('solenoid', sol3d)

#beam = Beam('beam.ini')
#sc3d = beam.get_sc3d()

#em3d.add_external_field('spacecharge', sc3d)


# In[563]:


import timeit

beam = Beam('beam1.ini'); # beam.dist=beam.dist[0:100000]
track = Tracking(beam, em3d, 1000e-12, 1e-12, out_inteval = 250)

n_steps = 2000
n_stepped = 0
stime = 0
while n_stepped<n_steps:
    time1 = timeit.default_timer()
    
    if track.is_emit():
        flag = 'Emitting'
        nstep = 1
        track.emit(nstep, NMIN = 100)
    else:
        flag = 'Tracking'
        nstep = 10
        track.step(nstep)
    
    time2 = timeit.default_timer()
    stime = stime+(time2-time1)

    n_stepped += nstep

    #print flag, ': 1 step takes: ', stime/n_stepped

print 'Total time:', stime
'''

# In[559]:

'''
data = np.loadtxt('trk.000050.001')
data[1:,2] += data[0,2]; data[1:,5] += data[0,5]
select = (data[:,9]>0)
print np.std(data[select,5]/1e3), np.mean(data[select,5]/1e3)
r = plt.hist(data[select,5]/1e3, bins = 30, histtype = 'step')
'''

# In[473]:

'''
beam = Beam('beam1.ini')
#beam.dist=beam.dist[0:100000]
track = Tracking(beam, em3d, 1000e-12, 1e-12)
#print track.beam.dist
import timeit
time1 = timeit.default_timer()
ref = track.step(1, NMAX = 3000)
#x = track.beam.dist[:,0]
#y = track.beam.dist[:,1]
#z = track.beam.dist[:,2]
#F2d = track.em3d.get_field(x, y, z, 0)
time2 = timeit.default_timer()
print time2-time1


beam = Beam('beam.ini')
track = Tracking(beam, em3d, 1000e-12, 1e-12)

import timeit
time1 = timeit.default_timer()
phi = track.auto_phase()
time2 = timeit.default_timer()
print time2-time1
print phi
'''

# In[238]:

'''
track = Tracking(beam, em3d, 1000e-12, 1e-12)
y0 = track.beam.dist[0,0:6]

import timeit
time1 = timeit.default_timer()
ref = track.ref(y0, 1000e-12, 10e-12)
time2 = timeit.default_timer()
print time2-time1
print track.time

fig, [ax1, ax2] = plt.subplots(figsize = (6, 8), nrows = 2)
Ek = momentum2kinetic(ref[:,5]/1e6); # print Ek[-1]
ax1.plot(ref[:,2], Ek, '-')
ax1.grid()

ax2.plot(ref[:,2], ref[:,9]/1e6, '-')
ax2.grid()
'''