# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 12:13:21 2021

@author: lixiangk
"""

from universal import *
from astra_modules import *
from my_object import *

color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']

def plot_config():
    from cycler import cycler
    from matplotlib.ticker import AutoMinorLocator

    fsize = 12 # a quarter of the paper width: 20 pt; half of the paper width: 12
    font = {'size' : fsize, 'family' : 'serif'}
    color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
    linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
    marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']
    
    mpl.rc('font', **font)
    mpl.rc('xtick', labelsize = 10, direction = 'in', top   = True)
    mpl.rc('ytick', labelsize = 10, direction = 'in', right = True)
    mpl.rc('xtick.major', size = 5, width = 1)
    mpl.rc('ytick.major', size = 5, width = 1)
    mpl.rc('xtick.minor', size = 3, width = 0.7, visible = True)
    mpl.rc('ytick.minor', size = 3, width = 0.7, visible = True)
    
    mpl.rc('lines', linewidth=2, markersize=6, color='r')
    # mpl.rc('lines', linestyle = 'solid')
    mpl.rc('axes', labelpad = 0, prop_cycle=(cycler('color', color_cycle) + cycler('linestyle', linestyle_cycle) + cycler('marker', marker_cycle)))
    mpl.rc('legend', fontsize = 12, labelspacing = 0.05, handletextpad=0.4, frameon=False, handlelength=2.1)
    
    mpl.rc('figure', dpi = 100, figsize = (4, 4))
    mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)
    
    mpl.rc('image', cmap = 'jet')
    
    return
plot_config()

rootdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\work'
simdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim'

u_emi_xy = r'$\varepsilon_{n, xy}$ (mmmrad)'
u_rms_xy = r'$\sigma_{xy}$ (mm)'

#%% Initial distribution
workdir = r'D:\PITZ\2020\LPS\solScan\Q-250.00pC-D-1.00mm-E1-55.90MV_m-phi1-0.00deg-E2-14.50MV_m-phi2-0.00deg-I-362.00A'
os.chdir(workdir)

data = np.loadtxt('beam_i.ini')
data[1:,5] += data[0,5]

plt.figure()
plt.hist(data[:,3], 100, histtype = r'step')
plt.hist(data[:,4], 100, histtype = r'step')
plt.hist(data[:,5], 100, histtype = r'step')
plt.legend(['Px', 'Py', 'Pz'])

P = np.sqrt(data[:,3]**2+data[:,4]**2+data[:,5]**2)
Pz = data[:,5]

Ek = M2K(P/1e6)*1e6

reset_margin()
fig, ax = plt.subplots(ncols = 3, figsize = (12, 4))

ax[0].hist(Ek, bins = 100)
ax[0].set_title('dE = %.2f eV' % (np.std(Ek)))

ax[1].hist(P, bins = 100)
ax[1].set_title('dP = %.2f eV/c' % (np.std(P)))

ax[2].hist(Pz, bins = 100)
ax[2].set_title('dPz = %.2f eV/c' % (np.std(Pz)))


#%% Solenoid scan

workdir = os.path.join(simdir,'2020', 'MBI')
#workdir = r'D:\PITZ\2020\MBI\solenoid-scan-7ps-mytracker'
os.chdir(workdir)

fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (6, 3))
#ax2 = ax.twinx()

start_index = 7
diag = BeamDiagnostics()
idx_nemit_x = start_index+diag.idx_nemit_x

data = np.loadtxt('ParaScan_21MeV_7ps@laserOffset2.dat')

var = [3000, 3500, 4000, 4500]
var = [123, 22, 24]
var = [223, 222, 224]
#var = [222, 22]

color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k', 'tab:purple', 'tab:brown']

res = []
for i, v in enumerate(var): 
    select = np.isclose(data[:,-1], v)*np.isclose(data[:,0], 4000)
    d1 = data[select]; print(d1[:,-1])
    #d1 = data
    
    #ax1.plot(d1[:,6], d1[:,idx_nemit_x], '-*')
    #ax1.plot(d1[:,6], d1[:,idx_nemit_x+1], '-*')
    ax1.plot(d1[:,6], np.sqrt(d1[:,idx_nemit_x]*d1[:,idx_nemit_x+1]), '-*')
    
    #ax2.plot(d1[:,6], d1[:,idx_nemit_x+2], '-*')
    #ax2.plot(d1[:,6], d1[:,idx_nemit_x+3], '-*')
    ax2.plot(d1[:,6], np.sqrt(d1[:,idx_nemit_x+2]*d1[:,idx_nemit_x+3]), '-*')
    
for axis in [ax1, ax2]:
    axis.set_xlabel(r'$I_{\rm main}$ (A)')
    axis.grid()
    axis.legend([r'$x$', r'$y$', r'$z$'])
    axis.legend([r'no offset', r'Xoff = Yoff = 0.5 mm'], loc = 'upper left', fontsize = 10)
    axis.legend([r'Symmetric', r'VC2', r'VC2+QEmap'], loc = 'upper left', fontsize = 10)
    axis.set_xlim(380, 394)

ax1.set_ylabel(u_emi_xy)
ax1.set_ylim(3, 8)
#ax3.set_ylim(30, 55)
#ax1.set_yticks(np.arange(0, 16, 3))
#ax1.xaxis.set_minor_locator(plt.MultipleLocator(50))

ax2.set_ylabel(u_rms_xy)
#ax3.set_ylabel(r'Higher order $\Delta P$ (keV/c)')


fig.tight_layout()

for ext_fig in ['.png', '.pdf']:
    fig.savefig('Xemit100-vs-Imain@2D+QE'+ext_fig)

#%% Solenoid scan 2 
workdir = os.path.join(simdir,'2020', 'MBI')
workdir = r'D:\PITZ\2020\MBI\solenoid-scan-7ps-mytracker'
workdir = r'D:\PITZ\2020\MBI\solenoid-scan-7ps-2021'
os.chdir(workdir)

fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (6, 3))
#ax2 = ax.twinx()

start_index = 7
diag = BeamDiagnostics()
idx_nemit_x = start_index+diag.idx_nemit_x

color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k', 'tab:purple', 'tab:brown']

res = []

v = 4500

fname_list= ['ParaScan_21MeV_7ps@trk3.dat', 'ParaScan_21MeV_7ps@trk2.dat', 'ParaScan_21MeV_7ps@trk4.dat']
fname_list = ['ParaScan_21MeV_7ps@5.28m1.dat', 'ParaScan_21MeV_7ps@5.28m2.dat', 'ParaScan_21MeV_7ps@5.28m3.dat']
for fname in fname_list:
    
    data = np.loadtxt(fname)
    select = np.isclose(data[:,0], v);d1 = data[select]
    #d1 = data
    
    ax1.plot(d1[:,6], np.sqrt(d1[:,idx_nemit_x]*d1[:,idx_nemit_x+1]), '-*')
    ax2.plot(d1[:,6], np.sqrt(d1[:,idx_nemit_x+2]*d1[:,idx_nemit_x+3]), '-*')

    #res.append([])


for axis in [ax1, ax2]:
    axis.set_xlabel(r'$I_{\rm main}$ (A)')
    axis.grid()
    axis.legend([r'Gaussian truncation', r'2D_3D 0.1 m', r'2D_3D 0.025 m'], loc = 'upper left', fontsize = 10)


ax1.set_ylabel(u_emi_xy)

#ax1.set_xlim(360, 370)
ax1.set_ylim(3, 8)
#ax3.set_ylim(30, 55)
#ax1.set_yticks(np.arange(0, 16, 3))
#ax1.xaxis.set_minor_locator(plt.MultipleLocator(50))

ax2.set_ylabel(u_rms_xy)
#ax3.set_ylabel(r'Higher order $\Delta P$ (keV/c)')


fig.tight_layout()
fig.savefig('Xemit100-vs-Imain@4500pC.pdf')

#%% Space charge 3D by FFT
from tracker.SpaceCharge3DFFT import *

workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\MBI\D-4.00mm-I-380A'
os.chdir(workdir)

dist = pd_loadtxt('trk.004000.001')

dist[1:,2] += dist[0,2] 
dist[1:,5] += dist[0,5]

Nx, Ny, Nz = 32, 32, 32
Nc = 5

xf, yf, zf = 0*sigma_x, 0, 0*sigma_x

import timeit
sc3d = SpaceCharge3DFFT(Nx, Ny, Nz, Nc, debug = 1, Lmirror = True)
time1 = timeit.default_timer()
sc3d.get_rho_3D(dist = dist, Lemit = False, Lmirror = False)
sc3d.build(Xshift = (xf, yf, zf))
time2 = timeit.default_timer()
print('time elapsed', time2-time1)

#%% Plot for SCO simulation
workdir = r'C:\Users\lixiangk\Desktop\sco-triplet-tuning4\Q1--0.15T_m-Q2-0.54T_m-Q3--1.06T_m-Q4-0.78T_m'
os.chdir(workdir)

reset_margin(left = 0.1, right = 0.9)
fig, ax = plt.subplots(figsize = (4.5, 3))

data = np.loadtxt('BeamDynamics.dat')
data[:,0] += 5.28

select = data[:,0]>16.3; data = data[select]; 

ax.plot(data[:,0], data[:,3], 'r-')
ax.plot(data[:,0], data[:,4], 'b-')

ax.grid()
ax.legend(['$x$', '$y$'])

ax.set_xlabel('z (m)')
ax.set_ylabel('RMS size (mm)')

ax.set_xlim(16, 28.5)
ax.set_ylim(0, 2.5)

#fig.tight_layout()
fig.savefig('matched-transport-simulation.pdf')

#%% Phase space
#workdir = r'D:\PITZ\2020\LPS\250pC2'
#os.chdir(workdir)
#fname = 'ast.0528.001'

#fname = r'GUN_250pC_mbi6ps1mm1Csig.0528.001'
fname = r'ast.0528.002'

data = pd_loadtxt(fname)
data[1:,2] += 0
data[1:,5] += 0

zz = data[1:,2]
Pz = data[1:,5]
Pz -= np.mean(Pz)

select = (zz>-1e-3)*(zz<1e-3)
zz = zz[select]
Pz = Pz[select]

f = lambda x, a, b, c: a+b*x+c*x**2
f = lambda x, a, b: a+b*x
popt, pcov = curve_fit(f, zz*1e3, Pz/1e3)

zz = data[1:,2]
Pz = data[1:,5]
x, y = -zz/g_c*1e12, Pz/1e3+30 #-f(zz*1e3, *popt)
#x = zz*1e3

w = -data[1:,7]*1e-9/g_qe

reset_margin(bottom = 0.3, top = 0.995)
aX, aY, signal = plot2d(x = x, y = y, bins = (200, 500),
       xlabel = r'$t$ (ps)', ylabel = r'$\Delta P_z$ (keV/$c$)',
       figname = 'zpz@'+fname, fig_ext = '.png', extent = [-15, 15, -100, 100],
       returned = True)

# np.savetxt('aX_remove_1st.dat', aX, fmt = '%14.6E')
# np.savetxt('aY_remove_1st.dat', aX, fmt = '%14.6E')
# np.savetxt('signal_remove_1st.dat', aX, fmt = '%14.6E')
#%%
plottype = 'imshow'
#plottype = 'contourf'

reset_margin()
fig, ax = plt.subplots(figsize = (5, 2.5))

if plottype is 'contourf':
    
    # Noise level for 2D plot
    noise = 0.001
    v = np.linspace(noise, 1, 99)*np.max(signal)

    cax = ax.contourf(aX, aY, signal[::-1,::-1], v, linestyles = None, zorder=-9)
    ax.set_rasterization_zorder(-1)
    #ax.set_aspect('auto')
    
elif plottype is 'imshow':
    
    tmp = np.copy(signal)
    tmp[tmp==0] = np.nan
    cax = ax.imshow(tmp[:,:].T, extent = [aX.min(), aX.max(), aY.min(), aY.max()],
                    aspect = 'auto', origin = 'lower')
    del tmp

cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04) #, format=format)
#cbar.set_ticks(np.linspace(0, 900, 10))
cbar.ax.tick_params(labelsize = 9, pad = 2)
cbar.ax.set_ylabel('Pixels', labelpad = 2)
#ax.grid()

# ax.set_title(title, fontsize = 11)
# ax.set_xlabel(r'$x$ (mm)')
# ax.set_ylabel(r'$y$ (mm)')

# xmax = 8
# ax.set_xlim(-xmax, xmax)
# ax.set_ylim(-xmax, xmax)
# ax.grid()
#%%
def plot2d(x, y, weights = None, xlabel = r'$x$ (mm)', ylabel = r'$y$ (mm)', figname = 'xy2d',\
           fig_ext = '.eps', vmin = 0.0001, vmax = 1., bins = None, extent = None, returned = False, **kwargs):
    '''
    Parameters
      bins: None or [xbins, ybins], set the sampling frequencies for x and y
      extent: None or [xmin, xmax, ymin, ymax], set the lower limit and upper limit of the x-axis and y-axis
    '''
    if bins == None:
        xbins = ybins = 100
    else:
        xbins, ybins = bins
    if extent == None:
        xs = np.sort(x[::5])
        xavg = np.mean(xs); dx = xs[-int(0.005*len(xs))]-xs[int(0.005*len(xs))]
        xavg = np.mean(xs); dx = np.max(xs)-np.min(xs)
        xmin, xmax = xavg-dx*0.75, xavg+dx*0.75; print(xavg, dx, xmin, xmax)
        
        ys = np.sort(y[::5])
        yavg = np.mean(ys); dy = ys[-int(0.005*len(ys))]-ys[int(0.005*len(ys))]
        yavg = np.mean(ys); dy = np.max(ys)-np.min(ys)
        ymin, ymax = yavg-dy*0.75, yavg+dy*0.75
    else:
        xmin, xmax, ymin, ymax = extent
    print('extent: ', xmin, xmax, ymin, ymax)
    
    fig = plt.figure(figsize=(5, 2.5))
    ax1 = fig.add_axes([0.18, 0.15, 0.675, 0.15]); ax1.patch.set_alpha(0)
    ax3 = fig.add_axes([0.18, 0.15, 0.125, 0.80]); ax3.patch.set_alpha(0)
    ax2 = fig.add_axes([0.18, 0.15, 0.75, 0.80]); ax2.patch.set_alpha(0)

    cnts, cens = hist_centered(x, bins=xbins, range=(xmin,xmax), weights=weights)
    ax1.plot(cens, smooth_easy(cnts, 4), 'b-')

    # vmin is the lower limit of the colorbar, and is free to change
    # xs, ys, cs = hist2d_scattered(x, y, bins=[xbins, ybins], range=[[xmin, xmax],[ymin, ymax]], weights=weights)
    # cax = ax2.scatter(xs, ys, s=2, c=cs/np.max(cs), edgecolor='', vmin=vmin, norm=mpl.colors.LogNorm())
   
    ax, ay, signal = hist2d_contourf(x, y, bins=[xbins, ybins],
                                     range=[[xmin, xmax],[ymin, ymax]],
                                     weights=weights)
    signal = np.abs(signal)
    signal = signal/np.max(signal)
    v = np.linspace(vmin, vmax, 999)
    #from matplotlib import ticker
    cax = ax2.contourf(ax, ay, signal, v)
    
    ## Estimate the 2D histograms
    #H, xedges, yedges = np.histogram2d(x, y, bins = (xbins, ybins), range = [[xmin, xmax],[ymin, ymax]], weights = weights)
    #H = np.abs(H)
    #H /= np.max(H)
    ## H needs to be rotated and flipped
    #H = np.rot90(H)
    #H = np.flipud(H)
    ## Mask zeros
    #Hmasked = np.ma.masked_where(H==0, H) # Mask pixels with a value of zero
    ## Plot
    #cax = ax2.pcolormesh(xedges, yedges, Hmasked)
    
    #cbar = fig.colorbar(cax)
    cbar = fig.colorbar(cax, fraction=0.09, pad=0.01, format = '%.1f')
    cbar.set_ticks([vmin, 1.0])
    cbar.ax.tick_params(labelsize=11, pad=0)

    cnts, cens = hist_centered(y, bins=ybins, range=(ymin,ymax), weights=weights)
    ax3.plot(smooth_easy(cnts, 16), cens, 'b-')

    ax1.axis('off')
    ax1.set_xlim(xmin, xmax)

    #ax2.set_xlim(xmin, xmax)
    #ax2.set_ylim(ymin, ymax)
    
    ax2.set_xlabel(xlabel)
    ax2.set_ylabel(ylabel, labelpad=-1)
    ax2.minorticks_on()
    ax2.grid()

    ax3.axis('off')
    ax3.set_ylim(ymin, ymax)
    
    ax2.set_ylim(-100, 100)
    ax3.set_ylim(-100, 100)

    fig.savefig(figname+fig_ext)
    
    if returned:
        return [ax, ay, signal]
    return

#%%
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2021\LPS@CW\emissionScan\Q-70.00pC-D-0.84mm-E1-30.00MV_m-phi1-0.00deg-E2-16.33MV_m-phi2-0.00deg-I-210.00A'
os.chdir(workdir)

data = pd_loadtxt('ast.0050.001')

fig, ax = plt.subplots()

select = data[:,-1]>0
_ = ax.hist(data[select,7], 100, histtype = r'step'); print(np.sum(data[select,7]))

select = data[:,-1]<0
_ = ax.hist(data[select,7], 100, histtype = r'step'); print(np.sum(data[select,7]))


#%% 250 pC with EMSY1 slit
aDir = 'Q-100.00pC-D-0.92mm-E1-40.00MV_m-phi1-0.00deg-E2-15.00MV_m-phi2--4.50deg-I-271.00A'
path = simdir+os.sep+'2021'+os.sep+'LPS@CW'+os.sep+aDir
fname = 'ast.0528.002'

# aDir = 'Q-100.00pC-D-1.00mm-E1-30.00MV_m-phi1-0.00deg-E2-15.00MV_m-phi2--1.50deg-I-210.00A'
# path = simdir+os.sep+'2021'+os.sep+'LPS@CW'+os.sep+aDir
# fname = 'ast.0528.001'

data = pd_loadtxt(path+os.sep+fname)

# Get reference z and P distributions
zRef = data[:,2]
PRef = data[:,5]

#select = (data[:,0]>-25e-6)*(data[:,0]<25e-6)
#data = data[select]; print(len(data))

#zRef = zRef[select]
#PRef = PRef[select]

#astra_format = '%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%4d%4d'
#np.savetxt(fname+'@50um', data, fmt = astra_format)

x = -zRef[1:]/g_c*1e12 
y = PRef[1:]/1e3+25

#select = (x>-2)*(x<2)
#popt, _ = curve_fit(linear, x, y)

plot2d(x = y, y = x, bins = (100, 100),
       ylabel = r'$t$ (ps)', xlabel = r'$\Delta P_z$ (keV/$c$)',
       figname = 'zpz@40MV@4.5deg@'+fname, fig_ext = '.png',
       extent = [-100, 100, -15, 15])

#%%
reset_margin()
fig, ax = plt.subplots(figsize = (3.3, 2.2))

d1 = np.loadtxt('Xemit100-vs-BSA1_CH@40MV.dat')
ax.plot(d1[:,0], d1[:,3], 'b-D')

d1 = np.loadtxt('Xemit100-vs-BSA_CH@30MV.dat')
ax.plot(d1[:,0], d1[:,3], 'r-o')

ax.set_xlim(0.6, 1.4)
ax.set_ylim(0.3, 0.7)
ax.set_yticks([0.3, 0.4, 0.5, 0.6, 0.7])
ax.legend(['40 MV/m', '30 MV/m'])

ax.grid()

ax.set_xlabel(r'BSA (mm)')
ax.set_ylabel(r'Norm. emit. ($\mu$m)')

fig.savefig('Xemit-vs-BSA-vs-gunGrad.png')
