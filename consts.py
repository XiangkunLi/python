import scipy as sp
from scipy.constants import codata

g_h   =codata.value('Planck constant')
g_hbar=codata.value('Planck constant over 2 pi')

g_g   =codata.value('standard acceleration of gravity')
g_c   =codata.value('speed of light in vacuum')

g_qe  =codata.value('elementary charge')
g_me  =codata.value('electron mass')
g_mec2=codata.value('electron mass energy equivalent in MeV')
g_eV  =codata.value('electron volt')

g_kB  =codata.value('Boltzmann constant')
g_sigma =codata.value('Stefan-Boltzmann constant')

g_eps0=codata.value('electric constant')
g_mu0 =codata.value('mag. constant')
