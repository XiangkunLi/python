
# coding: utf-8

# In[1]:

import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt

from consts import * 
import os

# get_ipython().magic(u'matplotlib inline')


# ## 运动粒子坐标变换
# ---
# 假设有坐标系$x-y$和$x'-y'$，二者之间的变换关系为：将$x-y$绕原点旋转$\alpha$，再平移$\Delta$后，与$x'-y'$重合。则$x'-y'$坐标系中的任意一点$(x',y')$，与其在坐标系$x-y$中对应坐标的关系为：
# 
# \begin{align}
#   X  &= MX'+\Delta \\
#   X' &= M^{-1}(X-\Delta) 
# \end{align}
# 
# 其中，$M$为从$x'-y'$到$x-y$的旋转矩阵，$\Delta$为$x'-y'$坐标系的原点在$x-y$坐标系中的坐标。
# 
# 与空间坐标相比，速度的变换方式稍有不同：
# 
# \begin{align}
#   V  &= MV' \\
#   V' &= M^{-1}V 
# \end{align}
# 
# 将$x'-y'$坐标系旋转$\alpha$度后得到$x-y$坐标系，则任意一点$(x,y)$在两组坐标系中的坐标满足
# 
# \begin{align}
#   x &= \cos(\alpha)x'+\sin(\alpha)y' \\
#   y &= -\sin(\alpha)x'+\cos(\alpha)y'
# \end{align}
# 
# 因此
# 
# \begin{equation}
# M=\left(\begin{array}{cc}
#   \cos(\alpha) & \sin(\alpha)\\
#   -\sin(\alpha) & \cos(\alpha)
# \end{array}\right)
# \end{equation}
# 

# In[2]:

def transform2D(r,alpha,delta=[0,0]):
    'rotated about z-axis. alpha is the angle from x-y to x1-y1.'
    cc,ss=np.cos(alpha),np.sin(alpha)
    [x,y]=r
    dx,dy=delta[0],delta[1]
    x1,y1=x*cc+y*ss+dx,-x*ss+y*cc+dy
    return [x1,y1]


# ## Example of transform for Astra

# In[29]:

'''
workdir='D:\\Desktop\\THz-FEL\\10ps\\Part2.1\\0.03245T'
os.chdir(workdir)

data=np.loadtxt('ast.0840.001')

#data[0,2]=0 #data[0,2]-8.3
data[1:,2]=data[1:,2]+data[0,2]
data[1:,5]=data[1:,5]+data[0,5]

xc=np.mean(data[:,0])
data[:,0]=data[:,0]-xc
zc=np.mean(data[:,2])
data[:,2]=data[:,2]-zc

fig,axes=plt.subplots(nrows=1, ncols=2, figsize=(12, 4))
axes[0].plot(data[:,2],data[:,0],'r.')
axes[1].plot(data[:,5],data[:,3],'r.')
axes[0].grid()
axes[1].grid()
'''

