import sys
sys.path.append('/afs/ifh.de/group/pitz/data/lixiangk/work/sync/python')

from _differentialevolution99 import *
from astra_modules import *
from my_object import *

from timeit import default_timer
import numpy as np

import time

from scipy.interpolate import interp1d, interp2d

import matplotlib as mpl
import matplotlib.pyplot as plt
plt.switch_backend('agg')

#x = [1, 1, 1, 0]
#obj = obj_und(x)
#exit()

print os.getcwd()

fig_ext = '.eps'
prefix = 'ast'
suffix = '001'

xmin, xmax = 24.5, 30.5

#plot_avg_xy(prefix = prefix, suffix = suffix, extent = None, figsize=(8, 4))
plot_rms_xy(prefix = prefix, suffix = suffix, extent = [xmin, xmax, 0, 10], figsize=(8, 4))
plot_emi_xy(prefix = prefix, suffix = suffix, extent = [xmin, xmax, 0, 20], figsize=(8, 4))
plot_kin(   prefix = prefix, suffix = suffix, extent = [xmin, xmax, 0, 18], figsize=(8, 4))

exit()


Ipart = 50000

var1 = np.array([20, 15, 10, 5, 2, 1, -2])
#var1 = np.array([0, 0.25, 0.5, 0.75, 1.0])
var2 = np.array([0])
combi = np.array([[1, 1, v1, v2] for v1 in var1 for v2 in var2])

fig_ext = '-alpha_x.png'

dirs = [str.format(r'n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
                            (Ipart/1000., x[0], x[1], x[2], x[3])) for x in combi]
lgds = [str.format(r'alpha_x = %.2f' % x) for x in var1]
print dirs, lgds

def plot_add(dirs, lgds, direction = 'x', index = 5, xlabel = u_z, ylabel = u_emi_x):
    if direction == 'x':
        fname = 'ast.Xemit.001'
    elif direction == 'y':
        fname = 'ast.Yemit.001'
    elif direction == 'z':
        fname = 'ast.Zemit.001'

    if direction == 'x' or direction == 'y':
        if index == 2:
            y = 'avg'
        elif index == 3:
            y = 'rms'
        elif index == 5:
            y = 'emi'
    
    fig, ax = plt.subplots()
    for direc in dirs:
        data = np.loadtxt(direc+'/'+fname)
        ax.plot(data[:,0], data[:,index], '-')

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_ylim(0, 3)
    ax.legend(lgds)
    #ax.set_ylim(4.2, 4.5)
    fig.savefig(direction+y+'-z'+fig_ext)
    return

plot_add(dirs, lgds, index = 3, ylabel = u_rms_x); exit()
plot_add(dirs, lgds)
plot_add(dirs, lgds, index = 2, ylabel = r'$x_{\rm avg}$ (mm)')


plot_add(dirs, lgds, direction = 'y', ylabel = u_emi_y)
plot_add(dirs, lgds, direction = 'y', index = 2, ylabel = r'$y_{\rm avg}$ (mm)')
plot_add(dirs, lgds, direction = 'y', index = 3, ylabel = u_rms_y)

exit()
