import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
import os

import sys
if sys.platform == "linux" or sys.platform == "linux2":
    #sys.path.append(r'/afs/ifh.de/group/pitz/data/lixiangk/work/sync/python')
    mkdir = "mkdir -p "
    pass
elif sys.platform == "win32":
    #sys.path.append(r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python')
    mkdir = "mkdir "
    pass
elif sys.platform == "darwin":
    print "OS X"
else:
    print "Unknown platform!"
    
I2B = lambda I: -(0.0000372+0.000588*I)
B2I = lambda B: (-B-0.0000372)/0.000588


def exist_(st, fname):
    '''
    Check if a string `st` exists in the file `fname`
    '''
    exist = False
    if os.path.exists(fname):
        log = open(fname)
        for i, line in enumerate(log):
            if st in line:
                exist = True
    return exist

class Module:
    '''
    Modules in Astra: newrun, output, scan, modules, error, charge, aperture,
                      wake, cavity, solenoid, quadrupole, dipole, laser
    Example:
      To add a `newrun` module, call
          newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation',\
                           Distribution = 'beam.ini', Auto_Phase = True, Track_All = True,\
                           check_ref_part = False, Lprompt = False, Max_step=200000)
      The inputs for newrun could be changed by 
          newrun.reset(Run = 2)
    '''
    def __init__(self, name = 'Module', **kwargs):
        self.name = name.upper()
        self.kv = {}
        for key, value in kwargs.items():
            self.kv.update({key.upper():value})
        self.build()
    def set(self, *args, **kwargs):
        if len(args)>0:
            self.name = args[0].upper()
        # self.kv.update(kwargs)
        for key, value in kwargs.items():
            self.kv.update({key.upper():value})
        self.build()
        return
    def reset(self, key, value, append = False):
        key = key.upper()
        self.kv.update({key.upper():value})
        self.build()
        return
    def delete(self, key):
        key = key.upper()
        if key in self.kv.keys:
            self.kv.pop(key)
    def build(self):
        output = '&'+self.name+' \n'
        for k, v in self.kv.items():
            if isinstance(v, int) and not isinstance(v, bool):
                output += str.format(' %s=%d\n' % (k, v))
            elif isinstance(v, float):
                output += str.format(' %s=%f\n' % (k, v))
            elif isinstance(v, str):
                output += str.format(' %s=\'%s\'\n' % (k, v))
            elif isinstance(v, bool):
                output += str.format(' %s=%s\n' % (k, str(v)))
            elif isinstance(v, (list, np.ndarray)):
                for i, vi in enumerate(v):
                    if isinstance(vi, int) and not isinstance(vi, bool):
                        output += str.format(' %s(%d)=%d\n' % (k, i+1, vi))
                    elif isinstance(vi, float):
                        output += str.format(' %s(%d)=%f\n' % (k, i+1, vi))
                    elif isinstance(vi, str):
                        output += str.format(' %s(%d)=\'%s\'\n' % (k, i+1, vi))
                    elif isinstance(vi, bool):
                        output += str.format(' %s(%d)=%s\n' % (k, i+1, str(vi)))    
        output += '&\n\n'
        self.output = output

class Generator1(Module):
    '''
    Renamed to `Generator1` to distinguish from `Platypus/Generator` 
    '''
    def __init__(self, **kwargs):
        Module.__init__(self, name = 'Input', **kwargs)
        self.set(Lprompt = False)
    def write(self, filename = 'gen.in'):
        ff = open(filename, 'w')
        ff.write(self.output)
        ff.close()
        return

class Astra:
    def __init__(self):
        self.modules = {}
    def add_module(self, module):
        self.modules.update({module.name:module})
    def add_modules(self, modules):
        for module in modules:
            self.add_module(module)
    def write(self, filename = 'ast.in'):
        output = ''
        for name, module in self.modules.items():
            output = module.output+output
        ff = open(filename, 'w')
        ff.write(output)
        ff.close()
        return
    def run(self, fname = None):
        if fname is None:
            fname = 'ast.in'
        
        name = fname.split('.in')[0]
        logfile = name+'.log'        
        finished = exist_('finished simulation', logfile)
        
        if not finished:
            #os.system('generator gen.in 2>&1 | tee gen.log')
            os.system('astra '+fname+' 2>&1 | tee '+logfile)
        
        return
    def qsub(self, job_name, ast_name, gen_name = None, direc = '.'):
        '''
        Write the batch file to the `filename`, which could be submitted to a server by "qsub filename"
        '''
        if gen_name is None:
            gen_name = 'gen.in'
        ss = '''\
#!/bin/zsh
#
#$ -cwd
#$ -o '''+job_name+'''.o
#$ -e '''+job_name+'''.e
#$ -V
#$ -l h_cpu=12:00:00
#$ -l h_rss=2G
#$ -P pitz
##$ -pe multicore 2
##$ -R y

'''
        chdir = '''cd '''+direc+'''
'''
        if gen_name is not None:  
            run_generator = '''generator '''+gen_name+'''.in > gen.log
'''
        else:
            run_generator = '''\n'''
        run_astra = '''astra '''+ast_name+'''.in > ast.log
'''
        ss = ss+chdir+run_generator+run_astra
        
        ff = open(job_name+'.sh', 'w')
        ff.write(ss)
        ff.close()
        return


def astra2slice(fname, fout = None, nslice = 100, nc = 5):
    '''
    The output file follows the format required for Genesis 1.3 simulation.
    From left the right, the column are:
      ZPOS GAMMA0 DELGAM EMITX EMITY BETAX BETAY XBEAM YBEAM PXBEAM PYBEAM ALPHAX ALPHAY CURPEAK ELOSS
    Parameters
      fname: filename of Astra output
      fout: filename of the output
      nslice: number of slices
      nc: number of slices to discard on the sides
    '''
    header = '? VERSION = 1.0\n'+\
    '? COLUMNS ZPOS GAMMA0 DELGAM EMITX EMITY BETAX BETAY XBEAM YBEAM PXBEAM PYBEAM ALPHAX ALPHAY CURPEAK ELOSS'

    data = np.loadtxt(fname)
    data[1:,2] += data[0,2]
    data[1:,5] += data[0,5]
    
    nop = len(data[:,0])
    #select = (data[:,0]>=-5.5e-3)*(data[:,0]<=5.5e-3)*(data[:,1]>=-2.5e-3)*(data[:,1]<=2.5e-3)
    select = (data[:,9]>0); data = data[select]
    nop = len(data[:,0])
    
    nc = nc
    zmin, zmax = np.min(data[:,2]), np.max(data[:,2])
    dz = (zmax-zmin)/(nslice+nc*2)
    zmin += nc*dz
    zmax -= 1*dz
    dz = (zmax-zmin)/nslice
    
    r = []; zpos = 0;
    for i in np.arange(nslice+1):
        zmin_i, zmax_i = zmin+i*dz, zmin+i*dz+dz
        select = (data[:,2]>=zmin_i)*(data[:,2]<=zmax_i)
        beam_i = data[select]
        
        if len(beam_i)>0:
            diag = BeamDiagnostics(dist = beam_i);
            Ipeak = -diag.Q_b*1e-12/dz*g_c
            if i == 0:
                r.append([zpos, kinetic2gamma(diag.Ekin), diag.std_Ekin*1e-3/g_mec2, \
                          diag.nemit_x*1e-6, diag.nemit_y*1e-6, diag.beta_x, diag.beta_y, \
                          0, 0, 0, 0, diag.alpha_x, diag.alpha_y, 0, 0])
                zpos += dz/2
            r.append([zpos, kinetic2gamma(diag.Ekin), diag.std_Ekin*1e-3/g_mec2, \
                 diag.nemit_x*1e-6, diag.nemit_y*1e-6, diag.beta_x, diag.beta_y, \
                 0, 0, 0, 0, diag.alpha_x, diag.alpha_y, Ipeak, 0])
            zpos += dz
            if i == nslice:
                zpos -= dz/2.
                r.append([zpos, kinetic2gamma(diag.Ekin), diag.std_Ekin*1e-3/g_mec2, \
                          diag.nemit_x*1e-6, diag.nemit_y*1e-6, diag.beta_x, diag.beta_y, \
                          0, 0, 0, 0, diag.alpha_x, diag.alpha_y, 0, 0])
        
    np.savetxt(fout, np.array(r), fmt = '%-15.6e', \
               header = header, comments='')
    return np.array(r)


field_maps = os.path.join('.')

def astra_demo(x, *args, **kwargs):
    '''
    Created on Oct. 29, 2019
    Simulation from photocathode to EMSY1 (5.277 m)
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
      *args: 
      **kwargs: 
    Returns:
      a batch file for FARM
    '''

    if 'FARM' in kwargs.keys():
        FARM = kwargs['FARM']
    else:
        FARM = True
    
    Ipart = 100000
    BSA = x[0]
    sigma_x = sigma_y = BSA/4. # Uniform transverse distribution
    C_sigma_x = C_sigma_y  = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = 12
    MaxB = I2B(Imain)

    Q_total = -4.0
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)
    # newrun.set(Run = Run)
    # newrun.set(Xrms = 1)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 50, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 20, Zemit = 400, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5.2770])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])

    # Create a folder and run Astra in that folder, useful for FARM
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system(mkdir+direc)
    
    job_name = 'myjob.'+('D-%.2fmm-I-%.2fA' % (BSA, Imain))
    gen_name = 'gen'
    ast_name = 'ast'
    
    generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    # Generate a batch file for FARM, or
    if FARM:
        astra.qsub(job_name, ast_name, gen_name, direc)
    # Run it directly
    else:
        os.chdir(direc)
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.system('astra ast.in 2>&1 | tee ast.log')
        #astra.run('ast.in')
        os.chdir('..')
    
    return

# Run the function once
x = [1, 0, 0, 370]
astra_demo(x)
exit()

# Simple parameter scan
var1 = np.linspace(0.7, 1.3, 7)
var2 = np.linspace(266, 274, 9)
combi = np.array([[v1, 0, 0, v2] for v1 in var1 for v2 in var2])

for x in combi:
    astra_demo(x)
exit()
