
# coding: utf-8

# In[11]:

# get_ipython().magic(u'matplotlib inline')

from universal import *

# ## 基本公式
# ---
# 
# 共振辐射波长与Undulator周期$\lambda_U$与电子能量$\gamma$及磁场参数$K$之间的关系为
# 
# \begin{equation}
#   \lambda_s=\frac{\lambda_U}{2\gamma^2}(1+K^2/2)
# \end{equation}
# 
# 上式变形可得
# 
# \begin{equation}
#   \gamma=\sqrt{\frac{\lambda_U}{2\lambda_s}(1+K^2/2)}
# \end{equation}
# 
# ## 波荡器辐射
# ---
# 
# 
# ## 高增益公式
# ---
# 
# Pierce参数：
# 
# \begin{equation}
#   \rho = \Bigg[ \frac{K_0^2 [JJ]^2}{32}\frac{k_p^2}{k_u^2}\Bigg]^{1/3} = \Bigg[ \frac{1}{16} \frac{I_e}{I_A} \frac{K_0^2 [JJ]^2}{\gamma_0^3\sigma_x^2k_u^2} \Bigg]^{1/3}
# \end{equation}
# 
# 1D增益长度为
# \begin{equation}
#   L_{G0} = \frac{\lambda_u}{4\pi\sqrt{3}\rho}
# \end{equation}

# ## 基本公式

# In[34]:

def undulator_parameter(B, lam_u):
    '''
    Calculate undulator parameters from `B` and `lam_u`
    Parameters
      B: peak magnetic field in nit of Tesla
      lam_u: undulator period in unit of meter
    Returns
      K: undulator parameter
    '''
    return 0.933728997664305*B*lam_u*1e2

def undulator_field(K, lam_u):
    '''
    Calculate the peak magnetic field from the undulator parameters `K` and `lam_u`
    Parameters
      K: undulator parameter
      lam_u: undulator period in unit of meter
    Returns
      B: peak magnetic field in nit of Tesla
    '''
    return K/(0.933728997664305*lam_u*1e2)

def resonant_wavelength(K,lam_u,gamma):
    '''
    Calculate the resonant wavelength
    Parameters
      K: undulator parameter
      lam_u: undulator period in unit of meter
      gamma: energy in unit of mec2
    Returns
      lam_s: radiation wavelength, meter
    '''
    return lam_u/2./gamma**2*(1+K**2/2)

def resonant_energy(K,lam_u,lam_s):
    '''
    Calcualte the resonant energy given the wavelength
    Parameters
      K: undulator parameter
      lam_u: undulator period
      lam_s: radiation wavelength
    Returns
      gamma: Lorentz factor of particles with the resonant energy 
    '''
    return np.sqrt(lam_u/2./lam_s*(1+K**2/2.))

def resonant_undulator_parameter(lam_s, lam_u, gamma):
    '''
    Calculate the resonant undulator parameter given the resonant wavelength
    and the undulator period and the beam energy
    Parameters
      lam_u: undulator period
      lam_s: radiation wavelength
      gamma: Lorentz factor of particles with the resonant energy
    Returns  
      K: undulator parameter
    '''
    return np.sqrt(2.*(2*gamma**2*lam_s/lam_u-1))
    
# ## Undulator辐射

# In[38]:

def JJ2(K,n=0):
    '''
    Parameters
      K: undulator parameter
      m=2n+1: harmonic number
    Returns
      [J_n(x)-J_n+1(x)]^2
    '''
    m=2*n+1
    x=1.0*m*K**2/(4+2*K**2)
    return (jn(n,x)-jn(n+1,x))**2
    
def sos(Nu,y):
    '''
    sin^2(Nu*y)/sin^2(y)
    '''
    if(y==0):
        return Nu**2
    else:
        return np.sin(Nu*y)**2/np.sin(y)**2
        
def lam2omega(lam):
    return 2*np.pi*g_c/lam

def lambda2omega(lam):
    return 2*np.pi*g_c/lam
    
def undulator_radiation(gamma,Nu,K,ww,w1,n=0):
    '''
    The spectral energy density per electron of the radiation emittied forward
    direction for the m-th harmonic
    Parameters
      Nu: number of period
      K: undulator parameter
      ww: angular frequency of interest
      w1=2*pi*c/lamda
      m=2n+1: harmonic number
    Returns
      
    '''
    m=2*n+1
    wm=w1*m
    y=np.pi*(ww-wm)/w1
    
    c1=g_qe**2*gamma**2*m**2*K**2/4.0/np.pi/g_eps0/g_c/(1+K**2/2)**2
    return c1*ss(Nu,y)*JJ2(K,n)

def angular_width(gamma,Nu,K,n=0):
    m=2*n+1
    return 2*np.pi/gamma**2/2./m/Nu*(1+K**2/2)

def frac_bandwidth(Nu,w1,n=0):
    m=2*n+1
    wm=w1*m
    return 1.0/m/Nu*wm

def radiation(gamma,Nu,K,ww,w1,n=0):
    return undulator_radiation(gamma,Nu,K,ww,w1,n)*angular_width(Nu,K,n)*frac_bandwidth(Nu,w1,n)

def form_factor(lam_s=300e-6,sigma_t=100e-15):
    '''
    Parameters
      lam_s: radiation wavelength, m
      sigma_t: rms bunch length, second
    Returns
      form factor
    '''
    sigma_z=sigma_t*g_c
    return np.exp(-2*np.pi**2*sigma_z**2/lam_s**2)

def radiation_from_bunch(gamma, Nu,K,ww,w1,sigma_t,Qe=20e-12,n=0):
    Ne=Qe/g_qe
    return radiation(gamma,Nu,K,ww,w1,n)*(Ne+Ne*(Ne-1)*form_factor(lam_s,sigma_t)**2)


# ## 高增益公式

# In[36]:

def pierce_parameter(I,gamma,sigma_x,lam_u,B):
    '''
    Parameters
      K: undulator parameter
      gamma: energy of electron bunch, mec2
      lam_u: undulator period, meter
      B: magnetic field amplitude, Tesla
    Returns
      rho: pierce parameter, unitless
    '''
    Alfven=17e3
    K=undulator_parameter(B,lam_u)
    ku=2*np.pi/lam_u
    return (1./16*I/Alfven*K**2*JJ2(K)/gamma**3/sigma_x**2/ku**2)**(1./3)

def gain_length(rho,lam_u):
    '''
    Parameters
      rho: pierce parameter
      lam_u: undulator period
    Returns
      Lg: gain length, meter
    '''
    return lam_u/4./np.pi/np.sqrt(3)/rho

def cooperation_length(lam_s, rho):
    '''
    Parameters
      rho: pierce parameter
      lam_s: radiation wavelength
    Returns
      Lc: cooperation length, meter
    '''
    return lam_s/4./np.pi/rho

# ## Genesis 1.3

# In[6]:

def write2file(ss, filename):
    '''
    Write the string `ss` into the `filename`
    '''
    ff = open(filename, 'w')
    ff.write(ss)
    ff.close()
    return



# ## genesis 1.3后处理

def astra2gen(infile = 'astra.beam', outfile = 'gen.beam', dist = 'p'):
    '''
    Transform a beam distribution from Astra simulation to the input beam distribution for Genesis 1.3
    '''
    data = np.loadtxt(infile)
    data[1:,2] += data[0,2]
    data[1:,5] += data[0,5]
    
    data[:,5] = np.sqrt(data[:,3]**2+data[:,4]**2+data[:,5]**2)/g_mec2/1e6 # Pz - > P/mc
    data[:,3] = data[:,3]/g_mec2/1e6 # Px -> Px/mc
    data[:,4] = data[:,4]/g_mec2/1e6 # Py -> Py/mc
    
    tmp = np.copy(data[:,2]) # z -> tmp

    data[:,2] = np.copy(data[:,1])
    data[:,1] = np.copy(data[:,3])
    data[:,3] = np.copy(data[:,4])
    
    if dist == 'p':
        tmp = tmp-np.mean(tmp)+3*np.std(tmp)
        data[:,4] = tmp # tmp -> z
    elif dist == 'g':
        tmp = tmp-np.mean(tmp)
        data[:,4] = tmp

    charge = np.abs(np.sum(data[:,7])*1e-9)
    size = len(data[:,0])
    title = '# Input Distribution for SR-UR\n? VERSION = 1.0\n? CHARGE = '\
            +str.format('%d' % charge)+'\n? SIZE = '+str.format('%d' % size)+'\n? COLUMNS X PX Y PY Z P'
    
    np.savetxt(outfile, data[:,0:6], fmt = '%12.4E', header = title, comments = '')
    return

def reform(arr):
    '''Change invalid number like NaNs into zero'''
    masked=np.ma.masked_invalid(arr)
    for i in np.arange(len(arr)):
        if masked.mask[i]==True:
            arr[i]=0
    return arr

def gen_split(datafile,subdir='./', prefix=''):
    '''
    Split the standard output (in raw format) of Genesis 1.3 into slices with each slice stored in a
    separate file named `prefixslice_xxx.txt`, where xxx is the slice number
    '''
    if subdir != './':
        if not os.path.exists(subdir):
            os.mkdir(subdir)
    data=open(datafile, 'r')
    line=data.readline()
    while True:
        while True:
            if not line:
                break
            match=re.search(r'[*]* output: slice',line)
            if match:
                #print match.group()
                snum=re.search(r'[\d]+',line)
                if snum:
                    t=(float)(snum.group())
                    fname=str.format('%s/%sslice_%05.0f.txt' % (subdir,prefix,t))
                    break
            else:
                line=data.readline()
        if not line:
            break
        subdata=open(fname,'w')
        subdata.write(line)
        while True:
            line=data.readline()
            if not line:
                break
            blank=re.search(r'[*]',line)
            if blank:
                #print blank
                break
            match = re.search(r'[ E]* current', line)
            if match:
                snum=re.search(r'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?',line)
                if snum:
                    current = float(snum.group())
                    with open(subdir+'/current.txt', 'a') as f_handle:
                        np.savetxt(f_handle,np.atleast_2d([t, current]),fmt='%15d%15.6E')
            else:
                subdata.write(line)
        subdata.close()
        #break
    data.close()
    return
#gen_split('template.out','slices')

def gen_sum(col=0,workdir='./'):
    '''Collect the col-th collumn of all the files under workdir into a 2D array and return it'''
    rr=[]
    for rt, dirs, files in os.walk(workdir):
        for f in files:
            #print f
            snum=re.match(r'slice_[\d]+.txt',f)
            if snum:
                fname=workdir+os.sep+f
                data=np.genfromtxt(fname,skip_header=6,usecols=(col,col+1),invalid_raise=False,filling_values=0,missing_values=0)
                tmp=reform(data[:,0])
                rr.append(tmp)
    return rr

def gen2power(power, lam_s, lam_u, delz, col = -1, N = 0):
    '''
    Calculate the radiation power and energy
    Parameters
      power: radiation power in shape of (nt, nz). nt corresponds to slices along the bunch; 
             nz corresponds to bunches along the undulator
      lam_s: radiation wavelength, which is used in Genesis 1.3 simulation
      lam_u: undulator wavelength
      delz:  step used in Genesis 1.3 simulation
    Returns
      An ndarray of (nz, 3), storing the positions, powers and energies of the radiation along the undulator
    '''
    power = np.atleast_2d(power)
    
    nt, nz = power.shape; # print nt, nz
    if N>nt:
        nt = N
    
    period_s = lam_s/g_c
    
    power1 = np.zeros((nz, 3))
    power1[:,0] = np.arange(nz)*lam_u*delz
    power1[:,1] = np.array(power.sum(axis=0)/1e6/nt)
    power1[:,2] = np.array(power.sum(axis=0)*period_s*1e6)
    
    return power1

def gen2spectrum(phase, amp, lam_s, col = -1, N = 0):
    '''
    Calculate the radiation spectrum
    Parameters
      phase and amp: the phases and amplitudes of the radiation pulses
    Returns
      spectrum: the spectrums of each radiation pulse
    '''
    # nt corresponds to slices along the bunch
    # nz corresponds to bunches along the undulator
    amp = np.atleast_2d(amp)
    phase = np.atleast_2d(phase)
    
    nt, nz = amp.shape 
    if N>nt:
        nt = N

    signal = np.zeros(nt, dtype = complex)
    for i in np.arange(nt):
        signal[i] = np.sqrt(amp[i,col])*np.complex(np.cos(phase[i,col]), np.sin(phase[i,col]))
            
    spectrum = np.abs(fftshift(fft(signal, nt)))
    spectrum = spectrum*spectrum

    period_s = lam_s/g_c; freq_s = 1./period_s
    F = 1.0*np.arange(-nt/2, nt/2,)/nt*freq_s+freq_s # Frequency
    F = g_c/F*1e6 # Frequency to wavelength
    
    spec = np.zeros((nt, 2)); #print spec.shape
    spec[:,0] = F; spec[:,1] = spectrum
    return spec

def gen2power2(power, lam_s, col = -1, N = 0):
    '''
    Calculate the radiation power and energy
    Parameters
      power: radiation power in shape of (nt, nz). nt corresponds to slices along the bunch; 
             nz corresponds to bunches along the undulator
      lam_s: radiation wavelength, which is used in Genesis 1.3 simulation
      lam_u: undulator wavelength
      delz:  step used in Genesis 1.3 simulation
    Returns
      An ndarray of (nz, 3), storing the positions, powers and energies of the radiation along the undulator
    '''
    
    nt, nz = power.shape; # print nt, nz
    if N>nt:
        nt = N
    
    period_s = lam_s/g_c
    
    power2 = np.zeros((nt, 2))
    power2[:,0] = np.arange(nt)*period_s*g_c*1e6 # um
    power2[:,1] = power[:,col]/1e6/nt

    return power2

def complexify_phase(phase):
    '''
    Complexify the phases saved in the 2D array
    Parameters
      phase: phases in unit of radian
    Returns
      phase_complex: phases in format of cos(phase)+j*sin(phase)
    '''
    # nt corresponds to slices along the bunch
    # nz corresponds to bunches along the undulator
    nt, nz = phase.shape
    phase_complex = np.zeros((nt,nz), dtype = complex)
    for i in np.arange(nt):
        for j in np.arange(nz):
            phase_complex[i,j] = np.complex(np.cos(phase[i,j]), np.sin(phase[i,j]))
    return phase_complex

def gen2spectrum0(phase, amp, lam_s, col = -1, N = 0):
    '''
    Calculate the radiation spectrum
    Parameters
      phase and amp: the phases and amplitudes of the radiation pulses
    Returns
      spectrum: the spectrums of each radiation pulse
    '''
    # nt corresponds to slices along the bunch
    # nz corresponds to bunches along the undulator
    nt, nz = amp.shape 
    if N>nt:
        nt = N
    spectrum = np.zeros((nt, nz)) # has the same shape as amp
    phase_complex = complexify_phase(phase)
    #np.savetxt('phase_complex.dat',phase_complex,fmt='%12.4E')
    for i in np.arange(nz):
        signal = np.sqrt(amp[:,i])*phase_complex[:,i]
        spectrum[:,i] = np.abs(fftshift(fft(signal, nt)))
        spectrum[:,i] = spectrum[:,i]*spectrum[:,i]
    
    period_s = lam_s/g_c; freq_s = 1./period_s
    F = 1.0*np.arange(-nt/2, nt/2,)/nt*freq_s+freq_s # Frequency
    F = g_c/F*1e6 # Frequency to wavelength
    
    return np.concatenate((np.atleast_2d(F).T, spectrum), axis = 1)


# ### 计算辐射功率谱
# 
#     Nu,lam_u,B=42,38e-2,0.4
#     lam_s=300e-6
#     K=undulator_parameter(B,lam_u)
#     w1=lam2omega(lam_s)
#     ww=w1
#     #print w1
# 
#     Qe=20e-12 # C
#     Ne=Qe/g_qe
# 
#     print undulator_radiation(Nu,K,ww,w1)/E_photon*w1*1e-3*1e-6
#     print radiation(Nu,K,ww,w1)

# ### 计算不同束长的电子束产生的辐射能量
# 
#     #workdir='./'
#     #os.chdir(workdir)
# 
#     Nu,lam_u,B=42,38e-2,0.4
#     lam_s=300e-6
#     K=undulator_parameter(B,lam_u)
#     w1=lam2omega(lam_s)
# 
#     E_photon=g_h*g_c/lam_s
#     print E_photon
# 
#     Ek=7.2 # MeV
#     gamma=1+Ek/g_mec2
#     
#     Qe=25e-12
#     sigma_t=300e-15 # fs
#     
#     lam=np.linspace(100e-6,500e-6,200)
#     rr=[]
#     for i in np.arange(len(lam)):
#         lam_s=lam[i]
#         w1=lam2omega(lam_s)
#         ww=w1
#         ff=form_factor(lam_s,sigma_z)**2
#         nj=radiation_from_bunch(Nu,K,ww,w1,sigma_t,Qe)*1e9
#         rr.append([lam_s,nj,ff])
#     rr=np.array(rr)
#     #np.savetxt('sigma_300fs.dat',rr,fmt='%12.4E')
# 
#     fig,axes=plt.subplots(nrows=1,ncols=2,figsize=(12,4))
#     axes[0].plot(rr[:,0]*1e6,rr[:,1])
#     axes[1].plot(rr[:,0]*1e6,rr[:,2])
# 
# 

# ### 计算7次谐波分量的辐射功率谱
# 
#     Nu,lam_u,B=42,38e-2,0.4
#     lam_s=300e-6
#     K=undulator_parameter(B,lam_u)
#     w1=lam2omega(lam_s)
#     n=3
#     wm=w1*(2*n+1)
#     
#     lam=np.linspace(42,44,501)*1e-6
#     rr=[]
#     for i in np.arange(501):
#         lam_s=lam[i]
#         ww=2*np.pi*g_c/lam_s
#         E_photon=g_h*wm/2/np.pi
#         r1=undulator_radiation(Nu,K,ww,w1,n)/E_photon*wm*1e-3*1e-6
#         r2=ss(Nu,np.pi*(ww-wm)/w1)
#         r3=JJ2(K,n)
#         rr.append([lam_s*1e6,r1,r2,r3])
#     rr=np.array(rr)
#     #np.savetxt('m7.dat',rr,fmt='%.4E')
#     plt.plot(rr[:,0],rr[:,1]*1e6,'+')
#     plt.show()

# ### 形状因子
# 
#     xx=np.linspace(30,1000,200)*1e-6
#     y1=form_factor(xx,100e-15)**2
#     y2=form_factor(xx,300e-15)**2
#     y3=form_factor(xx,500e-15)**2
#     y4=form_factor(xx,1000e-15)**2
#     plt.plot(xx,y1,'r-',xx,y2,'b-',xx,y3,'g-',xx,y4,'y-')
#     plt.show()
# 
#     xx=np.linspace(100e-15,1000e-15,100)
#     yy=form_factor(300e-6,xx)**2
#     fig,ax=plt.subplots()
#     ax.plot(xx*1e15,yy,'r-+')
#     ax.set_yscale("log")
#     ax.grid()
#     ax.set_xlabel('$\sigma_z$')
#     ax.set_ylabel('g$^2(\sigma_z$)')
#     #ax.show()
