from astra_modules import *
from FEL.basic import *

from timeit import default_timer
import time
from scipy.interpolate import interp1d, interp2d


class FieldMap3D():
    
    def __init__(self, dx, dy, dz, zmin, zmax, xmax, ymax = None, EM3D = None):
        '''
        Parameters:
          dx, dy, dz: grid sizes
          zmin, zmax: starting and end points of the area longitudinally
          rmax: half width of area transversely
          xmax, ymax: if not rotationally symmetric, use xmax and ymax to specify the width
                      and height of the area
          EM3D(x, y, z, t): the field interpolation function
        '''
        self.dx = dx; self.dy = dy; self.dz = dz
        self.zmin = zmin; self.zmax = zmax; self.xmax = xmax
        if ymax is None or 0:
            self.ymax = xmax
        else:
            self.ymax = ymax
        if EM3D is not None:
            self.EM3D = EM3D

    def export3D_old(self, fname = 'field3D', ext= ['.ex', '.ey', '.ez', '.bx', '.by', '.bz'], t=0):
        ext = ext
        ext_index={'.ex':0, '.ey':1, '.ez':2, '.bx':3, '.by':4, '.bz':5}
        
        dx, dy, dz = self.dx, self.dy, self.dz
        xwidth = self.xmax*2; ywidth = self.ymax*2; zlength = self.zmax-self.zmin
        Nx = int(xwidth/dx+1); Ny = int(ywidth/dy+1); Nz = int(zlength/dz+1)
        
        xx = np.linspace(-self.xmax, self.xmax, Nx)
        yy = np.linspace(-self.ymax, self.ymax, Ny)
        zz = np.linspace(0, zlength, Nz)
        
        for i, v in enumerate(ext):
            rr = np.concatenate(([len(xx)], xx))
            with open(fname+v,'w') as f_handle:
                np.savetxt(f_handle, rr.reshape(1, len(rr)), fmt='%15.6E')
            rr = np.concatenate(([len(yy)], yy))
            with open(fname+v,'a') as f_handle:
                np.savetxt(f_handle, rr.reshape(1, len(rr)), fmt='%15.6E')
            rr = np.concatenate(([len(zz)], zz))
            with open(fname+v,'a') as f_handle:
                np.savetxt(f_handle, rr.reshape(1, len(rr)), fmt='%15.6E')
        
        for z1 in zz:
            #print(z1)
            for y1 in yy:
                ff = []
                for x1 in xx:
                    f1 = self.EM3D(x1, y1, z1, t)
                    ff.append(f1)
                ff = np.array(ff) 
                
                for v in ext:
                    l = ext_index[v]
                    rr = ff[:,l].T
                    with open(fname+v,'a') as f_handle:
                        np.savetxt(f_handle, rr.reshape(1, len(rr)), fmt='%15.6E')

    def export3D_old2(self, fname = 'field3D', ext= ['.ex', '.ey', '.ez', '.bx', '.by', '.bz'], t=0):
        ext = ext
        ext_index={'.ex':0, '.ey':1, '.ez':2, '.bx':3, '.by':4, '.bz':5}
        
        dx, dy, dz = self.dx, self.dy, self.dz
        xwidth = self.xmax*2; ywidth = self.ymax*2; zlength = self.zmax-self.zmin
        Nx = int(xwidth/dx+1); Ny = int(ywidth/dy+1); Nz = int(zlength/dz+1)
        print('Nx, Ny, Nz = ', Nx, Ny, Nz)
        
        xx = np.linspace(-self.xmax, self.xmax, Nx)
        yy = np.linspace(-self.ymax, self.ymax, Ny)
        zz = np.linspace( self.zmin, self.zmax, Nz)
        
        for i, v in enumerate(ext):
            rr = np.concatenate(([len(xx)], xx))
            with open(fname+v,'w') as f_handle:
                np.savetxt(f_handle, rr.reshape(1, len(rr)), fmt='%15.6E')
            rr = np.concatenate(([len(yy)], yy))
            with open(fname+v,'a') as f_handle:
                np.savetxt(f_handle, rr.reshape(1, len(rr)), fmt='%15.6E')
            rr = np.concatenate(([len(zz)], zz))
            with open(fname+v,'a') as f_handle:
                np.savetxt(f_handle, rr.reshape(1, len(rr)), fmt='%15.6E')

        ff = []        
        for z1 in zz:
            print(z1, end = ' ')
            for y1 in yy:
                for x1 in xx:
                    f1 = self.EM3D(x1, y1, z1, t)
                    ff.append(f1)
        ff = np.array(ff)
        print(ff.shape)
        for v in ext:
            l = ext_index[v]
            rr = ff[:,l].T
            NN = len(rr); print(NN)
            with open(fname+v, 'a') as f_handle:
                np.savetxt(f_handle, rr.reshape(NN//Nx, Nx), fmt='%15.6E')
                
    def export3D(self, fname = 'field3D', ext= ['.ex', '.ey', '.ez', '.bx', '.by', '.bz'], t=0):
        '''
        Updated on April 20, 2020. 
		Use vectors as input arguments for `EM3D`, therefore it is  much faster than `export_3D2`.
        '''
        ext = ext
        ext_index={'.ex':0, '.ey':1, '.ez':2, '.bx':3, '.by':4, '.bz':5}
        
        dx, dy, dz = self.dx, self.dy, self.dz
        xwidth = self.xmax*2; ywidth = self.ymax*2; zlength = self.zmax-self.zmin
        Nx = int(xwidth/dx+1); Ny = int(ywidth/dy+1); Nz = int(zlength/dz+1)
        
        xx = np.linspace(-self.xmax, self.xmax, Nx)
        yy = np.linspace(-self.ymax, self.ymax, Ny)
        zz = np.linspace( self.zmin, self.zmax, Nz)
        
        for i, v in enumerate(ext):
            rr = np.concatenate(([len(xx)], xx))
            with open(fname+v,'w') as f_handle:
                np.savetxt(f_handle, rr.reshape(1, len(rr)), fmt='%15.6E')
            rr = np.concatenate(([len(yy)], yy))
            with open(fname+v,'a') as f_handle:
                np.savetxt(f_handle, rr.reshape(1, len(rr)), fmt='%15.6E')
            rr = np.concatenate(([len(zz)], zz))
            with open(fname+v,'a') as f_handle:
                np.savetxt(f_handle, rr.reshape(1, len(rr)), fmt='%15.6E')

        z3d, y3d, x3d = np.meshgrid(zz, yy, xx, indexing = 'ij')
        ff = self.EM3D(x3d.flatten(), y3d.flatten(), z3d.flatten(), t)
        
        print(ff.shape)
        for v in ext:
            l = ext_index[v]
            rr = ff[:,l].T
            with open(fname+v,'a') as f_handle:
                np.savetxt(f_handle, rr.reshape(len(rr)//Nx, Nx), fmt='%15.6E')

    def export3DGPT(self, fname = 'field3D', ext= ['.ex', '.ey', '.ez', '.bx', '.by', '.bz'], t=0):
        ext = ext
        ext_index={'.ex':0, '.ey':1, '.ez':2, '.bx':3, '.by':4, '.bz':5}
        
        dx, dy, dz = self.dx, self.dy, self.dz
        xwidth = self.xmax*2; ywidth = self.ymax*2; zlength = self.zmax-self.zmin
        Nx = int(xwidth/dx+1); Ny = int(ywidth/dy+1); Nz = int(zlength/dz+1)
        
        xx = np.linspace(-self.xmax, self.xmax, Nx)
        yy = np.linspace(-self.ymax, self.ymax, Ny)
        zz = np.linspace( self.zmin, self.zmax, Nz)
        
        for z1 in zz:
            print(z1, end = ' ')
            ff = []        
            for y1 in yy:
                for x1 in xx:
                    f1 = self.EM3D(x1, y1, z1, t)
                    ff.append([x1, y1, z1, f1[3], f1[4], f1[5]])
            ff = np.array(ff); print(ff.shape)
            with open(fname+'.dat','a') as f_handle:
                np.savetxt(f_handle, ff, fmt='%15.6E')


class Undulator3D():
    def __init__(self, Nu, lam_u, **kwargs):
        
        self.Nu = Nu
        self.lam_u = lam_u
        self.Lu = Nu*lam_u
        
        self.build(**kwargs)
    
    def build(self, amp = 1, z0 = 0, trim1 = 0.25, trim2 = 0.75):
        
        Nu = self.Nu
        lam_u = self.lam_u
        Lu = self.Lu
        
        self.K = undulator_parameter(amp, lam_u)
        
        ku = 2*np.pi/lam_u
        
        sample = 64
        F0 = np.zeros(sample//2)
        F1 = np.ones(sample//2)*trim1
        F2 = np.ones(sample//2)*trim2
        Fs = np.ones((Nu-3)*sample+1)
        
        zz = np.linspace(-Lu/2.0, Lu/2.0, Nu*sample+1)
        Fs = np.concatenate((F0, F1, F2, Fs, F2[::-1], F1[::-1], F0[::-1]))
        
        fFs = interp1d(zz, Fs, bounds_error = False, fill_value = 0)
        
        def EM3D(x, y, z, t=0, *args):
            
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            x = np.asarray(x).flatten()
            y = np.asarray(y).flatten()
            z = np.asarray(z).flatten()
            
            nn = np.max([x.size, y.size, z.size])
            
            if x.size == 1:
                x = np.ones(nn)*x[0]
            if y.size == 1:
                y = np.ones(nn)*y[0]
            if z.size == 1:
                z = np.ones(nn)*z[0]
            
            if not np.isscalar(t):
                t = np.asarray(t)
                
            z1 = z-z0
            
            B1y = amp*fFs(z)*np.cosh(ku*y)*np.sin(ku*z)
            B1z = amp*fFs(z)*np.sinh(ku*y)*np.cos(ku*z)
            
            #B1x = np.where((z1<-Lu/2.)*(z1> Lu/2.), 0, B1x)
            B1y = np.where((z1<-Lu/2.)*(z1> Lu/2.), 0, B1y)
            B1z = np.where((z1<-Lu/2.)*(z1> Lu/2.), 0, B1z)
            
            F2d = np.zeros((nn, 6))
            #F2d[:,3] = B1x
            F2d[:,4] = B1y
            F2d[:,5] = B1z
        
            return F2d
        
        self.EM3D = EM3D
        
        self.z = np.linspace(z0-Lu/2., z0+Lu/2., Nu*32+1)
        
        f2d = EM3D(0, 0, self.z)
        self.Bx = f2d[:,3]
        self.By = f2d[:,4]
        
        return
    
    def fieldIntegral(self, z = None, By = None):
        
        if z is None or By is None:
            zz = self.z
            By = self.By
        
        fBy_z = interp1d(zz, By, bounds_error = False, fill_value = 0)
        
        dz = zz[1]-zz[0]
        
        I1 = [0]+[quad(fBy_z, zi, zi+dz, limit = 100)[0] for zi in zz[:-1]]
        
        for i in np.arange(1, len(I1)):
            I1[i] += I1[i-1]
        
        fI1_z = interp1d(zz, I1, bounds_error = False, fill_value = 0)
        
        I2 = [0]+[quad(fI1_z, zi, zi+dz, limit = 100)[0] for zi in zz[:-1]]
        for i in np.arange(1, len(I2)):
            I2[i] += I2[i-1]
        
        fI2_z = interp1d(zz, I2, bounds_error = False, fill_value = 0)
        
        self.zI = zz
        self.I1 = np.array(I1)
        self.I2 = np.array(I2)
        
        self.fI1_z = fI1_z
        self.fI2_z = fI2_z
        
        return
    
    def phaseIntegral(self, gamma_b = kinetic2gamma(momentum2kinetic(17))):
        
        Nu = self.Nu
        lam_u = self.lam_u
        Lu = self.Lu
                
        zz = self.zI
        I1 = self.I1
        
        z0 = zz[0]
        dz = zz[1]-zz[0]
        
        fI1_squared_z = interp1d(zz, I1**2, bounds_error = False, fill_value = 0)
        
        # phase integral
        PI = [0]+[quad(fI1_squared_z, zi, zi+dz, limit = 100)[0] for zi in zz[:-1]]
        for i in np.arange(1, len(PI)):
            PI[i] += PI[i-1] 
        
        PI = np.array(PI)
        
        # K = self.K
        # PA = 2.*np.pi/lam_u/(1.+0.5*K**2)*((zz-z0)+1.0*(g_qe/g_me/g_c)**2*PI)
        
        # Slippage or path advance
        SA = 1./2./gamma_b**2*((zz-z0)+1.0*(g_qe/g_me/g_c)**2*PI)
        
        fSA_z = interp1d(zz, SA, bounds_error = False, fill_value = 0)
        
        zz1 = np.linspace(-Lu/2.+0.25*lam_u, Lu/2.-0.25*lam_u, 2*Nu); #print zpol
        SA1 = np.array([fSA_z(zi) for zi in zz1])

        dd = 20
        func = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(func, zz1[dd:-dd], SA1[dd:-dd])
        _, b = popt
        
        Keff = np.sqrt(2.*(2.*gamma_b**2*b-1))
        lam_s = resonant_wavelength(Keff, lam_u, gamma_b)
        
        # Phase advance
        PA = SA*2*np.pi/lam_s
        
        # Phase error
        PE = np.zeros((len(zz1[dd:-dd]), 2))
        PE[:,0] = zz1[dd:-dd]
        PE[:,1] = (SA1[dd:-dd]-func(zz1[dd:-dd], *popt))*2*np.pi/lam_s
        
        self.Keff = Keff
        self.lam_s = lam_s
        
        self.PE = PE
        self.PI = PI
        self.PA = PA
        self.SA = SA
        
        return

class U3DFromRaw(Undulator3D):
    def __init__(self, Nu, lam_u, filename, Nh, ratio = None, **kwargs):
        
        self.Nu = Nu
        self.lam_u = lam_u
        self.Lu = Nu*lam_u
        
        self.filename = filename
        self.Nh = Nh
        
        self.ratio = ratio
        
        self.build(**kwargs)
    
    def build(self, z0 = 0, x0 = 6e-3/2/np.tan(4.5e-3/2), taper_coef = 0.761412, 
              floor_cut = True):
        
        _, basename, _ = fileparts(self.filename)
        
        Nu = self.Nu
        lam_u = self.lam_u
        Nh = self.Nh
        Lu = self.Lu
        
        data = np.loadtxt(self.filename)
        
        ratio = self.ratio
        if ratio is not None:
            for i, r1 in enumerate(ratio):
                if i < data.shape[1]:
                    data[:,i] *= r1
                
            if data.shape[1]>len(ratio):
                for i in np.arange(len(ratio), data.shape[1]):
                    data[:,i] *= ratio[-1]
        
        z = data[:,0]
        By = data[:,1]
        dz = z[1]-z[0]
        if data.shape[1]>2:
            Bx = data[:,2]
            self.Bx0 = Bx
        
        self.K = undulator_parameter(0.5*(np.max(By)-np.min(By)), self.lam_u)
        
        # find the rough center
        zc = np.sum(z*np.abs(By))/np.sum(np.abs(By))
        z = z-zc

        # find exact center where By=0
        for _ in np.arange(3):
            index, _ = index_min(np.abs(z))
            Nz1 = int(lam_u/4./dz)
            fz_By = interp1d(By[index-Nz1:index+Nz1], z[index-Nz1:index+Nz1])
            zc = fz_By(0); print('zc = ', zc)
            z = z-zc
        
        self.z0 = z
        self.By0 = By
        
        # Get the noise floor
        select = (z<-Lu/2.-0.)
        self.z01 = z[select]
        self.By01 = By[select]
        if data.shape[1]>2:
            self.Bx01 = Bx[select]
        else:
            self.Bx01 = None
            
        select = (z>Lu/2.+0.)
        self.z02 = z[select]
        self.By02 = By[select]
        if data.shape[1]>2:
            self.Bx02 = Bx[select]
        else:
            self.Bx02 = None
        
        By_floor = 0.5*(np.mean(self.By01)+np.mean(self.By02))
        self.By_floor = By_floor
        
        # Get the main part of the fields
        select = (z<=Lu/2.)*(z>=-Lu/2.)
        self.z = z[select]
        self.dz = dz
        self.By = By[select]
        if data.shape[1]>2:
            self.Bx = Bx[select]
        else:
            self.Bx = None
         
        # Subtract the floor
        if floor_cut:
            self.By0 -= By_floor
            self.By -= By_floor
        
        # Fourier analysis
        nharm = np.arange(0, Nu*Nh+1)
        kn = 2.*np.pi*nharm/Lu
        
        #a0 = np.sum(self.By*dz)/Lu; print('a0: ', a0)
        #b0 = 0
        an = [2./Lu*np.sum(self.By*np.cos(kn[i]*self.z)*dz) for i in nharm]
        bn = [2./Lu*np.sum(self.By*np.sin(kn[i]*self.z)*dz) for i in nharm]
        
        an = np.array(an); an[0] = an[0]/2.; print ('a0: ', an[0])
        bn = np.array(bn); print ('b0: ', bn[0])
        spectrum = np.sqrt(an**2+bn**2)

        self.an = an
        self.bn = bn
        self.kn = kn
        
        self.nharm = nharm
        self.spectrum = spectrum
        
        if taper_coef > 0:
            func = lambda x:np.tanh(x*x0)*x-taper_coef
            r = sp.optimize.root(func, 0.9)
            kx = r.x[0]
        else:
            kx = 0
            
        kyn = np.sqrt(kn**2-kx**2)
        #x0 = -x0
        
        self.x0 = x0
        self.kx = kx
        
        #an[0] = 0
        def EM3D(x, y, z, t=0, *args):
            
            x = np.asarray(x).flatten()
            y = np.asarray(y).flatten()
            z = np.asarray(z).flatten()
            
            nn = np.max([x.size, y.size, z.size])
            
            if x.size == 1:
                x = np.ones(nn)*x[0]
            if y.size == 1:
                y = np.ones(nn)*y[0]
            if z.size == 1:
                z = np.ones(nn)*z[0]
            
            if not np.isscalar(t):
                t = np.asarray(t).flatten()
                
            z1 = z-z0
            
            cx = np.sinh(kx*(x0+x))/np.cosh(kx*x0)
            cy = np.cosh(kx*(x0+x))/np.cosh(kx*x0)
            cz = cy
            
            s = 1
            B1x = [np.sum(( an[s:]*np.cos(kn[s:]*z1[i])+bn[s:]*np.sin(kn[s:]*z1[i]))*np.sinh(kyn[s:]*y[i])*kx/kyn[s:]) for i in np.arange(nn)]
            B1y = [np.sum(( an[s:]*np.cos(kn[s:]*z1[i])+bn[s:]*np.sin(kn[s:]*z1[i]))*np.cosh(kyn[s:]*y[i])) for i in np.arange(nn)]
            B1z = [np.sum((-an[s:]*np.sin(kn[s:]*z1[i])+bn[s:]*np.cos(kn[s:]*z1[i]))*np.sinh(kyn[s:]*y[i])*kn[s:]/kyn[s:]) for i in np.arange(nn)]
            
            select = (z1>-Lu/2.)*(z1<Lu/2.)
            B1x = np.where(select, B1x, 0)
            B1y = np.where(select, B1y, 0)
            B1z = np.where(select, B1z, 0)
            
            F2d = np.zeros((nn, 6))
            F2d[:,3] = B1x*cx
            F2d[:,4] = B1y*cy
            F2d[:,5] = B1z*cz
        
            return F2d
        
        self.EM3D = EM3D
        
        f2d = EM3D(0, 0, self.z)
        self.Bx1 = f2d[:,3]
        self.By1 = f2d[:,4]
        
        Bmax = []
        for i in np.arange(2*Nu):
            select = (self.z>=-Lu/2.+i*lam_u*0.5)*(self.z<=-Lu/2.+(i+1)*lam_u*0.5)
            index, v = index_max(np.abs(self.By[select]))
            zi = self.z[select][index]
            Bmax.append([zi, v]) 
        Bmax = np.array(Bmax)
        
        self.Bmax = Bmax
        self.Bmax_avg = np.mean(Bmax)
        
        return
    
    def plot(self):
        _, basename, ext = fileparts(self.filename)
        
        Lu = self.Lu
        Nu = self.Nu
        NumberOfHarmonics = self.Nh
        nharm = self.nharm
        spectrum = self.spectrum
        
        mpl.rc('figure.subplot', bottom = 0.1, top = 0.95, left = 0.1, right = 0.95)
        figsize = (7.2, 3.2)
        
        fig, ax = plt.subplots(figsize = figsize)
        ax.plot(1.0*nharm/Nu, spectrum, '-')
        ax.set_yscale('log')
        ax.set_xlim(0, NumberOfHarmonics)
        ax.set_xticks(np.arange(NumberOfHarmonics+1))
        ax.set_ylim(1e-10, 1e5)
        ax.set_yticks([1e-10, 1e-5, 1e0, 1e5])
        ax.grid()
        ax.set_xlabel(r'$n/N_{\rm u}$')
        ax.set_ylabel(r'$(a_n^2+b_n^2)^{1/2}$')
        fig.savefig(basename+'_ham.eps')
        
        z = self.z
        By = self.By
        By1 = self.By1
        
        Bx = self.Bx
        Bx1 = self.Bx1
        
        fig, ax = plt.subplots(figsize = figsize)
        ax.plot(z, By1, '-')
        ax.plot(z, (By1-By)*1e3, '-')
        ax.grid()
        ax.set_xlabel(r'$z$ (m)')
        ax.set_ylabel(r'$B_y(z)$ (T)')
        ax.set_xlim(-Lu/2, Lu/2)
        ax.set_ylim(-2.0, 2.0)
        ax.legend([r'$B_{y1}$', r'$[B_{y1}-B_y^{\rm meas}]\times 10^3$'], ncol = 2)
        fig.savefig(basename+'-By-z.eps')
        
        if Bx is not None:
            fig, ax = plt.subplots(figsize = figsize)
            ax.plot(z, Bx1*1e3, '-')
            ax.plot(z, Bx*1e3, '-')
            ax.grid()
            ax.set_xlabel(r'$z$ (m)')
            ax.set_ylabel(r'$B_x(z)$ (mT)')
            ax.set_xlim(-Lu/2, Lu/2)
            ax.set_ylim(-10, 6)
            ax.legend([r'$B_{x1}$', r'$B_x^{\rm meas}$'], ncol = 2)
            fig.savefig(basename+'-Bx-z.eps')
            
        mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)
        
        return

def read2array(fname):

    xgrid = np.loadtxt(fname, max_rows = 1)
    ygrid = np.loadtxt(fname, max_rows = 1, skiprows = 1)
    zgrid = np.loadtxt(fname, max_rows = 1, skiprows = 2)
    field = np.loadtxt(fname, skiprows = 3)
    
    nx, ny, nz = len(xgrid)-1, len(ygrid)-1, len(zgrid)-1
    def _convert_(ijk):
        '''
        ijk = (i, j, k)
        '''
        i, j, k = ijk
        return field[j+k*ny,i]

    II = np.meshgrid(np.arange(nx), np.arange(ny), np.arange(nz), indexing = 'ij')
    
    fld3d = _convert_(II) #np.zeros(shape = (len(xgrid)-1, len(ygrid)-1, len(zgrid)-1))
    
    return [xgrid[1:], ygrid[1:], zgrid[1:], fld3d]

from scipy.interpolate import RegularGridInterpolator

class RF3D():
    def __init__(self, basename, amp0, z0 = 0, freq = 1.3e9, phi0 = 0) :
        """
        Register the parameters

        See the docstring of add_external_sstf_laser for the parameters
        """
        # Intermediate variables
        self.amp = amp0
        self.build(basename, z0, freq, phi0)
        
    def build(self, basename, z0 = 0, freq = 1.3e9, phi0 = 0):
        '''
        Parameters
          basename: 
        '''
        
        fname = basename+'.ex'
        xgrid, ygrid, zgrid, Ex3D = read2array(fname)
        
        fname = basename+'.ey'
        xgrid, ygrid, zgrid, Ey3D = read2array(fname)

        fname = basename+'.ez'
        xgrid, ygrid, zgrid, Ez3D = read2array(fname)
        
        fname = basename+'.bx'
        xgrid, ygrid, zgrid, Bx3D = read2array(fname)

        fname = basename+'.by'
        xgrid, ygrid, zgrid, By3D = read2array(fname)
        
        fname = basename+'.bz'
        xgrid, ygrid, zgrid, Bz3D = read2array(fname)
            
        fEx3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Ex3D, bounds_error = False, fill_value = 0)
        fEy3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Ey3D, bounds_error = False, fill_value = 0)
        fEz3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Ez3D, bounds_error = False, fill_value = 0)

        fBx3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Bx3D, bounds_error = False, fill_value = 0)
        fBy3D = RegularGridInterpolator((xgrid, ygrid, zgrid), By3D, bounds_error = False, fill_value = 0)
        fBz3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Bz3D, bounds_error = False, fill_value = 0)

        def EM3D(x, y, z, t = 0, *args):
            
            x = np.asarray(x).flatten()
            y = np.asarray(y).flatten()
            z = np.asarray(z).flatten()
            
            nn = np.max([x.size, y.size, z.size])
            
            if x.size == 1:
                x = np.ones(nn)*x[0]
            if y.size == 1:
                y = np.ones(nn)*y[0]
            if z.size == 1:
                z = np.ones(nn)*z[0]
            
            if not np.isscalar(t):
                t = np.asarray(t).flatten()
            
            z1 = z-z0
            
            omega = 2.*np.pi*freq
            phi1 = (phi0)*np.pi/180
            
            omega_t = omega*t+phi1
            ss = np.sin(omega_t)
            cc = np.cos(omega_t)
            
            E0x = fEx3D((x, y, z1))*ss
            E0y = fEy3D((x, y, z1))*ss
            E0z = fEz3D((x, y, z1))*ss
            
            B0x = fBx3D((x, y, z1))*cc
            B0y = fBy3D((x, y, z1))*cc
            B0z = fBz3D((x, y, z1))*cc

            F2d = np.zeros((nn, 6))

            F2d[:, 0] = E0x*self.amp
            F2d[:, 1] = E0y*self.amp
            F2d[:, 2] = E0z*self.amp
            
            F2d[:, 3] = B0x*self.amp
            F2d[:, 4] = B0y*self.amp
            F2d[:, 5] = B0z*self.amp
            
            return F2d
        
        self.EM3D = EM3D


class StaticM3D( FieldMap3D ):
    """Class that calculates analytically the field of an SSTF laser"""

    def __init__( self, basename, amp0 = 1, z0 = 0, gamma_boost = 1) :
        """
        Register the parameters
        
        See the docstring of add_external_sstf_laser for the parameters
        """
        # Intermediate variables
        self.amp = amp0
        self.z0 = z0
        self.gamma_boost = gamma_boost
        self.build(basename, z0, gamma_boost)
        
    def build(self, basename, z0 = None, gamma_boost = None):
        '''
        Parameters
          basename: 
        '''
        
        fname = basename+'.bx'
        xgrid, ygrid, zgrid, Bx3D = read2array(fname)

        fname = basename+'.by'
        xgrid, ygrid, zgrid, By3D = read2array(fname)
        
        fname = basename+'.bz'
        xgrid, ygrid, zgrid, Bz3D = read2array(fname)
            
        fBx3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Bx3D, bounds_error = False, fill_value = 0)
        fBy3D = RegularGridInterpolator((xgrid, ygrid, zgrid), By3D, bounds_error = False, fill_value = 0)
        fBz3D = RegularGridInterpolator((xgrid, ygrid, zgrid), Bz3D, bounds_error = False, fill_value = 0)

        if gamma_boost is None:
            gamma_boost = self.gamma_boost
        beta_boost = np.sqrt(1.0-1.0/gamma_boost**2)
        
        if z0 is None:
            z0 = self.z0
        
            
        def EM3D(x, y, z, t = 0, *args):
            x = np.asarray(x).flatten()
            y = np.asarray(y).flatten()
            z = np.asarray(z).flatten()
            
            nn = np.max([x.size, y.size, z.size])
            
            if x.size == 1:
                x = np.ones(nn)*x[0]
            if y.size == 1:
                y = np.ones(nn)*y[0]
            if z.size == 1:
                z = np.ones(nn)*z[0]
            
            if not np.isscalar(t):
                t = np.asarray(t).flatten()

            ##z1 = z-z0
            ##B0x = fBx3D((x, y, z1))
            ##B0y = fBy3D((x, y, z1))
            ##B0z = fBz3D((x, y, z1))

            ### transformation to lab frame is gamma_boost > 1
            z_lab = gamma_boost*(z+beta_boost*g_c*t)
            z1 = z_lab-z0
            
            B0x = fBx3D((x, y, z1))
            B0y = fBy3D((x, y, z1))
            B0z = fBz3D((x, y, z1))

            E1x = -gamma_boost*beta_boost*g_c*B0y
            E1y =  gamma_boost*beta_boost*g_c*B0x

            B1x =  gamma_boost*B0x
            B1y =  gamma_boost*B0y
            B1z =  B0z
            ###
            
            F2d = np.zeros((nn, 6))

            F2d[:, 0] = E1x
            F2d[:, 1] = E1y
            
            F2d[:, 3] = B1x
            F2d[:, 4] = B1y
            F2d[:, 5] = B1z
            
            ##F2d[:, 3] = B0x
            ##F2d[:, 4] = B0y
            ##F2d[:, 5] = B0z

            return F2d
        
        self.EM3D = EM3D

Magnet3D = StaticM3D

def build_cavity(filename, z0, amp, freq, phi0 = 0, zratio = 1, Eratio = 1):
    
    data = np.loadtxt(filename)
    data[:,0] *= zratio
    data[:,1] *= Eratio
    
    a, b = np.max(data[:,1]), np.min(data[:,1])
    Ez_z = data[:,1]/a*amp
    
    dz = data[1,0]-data[0,0]
    
    dEz_dz1 = np.gradient(   Ez_z, dz)
    dEz_dz2 = np.gradient(dEz_dz1, dz)
    dEz_dz3 = np.gradient(dEz_dz2, dz) 
    
    fEz_0 = interp1d(data[:,0],    Ez_z, bounds_error = False, fill_value = 0)
    fEz_1 = interp1d(data[:,0], dEz_dz1, bounds_error = False, fill_value = 0)
    fEz_2 = interp1d(data[:,0], dEz_dz2, bounds_error = False, fill_value = 0)
    fEz_3 = interp1d(data[:,0], dEz_dz3, bounds_error = False, fill_value = 0)
    
    def EM3D(x, y, z, t = 0, *args):
        x = np.asarray(x).flatten()
        y = np.asarray(y).flatten()
        z = np.asarray(z).flatten()
        
        nn = np.max([x.size, y.size, z.size])
        
        if x.size == 1:
            x = np.ones(nn)*x[0]
        if y.size == 1:
            y = np.ones(nn)*y[0]
        if z.size == 1:
            z = np.ones(nn)*z[0]
        
        if not np.isscalar(t):
            t = np.asarray(t).flatten()
            

        omega = 2.*np.pi*freq
        phi = (phi0+0)*np.pi/180

        ww = omega**2/g_c**2
        
        r = np.sqrt(x*x+y*y)
        
        z1 = z-z0
        Ez0 = fEz_0(z1); #print Ez0
        Ez1 = fEz_1(z1)
        Ez2 = fEz_2(z1)
        Ez3 = fEz_3(z1)
        
        omega_t = omega*t+phi
        ss = np.sin(omega_t)
        cc = np.cos(omega_t)
        
        Ez = (     Ez0-r**2/4. *(Ez2+ww*Ez0))*ss
        Er = (-0.5*Ez1+r**2/16.*(Ez3+ww*Ez1))*ss # Er/r instead of Er
        Bt = ( 0.5*Ez0-r**2/16.*(Ez2+ww*Ez0))*ww/omega*cc # Bt/r instead of Bt
        
        Ez = np.where(z<0, 0, Ez)
        Er = np.where(z<0, 0, Er)
        Bt = np.where(z<0, 0, Bt)
        
        F2d = np.zeros((nn, 6))
        F2d[:,0] = x*Er
        F2d[:,1] = y*Er
        F2d[:,2] = Ez
        F2d[:,3] =-y*Bt
        F2d[:,4] = x*Bt
        #F2d = [x*Er, y*Er, Ez, -y*Bt, x*Bt, 0*Bt]
        return F2d
    
    return EM3D

def build_solenoid(filename, z0, amp):
    
    data = np.loadtxt(filename)
    
    a, b = np.max(data[:,1]), np.min(data[:,1])
    Fz_z = data[:,1]/a*amp
    
    dz = data[1,0]-data[0,0]
    
    dFz_dz1 = np.gradient(Fz_z, dz)
    dFz_dz2 = np.gradient(dFz_dz1, dz)
    dFz_dz3 = np.gradient(dFz_dz2, dz)
    dFz_dz4 = np.gradient(dFz_dz3, dz)
    dFz_dz5 = np.gradient(dFz_dz4, dz)
    dFz_dz6 = np.gradient(dFz_dz5, dz)
    
    fFz_0 = interp1d(data[:,0], Fz_z,    bounds_error = False, fill_value = 0)
    fFz_1 = interp1d(data[:,0], dFz_dz1, bounds_error = False, fill_value = 0)
    fFz_2 = interp1d(data[:,0], dFz_dz2, bounds_error = False, fill_value = 0)
    fFz_3 = interp1d(data[:,0], dFz_dz3, bounds_error = False, fill_value = 0)
    fFz_4 = interp1d(data[:,0], dFz_dz4, bounds_error = False, fill_value = 0)
    fFz_5 = interp1d(data[:,0], dFz_dz5, bounds_error = False, fill_value = 0)
    fFz_6 = interp1d(data[:,0], dFz_dz6, bounds_error = False, fill_value = 0)
     
    def EM3D(x, y, z, t = 0, *args):
        
        x = np.asarray(x).flatten()
        y = np.asarray(y).flatten()
        z = np.asarray(z).flatten()
        
        nn = np.max([x.size, y.size, z.size])
        
        if x.size == 1:
            x = np.ones(nn)*x[0]
        if y.size == 1:
            y = np.ones(nn)*y[0]
        if z.size == 1:
            z = np.ones(nn)*z[0]
        
        if not np.isscalar(t):
            t = np.asarray(t).flatten()
        
        
        r = np.sqrt(x*x+y*y)
        
        z1 = z-z0
        Fz0 = fFz_0(z1)
        Fz1 = fFz_1(z1)
        Fz2 = fFz_2(z1)
        Fz3 = fFz_3(z1)
        Fz4 = fFz_4(z1)
        Fz5 = fFz_5(z1)
        Fz6 = fFz_6(z1)
        
        Bz = (Fz0-r*r/4.*Fz2+r**4/64.*Fz4-r**6/2034.*Fz6)
        Br = (-1/2.*Fz1+r**2/16.*Fz3-r**4/384.*Fz5) # Br/r instead of Br
        
        Bz = np.where(z<0, 0, Bz)
        Br = np.where(z<0, 0, Br)
        
        #import pdb; pdb.set_trace()
        F2d = np.zeros((nn, 6))
        F2d[:,3] = x*Br
        F2d[:,4] = y*Br
        F2d[:,5] = Bz
        #F2d = [0, 0, 0, x*Br, y*Br, Bz]
        return F2d
    
    return EM3D
