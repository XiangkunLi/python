
# coding: utf-8

# In[1]:


#get_ipython().magic(u'matplotlib inline')
#%matplotlib notebook
from universal import *

from scipy.optimize import minimize
from scipy.misc import derivative
from scipy.interpolate import interp2d
from scipy import signal
import pickle as pickle

from SpaceCharge3DFFT import *

# In[2]:


def plot_config():
    from cycler import cycler
    from matplotlib.ticker import AutoMinorLocator

    fsize = 14 # a quarter of the paper width: 20 pt; half of the paper width: 12
    font = {'size' : fsize, 'family' : 'serif'}
    color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
    linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
    marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']
    
    mpl.rc('font', **font)
    mpl.rc('xtick', labelsize = 10, direction = 'in', top   = True)
    mpl.rc('ytick', labelsize = 10, direction = 'in', right = True)
    mpl.rc('xtick.major', size = 5, width = 1)
    mpl.rc('ytick.major', size = 5, width = 1)
    mpl.rc('xtick.minor', size = 3, width = 0.7, visible = True)
    mpl.rc('ytick.minor', size = 3, width = 0.7, visible = True)
    
    mpl.rc('lines', linewidth=2, markersize=6, color='r')
    # mpl.rc('lines', linestyle = 'solid')
    mpl.rc('axes', labelpad = 0, prop_cycle=(cycler('color', color_cycle) + cycler('linestyle', linestyle_cycle) + cycler('marker', marker_cycle)))
    mpl.rc('legend', fontsize = 12, labelspacing = 0.05, handletextpad=0.4, frameon=False, handlelength=2.1)
    
    mpl.rc('figure', dpi = 100, figsize = (4, 4))
    mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)
    
    mpl.rc('image', cmap = 'jet')
    
    return
plot_config()

rootdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\work'
simdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim'



# # Tracking
def build_cavity(filename, z0, amp, freq, phi0 = 0, zratio = 1, Eratio = 1):
    #print phi_0
    data = np.loadtxt(filename)
    data[:,0] *= zratio
    data[:,1] *= Eratio
    
    a, b = np.max(data[:,1]), np.min(data[:,1])
    Ez_z = data[:,1]/a*amp
    
    dz = data[1,0]-data[0,0]
    
    dEz_dz1 = np.gradient(   Ez_z, dz)
    dEz_dz2 = np.gradient(dEz_dz1, dz)
    dEz_dz3 = np.gradient(dEz_dz2, dz) 
    
    fEz_0 = interp1d(data[:,0],    Ez_z, bounds_error = False, fill_value = 0)
    fEz_1 = interp1d(data[:,0], dEz_dz1, bounds_error = False, fill_value = 0)
    fEz_2 = interp1d(data[:,0], dEz_dz2, bounds_error = False, fill_value = 0)
    fEz_3 = interp1d(data[:,0], dEz_dz3, bounds_error = False, fill_value = 0)
    
    #dEz_dz = np.zeros((len(data[:,0]), 4))
    #dEz_dz[:,0] = data[:,0]
    #dEz_dz[:,1] = dEz_dz1
    #dEz_dz[:,2] = dEz_dz2
    #dEz_dz[:,3] = dEz_dz3
    #fEz_z = interp1d(dEz_dz[:,0], dEz_dz[:,1:4], bounds_error = False, fill_value = 0)
    
    def EM3D(x, y, z, t = 0, *args):
        if np.isscalar(x):
            x = [x]
            y = [y]
            z = [z]
        x = np.asarray(x)
        y = np.asarray(y)
        z = np.asarray(z)
        
        if not np.isscalar(t):
            t = np.asarray(t)

        omega = 2.*np.pi*freq
        phi = (phi0+0)*np.pi/180

        ww = omega**2/g_c**2
        
        r = np.sqrt(x*x+y*y)
        
        z1 = z-z0
        Ez0 = fEz_0(z1); #print Ez0
        Ez1 = fEz_1(z1)
        Ez2 = fEz_2(z1)
        Ez3 = fEz_3(z1)
        
        Ez = (     Ez0-r**2/4. *(Ez2+ww*Ez0))*np.sin(omega*t+phi)
        Er = (-0.5*Ez1+r**2/16.*(Ez3+ww*Ez1))*np.sin(omega*t+phi) # Er/r instead of Er
        Bt = ( 0.5*Ez0-r**2/16.*(Ez2+ww*Ez0))*ww/omega*np.cos(omega*t+phi) # Bt/r instead of Bt
        
        Ez = np.where(z<0, 0, Ez)
        Er = np.where(z<0, 0, Er)
        Bt = np.where(z<0, 0, Bt)
        
        n = np.max([len(x), len(y), len(z), len(x.T), len(y.T), len(z.T)])
        F2d = np.zeros((n, 6))
        F2d[:,0] = x*Er
        F2d[:,1] = y*Er
        F2d[:,2] = Ez
        F2d[:,3] =-y*Bt
        F2d[:,4] = x*Bt
        #F2d = [x*Er, y*Er, Ez, -y*Bt, x*Bt, 0*Bt]
        return F2d
    
    return EM3D
def build_solenoid(filename, z0, amp):
    
    data = np.loadtxt(filename)
    
    a, b = np.max(data[:,1]), np.min(data[:,1])
    Fz_z = data[:,1]/a*amp
    
    dz = data[1,0]-data[0,0]
    
    dFz_dz1 = np.gradient(Fz_z, dz)
    dFz_dz2 = np.gradient(dFz_dz1, dz)
    dFz_dz3 = np.gradient(dFz_dz2, dz)
    dFz_dz4 = np.gradient(dFz_dz3, dz)
    dFz_dz5 = np.gradient(dFz_dz4, dz)
    dFz_dz6 = np.gradient(dFz_dz5, dz)
    
    fFz_0 = interp1d(data[:,0], Fz_z,    bounds_error = False, fill_value = 0)
    fFz_1 = interp1d(data[:,0], dFz_dz1, bounds_error = False, fill_value = 0)
    fFz_2 = interp1d(data[:,0], dFz_dz2, bounds_error = False, fill_value = 0)
    fFz_3 = interp1d(data[:,0], dFz_dz3, bounds_error = False, fill_value = 0)
    fFz_4 = interp1d(data[:,0], dFz_dz4, bounds_error = False, fill_value = 0)
    fFz_5 = interp1d(data[:,0], dFz_dz5, bounds_error = False, fill_value = 0)
    fFz_6 = interp1d(data[:,0], dFz_dz6, bounds_error = False, fill_value = 0)
     
    def EM3D(x, y, z, t = 0, *args):
        
        if np.isscalar(x):
            x = [x]
            y = [y]
            z = [z]
        x = np.asarray(x)
        y = np.asarray(y)
        z = np.asarray(z)
        
        if not np.isscalar(t):
            t = np.asarray(t)
        
        r = np.sqrt(x*x+y*y)
        
        z1 = z-z0
        Fz0 = fFz_0(z1)
        Fz1 = fFz_1(z1)
        Fz2 = fFz_2(z1)
        Fz3 = fFz_3(z1)
        Fz4 = fFz_4(z1)
        Fz5 = fFz_5(z1)
        Fz6 = fFz_6(z1)
        
        Bz = (Fz0-r*r/4.*Fz2+r**4/64.*Fz4-r**6/2034.*Fz6)
        Br = (-1/2.*Fz1+r**2/16.*Fz3-r**4/384.*Fz5) # Br/r instead of Br
        
        Bz = np.where(z<0, 0, Bz)
        Br = np.where(z<0, 0, Br)
        
        n = np.max([len(x), len(y), len(z), len(x.T), len(y.T), len(z.T)]); #print 'hello', n
        #import pdb; pdb.set_trace()
        F2d = np.zeros((n, 6))
        F2d[:,3] = x*Br
        F2d[:,4] = y*Br
        F2d[:,5] = Bz
        #F2d = [0, 0, 0, x*Br, y*Br, Bz]
        return F2d
    return EM3D

class EMsolver():
    
    def __init__(self):
        self.em3d = {}
        
    def add_external_field(self, name, func):
        self.em3d.update({name:func})
        
    def update_external_field(self, name, func):
        self.em3d.update({name:func})
        
    def get_field(self, x, y, z, t = 0, *args, **kwargs):
        field = np.zeros((1, 6))
        
        if len(kwargs)>0:
            namelist = kwargs['namelist']
        else:
            namelist = list(self.em3d.keys())
        for name, func in self.em3d.items():
            if name in namelist:
                field = np.add(field, func(x, y, z, t, *args))
        return field

'''
field_maps = r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\field-maps'
gun3d = build_cavity(field_maps+os.sep+'gun42Cavity.txt', 0, 60, 1.3e9, 90)
sol3d = build_solenoid(field_maps+os.sep+'gunsolenoidsPITZ.txt', 0, 0.2)

em3d = EMsolver()
em3d.add_external_field('gun', gun3d)
em3d.add_external_field('solenoid', sol3d)

x, y, z = [0, 0, 0], [0, 0, 0.01], [0, 0.1, 0.1]
t = 0
#print gun3d(0, 0, 0), gun3d(0, 0, 0.1), gun3d(0, 0.01, 0.1)
get_ipython().magic(u'timeit gun3d(x, y, z, t)')
a = em3d.get_field([0, 0, 0], [0, 0, 0.01], [0, 0.1, 0.1]); print a
Ex, Ey, Ez, Bx, By, Bz = a.T[:]
print Ex, Ey, Ez

rr = np.linspace(0, 0.1, 200)
fr = np.array([em3d.get_field(v, 0, 0)[0,0] for v in rr])
zz = np.linspace(-0.1, 0.5, 200)
fz = np.array([em3d.get_field(0, 0.01, v, 0)[0,5] for v in zz])

fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (12, 4))
ax1.plot(rr, fr, '-')
ax1.grid()

ax2.plot(zz, fz, '-')
ax2.grid()
'''

def nemixrms(x, xp, w = None):
    '''
    Parameters
      x, xp: phase space coordinates. xp could be either divergence or normalized momentum
      w: weighting factor or charge or the particles
    Returns
      emittance: phase space emittance
    '''
    if w is None:
        w = np.ones(len(x))
    x2 = weighted_mean(x*x, w)
    xp2 = weighted_mean(xp*xp, w)
    xxp = weighted_mean(x*xp, w)
    emit_x = np.sqrt(x2*xp2-xxp*xxp)
    return emit_x

class BeamDiagnostics:
    
    def __init__(self, fname = None, dist = None, start_index = 0):

#         self.keyIndex = {'avg_z':0, 'nemit_x':1, 'nemit_y':2, 'std_x':3, 'std_y':4, 'std_z':5,\
#                          'Ekin':6, 'std_Ekin':7, 'nemit_z':8, 'Q_b':9, 'avg_x':10, 'avg_y':11,\
#                          'alpha_x':12, 'beta_x':13, 'gamma_x':14, 'emit_x':15, 'alpha_y':16,\
#                          'beta_y':17, 'gamma_y':18, 'emit_y':19, 'cor_zEk':20, 'loss_cathode':21,\
#                          'loss_aperture':22, 'FWHM':23}
        keys = ['avg_z', 'nemit_x', 'nemit_y', 'std_x', 'std_y', 'std_z', 'Ekin', 'std_Ekin', 'nemit_z', 'Q_b',\
                'avg_x', 'avg_y', 'alpha_x', 'beta_x', 'gamma_x', 'emit_x', 'alpha_y', 'beta_y', 'gamma_y',\
                'emit_y', 'cor_Ek', 'loss_cathode', 'loss_aperture', 'FWHM', 'NoP']
        indices = np.arange(len(keys))
        
        units = ['m', 'um', 'um', 'mm', 'mm', 'mm', 'MeV', 'keV', 'keV mm', 'pC',\
                 'm', 'm', ' ', 'm', 'm^-1', 'm', ' ', 'm', 'm^-1', 'm', 'keV', ' ',\
                 ' ', 'mm', ' ', 'A', 'A']
        
        keyIndex = {}
        keyUnit = {}
        for i in np.arange(len(keys)):
            keyIndex[keys[i]] = indices[i] + start_index
            keyUnit[keys[i]] = units[i]
        self.keyIndex = keyIndex
        self.keyUnit = keyUnit
    
        if fname is not None:
            dist = np.loadtxt(fname)
            dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
            self.dist = dist
            self.diagnostics()
        elif dist is not None:
            self.dist = dist
            self.diagnostics()
        else:
            self.x = np.zeros(len(self.keyIndex))

    def __getitem__(self, key):
        return self.get(key)
    
    def __getattr__(self, key):
        return self.get(key)
    
    @property
    def x(self):
        return self.__x

    @x.setter
    def x(self, x):
        self.__x = x
        
    def get(self, key):
        '''
        Return the corresponding parameter if key is one of `self.kv.keys()`, or return the index of the key 
        if requesting by "idx_"+key and key is one of `self.kv.keys()`
        '''
        if key in list(self.keyIndex.keys()):
            index = self.keyIndex[key]
            return self.x[index]
        elif key in ['idx_'+ i for i in list(self.keyIndex.keys())]:
            index = self.keyIndex[key[4:]]
            return index
        else:
            return -999
    
    def get_index(self, key):
        index = self.keyIndex[key]
        return index
    
    def diagnostics(self, key_value = None, em3d = None):
        '''
        Parameters
          x y z Px Py Pz w: 6D phase space of the particles
        Outputs
          [avg_z nemit_x nemit_y std_x std_y std_z Ekin std_Ekin nemit_z Q_b\
          avg_x avg_y alpha_x beta_x gamma_x emit_x alpha_y beta_y gamma_y emit_y\
          cor_zEk loss_cathode loss_aperture FWHM]: 1D array
        '''

        if key_value != None:
            ix = key_value['x']; iy = key_value['y']; iz = key_value['z']
            iux = key_value['ux']; iuy = key_value['uy']; iuz = key_value['uz']
            iw = key_value['w']
        else:
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
        
        dist = self.dist
        NoP = len(dist[:,1])
        select = (dist[:,9]==-89); n_lost_cathode = len(dist[select])
        select = (dist[:,9]==-15); n_lost_aperture = len(dist[select])
        
        select = (dist[:,9]>0); dist = dist[select]
        if np.sum(select) == 0:
            self.x = np.zeros(len(self.keyIndex))
            return
        
        x, y, z = dist[:,ix], dist[:,iy] ,dist[:,iz]
        bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
        w = dist[:,iw]*1e-9/g_qe

        gamma=np.sqrt(1+bgx**2+bgy**2+bgz**2)
        Ek=np.sqrt(1+bgx**2+bgy**2+bgz**2)*g_mec2-g_mec2

        x2 = weighted_mean(x*x, w); y2 = weighted_mean(y*y, w)
        xp2 = weighted_mean(bgx*bgx/bgz/bgz, w); yp2 = weighted_mean(bgy*bgy/bgz/bgz, w)
        xxp = weighted_mean(x*bgx/bgz, w); yyp = weighted_mean(y*bgy/bgz, w)
        emit_x = np.sqrt(x2*xp2-xxp*xxp); emit_y = np.sqrt(y2*yp2-yyp*yyp)
        alpha_x = -xxp/emit_x; beta_x = x2/emit_x; gamma_x = xp2/emit_x
        alpha_y = -yyp/emit_y; beta_y = y2/emit_y; gamma_y = yp2/emit_y

        std_z, zmean = weighted_std(z, w), weighted_mean(z, w)
        fwhm = get_FWHM(z)
        
        select = (z-zmean>-5*std_z)*(z-zmean<5*std_z); Ek1 = Ek[select]; z1 = z[select]; w1 = w[select]
        cor_zEkz = weighted_mean(Ek1*z1, w1)-weighted_mean(Ek1, w1)*weighted_mean(z1, w1)
        cor_zEkz = cor_zEkz/weighted_std(z1, w1)*1e3
        
        if em3d is not None:
            Bz = em3d.get_field(x, y, z)[:,5]
            bgx += 0.5*Bz*y*g_c/g_mec2/1e6
            bgy -= 0.5*Bz*x*g_c/g_mec2/1e6
        
        self.x = np.array([weighted_mean(z, w), nemixrms(x, bgx, w)*1e6, nemixrms(y, bgy, w)*1e6, weighted_std(x, w)*1e3,\
                           weighted_std(y, w)*1e3, weighted_std(z, w)*1e3, weighted_mean(Ek, w), weighted_std(Ek, w)*1e3,\
                           nemixrms(z, Ek, w)*1e6, np.sum(w)*g_qe*1e12, weighted_mean(x, w), weighted_mean(y, w), alpha_x,\
                           beta_x, gamma_x, emit_x, alpha_y, beta_y, gamma_y, emit_y, cor_zEkz, n_lost_cathode,\
                           n_lost_aperture, fwhm*1e3, NoP])
        
    def demo(self, name = None):
        if name is not None:
            filename = 'diag@'+name+'.txt'
        else:
            filename = 'diag@'+'.txt'
        
        a = sorted(list(self.keyIndex.items()), key=lambda x: x[1])
        ss = ''
        for i in np.arange(len(a)):
            ss += str('%17s: %15.6E %s\n' % (a[i][0], self.x[i], self.keyUnit.get(a[i][0])))
        print(ss)

        ff = open(filename, 'w')
        ff.write(ss)
        ff.close()
        return 

class Beam():
    def __init__(self, fname = None, dist = None, Qtot = None, debug = 0):
        #self.diag = BeamDiagnostics(fname, dist)
        #BeamDiagnostics(fname, dist)
        self.sc3d = SpaceCharge3DFFT(debug = debug)
        
        #self.key_value = {'x':0, 'ux':1, 'y':2, 'uy':3, 'z':4, 'uz':5, 'w':6}
        self.key_value = {'x':0, 'y':1, 'z':2, 'Px':3, 'Py':4, 'Pz':5, 'w':7}
        
        if fname is not None:
            self.dist = np.loadtxt(fname)
            self.dist[1:,2] += self.dist[0,2]
            self.dist[1:,5] += self.dist[0,5]
        elif dist is not None:
            self.dist = dist
        else:
            self.dist = []
        if len(self.dist)>0:
            if Qtot is not None:
                qq = Qtot/np.sum(self.dist[:,7])
                self.dist[:,7] *= (qq*1e9)
            
        return
    def load(self, fname):
        #self.diag = BeamDiagnostics(fname)
        self.dist = np.loadtxt(fname)        
        self.dist[1:,2] += self.dist[0,2]
        self.dist[1:,5] += self.dist[0,5]
        return
    def dump(self, fname):
        dist = np.copy(self.dist)
        if dist[0,9] == -89:
            select = (dist[:,9]>0)
            dist[0,2] = weighted_mean(dist[select,2], dist[select,7])
            dist[0,5] = weighted_mean(dist[select,5], dist[select,7])
        dist[1:,2] -= dist[0,2]
        dist[1:,5] -= dist[0,5]
        np.savetxt(fname, dist, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4.0f%4.0f')
        return
    def get_sc3d(self, **kwargs):
        self.sc3d.get_rho_3D(self.dist, **kwargs)
        self.sc3d.build(**kwargs)
        return self.sc3d.EM3D
    def diagnose(self, fname = None, key_value = None, em3d = None, time = None):
        #self.diag = BeamDiagnostics(dist = self.dist)
        self.diagnostics(key_value, em3d, time)
        if fname is not None:
            with open(fname, 'a') as f_handle:
                np.savetxt(f_handle, np.atleast_2d(self.x), fmt='%14.6E')
        return
    def diagnostics(self, key_value = None, em3d = None, time = 0):
        '''
        Parameters
          x y z Px Py Pz w: 6D phase space of the particles
        Outputs
          [avg_z nemit_x nemit_y std_x std_y std_z Ekin std_Ekin nemit_z Q_b\
          avg_x avg_y alpha_x beta_x gamma_x emit_x alpha_y beta_y gamma_y emit_y\
          cor_zEk loss_cathode loss_aperture FWHM]: 1D array
        '''

        if key_value != None:
            ix = key_value['x']; iy = key_value['y']; iz = key_value['z']
            iux = key_value['ux']; iuy = key_value['uy']; iuz = key_value['uz']
            iw = key_value['w']
        else:
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
        dist = self.dist
        NoP = len(dist[:,1])
        select = (dist[:,9]==-89); n_lost_cathode = len(dist[select])
        select = (dist[:,9]==-15); n_lost_aperture = len(dist[select])

        select = (dist[:,9]>0); dist = dist[select]
        if np.sum(select) == 0:
            self.x = np.zeros(1) #np.zeros(len(self.keyIndex))
            return
        
        x, y, z = dist[:,ix], dist[:,iy] ,dist[:,iz]
        bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
        w = dist[:,iw]*1e-9/g_qe

        gamma=np.sqrt(1+bgx**2+bgy**2+bgz**2)
        Ek=np.sqrt(1+bgx**2+bgy**2+bgz**2)*g_mec2-g_mec2

        x2 = weighted_mean(x*x, w); y2 = weighted_mean(y*y, w)
        xp2 = weighted_mean(bgx*bgx/bgz/bgz, w); yp2 = weighted_mean(bgy*bgy/bgz/bgz, w)
        xxp = weighted_mean(x*bgx/bgz, w); yyp = weighted_mean(y*bgy/bgz, w)
        emit_x = np.sqrt(x2*xp2-xxp*xxp); emit_y = np.sqrt(y2*yp2-yyp*yyp)
        alpha_x = -xxp/emit_x; beta_x = x2/emit_x; gamma_x = xp2/emit_x
        alpha_y = -yyp/emit_y; beta_y = y2/emit_y; gamma_y = yp2/emit_y

        std_z, zmean = weighted_std(z, w), weighted_mean(z, w)
        fwhm = get_FWHM(z)
        select = (z-zmean>-5*std_z)*(z-zmean<5*std_z); Ek1 = Ek[select]; z1 = z[select]; w1 = w[select]
        cor_zEkz = weighted_mean(Ek1*z1, w1)-weighted_mean(Ek1, w1)*weighted_mean(z1, w1)
        cor_zEkz = cor_zEkz/weighted_std(z1, w1)*1e3
        
        if em3d is not None:
            Bz = em3d.get_field(0, 0, z, time)[:,5]; # print np.max(Bz)
            bgx -= 0.5*Bz*y*g_c/g_mec2/1e6  # +=
            bgy += 0.5*Bz*x*g_c/g_mec2/1e6  # -=
        
        self.x = np.array([weighted_mean(z, w), nemixrms(x, bgx, w)*1e6, nemixrms(y, bgy, w)*1e6, weighted_std(x, w)*1e3,\
                           weighted_std(y, w)*1e3, weighted_std(z, w)*1e3, weighted_mean(Ek, w), weighted_std(Ek, w)*1e3,\
                           nemixrms(z, Ek, w)*1e6, np.sum(w)*g_qe*1e12, weighted_mean(x, w), weighted_mean(y, w), alpha_x,\
                           beta_x, gamma_x, emit_x, alpha_y, beta_y, gamma_y, emit_y, cor_zEkz, n_lost_cathode,\
                           n_lost_aperture, fwhm*1e3, NoP])

def unwrap_step_i(arg, **kwarg):
    return Tracking.step_i(*arg, **kwarg)

class Tracking():
    
    def __init__(self, beam, em3d, t1, dt = 1e-12, t0 = None, diag_interval = 10, dump_interval = 100,\
                 Lspch = False, Run = 1):
        self.beam = beam
        self.em3d = em3d
        #self.sc3d = sc3d
        
        self.clock = None
        self.time = t0
        self.dt = dt
        self.tend = t1
        self.stepped = 0
        self.diag_interval = diag_interval
        self.dump_interval = dump_interval
        self.dump_factor = 1
        
        self.Lspch = Lspch
        self.Run = Run
        
        return
    
    def deriv(self, t, y):
        '''
        Calculate derivatives of many particles altogeter
        Parameters
          t: time/s
          y[0]: x/m
          y[1]: y/m
          y[2]: z/m
          y[3]: P_x*c
          y[4]: P_y*c
          y[5]: P_z*c
        Returns
          derivatives of y with respect to t
        '''
        #import pdb; pdb.set_trace()
        #print inspect.stack()[0][3], len(y)
        y = np.reshape(y, (len(y)/6, 6))
        Pxc, Pyc, Pzc = y[:,3], y[:,4], y[:,5]
        #if len(f_args)>0:
        #    size = size()
        #    clock = f_args[:,0]*1e-9
        #    charge = f_args[:,1]
        #    index = f_args[:,2]
        #    status = f_args[:,3]
        if self.clock is not None:
            clock = self.clock
        else:
            clock = np.zeros(len(y))
        
        #Ex, Ey, Ez, Bx, By, Bz = np.zeros((len(y), 6)).T[:]
        Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(y[:,0], y[:,1], y[:,2], t+clock, self.time).T[:]
        #Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(y[0,0], y[0,1], y[0,2], t)[0]
        
        
        
        g_mec2_eV = g_mec2*1e6
        gamma = np.sqrt(g_mec2_eV**2+Pxc**2+Pyc**2+Pzc**2)/g_mec2_eV
        cc0 = g_c/gamma/g_mec2_eV
        cc1 = g_mec2_eV/g_c*gamma
        cc2 = 1./gamma/g_me*g_qe
        
        
        #F2D = self.em3d.get_field(y[:,0], y[:,1], y[:,2], t+clock)
        #print F2D.T
        #if self.sc3d is not None:
        #    vz = Pzc*g_qe/g_me/gamma/g_c
        #    #print vz
        #    print self.sc3d(y[:,0], y[:,1], y[:,2]-(t-self.time)*vz)
        #    F2D = np.add(F2D, self.sc3d(y[:,0], y[:,1], y[:,2], t-self.time))
        #Ex, Ey, Ez, Bx, By, Bz = F2D.T[:]
        #import pdb; pdb.set_trace()
        
        dydt = np.zeros((len(y), 6))
        dydt[:,0] = Pxc*cc0
        dydt[:,1] = Pyc*cc0
        dydt[:,2] = Pzc*cc0
        dydt[:,3] = (Pyc*Bz-Pzc*By+cc1*Ex)*cc2
        dydt[:,4] = (Pzc*Bx-Pxc*Bz+cc1*Ey)*cc2
        dydt[:,5] = (Pxc*By-Pyc*Bx+cc1*Ez)*cc2

        return dydt.flatten()
    
    def step_i(self, i):
        y0 = self.get(i)
        t0 = self.time

        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()
        r.integrate(r.t+self.dt)

        # self.set(i, r.y)
        return r.y 
    
    def step(self, nstep = 1, num_cores = 1, NMAX = 20000, **kwagrs):
        '''
        Integrate all particles together
        Returns
          (x, y, z, Px*c, Py*c, Pz*c)
        '''
        
        while nstep>0:
            self.stepped += 1
            
            #if self.Lspch is True:
            #    sc3d = self.beam.get_sc3d(**kwagrs)
            #    self.em3d.update_external_field('spacecharge', sc3d)
            
            dist = self.beam.dist
            select = (dist[:,9]>0)
            dist = dist[select]
            if num_cores>1:
                # multi-core run
                # num = np.arange(self.NP)
                # results = Parallel(n_jobs=num_cores)(delayed(unwrap_step_i)(i) for i in zip([self]*len(num), num))
                # self.update(results)
                pass
            else:
                # run with one core
                nop = len(dist)
                if nop>NMAX:
                    nbeamlet = int(1.0*(nop-1)/NMAX)+1
                    #print inspect.stack()[0][3], 'beamlets: ', nbeamlet
                    r = ode(self.deriv).set_integrator('dopri5')
                    for i in np.arange(nbeamlet):
                        #print inspect.stack()[0][3], 'beamlet: #', i
                        i0 = i*NMAX
                        i1 = i*NMAX+NMAX
                        if i1>nop:
                            i1 = nop
                        y0 = dist[i0:i1,0:6].flatten()
                        t0 = self.time
                        
                        r.set_initial_value(y0, t0).set_f_params()
                        r.integrate(r.t+self.dt)
                        
                        if not r.successful():
                            print(inspect.stack()[0][3], 'Integration failed.')
                        
                        dist[i0:i1,0:6] = np.reshape(r.y, (i1-i0, 6))
                else:
                    r = ode(self.deriv).set_integrator('dopri5')
                    y0 = dist[:,0:6].flatten()
                    t0 = self.time

                    r.set_initial_value(y0, t0).set_f_params()
                    r.integrate(r.t+self.dt)
                    
                    if not r.successful():
                        print(inspect.stack()[0][3], 'Integration failed.')
                            
                    dist[:,0:6] = np.reshape(r.y, (nop, 6))
            
            select2 = (dist[:,5]<0)|(dist[:,2]<0)
            dist[select2,-1] = -89
            self.beam.dist[select] = dist
            
            
            self.time += self.dt
            nstep = nstep-1
            
            self.beam.diagnose()
            self.zmean = self.beam.x[0]
            
            if self.stepped % self.diag_interval == 0:
                self.beam.diagnose('trk.diag.%03d' % self.Run, em3d = self.em3d, time = self.time)
            
            #if self.stepped % self.dump_interval == 0:
            #    while int(self.zmean*self.dump_factor)<10:
            #        self.dump_factor *= 10
            #    self.beam.dump('trk.%04d.%03d' % (self.zmean*self.dump_factor, self.Run))
            
            #if self.stepped % self.dump_interval == 0:
            #    self.beam.dump('trk.%06d.%03d' % (self.stepped, self.Run))
            
            if self.stepped % 100 == 0:
                print(str.format('Tracking: iter = %6d, time = %12.6E' % (self.stepped, self.time)))
        return
    
    def dump(self):
        while int(self.zmean*self.dump_factor)<1:
            self.dump_factor *= 10
        self.beam.dump('trk.%04d.%03d' % (self.zmean*self.dump_factor, self.Run))
        
    def is_emit(self):
        select = (self.beam.dist[:,9]==-1)|(self.beam.dist[:,9]==-3)
        if np.sum(select)>0:
            return True
        else:
            return False
        
    def cathode(self, namelist, fnamebase = 'trk.cathode'):
        r = [self.beam.x[0], self.time]
        #if self.sc3d is not None:
        #    Ex, Ey, Ez, Bx, By, Bz = self.sc3d(0, 0, 0).T[:]
        #    r.append(Ez[0])
        #else:
        #    r.append(0)
        for name in namelist:
            Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(0, 0, 0, self.time, namelist = [name]).T[:]
            r.append(Ez[0])
        r.append(self.beam.x[9]) # Qtot
        
        with open(fnamebase+('.%03d' % self.Run), 'a') as f_handle:
            np.savetxt(f_handle, np.atleast_2d(r), fmt='%14.6E')
         
    def emit(self, nstep = 1, num_cores=1, NMAX = 10000, NMIN = 30, **kwargs):
        '''
        Integrate all particles together
        Returns
          (x, y, z, Px*c, Py*c, Pz*c)
        '''
        
        while nstep>0:
            self.stepped += 1
            
            #if self.Lspch is True and self.stepped>5:
            #    sc3d = self.beam.get_sc3d(**kwargs)
            #    self.em3d.update_external_field('spacecharge', sc3d)
            
            # Get the particles not emitted yet
            # dist = self.beam.dist
            select = (self.beam.dist[:,9]==-1)|(self.beam.dist[:,9]==-3)
            # Get also the particles emitted already
            select2 = (self.beam.dist[:,9]>0)
            dt_max = self.dt
            #import pdb; pdb.set_trace()
            
            # Select those to be emitted
            if np.sum(select)<=NMIN:
                sort = (self.beam.dist[select,6]).argsort()[::-1]; # print sort
                t1 = self.beam.dist[select][sort][0,6] # first particle to emit this time
                tc = self.beam.dist[select][sort][-1,6] # first particle to emit next time
                dt_max = (t1-tc)*1.05e-9
            else:
                sort = (self.beam.dist[select,6]).argsort()[::-1]; # print sort
                t1 = self.beam.dist[select][sort][0,6]
                tc = self.beam.dist[select][sort][NMIN,6]
                select *= (self.beam.dist[:,6]>tc)
                dt_max = (t1-tc)*1e-9
            dist_emit = self.beam.dist[select]
            
            if 1: 
                # Particles emission one by one
                for i in np.arange(len(dist_emit)):
                    y0 = dist_emit[i,0:6]
                    
                    if self.time is None:
                        self.time = -t1*1e-9
                        print('Initial time is set to ', self.time*1e12, ' ps')
                    
                    dt = dt_max-(t1-dist_emit[i,6])*1e-9
                    t0 = self.time+dt_max-dt
                    
                    Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(y0[0], y0[1], y0[2], t0).T[:]
                    if Ez[0]<=0:
                        dist_emit[i,9] = -89
                        continue
                    
                    r = ode(self.deriv).set_integrator('dopri5')
                    
                    r.set_initial_value(y0, t0).set_f_params()
                    r.integrate(r.t+dt)
                    
                    if not r.successful():
                        print(inspect.stack()[0][3], 'Integration failed.')
                    
                    dist_emit[i,0:6] = r.y
                    dist_emit[i,6] = t0/1e-9 # ns, emitting time
                    
                    if dist_emit[i,9] == -1:
                        dist_emit[i,9] = 5;
                    elif dist_emit[i,9] == -3:
                        dist_emit[i,9] = 3
                    else:
                        dist_emit[i,9] *= -1
            else:
                # Shift backwards such that all particles start movement at the same time
                nop = len(dist_emit)
                shift_dt = (t1-dist_emit[:,6])*1e-9
                Pxc, Pyc, Pzc = dist_emit[:,3], dist_emit[:,4], dist_emit[:,5]
                g_mec2_eV = g_mec2*1e6
                gamma = np.sqrt(g_mec2_eV**2+Pxc**2+Pyc**2+Pzc**2)/g_mec2_eV
                cc0 = g_qe/(gamma*g_me*g_c)
                dist_emit[:,0] += -Pxc*cc0*shift_dt
                dist_emit[:,1] += -Pyc*cc0*shift_dt
                dist_emit[:,2] += -Pzc*cc0*shift_dt

                y0 = dist_emit[:,0:6].flatten()
                dt = dt_max
                t0 = self.time
                r = ode(self.deriv).set_integrator('dopri5')

                r.set_initial_value(y0, t0).set_f_params()
                r.integrate(r.t+dt)
                if not r.successful():
                    print(inspect.stack()[0][3], 'Integration failed.')
                
                dist_emit[:,0:6] = np.reshape(r.y, (nop, 6))
                dist_emit[:,9] = np.where(dist_emit[:,9]==-1, 5, -dist_emit[:,9])                   
            
            # Replace the distributions with those
            self.beam.dist[select] = dist_emit
            
            # Now it's turn to advance one step for those emitted already
            dist = self.beam.dist[select2]
            
            if num_cores>1:
                # multi-core run
                pass
            else:
                # run with one core
                nop = len(dist); # print inspect.stack()[0][3], nop
                if nop == 0:
                    pass
                elif nop>NMAX:
                    nbeamlet = int(1.0*(nop-1)/NMAX)+1
                    #print inspect.stack()[0][3], 'beamlets: ', nbeamlet
                    r = ode(self.deriv).set_integrator('dopri5')
                    for i in np.arange(nbeamlet):
                        #print inspect.stack()[0][3], 'beamlet: #', i
                        i0 = i*NMAX
                        i1 = i*NMAX+NMAX
                        if i1>nop:
                            i1 = nop
                        y0 = dist[i0:i1,0:6].flatten()
                        t0 = self.time
                        r.set_initial_value(y0, t0).set_f_params()
                        r.integrate(r.t+dt_max)
                        
                        if not r.successful():
                            print(inspect.stack()[0][3], 'Integration failed.')
                        
                        dist[i0:i1,0:6] = np.reshape(r.y, (i1-i0, 6))
                else:
                    r = ode(self.deriv).set_integrator('dopri5')
                    y0 = dist[:,0:6].flatten()
                    t0 = self.time
                    r.set_initial_value(y0, t0).set_f_params()
                    r.integrate(r.t+dt_max)
                    
                    if not r.successful():
                        print(inspect.stack()[0][3], 'Integration failed.')
                    
                    dist[:,0:6] = np.reshape(r.y, (nop, 6))
            
            select3 = (dist[:,5]<0)|(dist[:,2]<0)
            dist[select3,-1] = -89
            self.beam.dist[select2] = dist
            
            self.time += dt_max
            nstep = nstep-1
            
            self.beam.diagnose()
            if self.stepped % self.diag_interval == 0:
                self.beam.diagnose('trk.diag.%03d' % self.Run, em3d = self.em3d, time = self.time)
            if self.stepped % self.dump_interval == 0:
                self.beam.dump('trk.%06d.%03d' % (self.stepped, self.Run))
            
            if self.stepped % 100 == 0:
                print(str.format('Emitting: iter = %6d, time = %12.6E' % (self.stepped, self.time)))
        return
    
    def ref(self, y0, t1, dt, t0 = 0):
        '''
        Parameters
          y0: (x, y, z, P_x*c, P_y*c, P_z*c)
          t1: end time for tracking
          dt: time step for tracking
          t0: start time for tracking
        Returns
          2D arraylike: (x, y, z, P_x*c, P_y*c, P_z*c, t)
        '''

        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()

        sol = [list(y0)+[t0]+list(self.em3d.get_field(y0[0], y0[1], y0[2], t0)[0])]
        while r.successful() and r.t<t1:
            r.integrate(r.t+dt)
            
            a = list(r.y)+[r.t]+list(self.em3d.get_field(r.y[0], r.y[1], r.y[2], r.t)[0])
            
            with open('trk.ref'+('.%03d' % self.Run), 'a') as f_handle:
                np.savetxt(f_handle, np.atleast_2d(a), fmt='%14.6E')
            
            #sol.append(a)
        
        #sol = np.array(sol)
        #np.savetxt('trk.ref.001', sol, fmt = '%12.6E')
        return sol  
    
    def auto_phase(self, freq = 1.3e9, t1 = 1000e-12, dt = 10e-12):
        '''
        Returns
          2D arraylike: (x, y, z, P_x*c, P_y*c, P_z*3, t)
        '''
        y0 = self.beam.dist[0]
        dist = np.array([y0 for i in np.arange(901)])
        #print inspect.stack()[0][3], dist.shape
        
        phase = np.linspace(0, 90, 901)
        self.clock = phase/360./freq
        #print self.clock*1e12
        
        t0 = 0
        y0 = dist[:,0:6].flatten(); #print inspect.stack()[0][3], len(y0)
        
        #import pdb; pdb.set_trace()
        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()
        
        r.integrate(r.t+dt)
        #import pdb; pdb.set_trace()
        #print inspect.stack()[0][3], r.successful()
        if not r.successful():
            print(inspect.stack()[0][3], 'Integration failed.')
        
        while r.successful() and r.t < t1:
            #print inspect.stack()[0][3], r.t
            r.integrate(r.t+dt)
        #print inspect.stack()[0][3], r.t
        
        dist = np.reshape(r.y, (len(r.y)/6, 6))
        np.savetxt('auto.dat', dist, fmt = '%12.6E')
        
        i, m = index_max(dist[:,5])
        p0 = phase[i]
        #select = (phase>p0-10)*(phase<p0+10)
        
        #fun = lambda x, a, b, c, d, e: a+b*x+c*x**2+d*x**3+e*x**4
        #popt, pcov = sp.optimize.curve_fit(fun, phase[select], 1./dist[select,5])
        #res = sp.optimize.minimize(fun, p0, tuple(popt))
        
        fig, ax = plt.subplots(figsize = (6, 4))
        ax.plot(phase[:], dist[:,5]/1e6, '-')
        #ax.plot(phase[select], 1./fun(phase[select], *popt)/1e6, '-')
        ax.grid()
        ax.set_xlabel(r'phase (degree)')
        ax.set_ylabel(u_kinetic)
        fig.savefig('phase_scan.eps')
        
        #return [res.x[0], momentum2kinetic(1./fun(res.x[0], *popt)/1e6)]
        return [p0, momentum2kinetic(m/1e6)]

def emitting():
    dist = np.loadtxt('beam1.ini')
    NMIN = 20

    select = (dist[:,9]==-1)|(dist[:,9]==-3); print(np.sum(select))
    #import pdb; pdb.set_trace()

    if np.sum(select)<=NMIN:
        dist_emit = dist[select]
        sort = (dist[select,6]).argsort()[::-1]; # print sort
        t0 = dist[select][sort][0,6]
        tc = dist[select][sort][-1,6];  print(tc, (t0-tc)*1e3)
    else:
        sort = (dist[select,6]).argsort()[::-1]; # print sort
        t0 = dist[select][sort][0,6]
        tc = dist[select][sort][NMIN,6]; print(tc, (t0-tc)*1e3)
        select *= (dist[:,6]>tc)
        dist_emit = dist[select]

    dist_emit[:,9] = 5
    dist[select] = dist_emit

    np.savetxt('beam2.ini', dist, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4.0f%4.0f')

    fig, ax = plt.subplots(figsize = (6, 4))
    ax.hist(dist[:,6], bins = 20, range = (-0.003, 0.003))
    select = (dist[:,9]==5)
    ax.hist(dist[select,6], bins = 20, range = (-0.003, 0.003), histtype = 'step')
    return
#emitting()

'''
workdir = r"\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python\space-charge"
os.chdir(workdir)

field_maps = r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\field-maps'
gun3d = build_cavity(field_maps+os.sep+'gun42Cavity.txt', 0, 39.44e6, 1.3e9, 37)
sol3d = build_solenoid(field_maps+os.sep+'gunsolenoidsPITZ.txt', 0, -0.158797)

em3d = EMsolver()
em3d.add_external_field('gun', gun3d)
em3d.add_external_field('solenoid', sol3d)

#beam = Beam('beam.ini')
#sc3d = beam.get_sc3d()

#em3d.add_external_field('spacecharge', sc3d)


# In[563]:


import timeit

beam = Beam('beam1.ini'); # beam.dist=beam.dist[0:100000]
track = Tracking(beam, em3d, 1000e-12, 1e-12, out_inteval = 250)

n_steps = 2000
n_stepped = 0
stime = 0
while n_stepped<n_steps:
    time1 = timeit.default_timer()
    
    if track.is_emit():
        flag = 'Emitting'
        nstep = 1
        track.emit(nstep, NMIN = 100)
    else:
        flag = 'Tracking'
        nstep = 10
        track.step(nstep)
    
    time2 = timeit.default_timer()
    stime = stime+(time2-time1)

    n_stepped += nstep

    #print flag, ': 1 step takes: ', stime/n_stepped

print 'Total time:', stime
'''

# In[559]:

'''
data = np.loadtxt('trk.000050.001')
data[1:,2] += data[0,2]; data[1:,5] += data[0,5]
select = (data[:,9]>0)
print np.std(data[select,5]/1e3), np.mean(data[select,5]/1e3)
r = plt.hist(data[select,5]/1e3, bins = 30, histtype = 'step')
'''

# In[473]:

'''
beam = Beam('beam1.ini')
#beam.dist=beam.dist[0:100000]
track = Tracking(beam, em3d, 1000e-12, 1e-12)
#print track.beam.dist
import timeit
time1 = timeit.default_timer()
ref = track.step(1, NMAX = 3000)
#x = track.beam.dist[:,0]
#y = track.beam.dist[:,1]
#z = track.beam.dist[:,2]
#F2d = track.em3d.get_field(x, y, z, 0)
time2 = timeit.default_timer()
print time2-time1


beam = Beam('beam.ini')
track = Tracking(beam, em3d, 1000e-12, 1e-12)

import timeit
time1 = timeit.default_timer()
phi = track.auto_phase()
time2 = timeit.default_timer()
print time2-time1
print phi
'''

# In[238]:

'''
track = Tracking(beam, em3d, 1000e-12, 1e-12)
y0 = track.beam.dist[0,0:6]

import timeit
time1 = timeit.default_timer()
ref = track.ref(y0, 1000e-12, 10e-12)
time2 = timeit.default_timer()
print time2-time1
print track.time

fig, [ax1, ax2] = plt.subplots(figsize = (6, 8), nrows = 2)
Ek = momentum2kinetic(ref[:,5]/1e6); # print Ek[-1]
ax1.plot(ref[:,2], Ek, '-')
ax1.grid()

ax2.plot(ref[:,2], ref[:,9]/1e6, '-')
ax2.grid()
'''