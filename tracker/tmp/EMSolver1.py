# coding: utf-8
# get_ipython().magic(u'matplotlib inline')
# %matplotlib notebook

from universal import *

from scipy.optimize import minimize
from scipy.misc import derivative
from scipy.interpolate import interp2d
from scipy import signal
import pickle as pickle

from .SpaceCharge3DFFT import *

class EMsolver():
    
    def __init__(self):
        self.em3d = {}
        
    def add_external_field(self, name, func):
        self.em3d.update({name:func})
        
    def update_external_field(self, name, func):
        self.em3d.update({name:func})
        
    def get_field(self, x, y, z, t = 0, *args, **kwargs):
        field = np.zeros((1, 6))
        
        if len(kwargs)>0:
            namelist = kwargs['namelist']
        else:
            namelist = list(self.em3d.keys())
        for name, func in self.em3d.items():
            if name in namelist:
                field = np.add(field, func(x, y, z, t, *args))
        return field


def build_cavity(filename, z0, amp, freq, phi0 = 0, zratio = 1, Eratio = 1):
    #print phi_0
    data = np.loadtxt(filename)
    data[:,0] *= zratio
    data[:,1] *= Eratio
    
    a, b = np.max(data[:,1]), np.min(data[:,1])
    Ez_z = data[:,1]/a*amp
    
    dz = data[1,0]-data[0,0]
    
    dEz_dz1 = np.gradient(   Ez_z, dz)
    dEz_dz2 = np.gradient(dEz_dz1, dz)
    dEz_dz3 = np.gradient(dEz_dz2, dz) 
    
    fEz_0 = interp1d(data[:,0],    Ez_z, bounds_error = False, fill_value = 0)
    fEz_1 = interp1d(data[:,0], dEz_dz1, bounds_error = False, fill_value = 0)
    fEz_2 = interp1d(data[:,0], dEz_dz2, bounds_error = False, fill_value = 0)
    fEz_3 = interp1d(data[:,0], dEz_dz3, bounds_error = False, fill_value = 0)
    
    def EM3D(x, y, z, t = 0, *args):
        if np.isscalar(x):
            x = [x]
            y = [y]
            z = [z]
        x = np.asarray(x)
        y = np.asarray(y)
        z = np.asarray(z)
        
        if not np.isscalar(t):
            t = np.asarray(t)

        omega = 2.*np.pi*freq
        phi = (phi0+0)*np.pi/180

        ww = omega**2/g_c**2
        
        r = np.sqrt(x*x+y*y)
        
        z1 = z-z0
        Ez0 = fEz_0(z1); #print Ez0
        Ez1 = fEz_1(z1)
        Ez2 = fEz_2(z1)
        Ez3 = fEz_3(z1)
        
        omega_t = omega*t+phi
        ss = np.sin(omega_t)
        cc = np.cos(omega_t)
        
        Ez = (     Ez0-r**2/4. *(Ez2+ww*Ez0))*ss
        Er = (-0.5*Ez1+r**2/16.*(Ez3+ww*Ez1))*ss # Er/r instead of Er
        Bt = ( 0.5*Ez0-r**2/16.*(Ez2+ww*Ez0))*ww/omega*cc # Bt/r instead of Bt
        
        Ez = np.where(z<0, 0, Ez)
        Er = np.where(z<0, 0, Er)
        Bt = np.where(z<0, 0, Bt)
        
        n = np.max([len(x), len(y), len(z), len(x.T), len(y.T), len(z.T)])
        F2d = np.zeros((n, 6))
        F2d[:,0] = x*Er
        F2d[:,1] = y*Er
        F2d[:,2] = Ez
        F2d[:,3] =-y*Bt
        F2d[:,4] = x*Bt
        #F2d = [x*Er, y*Er, Ez, -y*Bt, x*Bt, 0*Bt]
        return F2d
    
    return EM3D

def build_solenoid(filename, z0, amp):
    
    data = np.loadtxt(filename)
    
    a, b = np.max(data[:,1]), np.min(data[:,1])
    Fz_z = data[:,1]/a*amp
    
    dz = data[1,0]-data[0,0]
    
    dFz_dz1 = np.gradient(Fz_z, dz)
    dFz_dz2 = np.gradient(dFz_dz1, dz)
    dFz_dz3 = np.gradient(dFz_dz2, dz)
    dFz_dz4 = np.gradient(dFz_dz3, dz)
    dFz_dz5 = np.gradient(dFz_dz4, dz)
    dFz_dz6 = np.gradient(dFz_dz5, dz)
    
    fFz_0 = interp1d(data[:,0], Fz_z,    bounds_error = False, fill_value = 0)
    fFz_1 = interp1d(data[:,0], dFz_dz1, bounds_error = False, fill_value = 0)
    fFz_2 = interp1d(data[:,0], dFz_dz2, bounds_error = False, fill_value = 0)
    fFz_3 = interp1d(data[:,0], dFz_dz3, bounds_error = False, fill_value = 0)
    fFz_4 = interp1d(data[:,0], dFz_dz4, bounds_error = False, fill_value = 0)
    fFz_5 = interp1d(data[:,0], dFz_dz5, bounds_error = False, fill_value = 0)
    fFz_6 = interp1d(data[:,0], dFz_dz6, bounds_error = False, fill_value = 0)
     
    def EM3D(x, y, z, t = 0, *args):
        
        if np.isscalar(x):
            x = [x]
            y = [y]
            z = [z]
        x = np.asarray(x)
        y = np.asarray(y)
        z = np.asarray(z)
        
        if not np.isscalar(t):
            t = np.asarray(t)
        
        r = np.sqrt(x*x+y*y)
        
        z1 = z-z0
        Fz0 = fFz_0(z1)
        Fz1 = fFz_1(z1)
        Fz2 = fFz_2(z1)
        Fz3 = fFz_3(z1)
        Fz4 = fFz_4(z1)
        Fz5 = fFz_5(z1)
        Fz6 = fFz_6(z1)
        
        Bz = (Fz0-r*r/4.*Fz2+r**4/64.*Fz4-r**6/2034.*Fz6)
        Br = (-1/2.*Fz1+r**2/16.*Fz3-r**4/384.*Fz5) # Br/r instead of Br
        
        Bz = np.where(z<0, 0, Bz)
        Br = np.where(z<0, 0, Br)
        
        n = np.max([len(x), len(y), len(z), len(x.T), len(y.T), len(z.T)]); #print 'hello', n
        #import pdb; pdb.set_trace()
        F2d = np.zeros((n, 6))
        F2d[:,3] = x*Br
        F2d[:,4] = y*Br
        F2d[:,5] = Bz
        #F2d = [0, 0, 0, x*Br, y*Br, Bz]
        return F2d
    return EM3D



'''
field_maps = r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\field-maps'
gun3d = build_cavity(field_maps+os.sep+'gun42Cavity.txt', 0, 60, 1.3e9, 90)
sol3d = build_solenoid(field_maps+os.sep+'gunsolenoidsPITZ.txt', 0, 0.2)

em3d = EMsolver()
em3d.add_external_field('gun', gun3d)
em3d.add_external_field('solenoid', sol3d)

x, y, z = [0, 0, 0], [0, 0, 0.01], [0, 0.1, 0.1]
t = 0
#print gun3d(0, 0, 0), gun3d(0, 0, 0.1), gun3d(0, 0.01, 0.1)
get_ipython().magic(u'timeit gun3d(x, y, z, t)')
a = em3d.get_field([0, 0, 0], [0, 0, 0.01], [0, 0.1, 0.1]); print a
Ex, Ey, Ez, Bx, By, Bz = a.T[:]
print Ex, Ey, Ez

rr = np.linspace(0, 0.1, 200)
fr = np.array([em3d.get_field(v, 0, 0)[0,0] for v in rr])
zz = np.linspace(-0.1, 0.5, 200)
fz = np.array([em3d.get_field(0, 0.01, v, 0)[0,5] for v in zz])

fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (12, 4))
ax1.plot(rr, fr, '-')
ax1.grid()

ax2.plot(zz, fz, '-')
ax2.grid()
'''