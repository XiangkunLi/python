import sys
if sys.platform == "linux" or sys.platform == "linux2":
    sys.path.append(r'/afs/ifh.de/group/pitz/data/lixiangk/work/sync/python')
    field_maps = r'/afs/ifh.de/group/pitz/data/lixiangk/work/sync/field-maps'
elif sys.platform == "win32":
    sys.path.append(r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python')
    field_maps = r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\field-maps'
elif sys.platform == "darwin":
    print("OS X")
else:
    print("Unknown platform!")

I2B = lambda I: -(0.0000372+0.000588*I)
B2I = lambda B: (-B-0.0000372)/0.000588

# mm, pC, A
BSA = float(sys.argv[1])
Qtot = float(sys.argv[2])*1e-12
Bsol = I2B(float(sys.argv[3]))

print(sys.argv, BSA, Qtot, Bsol)

import timeit

from astra_modules import *
from tracker import *

import matplotlib as mpl
import matplotlib.pyplot as plt
plt.switch_backend('agg')

#workdir = r"\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python\space-charge"
#os.chdir(workdir)

gun3d = build_cavity(field_maps+os.sep+'gun42cavity.txt', 0, 59.04e6, 1.3e9, 224.70-180)
#boo3d = build_cavity(field_maps+os.sep+'CDS14_15mm.txt', 2.675, 12.50e6, 1.3e9, 28.20-20)
boo3d = build_cavity(field_maps+os.sep+'CDS14_15mm.txt', 2.675, 12.50e6, 1.3e9, 105.81-20)
sol3d = build_solenoid(field_maps+os.sep+'gunsolenoidsPITZ.txt', 0, Bsol) # 270 A

em3d = EMsolver()
em3d.add_external_field('gun', gun3d)
em3d.add_external_field('booster', boo3d)
em3d.add_external_field('solenoid', sol3d)
#import pdb; pdb.set_trace()

Run = 4 # 2D 200k 2020.01.19
Run = 1 # 2D 200k 2020.01.20
Run = 5 # 2D 200k 2020.01.29
Run = 6 # 2D 200k 2020.01.29
Run = 7 # 2D 200k 2020.01.29

beamdist = '..'+os.sep+str.format('beam2D_500k.ini')
beamdist = str.format('trk.0005.001')
#beamdist = 'trk.0005.017'
#beamdist = 'ast.0050.002'
beam = Beam(beamdist, Qtot = Qtot, debug = 0); # beam.dist=beam.dist[0:100000]
nop = len(beam.dist)

Nmin = 250
dump_interval = nop/Nmin/2
screens = [0.3, 0.5, 1.0, 5.28]

#t0 = 1.786915E-11
t1, dt, t0 = 20000e-12, 2e-12, 0
track = Tracking(beam, em3d, t1, dt, 0, diag_interval = 1, dump_interval = dump_interval, Lspch = True, Run = Run)
track.beam.diagnose(em3d = track.em3d, time = t0)
print(track.beam.x)
#track.beam.demo()

#y0 = beam.dist[0,0:6]
#track.ref(y0, t1, dt*10, 5e-12)
exit()

n_steps = 200000
n_stepped = 0
stime = 0
zmean = 0

import gc
import psutil
proc = psutil.Process(os.getpid())

while n_stepped<n_steps and zmean < 5.28:
    time1 = timeit.default_timer()
    #import pdb; pdb.set_trace()
    if track.is_emit():
        flag = 'Emitting'
        nstep = 1
        
        nz = 64
        kwargs = dict(nz = nz, Lemit = True, Lmirror = True)
        if n_stepped % dump_interval == 0:
            kwargs = dict(nz = nz, Lemit = True, Lmirror = True, fig_ext = '.%04d.%03d.eps' % (n_stepped, Run))
            
        if track.Lspch is True and track.stepped>1:
            sc3d = track.beam.get_sc3d(**kwargs)
            track.em3d.update_external_field('spacecharge', sc3d)
            #track.sc3d = sc3d
            track.cathode(['spacecharge', 'gun'], 'trk.cathode')
        
        track.emit(nstep, NMIN = Nmin, **kwargs)
    else:
        flag = 'Tracking'
        nstep = 1
        
        nz = 32
        kwargs = dict(nz = nz, Lemit = False, Lmirror = False)
        #if n_stepped % 500 == 0:
        #    kwargs = dict(nz = nz, Lemit = False, Lmirror = True, fig_ext = '.%04d.%03d.eps' % (n_stepped, Run))
        
        if track.Lspch is True and track.stepped>1:
            #if sc3d in locals():
            #    del sc3d
            sc3d = track.beam.get_sc3d(**kwargs)
            track.em3d.update_external_field('spacecharge', sc3d)
            track.cathode(['spacecharge', 'gun'], 'trk.cathode')
            
        #track.dump_interval = 500
        
        track.step(nstep, NMAX = 50000, **kwargs)

    plt.close('all')

    gc.collect()
    mem0 = proc.memory_info().rss
    print('Current memory: ', mem0/1e9)

    zmean = track.beam.x[0]
    for scr in screens:
        if np.abs(zmean>=scr):
            track.dump()
            screens.remove(scr)
    
    time2 = timeit.default_timer()
    stime = stime+(time2-time1)

    n_stepped += nstep

    if n_stepped % 1 == 0:
        print('Current -> ', flag, ': on average 1 step takes: ', stime/n_stepped)

track.dump()
print('Total time:', stime)
