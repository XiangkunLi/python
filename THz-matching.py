# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 00:10:39 2021

@author: lixiangk
"""

# from IPython.display import Image
import numpy as np
import scipy as sp
from scipy.interpolate import interp1d, interp2d
from scipy.optimize import curve_fit

from scipy.special import jn, jn_zeros, j0, j1
from scipy.integrate import quad, ode

from scipy.constants import codata

import matplotlib as mpl
import matplotlib.pyplot as plt
from cycler import cycler
from matplotlib.ticker import AutoMinorLocator

import os, re

from timeit import default_timer
import time

from universal import *

#%% Simulation vs simulation
def make_folder(y):
    direc = ''
    for i, xi in enumerate(y):
        direc += str.format('Q%d-%.2fT_m-' %  (i+1, xi))
    direc = direc[:-1]
    return direc

def polyfunc(x, *popt):
    x = np.atleast_1d(x)
    r = np.zeros(len(x))
    for i, v in enumerate(popt):
        r += v*x**i
    return r

def polyfit(x, y, *args, **kwargs):
    
    def func(x, a, b, c):
        return a+b*x+c*x*x
        
    popt, pcov = curve_fit(func, x, y, *args, **kwargs)
    return popt, pcov

#%% Backward High3.Scr1  to High2.Scr3
workdir = r'scan-und-to-High2.Scr3'
os.chdir(workdir)

res = np.loadtxt('results.dat')

reset_margin()
fig, ax = plt.subplots(); ax.grid()

temp = []
for i, v in enumerate(res):
    #print(v[0:3][::-1])
    direc = make_folder(v[0:3])
    print(direc)

    data = np.loadtxt(direc+os.sep+'BeamDynamics.dat')
    select = data[:,0]>0.8+0.2685+0.0675
    data = data[select]
    
    #fx = easy_interp1d(data[:,0], data[:,3])
    #fy = easy_interp1d(data[:,0], data[:,4])
    #temp.append([v[0], v[1], v[2], fx(5.227), fy(5.227), fx(0.46), fy(0.46)])
    
    popt, pcov = polyfit(data[:,0], data[:,3])
    x1, x2 = polyfunc([5.227, 0.46], *popt)
    popt, pcov = polyfit(data[:,0], data[:,4])
    y1, y2 = polyfunc([5.227, 0.46], *popt)#polyfunc(5.227), polyfunc(0.46)
    
    zz = np.linspace(0, 5.5)
    ax.plot(data[:,0], data[:,4], '*')
    ax.plot(zz, polyfunc(zz, *popt), '--')
    
    temp.append([v[0], v[1], v[2], x1, y1, x2, y2])

temp = np.array(temp); temp1 = temp
np.savetxt('RMS-size-with-focusing.dat', temp, fmt = '%12.6f')

fig, ax = plt.subplots(figsize = (3, 3))
ax.plot(temp[1:,3], temp[1:,5], '-o')
ax.grid()
ax.set_xlabel(r'RMS size at High2.Scr3 (mm)')
ax.set_ylabel(r'RMS size at High3.Scr1 (mm)')

fig.savefig('RMS-size-with-focusing.png')

os.chdir('..')

#%% Forward EMSY1 to High1.Scr4 to PST.Scr1
workdir = r'scan-High1.Scr1-to-High3.Scr1'
os.chdir(workdir)

res = np.loadtxt('results.dat')

#fig, ax = plt.subplots(); ax.grid()

temp = []
for i, v in enumerate(res[2:]):
    #print(v[0:3][::-1])
    direc = make_folder(v)
    print(direc)

    data = np.loadtxt(direc+os.sep+'BeamDynamics.dat')
    
    fx = interp1d(data[:,0], data[:,3])
    fy = interp1d(data[:,0], data[:,4])
    
    #ax.plot(data[:,0], data[:,3], '--')
    
    temp.append([v[0], v[1], v[2], v[3], fx(17.58), fy(17.58), fx(21.67), fy(21.67)])

temp = np.array(temp); temp2 = temp
np.savetxt('RMS-size-with-focusing.dat', temp, fmt = '%12.6f')

#fig, ax = plt.subplots(figsize = (5, 4))
ax.plot(temp[1:,4], temp[1:,6], '-o')
#ax.grid()
ax.set_xlabel(r'RMS size at High2.Scr3 (mm)')
ax.set_ylabel(r'RMS size at High3.Scr1 (mm)')

ax.set_xlim(0.5, 2.5)
ax.set_ylim(0.9, 1.5)
ax.legend(['Backward tracking', 'Forward tracking'], loc = 'upper left')
fig.savefig('RMS-size-with-focusing.pdf')

os.chdir('..')

#%%
# fig, ax = plt.subplots()
# ax.plot(temp[:,0], -temp[:,1], '-<')
# ax.plot(temp[:,0], temp[:,2], '-<')

from intersect import intersection

x, y = intersection(temp1[:,3], temp1[:,5], temp2[:,4], temp2[:,6])

T2 = np.zeros(4)
for i in [0, 1, 2, 3]:
    fQ = interp1d(temp2[:,4], temp2[:,i])
    T2[i] = fQ(x)[0]
print(T2)

T1 = np.zeros(3)
for i in [0, 1, 2]:
    fQ = interp1d(temp1[:,3], temp1[:,i])
    T1[i] = fQ(x)[0]
print(T1)    
#%% Simulation vs experiment
def Gx2I(G):
    if G<0:
        r = -1.4578783*G+0.11869394
    else:
        r = -1.45460901*G-0.10700756
    return r

def Gy2I(G):
    if G>0:
        r = 1.40239547*G+0.1401052
    else:
        r = 1.41152547*G-0.14604085
    return r

def G2I(G):
    return 0.5*(Gx2I(G)+Gy2I(-G))


workdir = r'C:\Users\lixiangk\Desktop\THz-transport'
os.chdir(workdir)

h2p = np.loadtxt('High1-to-PST.dat')
p2h = np.loadtxt('PST-to-High1.dat')

#
f1 = interp1d(h2p[:,-1], h2p[:,-2], fill_value = 'extrapolate', bounds_error = False) # y is beam size at High1.Scr4
f2 = interp1d(p2h[:,5], p2h[:,3], fill_value = 'extrapolate', bounds_error = False) # y is beam size at High1.Scr4


tmp_x = np.linspace(1., 2)
tmp_y = f1(tmp_x)-f2(tmp_x)
f0 = interp1d(tmp_y, tmp_x, fill_value = 'extrapolate', bounds_error = False) # y is beam size at PST.Scr1

matched_rms_pst = f0(0)
matched_rms_high = f1(f0(0))
print('intersection: %.2f mm at High1.Scr4 and %.2f mm at PST.Scr1' % (matched_rms_high, matched_rms_pst))

fI_Q4 = interp1d(h2p[:,3], h2p[:,0], fill_value = 'extrapolate', bounds_error = False) # rms at H1S4 vs I_Q4
I_Q4 = fI_Q4(matched_rms_high)
print('High1.Q4 current is ', I_Q4, ' A')

fI_Q6 = interp1d(h2p[:,3], h2p[:,1], fill_value = 'extrapolate', bounds_error = False) # rms at H1S4 vs I_Q4
I_Q6 = fI_Q6(matched_rms_high)
print('High1.Q6 current is ', I_Q6, ' A')

fI_Q7 = interp1d(h2p[:,3], h2p[:,2], fill_value = 'extrapolate', bounds_error = False) # rms at H1S4 vs I_Q4
I_Q7 = fI_Q7(matched_rms_high)
print('High1.Q7 current is ', I_Q7, ' A')

fI_QM2 = interp1d(p2h[:,3], p2h[:,0], fill_value = 'extrapolate', bounds_error = False) # rms at H1S4 vs I_QM2
I_QM2 = G2I(fI_QM2(matched_rms_high))
print('PST.QM2 current is ', I_QM2, ' A')

fI_QM3 = interp1d(p2h[:,3], p2h[:,1], fill_value = 'extrapolate', bounds_error = False) # rms at H1S4 vs I_QM2
I_QM3 = G2I(fI_QM3(matched_rms_high))
print('PST.QM2 current is ', I_QM3, ' A')

fI_QT1 = interp1d(p2h[:,3], p2h[:,2], fill_value = 'extrapolate', bounds_error = False) # rms at H1S4 vs I_QT1
I_QT1 = G2I(fI_QT1(matched_rms_high))
print('PST.QM2 current is ', I_QT1, ' A')

#%%

workdir = r'scan-PST.Scr3-to-PST.Scr1-new\Q1-1.95T_m-Q2--3.53T_m-Q3-1.49T_m'
#workdir = r'scan-PST.Scr3-to-PST.Scr1-new\Q1-2.00T_m-Q2--3.61T_m-Q3-1.56T_m'
os.chdir(workdir)

fig, ax = plt.subplots(figsize = (4.5, 3))

data = np.loadtxt('BeamDynamics-backward.dat')
ax.plot(data[:,0]+8.41, data[::-1,3], 'r-')
ax.plot(data[:,0]+8.41, data[::-1,4], 'b-')
ax.plot([], [], 'r:<')
ax.plot([], [], 'b:>')

data = np.loadtxt('BeamDynamics-forward.dat')
ax.plot(data[:,0]+13.798, data[:,3], 'r-')
ax.plot(data[:,0]+13.798, data[:,4], 'b-')

data = np.loadtxt('matched1-1-2.txt')
#data = np.loadtxt('matched2-2.txt')

ax.plot(data[:,0], data[:,1], 'r--<')
ax.plot(data[:,0], data[:,2], 'b-->')


# data = np.loadtxt('matched1-2.txt')
# data = np.loadtxt('matched2-2.txt')

# ax.plot(data[:,0], data[:,2], 'r:<')
# ax.plot(data[:,0], data[:,1], 'b:>')

ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel('RMS size (mm)')
ax.legend(['Simulated, $x$', 'Simulated, $y$', 'Measured, $x$', 'Measured, $y$'],
          ncol = 1, fontsize = 11, loc = 'lower left', handlelength = 2)

ax.set_ylim(0, 2.)
ax.set_yticks([0, 0.5, 1, 1.5, 2])
#fig.savefig('matching2-2.png')
fig.savefig('matched-transport-experiment.pdf')

os.chdir('../..')
#%%
data = pd_loadtxt('../ast.2745.001')

data[:,3] *= -1
data[:,4] *= -1
data[1:,5] *= -1

np.savetxt('../forward.2745.001', data, ast_fmt)


#%%
workdir = r'C:\Users\lixiangk\Desktop\sco-triplet-tuning4\Q1-2.20T_m-Q2--3.60T_m-Q3-1.18T_m'
os.chdir(workdir)

data = np.loadtxt('BeamDynamics.dat')

fig, ax = plt.subplots(figsize = (5, 3))
ax.plot(data[:,0], data[:,3], '-')
ax.plot(data[:,0], data[:,4], '-')

ax.set_xlim(5, 0)
ax.set_ylim(0, 2)

ax.grid()

ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'RMS size (mm)')
ax.legend(['$x$', '$y$'])

ax.set_yticks([0, 0.5, 1., 1.5, 2])

plt.tight_layout()
fig.savefig('example.pdf')

#%% Astra simulation of matching
#   Backward High3.Scr1  to High2.Scr3
workdir = r'scan-und-to-High2.Scr3_2'
os.chdir(workdir)

res = np.loadtxt('results-backward-astra2.dat')

reset_margin()
fig, ax = plt.subplots(); ax.grid()

temp = []
for i, v in enumerate(res):
    #print(v[0:3][::-1])
    direc = make_folder(v[0:3])
    print(direc)

    data = np.loadtxt(direc+os.sep+'ast.Xemit.003')
    select = data[:,0]>0.8+0.2685+0.0675+0.2
    data = data[select]
    
    #fx = easy_interp1d(data[:,0], data[:,3])
    #fy = easy_interp1d(data[:,0], data[:,4])
    #temp.append([v[0], v[1], v[2], fx(5.227), fy(5.227), fx(0.46), fy(0.46)])
    
    popt, pcov = polyfit(data[:,0], data[:,3])
    x1, x2 = polyfunc([4.75, 0.46], *popt)
    
    data = np.loadtxt(direc+os.sep+'ast.Yemit.003')
    select = data[:,0]>0.8+0.2685+0.0675+0.2
    data = data[select]
    
    popt, pcov = polyfit(data[:,0], data[:,3])
    y1, y2 = polyfunc([4.75, 0.46], *popt)#polyfunc(5.227), polyfunc(0.46)
    
    zz = np.linspace(0, 5.5)
    ax.plot(data[:,0], data[:,3], '*')
    ax.plot(zz, polyfunc(zz, *popt), '--')
    
    temp.append([v[0], v[1], v[2], x1, y1, x2, y2])

temp = np.array(temp); temp1 = temp
np.savetxt('RMS-size-with-focusing.dat', temp, fmt = '%12.6f')

fig, ax9 = plt.subplots(figsize = (4, 4))
ax9.plot(temp[:,3], temp[:,5], '-o')
ax9.grid()
ax9.set_xlabel(r'RMS size at High2.Scr3 (mm)')
ax9.set_ylabel(r'RMS size at High3.Scr1 (mm)')

fig.savefig('RMS-size-with-focusing.png')

os.chdir('..')

#%% Forward EMSY1 to High1.Scr4 to PST.Scr1
workdir = r'scan-High2.Scr3-to-und'
os.chdir(workdir)

res = np.loadtxt('results-forward-astra.dat')

fig, ax = plt.subplots(); ax.grid()

temp = []
for i, v in enumerate(res[:]):
    #print(v[0:3][::-1])
    direc = make_folder(v)
    print(direc)

    data = np.loadtxt(direc+os.sep+'ast.Xemit.007')
    fx = interp1d(data[:,0], data[:,3])
    
    data = np.loadtxt(direc+os.sep+'ast.Yemit.007')
    fy = interp1d(data[:,0], data[:,3])
    
    ax.plot(data[:,0], data[:,3], '--')
    
    temp.append([v[0], v[1], v[2], v[3], fx(23.337), fy(23.337), fx(27.627), fy(27.627)])

temp = np.array(temp); temp2 = temp
np.savetxt('RMS-size-with-focusing.dat', temp, fmt = '%12.6f')

#fig, ax = plt.subplots(figsize = (5, 4))
ax9.plot(temp[:,4], temp[:,6], '-o')
#ax9.grid()
ax9.set_xlabel(r'RMS size at High2.Scr3 (mm)')
ax9.set_ylabel(r'RMS size at High3.Scr1 (mm)')

#ax9.set_xlim(0.5, 2.5)
#ax9.set_ylim(0.9, 1.5)
ax9.legend(['Backward tracking', 'Forward tracking'], loc = 'upper left')
fig.savefig('RMS-size-with-focusing.pdf')

os.chdir('..')

#%%
# fig, ax = plt.subplots()
# ax.plot(temp[:,0], -temp[:,1], '-<')
# ax.plot(temp[:,0], temp[:,2], '-<')

from intersect import intersection

x, y = intersection(temp1[:,3], temp1[:,5], temp2[:,4], temp2[:,6])

kind = 'quadratic'
#kind = 'linear'

T2 = np.zeros(4)
for i in [0, 1, 2, 3]:
    fQ = interp1d(temp2[:,4], temp2[:,i], kind = kind)
    T2[i] = fQ(x)[0]
print(T2)

T1 = np.zeros(3)
for i in [0, 1, 2]:
    fQ = interp1d(temp1[:,3], temp1[:,i], kind = kind)
    T1[i] = fQ(x)[0]
print(T1) 

#%%
x = [0.15,       -0.51943363,  1.05887464, -0.79738536, 1.02759956, -3.35761957,  2.07308892]
x = [0.15,       -0.51938800,  1.05871047, -0.79719188, 1.04088708, -3.37943773,  2.08655737]
x = [0.15,       -0.51903419,  1.05743732, -0.79569190, 1.10006695, -3.47237152,  2.13636779]


quadNames = ['HIGH2.Q2', 'HIGH2.Q3', 'HIGH2.Q4', 'HIGH2.Q5', 'HIGH3.Q1', 'HIGH3.Q2', 'HIGH3.Q3']

Distribution = '../ast.1630.005'
Zstop = scrns['HIGH3.UND']
Screen = [scrns['HIGH2.SCR3'], scrns['HIGH3.SCR1']]

args = (quadNames,)
kwargs = {'Run':6, 'Distribution':Distribution, 'Zstop':Zstop, 'Screen':Screen}

obj_quads(x, *args, **kwargs)

#%%
workdir = r'scan-TDS-amplitude'
#os.chdir(workdir)

var1 = [-1.5]
var2 = [1.35]
var3 = np.linspace(-45, 0, 10)
var3 = [0.0, 0.1, 0.15, 0.20]#, 0.15, 0.25, 0.3]
combi = np.array([[-5.500000E-01, 9.708219E-01, -5.232538E-01, v3, 64.337550] for v1 in var1 for v2 in var2 for v3 in var3])


fig, ax = plt.subplots(); ax.grid()

temp = []
for i, x in enumerate(combi):
    direc = ''
    #for i in np.arange(len(x[:-2])):
    #    direc += str.format('Q%d-%.2fT_m-' %  (i+1, x[i]))
    #direc = direc[:-1]
    direc += str.format('%.2f-%2f' % (x[-2], x[-1]))
    
    print(direc)

    #data = np.loadtxt(direc+os.sep+'ast.ref.004')

    data = np.loadtxt(direc+os.sep+'ast.Xemit.004')
    #fx = interp1d(data[:,0], data[:,3])
    ax.plot(data[:,0], data[:,3], '-', color = color_cycle[i], linestyle = linestyle_cycle[i])
    
    data = np.loadtxt(direc+os.sep+'ast.Yemit.004')
    #fy = interp1d(data[:,0], data[:,3])
    ax.plot(data[:,0], data[:,3], '-', color = color_cycle[i], linestyle = linestyle_cycle[i])
    
    data = np.loadtxt(direc+os.sep+'ast.1830.004')
    select = data[:,-1]>0
    data = data[select]
    
    fig, ax2 = plt.subplots()
    _ = ax2.hist2d(data[1:,2], data[1:,1], bins = 100)
    #ax2.plot(data[1::2,2], data[1::2,1], '.')
#os.chdir('..')

#%%
popt, pcov = curve_fit(linear, data[1:,2], data[1:,1])
fig, ax2 = plt.subplots()
#_ = ax2.hist2d(data[1:,2], data[1:,1], bins = 100)
ax2.plot(data[1::2,2], data[1::2,1], '.')

xtmp = np.linspace(-5, 5, 100)*1e-3
ax2.plot(xtmp, linear(xtmp, *popt), '-')

#%%
k, b = popt

gap = 1e-3/np.abs(k); print(gap)

dist = np.copy(data)
dist[0,2] = 0

tmp = (np.abs(dist[:,1]/(gap/2))+1)%4
select = (tmp>=0)*(tmp<=2)

dist = dist[select]

fig, ax = plt.subplots()
ax.hist(dist[:,2], bins = 1000, histtype = r'step')
ax.hist(data[:,2], bins = 1000, histtype = r'step')


plt.figure()
plt.plot(dist[:,2], dist[:,1], '.')

