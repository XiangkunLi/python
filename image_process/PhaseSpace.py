# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 11:18:43 2020

@author: lixiangk
"""
from universal import *
import pickle

from .ImageProcess import *

import re
def InnerLoopParse(line):
    line = line.strip()
    
    tag = re.match(r'<.*?>', line).group() 
    val = re.findall(r'[0-9.]+', line)
    
    if tag:
        tag = tag[1:-1]
        
    if val:
        val = np.float(val[0])
        return tag, val
    else:
        return tag, None
def GetMustHave(fname):
    with open(fname, 'r') as f_handle:
        kv = {}
        header = ''
        line = f_handle.readline()
        line = f_handle.readline()
        while line:
            
            tag, val = InnerLoopParse(line)
            if val is not None:
                if header is not '':
                    kv.update({header+'.'+tag: val})
                else:
                    kv.update({tag: val})
            else:
                if tag[0] == '/':
                    m = re.match('.*\.', header)
                    if m:
                        header = m.group()[:-1]
                else:
                    if header == '':
                        header = tag
                    else:
                        header += '.'+tag        
            line = f_handle.readline()
    return kv

class SlitScan():
    EMSY = 'EMSY1X'
    MOI = 'High1.Scr4'
    drift = 3.133 # meter
    momentum = 17 # MeV/c
    step = 0.5e-3*0.1 # actuator step, m
    slitWidth = 50e-6 # slit width
    slit = 'x'
    
    def __init__(self, EMSY = None, MOI = None, drift = 0, momentum = 0, step = 0,
                 musthave = None):
        if EMSY is not None:
            self.EMSY = EMSY
        if MOI is not None:
            self.MOI = MOI
        if drift > 0:
            self.drift = drift
        if momentum > 0:
            self.momentum = momentum
        if step >0:
            self.step = step
        
        if musthave:
            kv = GetMustHave(musthave); print(kv)
            self.drift = kv.get('MustHaves.LDrift')
            self.step = kv.get('MustHaves.ActuatorSpeed')*1e-4
            self.momentum = kv.get('MustHaves.AfterBooster.BeamMeanMomentum')
            
        gg = M2G(self.momentum)
        gb = gamma2bg(gg)
        self.gb = gb  

class PhaseSpace(SlitScan):
    def __init__(self, fname = None, ROI = None, **kwargs):
        
        if fname is None:
            fname = get_file('.imc')
            path, name, ext = fileparts(fname)
            m = re.search('from', name)
            s = m.end()
            slit0 = float(name[s:s+2])+float(name[s+3:s+5])*0.01
            slit1 = float(name[s+8:s+10])+float(name[s+11:s+13])*0.01
        
        musthave = fname[:-4]+'.MUST'
        super(PhaseSpace, self).__init__(musthave = musthave, **kwargs)
        
        imgs, camInfos, n = PITZ_loadImage(fname)

        fname = fname[0:-4]+'.bkc'
        bkgs, camInfos, n = PITZ_loadImage(fname)
        self.fname = fname
        
        fname = os.path.join(path, 'EMSY1', '.imc')
        
        self.info = camInfos[0]; print('Scale: ', self.info.scale)
        
        # Select ROI
        if ROI is None:
            ROI = getROI(np.mean(imgs, axis = 0))
        self.ROI = ROI
        
        imgs = imgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]
        bkgs = bkgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]
        
        # Get background
        env, avg, std = getBackground(bkgs, treat = None);
        print(np.mean(np.mean(env)))
        
        # Slit scan setting
        slit = name[5]; print('slit: ', slit)
        self.slit = slit
        
        if self.slit is 'X':
            PS = np.zeros((ROI[2], n))
        elif self.slit is 'Y':
            PS = np.zeros((ROI[3], n))
            slit0 = -slit0
            slit1 = -slit1
        else:
            print('Unknow slit!')
            return

        fig, ax = plt.subplots()
        
        res = []
        gb = self.gb
        step = self.step
        drift = self.drift
        Xscale, Yscale = self.step, self.info.scale/1e3 # meter
        for i in np.arange(n):
            
            if slit is 'X':
                x0 = (i-0/2)*step  # EMSY1X, x0 increases since the slit moves from right (negative) to left (positive)
                #x0 = -(i-n/2)*step  # EMSY2X, x0 increases since the slit moves from right (negative) to left (positive)
            if slit is 'Y':
                x0 = (n/2-i)*step # x0 decreases since the slit moves from top (positive) to bottom (negative)
            
            
            img = imgs[i]-avg
            img[img<0] = 0; #print('image before filterring: ', img.dtype)
            img = emcalcFilter(img, std, 1, 1); #print('image after filterring: ', img.dtype)
            
            imgs[i,:,:] = img[:,:]
            
            [xc, xpc, xrms, xprms] = calcRMS(img[::-1,::-1], Xscale, Yscale)
            res.append([x0, xc, xpc, xrms, xprms])
            
            signal = img
            
            if slit is 'X':
                ncols = PS.shape[0]
                #import pdb; pdb.set_trace()
                
                r9 = np.sign(np.random.rand(1)-0.5)[0]+2
                
                shift = np.mod(int(x0/Yscale), ncols); print('Shift: ', shift, np.mod(x0/Yscale, ncols))
                # shift toward smaller divergence which is on the right side of the array
                
                PS[:,i] = np.roll(np.sum(signal, axis = 0), shift)  # EMSY1X
                #PS[:,n-i-1] = np.roll(np.sum(signal, axis = 0), shift)   # EMSY2X
            elif slit is 'Y':
                ncols = PS.shape[0]
                shift = np.mod(int(x0/Yscale), ncols)
                # shift toward smaller divergence which is on the bottom side of the array
                
                PS[:,n-i-1] =  np.roll(np.sum(signal, axis = 1), shift)
            
            if (np.mod(i,3)) == 0:
                tmp = np.copy(signal)
                tmp[tmp==0] = np.nan
                ax.imshow(tmp)
                #ax.set_title('frame: %d' % i)
                ax.set_title('slit pos.: %.1f mm' % (np.abs(x0*1e3+slit0)))
                ax.set_xlabel(r'$x$ (pixel)')
                ax.set_ylabel(r'$y$ (pixel)')
                
                fig.savefig(slit+'slit-frame-%d.png' % i)
                
                plt.pause(0.01)
                plt.cla()
                
        #plt.close('all')
        self.imgs = imgs
        self.PS = PS
        
        [xc, xpc, xrms, xprms] = calcRMS(PS[::-1], Xscale, Yscale/drift)
        [eps_x, beta_x, gamma_x, alpha_x] = calcTwiss(PS[::-1], Xscale, Yscale/drift)

        eps_x = eps_x*gb
        self.eps_x = eps_x
        self.twiss = [eps_x, beta_x, gamma_x, alpha_x]
        print('Normalized emittance: ', eps_x*1e6, ' um')

        self.plot()
        
    def plot(self, **kwargs):
        '''
        
        '''
        #plottype = 'contourf'

        plotPhaseSpace(self, **kwargs)
        return
    
class VPP():
    
    def __init__(self, psx = None, psy = None, save_data = True):
        
        h, w = psx.imgs[0].shape
        nx, ny = psx.imgs.shape[0], psy.imgs.shape[0]
        print(nx, ny)
        imgs_cross = np.zeros((ny, nx))
        
        yfactor = np.sum(psx.PS)/np.sum(psy.PS); print('yfactor', yfactor)
        
        step = psx.step
        drift = psx.drift
        Xscale = psx.info.scale*1e-3
        Yscale = psy.info.scale*1e-3
        
        nrows, ncols = psx.imgs[0].shape
        
        vpp = []
        for i, ximg in enumerate(psx.imgs[1:-1:2], start=1):
            x0 = i*step*1; print(i, end = ' ')
            for j, yimg in enumerate(psy.imgs[1:-1:2], start=1):
                y0 = -j*step*1
                
                if np.sum(ximg)>0 and np.sum(yimg)>0:
                    
                    img_cross = np.array(list(map(min, ximg.ravel(), yimg.ravel()*yfactor))).reshape(ximg.shape)
                    
                    ss= np.sum(img_cross)
                    if ss>0:
                                                
                        xc, yc, xx, yy, xy = calcCov(img_cross[::-1, ::-1], Xscale, Yscale, Xshift = -x0, Yshift = -y0)
                        x2 = xx+xc**2
                        y2 = yy+yc**2
                        xy = xy+xc*yc
                        
                        xp0 = xc/drift
                        yp0 = yc/drift
                        xp02 = x2/drift**2
                        yp02 = y2/drift**2
                        xp0yp0 = xy/drift**2
                        
                        vpp.append([x0, xp0, y0, yp0, xp02, yp02, xp0yp0, ss, i, j])
                    else:
                        vpp.append([x0, 0, y0, 0, 0, 0, 0, 0, i, j])
                else:
                    #pass
                    vpp.append([x0, 0, y0, 0, 0, 0, 0, 0, i, j])
        
        
        vpp = np.atleast_2d(np.array(vpp));print(vpp)
        tmp = np.copy(vpp)
        
        select = (vpp[:,1] != 0)*(vpp[:,3] != 0)*(vpp[:,4] != 0)
        vpp = vpp[select]
        
        mean= np.array([ weighted_mean(vpp[:,i], vpp[:,7]) for i in range(7) ])
        
        x02 = weighted_mean(vpp[:,0]**2, vpp[:,7])-mean[0]**2
        x0xp0 = weighted_mean(vpp[:,0]*vpp[:,1], vpp[:,7])-mean[0]*mean[1]
        xp02 = mean[4]-mean[1]**2
        
        y02 = weighted_mean(vpp[:,2]**2, vpp[:,7])-mean[2]**2
        y0yp0 = weighted_mean(vpp[:,2]*vpp[:,3], vpp[:,7])-mean[2]*mean[3]
        yp02 = mean[5]-mean[3]**2
        
        x0y0 = weighted_mean(vpp[:,0]*vpp[:,2], vpp[:,7])-mean[0]*mean[2]
        x0yp0 = weighted_mean(vpp[:,0]*vpp[:,3], vpp[:,7])-mean[0]*mean[3]
        y0xp0 = weighted_mean(vpp[:,1]*vpp[:,2], vpp[:,7])-mean[2]*mean[1]
        xp0yp0 = mean[6]-mean[1]*mean[3]
        
        bm = np.zeros((4, 4))
        bm[0,:] = x02, x0xp0, x0y0, x0yp0
        bm[1,:] = x0xp0, xp02, y0xp0, xp0yp0
        bm[2,:] = x0y0, y0xp0, y02, y0yp0
        bm[3,:] = x0yp0, xp0yp0, y0yp0, yp02
        
        gb = psx.gb
        eps_x = np.sqrt(np.linalg.det(bm[0:2,0:2]))*gb
        eps_y = np.sqrt(np.linalg.det(bm[2:4,2:4]))*gb
        eps4d = (np.linalg.det(bm))**0.25*gb
        
        vpp = tmp
        if save_data:
            save_data = {}
            save_data.update(psx = psx)
            save_data.update(psy = psy)
            save_data.update(yfactor = yfactor)
            save_data.update(vpp = vpp)
            save_data.update(bm = bm)
            save_data.update(eps_x = eps_x)
            save_data.update(eps_y = eps_y)
            save_data.update(eps4d = eps4d)
            
            fullname = get_file_to_save('.pkl')
            with open(fullname, 'wb') as f:
                pickle.dump(save_data, f)
        
        np.savetxt('vpp.dat', vpp, fmt = '%14.6E')
        np.savetxt('bm.dat', bm, fmt = '%14.6E')
        np.savetxt('eps.dat', 
                   [[psx.eps_x, psy.eps_x, np.sqrt(psx.eps_x*psy.eps_x)],
                    [eps_x, eps_y, eps4d]], fmt = '%14.6E',
                   header = 'eps_x   eps_y  eps4d^0.5')
        
        self.psx = psx
        self.psy = psy
        self.yfactor = yfactor
        
        self.vpp = vpp
        self.bm = bm
        
        self.eps_x = eps_x
        self.eps_y = eps_y
        self.eps4d = eps4d