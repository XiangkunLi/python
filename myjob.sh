#!/bin/zsh
#
#$ -cwd
#$ -V
#$ -l h_cpu=48:00:00
#$ -l h_rss=2G
#$ -P pitz
#$ -pe multicore 16
#$ -R y

#source $HOME/.zshrc
which python
echo $PATH


#python -c "print('hello!')"
python test.py
